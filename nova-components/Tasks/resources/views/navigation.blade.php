<?php
//TODO Make as component
$links = [
    [
        "name"  => "My Tasks",
        "icon"  => "tasks",
        "route" => "tasks-my",
    ],
    [
        "name"  => "All Tasks",
        "icon"  => "business-time",
        "route" => "tasks",
    ],
    [
        "name"  => "Assigned Tasks",
        "icon"  => "share-square",
        "route" => "tasks-assigned",
    ]
]
?>

<collapsable header="Tasks" :icon="'tasks'">
    @foreach($links as $key =>$link)
        <li class="leading-wide mb-4 text-sm" key="{{$key}}">
            <router-link :to="{name: '{{$link['route']}}'}"
                         class="text-white ml-8 no-underline dim">
                <fa icon="{{$link['icon']}}"></fa>
                <span class="sidebar-label">{{$link['name']}}</span>
            </router-link>
        </li>
    @endforeach
</collapsable>
