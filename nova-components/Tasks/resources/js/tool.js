import Expand from './components/Plugins/Expand'
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';

import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import PortalVue from "portal-vue";

Nova.booting((Vue, router, store) => {
    Vue.component('fa', FontAwesomeIcon);
    Vue.component('collapsable', Expand);
    library.add(fas);
    Nova.$router = router;
    Nova.$store = store;
    Nova.Vue.use(PortalVue);
    store.state.user = Nova.config.user;
    window.$t = function (translatableObject) {
        return translatableObject[store.state.lang]
    };
    window.$userCan = function (permission) {
        if (!store.state.user)
            return false;
        return _.find(store.state.user.permissions, item => {
            return item.name == permission
        })
    };
    window.$userIs = function (role) {
        if (!store.state.user)
            return false;
        return _.find(store.state.user.roles, item => {
            return item.name == role
        })
    };
    window.$getDateTime = function (dbDateTime) {
        var monthNames = [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Ноябрь',
            'Декабрь',
        ];

        var date = new Date(Date.parse(dbDateTime.replace('-', '/', 'g')));
        return date.getDate() + '-' + ('0' + date.getMonth()).slice(-2) + '-' + date.getFullYear()
    }

    window.Vue = Vue;
    router.addRoutes([
        {
            name: 'tasks',
            path: '/tasks',
            component: require('./components/Tasks'),
        },
        {
            name: 'tasks-my',
            path: '/tasks/my',
            component: require('./components/MyTasks'),
        },
        {
            name: 'tasks-assigned',
            path: '/tasks/assigned',
            component: require('./components/AssignedTasks'),
        },
        {
            name: 'task-assign',
            path: '/task/assign/:id/:target_type/:target_id',
            props: true,
            component: require('./components/Assign'),
        },
    ])
})
