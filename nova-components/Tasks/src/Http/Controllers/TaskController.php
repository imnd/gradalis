<?php

namespace Marketplace\Tasks\Http\Controllers;

use App\Factories\TaskFactory;
use App\Models\Task;
use App\Models\Tech;
use Illuminate\Http\Request;


class TaskController extends Controller
{

    public function get($type, $id)
    {
        $tasks = Task::where('target_type', $type);
        if ($id) {
            $tasks->where('target_id', $id);
        }
        return $tasks->get();
    }

    public function getTechs()
    {
        $techs = Tech::all()->map(function ($tech) {
            return ['id' => $tech->id, 'name' => $tech->getTranslation('name', app()->getLocale())];
        });
        return $techs;
    }

    public function getTaskTypes()
    {
        $types = Task::getTaskTypes();
        $data = [];
        foreach ($types as $val => $name) {
            $data[] = ['val' => $val, 'name' => $name];
        }
        array_shift($data);
        return response()->json($data);
    }

    public function assignTask(Request $request, Task $task)
    {
        $target_id = $request->get('target_id');
        $target_type = $request->get('target_type');
        $task_type = $request->get('task_type');

        $factory = new TaskFactory($target_type, $target_id);
        $tasker = $factory->getTasker();

        $res = $tasker->assignTask($target_id, $target_type, $task->step, $task_type);

        $response = ($res) ? ['status' => 'ok'] : ['status' => 'error'];

        return response()->json($response);
    }

    public function takeTask(Task $task)
    {
        $task->status = Task::STATUS_IN_PROGRESS;
        $task->to_id = auth()->user()->id;
        $task->save();
        return response()->json(['status' => 'ok']);
    }

    public function rejectTask(Request $request, Task $task)
    {
        $task->status = Task::STATUS_REJECTED;
        $task->message = $request->get('message');
        $task->save();
        return response()->json(['status' => 'ok']);
    }
}
