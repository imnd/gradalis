<?php

namespace Marketplace\Tasks\Http\Controllers;

use App\Models\Task;

use Illuminate\Http\Request;
use Marketplace\Tasks\Tables\TaskTable;


class TaskTableController extends Controller
{

    public function data(Request $request){
        return TaskTable::get($request->get('scope'));
    }
    public function takeTask(Task $task)
    {
        $task->status = Task::STATUS_IN_PROGRESS;
        $task->to_id = auth()->user()->id;
        $task->save();
        return response()->json(['status' => 'ok']);
    }
    public function rejectTask(Request $request,Task $task)
    {
        $task->status = Task::STATUS_REJECTED;
        $task->message = $request->get('message');
        $task->save();
        return response()->json(['status' => 'ok']);
    }
}
