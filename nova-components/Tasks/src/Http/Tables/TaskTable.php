<?php
/**
 * Created by PhpStorm.
 * User: MIKHAIL
 * Date: 15.03.2019
 * Time: 19:20
 */

namespace Marketplace\Tasks\Tables;

use App\Models\Task;
use DataTables;
use DB;

class TaskTable
{
    public static function get($scope = null)
    {
        $user_id = auth()->user()->id;
        $tasks = Task::select([
            DB::raw('tasks.id'),
            DB::raw("JSON_EXTRACT(businesses.name, '$." . app()->getLocale() . "') AS 'target_name'"),
            'tasks.priority',
            'tasks.target_id',
            'tasks.target_type',
            'tasks.task_type',
            'tasks.from_id',
            'tasks.to_id',
            'tasks.status',
            'tasks.planed_at',
        ])->join('businesses', function ($join) {
            $join->on('tasks.target_id', '=', 'businesses.id')
                ->where('tasks.target_type', Task::TARGET_TYPE_BUSINESS);
        });
        if ($scope === null) {
            $tasks->where('tasks.to_id', null)
                ->where('tasks.from_id', '!=', $user_id);
        } elseif ($scope === "my") {
            $tasks->where('tasks.to_id', $user_id);
        } elseif ($scope === "assigned") {
            $tasks->where('tasks.from_id', $user_id);
        }

        return DataTables::eloquent($tasks)
            ->addColumn('target_type_name', function ($task) {
                return Task::getTargetType($task->target_type);
            })
            ->editColumn('task_type', function ($task) {
                return Task::getTaskType($task->task_type);
            })
            ->editColumn('status', function ($task) {
                return Task::getStatus($task->status);
            })
            ->editColumn('priority', function ($task) {
                return Task::getPriority($task->priority);
            })
            ->addColumn('actions', function ($task) {
                return 'actions';
            }, false)
            ->make(true);
    }
}
