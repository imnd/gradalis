<?php

namespace Marketplace\Tasks;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;
use Marketplace\Tasks\Http\Middleware\Authorize;

class ToolServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'tasks');

        $this->app->booted(function () {
            $this->routes();
        });

        Nova::serving(function (ServingNova $event) {
            if (auth()->check()) {
                $user = auth()->user()->load(['roles']);
                $u = $user->toArray();
                $u['permissions'] = $user->getPermissionsViaRoles()->toArray();
            } else {
                $user = null;
            }
            Nova::provideToScript([
                'user' => $u,
            ]);
        });
    }

    /**
     * Register the tool's routes.
     *
     * @return void
     */
    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::middleware(['nova', Authorize::class])
            ->prefix('marketplace/tasks')
            ->group(__DIR__ . '/../routes/api.php');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function getLinks(){
       return [
            [
                "name"  => "My Tasks",
                "icon"  => "tasks",
                "route" => "tasks-my",
            ],
            [
                "name"  => "All Tasks",
                "icon"  => "business-time",
                "route" => "tasks",
            ],
            [
                "name"  => "Assigned Tasks",
                "icon"  => "share-square",
                "route" => "tasks-assigned",
            ]
        ];
    }
}
