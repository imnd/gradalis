<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/
Route::namespace('Marketplace\Tasks\Http\Controllers')->name('marketplace.tasks.')->group(function () {
    Route::get('data', 'TaskTableController@data')->name('data');

    //Actions
    Route::get('take/{task}', 'TaskTableController@takeTask')->name('take');
    Route::post('reject/{task}', 'TaskTableController@rejectTask')->name('reject');
    Route::get('approve/{task}', 'TaskTableController@takeTask')->name('approve');
    Route::post('assign/{task}', 'TaskController@assignTask')->name('assign');

    Route::get('get/{type}/{id?}', 'TaskController@get')->name('get');
    Route::get('get-techs', 'TaskController@getTechs')->name('get-techs');
    Route::get('get-task-types', 'TaskController@getTaskTypes')->name('get-task-types');
});

