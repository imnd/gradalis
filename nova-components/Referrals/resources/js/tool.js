import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';

import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import PortalVue from "portal-vue";
Nova.booting((Vue, router) => {
    Vue.component('fa', FontAwesomeIcon);
    library.add(fas);
    Nova.$router = router;
    Nova.Vue = Vue;
    Nova.Vue.use(PortalVue);
    router.addRoutes([
        {
            name: 'referrals',
            path: '/referrals',
            component: require('./components/Tool'),
        },
        {
            name: 'partner-targets',
            path: '/referrals/:id/targets',
            component: require('./components/Targets'),
            props: true
        },
    ]);
});
