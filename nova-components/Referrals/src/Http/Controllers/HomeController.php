<?php

namespace Marketplace\Referrals\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Referral\Campaign;
use App\Models\Referral\CounterTarget;
use App\Models\Referral\InvitationCounter;
use App\Models\Referral\Partner;
use Carbon\Carbon;
use DataTables;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    public function setTargetsStatus(Request $request)
    {
        $ids = [];
        foreach ($request->get('ids') as $id) {
            $ids[] = (integer) $id;
        }
        CounterTarget::whereIn('id', $ids)->update(['status' => $request->get('status')]);
        return response()->json(['status' => 'ok']);
    }

    public function index()
    {
        $partners = DB::table('partners')->select([
            DB::raw('partners.id'),
            DB::raw('partners.created_at as reg_date'),
            DB::raw('concat(users.first_name, " ",users.last_name) as name'),
            DB::raw('count(invitation_counter.id) as uniq'),
            DB::raw('sum(invitation_counter.count) as clicks'),
            DB::raw('sum(case when `invitation_counter`.`status` > 0 then 1 else 0 end) as regs'),
            DB::raw('sum(case when `counter_target`.`status` = '.CounterTarget::STATUS_PAYED.' then counter_target.sum else 0 end) as percent'),
        ])->join('invitations', 'invitations.partner_id', 'partners.id')
            ->join('users', 'users.id', 'partners.user_id')
            ->join('invitation_counter', 'invitation_counter.invitation_id', 'invitations.id')
            ->join('counter_target', 'counter_target.counter_id', 'invitation_counter.id')
            ->groupBy('partners.id');

        return DataTables::query($partners)
            ->editColumn('percent', function ($partner) {
                return format_price($partner->percent);
            })
            ->editColumn('reg_date', function ($partner) {
                return Carbon::parse($partner->reg_date)->format('d.m.Y');
            })
            ->make(true);
    }

    public function partnerData(Request $request, Partner $partner)
    {
        $targets = DB::table('counter_target')->select([
            'counter_target.id',
            'counter_target.created_at',
            'counter_target.sum',
            'counter_target.status',
            'invitation_counter.ip',
            'invitation_counter.http_referrer',
            'counter_target.type',
            'campaign_targets.name',
            DB::raw('countries.translation as country'),
        ])->join('invitation_counter', 'invitation_counter.id', 'counter_target.counter_id')
            ->join('invitations', 'invitations.id', 'invitation_counter.invitation_id')
            ->join('campaign_targets', 'campaign_targets.id', 'counter_target.target_id')
            ->join('countries', 'countries.id', 'campaign_targets.country_id')
            ->orderBy('counter_target.created_at');

        return DataTables::query($targets)
            ->filter(function ($query) use ($request) {
                if ($request->has('status') && !empty($request->get('status')) && $request->get('status') != 'all') {
                    $query->where('counter_target.status', '=', (int) $request->get('status'));
                }

                if ($request->has('type') && !empty($request->get('type')) && $request->get('type') != 'all') {
                    $query->where('counter_target.type', '=', $request->get('type'));;
                }
            })
            ->editColumn('created_at', function ($target) {
                return Carbon::parse($target->created_at)->format('d.m.Y');
            })
            ->editColumn('name', function ($target) {
                return json_decode($target->name, true)[app()->getLocale()];
            })
            ->editColumn('country', function ($target) {
                return json_decode($target->country, true)[app()->getLocale()];
            })
            ->editColumn('type', function ($target) {
                return CounterTarget::getType($target->type);
            })
            ->editColumn('sum', function ($target) {
                return format_price($target->sum);
            })
            ->editColumn('status', function ($target) {
                return CounterTarget::getStatus($target->status);
            })
            ->make(true);
    }

    public function getFilters()
    {
        $statuses = CounterTarget::getStatuses();
        $types = CounterTarget::getTypes();
        foreach ($statuses as $key => $label) {
            $data['statuses'][$key]['value'] = $key;
            $data['statuses'][$key]['label'] = $label;
        }
        foreach ($types as $key => $label) {
            $data['types'][$key]['value'] = $key;
            $data['types'][$key]['label'] = $label;
        }

        return response()->json($data);
    }

    public function getSellersBuyers()
    {
        $data['sellers'] = User::role('Продавец')->select('id',
            DB::raw("CONCAT(email,' \"',first_name,'\"') AS name"))->get();
        $data['buyers'] = User::role('Покупатель')->select('id',
            DB::raw("CONCAT(email,' \"',first_name,'\"') AS name"))->get();
        return response()->json($data);
    }

    public function getCampaigns()
    {
        $campaigns = Campaign::select('id', 'name')->get();

        return response()->json($campaigns);
    }

    public function getTypes()
    {
        $types = Campaign::getTypes();
        return response()->json($types);
    }

    public function statistics()
    {
        $subYear = Carbon::now()->subYear();
        $subMonth = Carbon::now()->subMonth();
        $subWeek = Carbon::now()->subWeek();

        $data['clickCountYear'] = InvitationCounter::where('created_at', '>=', $subYear)->sum('count');
        $data['clickCountMonth'] = InvitationCounter::where('created_at', '>=', $subMonth)->sum('count');
        $data['clickCountWeek'] = InvitationCounter::where('created_at', '>=', $subWeek)->sum('count');

        $data['regCountYear'] = InvitationCounter::where('created_at', '>=', $subYear)->where('status', '>',
            0)->count();
        $data['regCountMonth'] = InvitationCounter::where('created_at', '>=', $subMonth)->where('status', '>',
            0)->count();
        $data['regCountWeek'] = InvitationCounter::where('created_at', '>=', $subWeek)->where('status', '>',
            0)->count();

        return response()->json($data);
    }
}
