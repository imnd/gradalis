<?php

namespace Marketplace\IconSelect;

use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;
use Storage;

class IconSelect extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'icon-select';
    private $path = '/svg/icons/category/';
    private $disk = 'public-web';


    /**
     * @param string|null $path
     * @param string|null $disk
     * @return $this
     */
    public function setPath(string $path = null, string $disk = null)
    {
        if ($path !== null) {
            $this->path = $path;
        }
        if ($disk !== null) {
            $this->disk = $disk;
        }
        return $this;
    }

    /**
     * @param string $path
     * @param null $disk
     * @return $this
     */
    public function init()
    {
        $icons_files = Storage::disk($this->disk)->files($this->path);
        $icons = [];
        foreach ($icons_files as $file) {
            $file_array = explode('/', $file);
            $fil_name = end($file_array);
            $icons[] = [
                'title' => $fil_name,
                'icon'  => $this->path . $fil_name
            ];
        }
        return $this->withMeta([
            'icons' => $icons,
            'path'  => $this->path,
        ]);
    }

    /**
     * Hydrate the given attribute on the model based on the incoming request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  string $requestAttribute
     * @param  object $model
     * @param  string $attribute
     * @return void
     */
    protected function fillAttributeFromRequest(NovaRequest $request,
                                                $requestAttribute,
                                                $model,
                                                $attribute)
    {
        if ($request->exists($requestAttribute)) {
            $model->{$attribute} = $request[$requestAttribute];
        }
    }
}
