Nova.booting((Vue, router, store) => {
    Vue.component('index-icon-select', require('./components/IndexField'))
    Vue.component('detail-icon-select', require('./components/DetailField'))
    Vue.component('form-icon-select', require('./components/FormField'))
})
