let mix = require('laravel-mix');

const path = require('path');
const autoprefixer = require('autoprefixer');
const prefixer = require('postcss-prefixer');
mix.options({
    extractVueStyles: false,
});

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.sass$/,
                use: [
                    "sass-loader",
                    "css-loader",
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [
                                prefixer({
                                    prefix: 'bu-'
                                })
                            ],
                            sourceMap: true
                        }
                    },
                ],
            },
            {
                test: /\.css$/,
                exclude: [path.resolve(__dirname, 'node_modules/bulma')],
                use: ['style-loader', 'css-loader']
            },
            {

                test: /\.css$/,
                include: ["~bulma"],
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [
                                prefixer({
                                    prefix: 'bu-'
                                })
                            ]
                        }
                    }
                ]
            }
        ]
    }
})
mix.setPublicPath('dist')
    .js('resources/js/tool.js', 'js')
    .sass('resources/sass/tool.scss', 'css')
    .sass('resources/sass/business.scss', 'css');
