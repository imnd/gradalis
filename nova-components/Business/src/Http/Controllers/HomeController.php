<?php

namespace Marketplace\Business\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Business\Business;
use App\Models\Language;
use App\Models\Task;
use App\Models\Tech;
use App\Services\BusinessTask;
use DataTables;
use DB;
use Illuminate\Http\Request;


class HomeController extends Controller
{


    public function index()
    {
        $statuses = Business::getStatuses();

        $businesses = Business::select([
            'id',
            'name->' . app()->getLocale(),
            'status',
        ])->with(['tasks' => function ($query) {

        }]);

        return DataTables::eloquent($businesses)
            ->editColumn('name', function ($object) {
                return json_decode($object->name, true);
            })
            ->addColumn('edit', function ($businesses) {
                return 'edit';
            }, false)
            ->addColumn('assign', function ($businesses) {
                return 'assign';
            }, false)
            ->filter(function ($query) {
                if (request()->has('status')) {
                    $status = request()->get('status');
                    if ($status != "")
                        $query->where('status', request()->get('status'));
                }
                if (request()->has('type')) {
                    $status = request()->get('type');
                    if ($status != "")
                        $query->where('type', request()->get('type'));
                }
            }, true)
            ->editColumn('status', function ($business) use ($statuses) {
                return $statuses[$business->status];
            })
            ->rawColumns(['edit'])
            ->make(true);
    }

    public function tasks()
    {
        $user = auth()->user();
        $tasks = DB::table('tasks')->select([
            'tasks.id',
            'tasks.status',
            'priority',
            'planed_at',
            'target_id',
            'message',
            DB::raw("JSON_EXTRACT(businesses.name, '$." . app()->getLocale() . "') AS 'business_name'"),
        ])
            ->join('businesses', 'businesses.id', '=', 'tasks.target_id')
            ->where('to_id', auth()->user()->id);
        $tasks->whereIn('tasks.status', [Task::STATUS_AWAIT, Task::STATUS_APPROVED, Task::STATUS_REJECTED]);

        return DataTables::query($tasks)
            ->editColumn('status', function ($task) {
                return Task::getStatus($task->status);
            })
            ->editColumn('priority', function ($task) {
                return Task::getPriority($task->priority);
            })
            ->addColumn('edit', function ($task) {
                return 'edit';
            }, false)
            ->addColumn('actions', function ($task) {
                return 'edit';
            }, false)
            ->addColumn('send', function ($task) {
                return 'edit';
            }, false)
            ->make(true);
    }

    public function endTask(Task $task)
    {
        if (config('settings.automation') == true) {
            $business_task = new BusinessTask();
            $business_task->approveTask($task->target_id, $task->task_type, $task->step++, $task->id);
        } else {
            $task->status = Task::STATUS_APPROVED;
        }
        return response()->json(['status' => 'ok']);
    }

    public function get(Business $business)
    {
        $data['options'] = $business->options;
        unset($business->options);
        $business->getMedia('business');
        $business->country_id = $business->city->country_id;
        $data['business'] = $business;
        return $data;
    }

    public function update(Request $request, Business $business)
    {
        $businessData = json_decode($request->get('business'), true);
        $business->update($businessData);
        foreach ($request->file('files') as $file) {
            $business->addMedia($file)->toMediaCollection('business', 'business');
        }
        return response()->json(['status' => 'ok']);
    }

    public function getTechs()
    {
        $techs = Tech::all()->map(function ($tech) {
            return ['id' => $tech->id, 'name' => $tech->getTranslation('name', app()->getLocale())];
        });
        return $techs;
    }

    public function send(Request $request, Business $business)
    {
        $task_type = $request->get('task_type');
        $planed_at = $request->get('planed_at');
        $tech_id = $request->get('tech_id');

        foreach (Language::all() as $lang) {
            Task::create([
                'task_type'   => $this->getTaskType($task_type),
                'target_type' => Task::TARGET_TYPE_BUSINESS,
                'target_id'   => $business->id,
                'tech_id'     => $tech_id,
                'status'      => Task::STATUS_AWAIT,
                'planed_at'   => $planed_at,
                'from_id'     => auth()->user()->id,
            ]);
        }
        return response()->json(['status' => 'ok']);
    }

    private function getTaskType($type)
    {
        switch ($type) {
            case'translation':
                return Task::TASK_TYPE_TRANSLATION;
                break;
            case'seo':
                return Task::TASK_TYPE_SEO;
                break;
            case'marketing':
                return Task::TASK_TYPE_MARKETING;
                break;

        }
    }

    private function getWorker($type, $lang): User
    {
        $permission = '';
        switch ($type) {
            case'translation':
                $permission = 'translate-';
                break;
            case'seo':
                $permission = 'seo-';
                break;
            case'marketing':
                $permission = 'ads-';
                break;

        }

        return User::permission($permission . $lang)->with('tasks')->get()->sortBy(function ($user) {
            return $user->tasks->count();
        })->first();
    }
}
