import PortalVue from 'portal-vue'
import VTooltip from 'v-tooltip'
Nova.booting((Vue, router, store) => {
    window.trans = (string) => _.get(Nova.config.i18n, string);
    store.state.currency = Nova.config.currency;
    store.state.rates = Nova.config.rates;
    window.lang = document.documentElement.lang;
    window.locales = Nova.config.locales;
    window.view_locales = Nova.config.view_locales;
    Vue.mixin({
        methods: {
            $formatPrice(value, locale = this.$store.state.currency.locale, currency = this.$store.state.currency.val) {
                let opt = {
                    minimumFractionDigits: 0,
                    maximumFractionDigits: 0,
                };
                if (this.$store.state.currency.val !== 'EUR') {
                    value *= this.$store.state.rates['EUR_' + this.$store.state.currency.val];
                }
                if (this.$store.state.currency.val == 'BTC') {
                    opt = {
                        minimumFractionDigits: 0,
                        maximumFractionDigits: 6,
                    };
                }
                return value.toLocaleString(locale,
                    {
                        style: 'currency',
                        currency: currency,
                        ...opt
                    })
            },
        }
    });
    Nova.Vue = Vue;
    Nova.$router = router;
    Nova.Vue.use(PortalVue);
    Nova.Vue.use(VTooltip);
    Nova.Vue.prototype.trans = (string, args) => {
        let value = _.get(Nova.config.i18n, string);

        _.eachRight(args, (paramVal, paramKey) => {
            value = _.replace(value, `:${paramKey}`, paramVal);
        });
        return value;
    };

    router.addRoutes([
        {
            name: 'business-index',
            path: '/business',
            component: require('./components/Index'),
        },
        {
            name: 'business-create',
            path: '/business/create',
            component: require('./components/Create'),
        },
        {
            name: 'business-edit',
            path: '/business/:id/edit',
            props: true,
            component: require('./components/edit'),
        },
        {
            name: 'business-tasks',
            path: '/business/tasks',
            props: true,
            component: require('./components/tasks'),
        },
    ])
})
