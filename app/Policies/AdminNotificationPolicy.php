<?php

namespace App\Policies;

use App\Models\Auth\User;
use App\Models\Roles;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminNotificationPolicy
{
    use HandlesAuthorization;

    /**
     * Почему то нужно явно указывать crud, хотя по идее before
     * должен работать на все
     */
    public function before(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function viewAny(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function view(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function create(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function update(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function delete(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function restore(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function forceDelete(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }
}
