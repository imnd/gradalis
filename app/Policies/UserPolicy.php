<?php

namespace App\Policies;

use App\Models\Auth\User;
use App\Models\Roles;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;
    
    public function viewAny(User $user)
    {
        return $user->hasAnyRole([Roles::ROLE_ADMIN, Roles::ROLE_MAIN_BROKER, Roles::ROLE_BROKER_BUYER, Roles::ROLE_BROKER_SELLER]);
    }

    public function view(User $user)
    {
        return $user->hasRole([Roles::ROLE_ADMIN, Roles::ROLE_MAIN_BROKER, Roles::ROLE_BROKER_BUYER, Roles::ROLE_BROKER_SELLER]);
    }

    public function create(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function update(User $user)
    {
        return $user->hasRole([Roles::ROLE_ADMIN, Roles::ROLE_MAIN_BROKER, Roles::ROLE_BROKER_BUYER, Roles::ROLE_BROKER_SELLER]);
    }

    public function delete(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function restore(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }

    public function forceDelete(User $user)
    {
        return $user->hasRole(Roles::ROLE_ADMIN);
    }
}
