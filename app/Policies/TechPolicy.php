<?php

namespace App\Policies;

use App\Models\Auth\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TechPolicy
{
    use HandlesAuthorization;


    public function view(User $user)
    {
        return $user->hasPermissionTo('view Tech');
    }

    public function create(User $user)
    {
        return $user->hasPermissionTo('create Tech');
    }

    public function update(User $user)
    {
        return $user->hasPermissionTo('update Tech');
    }

    public function delete(User $user)
    {
        return $user->hasPermissionTo('delete Tech');
    }

    public function restore(User $user)
    {
        return $user->hasPermissionTo('restore Tech');
    }

    public function forceDelete(User $user)
    {
        return $user->hasPermissionTo('delete Tech');
    }
}
