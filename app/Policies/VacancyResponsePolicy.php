<?php

namespace App\Policies;

use App\Models\Auth\User;
use App\Models\Roles;
use Illuminate\Auth\Access\HandlesAuthorization;

class VacancyResponsePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->hasAnyRole(['Админ', Roles::ROLE_TECH_BRANCH_MANAGER]);
    }

    public function view(User $user)
    {
        return $user->hasAnyRole(['Админ', Roles::ROLE_TECH_BRANCH_MANAGER]);
    }

    public function create(User $user)
    {
        return false;
    }

    public function update(User $user)
    {
        return false;
    }

    public function delete(User $user)
    {
        return false;
    }

    public function restore(User $user)
    {
        return $user->hasRole('Админ');
    }

    public function forceDelete(User $user)
    {
        return false;
    }
}
