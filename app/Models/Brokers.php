<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model,
    App\Models\Business\BusinessCategory,
    App\Models\Franchise\Franchise,
    App\Models\Auth\User
;

class Brokers extends Model
{
    public $timestamps = true;

    protected $table = 'brokers';
    protected $guarded = ['id'];
    protected $fillable = [
        'user_id', 'business_category_id', 'is_company', 'company',
    ];

    # Relations

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function bizCategory()
    {
        return $this->belongsTo(BusinessCategory::class, 'business_category_id');
    }

    public function franchises()
    {
        return $this->morphMany(Franchise::class, 'business_category_id');
    }
}
