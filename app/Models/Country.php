<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model,
    Spatie\Translatable\HasTranslations;

class Country extends Model 
{
    use HasTranslations;

    public $translatable = ['translation'];
    public $timestamps = false;

    protected $table = 'countries';
    protected $fillable = array('name', 'translation');

    public function cities()
    {
        return $this->hasManyThrough('App\Models\City', Region::class);
    }
}
