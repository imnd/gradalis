<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model,
    Spatie\Translatable\HasTranslations;

class Region extends Model
{
    use HasTranslations;

    public $translatable = ['translation'];

    protected $table = 'regions';
    public $timestamps = true;
    protected $fillable = array('name', 'translation', 'country_id','country_id');

    public function city()
    {
        return $this->hasMany('App\Models\City');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

}
