<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model,
    Spatie\Translatable\HasTranslations;

class ListModel extends Model
{
    use HasTranslations;

    public $timestamps = false;

    public $translatable = ['translation'];

    protected $fillable = ['name', 'translation'];

    protected $guarded = ['id'];
}
