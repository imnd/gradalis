<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacancyResponse extends Model
{
    protected $guarded = [];

    public function vacancy()
    {
        return $this->belongsTo('App\Models\Vacancy');
    }
}
