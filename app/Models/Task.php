<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 13 Mar 2019 17:08:03 +0300.
 */

namespace App\Models;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 *
 * @property int $id
 * @property int $target_id
 * @property int $target_type
 * @property int $task_type
 * @property int $status
 * @property int $priority
 * @property int $tech_id
 * @property int $from_id
 * @property int $to_id
 * @property \Carbon\Carbon $started_at
 * @property \Carbon\Carbon $ended_at
 * @property \Carbon\Carbon $planed_at
 * @property string $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Auth\User $user
 *
 * @package App\Models
 */
class Task extends Model
{
    public const STATUS_AWAIT = 0;
    public const STATUS_IN_PROGRESS = 1;
    public const STATUS_REJECTED = 2;
    public const STATUS_APPROVED = 3;

    public const TARGET_TYPE_BUSINESS = 1;
    public const TARGET_TYPE_FRANCHISE = 2;

    public const TASK_TYPE_BROKER_REVIEW = 1;
    public const TASK_TYPE_TRANSLATION = 2;
    public const TASK_TYPE_SEO = 3;
    public const TASK_TYPE_MARKETING = 4;

    public const PRIORITY_LOW = 0;
    public const PRIORITY_MEDIUM = 1;
    public const PRIORITY_HIGH = 2;

    protected $casts = [
        'target_id' => 'int',
        'task_type' => 'int',
        'status'    => 'int',
        'priority'  => 'int',
        'tech_id'   => 'int',
        'broker_id' => 'int',
        'worker_id' => 'int'
    ];

    protected $dates = [];

    protected $fillable = [
        'target_id',
        'target_type',
        'task_type',
        'status',
        'priority',
        'tech_id',
        'from_id',
        'to_id',
        'started_at',
        'ended_at',
        'planed_at',
        'message'
    ];

    public static function getStatuses()
    {
        return [
            self::STATUS_AWAIT       => 'В ожидании',
            self::STATUS_IN_PROGRESS => 'В процессе',
            self::STATUS_REJECTED    => 'Отклонено',
            self::STATUS_APPROVED    => 'Одобрено',
        ];
    }

    public static function getStatus($status)
    {
        return self::getStatuses()[$status] ?? null;
    }

    public static function getTargetTypes()
    {
        return [
            self::TARGET_TYPE_BUSINESS => 'Бизнес',
        ];
    }

    public static function getTargetType($type)
    {
        return self::getTargetTypes()[$type] ?? null;
    }

    public static function getTaskTypes()
    {
        return [
            self::TASK_TYPE_BROKER_REVIEW => 'Модерация брокера',
            self::TASK_TYPE_TRANSLATION   => 'Перевод',
            self::TASK_TYPE_SEO           => 'SEO',
            self::TASK_TYPE_MARKETING     => 'Маркетинг',
        ];
    }

    public static function getTaskType($type)
    {
        return self::getTaskTypes()[$type] ?? null;
    }

    public static function getPriorities()
    {
        return [
            self::PRIORITY_LOW    => 'Низкий',
            self::PRIORITY_MEDIUM => 'Средний',
            self::PRIORITY_HIGH   => 'Высокий',
        ];
    }

    public static function getPriority($priority)
    {
        return self::getPriorities()[$priority] ?? null;
    }

    public function executive()
    {
        return $this->belongsTo(User::class, 'to_id');
    }

    public function assigner()
    {
        return $this->belongsTo(User::class, 'from_id');
    }
}
