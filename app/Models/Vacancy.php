<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Vacancy extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = [
        'name',
        'salary',
        'work_time',
        'language',
        'description',
        'url',
        'seo_description',
        'seo_title',
        'seo_keywords',
    ];


    protected $fillable = [
        'name',
        'city_id',
        'work_time',
        'url',
        'salary',
        'language',
        'seo_description',
        'seo_title',
        'seo_keywords',
    ];
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
}
