<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];
    protected $table = 'roles';
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'guard_name',
    ];

    const NAMES = [
        self::ROLE_ADMIN,
        self::ROLE_MANAGER,
        self::ROLE_CONTENT_MANAGER,
        self::ROLE_SELLERS_BROKER,
        self::ROLE_BROKER,
        self::ROLE_BROKER_BUYER,
        self::ROLE_BROKER_SELLER,
        self::ROLE_MAIN_BROKER,
        self::ROLE_SELLER,
        self::ROLE_BUYER,
        self::ROLE_EXECUTIVE,
        self::ROLE_ACCOUNT_MANAGER,
        self::ROLE_MEDIA_BUYER,
        self::ROLE_TECH_BRANCH_MANAGER,
        self::ROLE_REGION_ADMIN,
        self::ROLE_SENIOR_REGION_ADMIN,
        self::ROLE_SENIOR_COUNTRY_ADMIN,
        self::ROLE_MARKETER,
        self::ROLE_SEO,
        self::ROLE_TRANSLATOR,
        self::ROLE_REGION_BROKER,
        self::ROLE_NEWS_EDITOR,
        self::ROLE_OPERATIONAL_EDITOR,
        self::ROLE_CLAIM_EDITOR,
        self::ROLE_SEO_EDITOR,
        self::ROLE_SENIOR_CONTENT_MANAGER,
        self::ROLE_JUNIOR_CONTENT_MANAGER,
        self::ROLE_TRANSLATE_MANAGER,
        self::ROLE_COMPLAINTS_MANAGER,
        self::ROLE_TECH_DEPARTMENT,
    ];

    const ROLE_ADMIN_ID = 1;
    const ROLE_MANAGER_ID = 2;
    const ROLE_CONTENT_MANAGER_ID = 3;
    const ROLE_SELLERS_BROKER_ID = 4;
    const ROLE_BROKER_ID = 5;
    const ROLE_BROKER_BUYER_ID = 6;
    const ROLE_BROKER_SELLER_ID = 7;
    const ROLE_MAIN_BROKER_ID = 8;
    const ROLE_SELLER_ID = 9;
    const ROLE_BUYER_ID = 10;
    const ROLE_EXECUTIVE_ID = 11;
    const ROLE_ACCOUNT_MANAGER_ID = 12;
    const ROLE_MEDIA_BUYER_ID = 13;
    const ROLE_TECH_BRANCH_MANAGER_ID = 14;
    const ROLE_REGION_ADMIN_ID = 15;
    const ROLE_SENIOR_REGION_ADMIN_ID = 16;
    const ROLE_SENIOR_COUNTRY_ADMIN_ID = 17;
    const ROLE_MARKETER_ID = 18;
    const ROLE_SEO_ID = 19;
    const ROLE_TRANSLATOR_ID = 20;
    const ROLE_REGION_BROKER_ID = 21;
    const ROLE_NEWS_EDITOR_ID = 22;
    const ROLE_OPERATIONAL_EDITOR_ID = 23;
    const ROLE_CLAIM_EDITOR_ID = 24;
    const ROLE_SEO_EDITOR_ID = 25;
    const ROLE_SENIOR_CONTENT_MANAGER_ID = 26;
    const ROLE_JUNIOR_CONTENT_MANAGER_ID = 27;
    const ROLE_TRANSLATE_MANAGER_ID = 28;
    const ROLE_COMPLAINTS_MANAGER_ID = 29;
    const ROLE_TECH_DEPARTMENT_ID = 30;

    const ROLE_ADMIN = 'Админ';
    const ROLE_REGION_ADMIN = 'Админ региона';
    const ROLE_SENIOR_REGION_ADMIN = 'Старший админ региона';
    const ROLE_SENIOR_COUNTRY_ADMIN = 'Старший админ страны';
    const ROLE_SELLER = 'Продавец';
    const ROLE_BUYER = 'Покупатель';
    const ROLE_MEDIA_BUYER = 'Медиа-баер';
    const ROLE_MANAGER = 'Менеджер';
    const ROLE_MARKETER = 'Маркетолог';
    const ROLE_SEO = 'SEO';
    const ROLE_BROKER = 'Брокер';
    const ROLE_BROKER_BUYER = 'Брокер-покупатель';
    const ROLE_TRANSLATOR = 'Переводчик';
    const ROLE_EXECUTIVE = 'Исполнитель';
    const ROLE_MAIN_BROKER = 'Главный брокер';
    const ROLE_REGION_BROKER = 'Региональный брокер';
    const ROLE_SELLERS_BROKER = 'Брокер продавцов';
    const ROLE_BROKER_SELLER = 'Брокер-продавец';
    const ROLE_NEWS_EDITOR = 'Редактор новостей';
    const ROLE_OPERATIONAL_EDITOR = 'Операционный редактор';
    const ROLE_CLAIM_EDITOR = 'Редактор заявок';
    const ROLE_SEO_EDITOR = 'Сео редактор';
    const ROLE_ACCOUNT_MANAGER = 'Акаунт-менеджер';
    const ROLE_CONTENT_MANAGER = 'Контент менеджер';
    const ROLE_SENIOR_CONTENT_MANAGER = 'Старший менеджер контент группы';
    const ROLE_JUNIOR_CONTENT_MANAGER = 'Младший контент-менеджер';
    const ROLE_TRANSLATE_MANAGER = 'Менеджер переводов';
    const ROLE_COMPLAINTS_MANAGER = 'Отдел жалоб и проблем';
    const ROLE_TECH_DEPARTMENT = 'Тех отдел';
    const ROLE_TECH_BRANCH_MANAGER = 'Руководитель филиала';
}
