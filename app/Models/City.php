<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model,
    Spatie\Translatable\HasTranslations,
    Znck\Eloquent\Traits\BelongsToThrough
;

class City extends Model 
{
    use HasTranslations,
        BelongsToThrough;

    public $translatable = ['translation'];
    public $timestamps = false;

    protected $table = 'cities';
    protected $fillable = ['name', 'translation', 'country_id', 'region_id'];

    # Relations

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function country()
    {
        return $this->belongsToThrough(Country::class, Region::class);
    }
}
