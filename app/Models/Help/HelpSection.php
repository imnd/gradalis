<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * Class HelpSection
 *
 * @property int $id
 * @property array $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class HelpSection extends Model
{
    use HasTranslations;

    protected $table = 'help_section';

    public static $search = [
        'title',
    ];

    public $translatable = [
        'title',
        'url',
        'seo_description',
        'seo_title',
        'seo_keywords',
    ];

    protected $fillable = [
        'title',
        'url',
        'seo_description',
        'seo_title',
        'seo_keywords',
    ];

    public function categories()
    {
        return $this->hasMany('App\Models\Help\HelpCategory', 'section_id',
            'id');
    }
}
