<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * Class HelpCategory
 * 
 * @property int $id
 * @property array $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class HelpCategory extends Model
{
    use HasTranslations;
    
	protected $table = 'help_category';
    protected $primaryKey = 'id';
    
    public $translatable = [
        'title',
        'seo_description',
        'seo_title',
        'seo_keywords',
        'url',
    ];

    /*	protected $casts = [
            'title' => 'json',
            'section_id' => 'int',
            'seo_description' => 'json',
            'seo_title' => 'json',
            'seo_keywords' => 'json',
            'url' => 'json',

        ];*/

	protected $fillable = [
		'title',
        'section_id',
        'seo_description',
        'seo_title',
        'seo_keywords',
        'url',
	];
    

    public function section()
    {
        return $this->belongsTo(HelpSection::class, 'section_id', 'id');
    }
    
    public function helps()
    {
        return $this->hasMany(Help::class, 'category_id', 'id');
    }

    public function approvedHelps()
    {
        return $this->helps()->where('status', Help::STATUS_APPROVED);
    }
}
