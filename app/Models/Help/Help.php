<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Help
 * 
 * @property int $id
 * @property array $title
 * @property array $description
 * @property array $seo_description
 * @property array $seo_title
 * @property array $seo_keywords
 * @property array $vk
 * @property array $ok
 * @property array $google
 * @property array $mail
 * @property array $fb
 * @property array $yandex
 * @property int $status
 * @property int $category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Help extends Model
{
    use HasTranslations, HasMediaTrait;
    
	protected $table = 'help';
    
    //В ожидании
    public const STATUS_AWAIT = 0;
    //Прошел модерацию
    public const STATUS_MODERATED = 1;
    //Одобрен
    public const STATUS_APPROVED = 2;
    //Отклонен
    public const STATUS_DECLINED = 4;
    
    public $translatable = [
        'title',
        'description',
        'url',
        'seo_description',
        'seo_title',
        'seo_keywords',
    ];


	protected $fillable = [
		'title',
		'description',
        'url',
		'status',
        'category_id',
        'seo_description',
        'seo_title',
        'seo_keywords',
	];
    
    public static function getStatuses()
    {
        return [
            self::STATUS_AWAIT     => 'В ожидании',
            self::STATUS_MODERATED => 'Прошел модерацию',
            self::STATUS_APPROVED => 'Одобрен',
            self::STATUS_DECLINED  => 'Отклонен',
        ];
    }
    
    public function category()
    {
        return $this->belongsTo(HelpCategory::class, 'category_id', 'id');
    }
    
    public function helpMediaru()
    {
        return $this->media()->where('collection_name', 'ru');
    }
    public function helpMediaen()
    {
        return $this->media()->where('collection_name', 'en');
    }
    public function helpMediapl()
    {
        return $this->media()->where('collection_name', 'pl');
    }
}
