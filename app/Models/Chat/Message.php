<?php

namespace App\Models\Chat;

use
    App,
    Session,
    Illuminate\Database\Eloquent\Model,
    Illuminate\Database\Eloquent\SoftDeletes,
    Spatie\MediaLibrary\Models\Media,
    Spatie\MediaLibrary\HasMedia\HasMedia,
    Spatie\MediaLibrary\HasMedia\HasMediaTrait,
    App\Models\Auth\User
;

class Message extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;

    const STATUS_MODERATION = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = 2;

    public function __construct(array $attributes = array())
    {
        $this->attributes['locale'] = App::getLocale();
        $this->attributes['ip_info'] = json_encode(Session::get('ip_info'));

        parent::__construct($attributes);
    }

    protected $fillable = ['from', 'to', 'text', 'status', 'dialog_id', 'locale', 'ip_info'];

    protected $dates = ['deleted_at'];

    protected $appends = ['media_links'];

    protected $attributes = [
        'status' => 0
    ];

    protected $casts = [
        'ip_info' => 'array',
    ];

    //TODO протестить
    public function registerMediaCollections()
    {
        $this->addMediaCollection('attachment')
            ->acceptsFile(function (File $file) {
                return $file->mimeType === 'image/jpeg'
                    || $file->mimeType === 'image/png'
                    || $file->mimeType === 'application/pdf';
            });
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this
            ->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10);
    }

    # Attributes

    public function getMediaLinksAttribute()
    {
        $mediaMapped = $this->media;
        foreach ($this->media as $k => $media){
            $mediaMapped[$k]->url = [
                'origin' => $media->getUrl(),
                'thumb' =>$media->getUrl('thumb')
            ];
        }
        return $mediaMapped;
    }

    public function getTextAttribute($text)
    {
        return (string)$text;
    }

    # Relations

    public function dialog()
    {
        return $this->belongsTo(Dialog::class);
    }

    public function from()
    {
        return $this->belongsTo(User::class, 'from', 'id');
    }

    public function to()
    {
        return $this->belongsTo(User::class, 'to', 'id');
    }
}
