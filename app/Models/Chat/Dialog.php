<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model,
    App\Models\Auth\User;

class Dialog extends Model
{
    /** @const Типы диалогов */
    const TYPE_SUPPORT = 1;
    const TYPE_BUYER = 2;
    const TYPE_SELLER = 3;
    const TYPE_UNNAMED = 4;
    const TYPES = [
        self::TYPE_SUPPORT => 'support',
        self::TYPE_BUYER => 'buyer',
        self::TYPE_SELLER => 'seller',
        self::TYPE_UNNAMED => 'unnamed',
    ];

    protected $fillable = ['type', 'related_to', 'user_id', 'theme'];
    protected $morphToTypes = [
        'App\Models\Business\Business' => 'business',
        'App\Models\Business\Franchise' => 'franchise'
    ];
    protected $appends = ['object_type_code'];

    # Attributes

    public function getObjectTypeCodeAttribute()
    {
        if ($type = $this->object_type) {
            return array_get($this->morphToTypes, $type, $type);
        }
    }

    public function getTypeAttribute($type)
    {
        return self::TYPES[$type];
    }

    public function setTypeAttribute($value)
    {
        if (is_int($value))
            $this->attributes['type'] = $value;
        elseif (
                is_string($value)
            and $type = array_search($value, self::TYPES)
        )
            $this->attributes['type'] = $type;
        else
            throw new \Exception('Wrong Dialog model type');
    }

    # Relations

    public function object()
    {
        return $this->morphTo();
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * Аффтар
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Пользователи, добавленные к текущему диалогу
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
