<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class OperationRestrictions extends ListModel
{
    protected $table = 'franchise_operation_restrictions';

    public $timestamps = false;
}
