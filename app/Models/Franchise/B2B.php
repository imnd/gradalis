<?php

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model;

class B2B extends Model
{
    protected $table = 'franchise_b2b';

    protected $fillable = [
        'average_check_clients',
        'have_existing_contracts',
        'main_category_business_partners',
        'count_perpetual_service_contracts',
        'main_attract_client_adv_src'
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $casts = [
        'main_category_business_partners' => 'array',
        'main_attract_client_adv_src' => 'array',
    ];

    public function contracts()
    {
        return $this->hasMany(B2BContractsList::class, 'franchise_b2b_id', 'id');
    }

    public function franchise()
    {
        return $this->belongsTo(Franchise::class, 'franchise_id', 'id');
    }

    public function mainAttractClientAdvSrc()
    {
        $data = [];

        if ($this->main_attract_client_adv_src) {

            foreach ($this->main_attract_client_adv_src as $list) {
                $data[] = MainAdvertisingSourcesAttractClients::find($list)->slug;
            }

        }

        return implode(', ', $data);
    }

    public function main_category_business_partners()
    {
        $data = [];

//        if ($this->main_category_business_partners) {
//            foreach ($this->main_category_business_partners as $list) {
//                $data[] = ::find($list)->slug;
//            }
//        }

        return implode(', ', $data);
    }
}
