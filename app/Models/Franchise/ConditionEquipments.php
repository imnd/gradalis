<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class ConditionEquipments extends ListModel
{
    protected $table = 'franchise_condition_equipments';
}
