<?php

namespace App\Models\Franchise;

use
    Carbon\Carbon,
    Illuminate\Database\Eloquent\Model,
    ChristianKuri\LaravelFavorite\Traits\Favoriteable,
    Spatie\MediaLibrary\HasMedia\HasMedia,
    Spatie\MediaLibrary\HasMedia\HasMediaTrait,

    App\Models\Buyer,
    App\Models\ObjectRequest,
    App\Models\Referral\Campaign,
    App\Models\Task,
    App\Models\Auth\User,
    App\Models\City,
    App\Models\Region,
    App\Models\Country,
    App\Models\News\News,

    App\Traits\HasTranslations,
    App\Traits\Pricable,
    App\Traits\MediaConversion
;

class Franchise extends Model implements HasMedia
{
    use HasTranslations,
        Favoriteable,
        HasMediaTrait,
        MediaConversion,
        Pricable;

    // Новая
    const STATUS_NEW = 0;
    // В ожидании
    const STATUS_AWAIT = 1;
    // Прошел модерацию
    const STATUS_MODERATED = 2;
    // Отклонен
    const STATUS_DECLINED = 3;
    // Продан
    const STATUS_SOLD = 4;
    // Зарезервирован
    const STATUS_RESERVED = 5;

    // Продавать ч/з брокера или нет
    const SELL_WITH_BROKER = 1;
    const SELL_WITHOUT_BROKER = 0;

    const CONDITION_WITHOUT_ANYTHING = 0;
    const CONDITION_ROYALTY = 1;
    const CONDITION_LUMP_SUM = 2;

    // Видео в модерации
    const VIDEO_ON_MODERATION = 0;
    // Видео одобрен
    const VIDEO_APPROVED = 1;
    // Видео откленен
    const VIDEO_DECLINED = 2;

    const POLYMORPHIC_NAME = 'franchises';

    public $pricable = [
        'price',
        'profitability',
        'options',
        'revenue',
        'profit',
        'profitability',
        'general_expenses',
        'month_expenses',
        'staff_costs',
        'property_costs',
    ];
    public $translatable = [
        'name', 'description', 'name_business',
        'justification_financial_indicators',
        'name_legal_entity', 'url',
        'seo_title', 'seo_description', 'seo_keywords',
        'education', 'metrics_mail', 'metrics_google',
        'metrics_fb', 'metrics_vk', 'metrics_ok',
        //'franchise_video_review',
        //'name_video_review',
    ];

    protected $table = 'franchises';
    protected $appends = ['type', 'euro', 'bitcoin', 'card', 'logo', 'published_at'];
    protected $fillable = [
        'name',
        'description',
        'commission',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'education',
        'price',
        'profitability',
        'profit',
        'status',
        'user_id',
        'roality',
        'foundation_year',
        'start_year',
        'own_enterprices',
        'region_id',
        'country_id',
        'city_id',
        'category_id',
        'percent',
        'revenue',
        'weight',
        'discount',
        'call_count',
        'metrics_mail',
        'metrics_google',
        'metrics_fb',
        'metrics_vk',
        'metrics_ok',
        'url',
        'condition',
        'lump_sum',
        'subcat_id',
        'have_packages',
        'name_business',
        'name_legal_entity',
        'video_exists',
        'video_status',
        'video_title',
        'price_including_vat',
        'royalty_rate_period',
        'video_url',
        'staff_costs',
        'have_certificates',
        'launch_dates',
        'turnover_year',
        'turnover_month',
        'month_expenses',
        'property_costs',
        //'return_payback',
        'general_expenses',
        'cost_business_per_year',
        'cost_business_per_quarter',
        'justification_financial_indicators',
        'title_documents',
        'property_type_id',
        'category_property_id',
        'price_including_vat_bool',
        'price_square_meters',
        //'ref_register_estate',
        'number_square_meters',
        'restrictions_operation',
        //'technical_property_plan',
        'coordination_redevelopment',
        'list_restrictions_operation',
        'month_teach',
        'needed_licenses',
        'transfer_work_schemes',
        'transfer_customer_base',
        'type_training_materials',
        'ready_training_materials',
        'type_transfer_customer_base',
        'list_equipment',
        'discount_equipment',
        'included_equipment',
        'condition_equipment_id',
        'total_amount_equipment',
        'staff_training_equipment',
        'total_amount_equipment_with_discount',
        'type_audience',
        'tax_system',
        'legal_status',
        //'have_penalties',
        //'list_penalties',
        //'have_disputable_situations',
        //'list_disputable_situations',
        //'changes_profile_legal_entity',
        'sell_with_broker',
    ];

    protected $casts = [
        //'list_disputable_situations' => 'array',
        //'list_penalties' => 'array',
        'justification_financial_indicators' => 'array',
        'list_restrictions_operation' => 'array',
        'type_training_materials' => 'array',
        'type_transfer_customer_base' => 'array',
        'list_equipment' => 'array',
        'legal_status' => 'array',
        'tax_system' => 'array'
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('franchise')
            ->registerMediaConversions(function() {
                $this->addThumb();
                $this->addWatermark();
                $this->addCard();
                $this->addOriginal();
            });

        $this->addMediaCollection('logo')
            ->registerMediaConversions(function() {
                $this->addThumb();
                $this->addOriginal(371, 247);
            });

        $this->addMediaCollection('docs');
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_AWAIT     => 'В ожидании',
            self::STATUS_MODERATED => 'Прошел модерацию',
            self::STATUS_DECLINED  => 'Отклонен',
        ];
    }
    public static function getVideoStatuses()
    {
        return [
            self::VIDEO_ON_MODERATION => 'В ожидании модерации',
            self::VIDEO_APPROVED => 'Прошел модерацию',
            self::VIDEO_DECLINED => 'Отклонен',
        ];
    }

    # Attributes

    public function getAttributeValue($key)
    {
        if ($pricable = $this->getPricableAttributeValue($key)) {
            return $pricable;
        }
        return $this->getTranslatableAttributeValue($key);
    }

    public function setAttribute($key, $value)
    {
        if ($pricable = $this->setPricableAttribute($key, $value)) {
            return $pricable;
        }
        return $this->setTranslationAttribute($key, $value);
    }

    public function getTypeAttribute()
    {
        return 'franchise';
    }

    public function getCardAttribute()
    {
        if ($this->media->isEmpty()) {
            return 'default.jpg';
        } else {
            $cover = $this->media->filter(function ($media, $key) {
                return $media->hasCustomProperty('cover') && $media->getCustomProperty('cover');
            });

            return sizeof($cover) ? $cover->first()->getUrl('card') : $this->media->first()->getUrl('card');
        }
    }

    public function getLogoAttribute()
    {
        if ($this->media->isEmpty()) {
            return 'default.jpg';
        } else {
            return $this->media->first()->getUrl('card');
        }
    }

    public function getPublishedAtAttribute()
    {
        return Carbon::parse($this->created_at)->diffInDays(Carbon::now());
    }

    public function getEuroAttribute()
    {
        return $this->price * config('currency.PLN_EUR');
    }

    public function getBitcoinAttribute()
    {
        return $this->price * config('currency.PLN_BTC');
    }

    public function getBrokerFullNameAttribute()
    {
        if ($broker = $this->broker) {
            return $broker->getFullNameAttribute();
        }
    }

    public function getBrokerAvatarAttribute()
    {
        if ($broker = $this->broker) {
            return $broker->avatar;
        }
    }

    public function listRestrictionsOperation()
    {
        $data = [];
        foreach ($this->list_restrictions_operation as $list) {
            $data[] = OperationRestrictions::find($list)->translation;
        }
        return implode(', ', $data);
    }

    # Relations

    public function approvedNews()
    {
        return $this->news()->where('status', News::STATUS_APPROVED);
    }

    public function getLatestNews()
    {
        return $this->approvedNews()->orderBy('created_at', 'DESC')->get();
    }

    public function approvedObjects()
    {
        return $this->objects()->where('status', FranchiseObject::STATUS_APPROVED);
    }

    public function getLatestObjects()
    {
        return $this->approvedObjects()->orderBy('created_at', 'DESC')->get();
    }

    public function approvedReviews()
    {
        return $this->reviews()->where('status', FranchiseReview::STATUS_APPROVED);
    }

    public function getLatestReviews()
    {
        return $this->approvedReviews()->orderBy('created_at', 'DESC')->get();
    }

    public function franchiseObjectsMarkersData()
    {
        $markersData = [];
        foreach ($this->objects as $object) {
            if ($object->lat > 0 && $object->lng > 0) {
                $markersData[] = [
                    'position' => [
                        'lat' => $object->lat,
                        'lng' => $object->lng
                    ],
                    'info' => [
                        'img' => $object->getFirstMediaUrl('franchise_objects'),
                        'title' => $object->title,
                        'address' => $object->address,
                        'link' => ''
                    ]
                ];
            }
        }
        return $markersData ? ['centerCoordinates' => json_encode($markersData[0]['position']), 'markersData' => json_encode($markersData)] : '';
    }

    public function category()
    {
        return $this->belongsTo(FranchiseCategory::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(Subcategories::class, 'subcat_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function packages()
    {
        return $this->hasMany(FranchisePackage::class);
    }

    public function objects()
    {
        return $this->hasMany(FranchiseObject::class);
    }

    public function reviews()
    {
        return $this->hasMany(FranchiseReview::class, 'franchise_id');
    }

    public function news()
    {
        return $this->morphMany(News::class, 'owner');
    }

    public function docs()
    {
        return $this->hasMany(DocAttachment::class, 'franchise_id', 'id');
    }

    public function acceptedDocs()
    {
        return $this->docs(); //TODO get only docs with status 1
    }

    public function campaign()
    {
        return $this
            ->belongsTo('App\Models\Referral\Campaign', 'target_id')
            ->where('type', '=', Campaign::TYPE_FRANCHISE);
    }

    public function request()
    {
        return $this->morphMany(ObjectRequest::class, 'object');
    }

    public function viewRequest()
    {
        return $this
            ->morphMany(ObjectRequest::class, 'object')
            ->where('type', ObjectRequest::TYPE_VIEW);
    }

    public function docRequest()
    {
        return $this
            ->morphMany(ObjectRequest::class, 'object')
            ->where('type', ObjectRequest::TYPE_DOC);
    }

    public function docNewRequest()
    {
        return $this->docRequest()->where('status', ObjectRequest::STATUS_NEW);
    }

    public function buyer()
    {
        return $this->morphMany(Buyer::class, 'target', 'target_type', 'target_id', 'id');
    }

    public function broker()
    {
        return $this->belongsTo(User::class, 'broker_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'target_id', 'id');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class, 'franchise_id', 'id');
    }

    public function staffs()
    {
        return $this->hasMany(Staff::class, 'franchise_id', 'id');
    }

    public function certificates()
    {
        return $this->hasMany(Certificates::class, 'franchise_id', 'id');
    }

    public function b2b()
    {
        return $this->hasOne(B2B::class, 'franchise_id', 'id');
    }

    public function b2c()
    {
        return $this->hasOne(B2C::class, 'franchise_id', 'id');
    }

    public function propertyType()
    {
        return $this->belongsTo(PropertyTypes::class, 'property_type_id', 'id');
    }

    public function propertyCategory()
    {
        return $this->belongsTo(PropertyCategories::class, 'category_property_id', 'id');
    }
}
