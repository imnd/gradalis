<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class PropertyTypes extends ListModel
{
    protected $table = 'property_type';
}
