<?php

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model;

class AltAddressBusines extends Model
{
    protected $table = 'alt_address_busines_for_franchise';

    protected $fillable = ['franchise_id', 'index', 'address', 'number_house', 'housing_house', 'number_office'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function franchises()
    {
        return $this->belongsTo(Franchise::class, 'franchise_id', 'id');
    }
}
