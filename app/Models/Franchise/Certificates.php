<?php

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model;

class Certificates extends Model
{
    protected $table = 'franchise_certificates';

    protected $fillable = ['franchise_id', 'certificate_document_id', 'certificate_type_id', 'license_period'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function document()
    {
        return $this->hasMany(CertificateDocuments::class);
    }

    public function type()
    {
        return $this->hasMany(CertificateTypes::class);
    }

    public function franchises()
    {
        return $this->belongsTo(Franchise::class, 'franchise_id', 'id');
    }
}
