<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class LegalStatuses extends ListModel
{
    protected $table = 'franchise_legal_statuses';
}
