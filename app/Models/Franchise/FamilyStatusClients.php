<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class FamilyStatusClients extends ListModel
{
    protected $table = 'family_status_clients';
}
