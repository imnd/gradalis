<?php

namespace App\Models\Franchise;

use App\Traits\HasTranslations;
use App\Traits\MediaConversion;
use App\Traits\Pricable;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class DocAttachment extends Model
{

    use HasTranslations, Favoriteable;

    protected $table = 'franchise_doc_attachments';

    protected $fillable = ['franchise_id', 'name', 'file'];

    protected $guarded = ['id','created_at','updated_at'];

    public function franchise()
    {
        return $this->belongsTo(Franchise::class );
    }



}
