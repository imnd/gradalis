<?php

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model;

class B2BContractsList extends Model
{
    protected $table = 'franchise_b2b_contracts_list';

    protected $fillable = ['franchise_b2b_id', 'name', 'date'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

}
