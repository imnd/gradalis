<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class B2C extends ListModel
{
    protected $table = 'franchise_b2c';

    protected $fillable = ['franchise_id', 'age_men_to', 'age_men_from', 'age_women_to', 'age_women_from', 'sex_ratio_to',
        'sex_ratio_from', 'alone_clients', 'child_clients', 'couples_clients', 'students_clients', 'pensioners_clients',
        'average_check_clients', 'family_status_clients', 'social_status_clients', 'gender_target_audience', 'families_with_children_clients',
        'average_income_target_clients_to', 'main_category_business_partners', 'average_income_target_clients_from_to',
        'average_income_target_clients_from',
        'main_attract_client_adv_src',
        'translation',
        'family_status_clients_stat',
        'social_status_clients_stat'
    ];

    protected $casts = [
        'family_status_clients' => 'array',
        'social_status_clients' => 'array',
        'main_attract_client_adv_src' => 'array',
        'family_status_clients_stat' => 'array',
        'social_status_clients_stat' => 'array'
    ];

    public function franchise()
    {
        return $this->belongsTo(Franchise::class, 'franchise_id', 'id');
    }

    public function familyStatus()
    {
        $data = [];
        if ($this->family_status_clients) {


            foreach ((array)$this->family_status_clients as $id) {
                $data[] = FamilyStatusClients::find($id)->translation;
            }

        }
        return implode(', ', $data);
    }

    public function socialStatus()
    {
        $data = [];
        if ($this->social_status_clients) {

            foreach ((array)$this->social_status_clients as $id) {
                $data[] = SocialStatusClient::find($id)->translation;
            }
        }
        return implode(', ', $data);
    }

    public function mainAttractClientAdvSrc()
    {
        $data = [];
        if ($this->main_attract_client_adv_src) {
            foreach ($this->main_attract_client_adv_src as $id) {
                $data[] = MainAdvertisingSourcesAttractClients::find($id)->translation;
            }
        }
        return implode(', ', $data);
    }
}
