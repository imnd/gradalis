<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class TrainingMaterialTypes extends ListModel
{
    protected $table = 'franchise_training_material_types';
}
