<?php

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model,
    Spatie\MediaLibrary\HasMedia\HasMedia,
    Spatie\MediaLibrary\HasMedia\HasMediaTrait,
    App\Traits\MediaConversion,
    Spatie\Translatable\HasTranslations;

class FranchiseObject extends Model implements HasMedia
{
    use HasTranslations, HasMediaTrait, MediaConversion;

    // В ожидании
    public const STATUS_AWAIT = 0;
    // Прошел модерацию
    public const STATUS_MODERATED = 1;
    // Одобрен
    public const STATUS_APPROVED = 2;
    // Отклонен
    public const STATUS_DECLINED = 4;

    public $timestamps = true;
    public $translatable = ['title', 'description'];

    protected $table = 'franchise_objects';
    protected $fillable = [
        'title',
        'description',
        'phone',
        'email',
        'franchise_id',
        'address',
        'lat',
        'lng',
        'status',
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('franchise_objects')
            ->registerMediaConversions(function () {
                $this->addThumb();
                $this->addCard();
                $this->addOriginal();
            });
    }

    public function franchise()
    {
        return $this->belongsTo('App\Models\Franchise\Franchise');
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_AWAIT     => 'В ожидании',
            self::STATUS_MODERATED => 'Прошел модерацию',
            self::STATUS_APPROVED => 'Одобрен',
            self::STATUS_DECLINED  => 'Отклонен',
        ];
    }
}
