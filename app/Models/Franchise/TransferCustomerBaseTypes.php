<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class TransferCustomerBaseTypes extends ListModel
{
    protected $table = 'transfer_customer_base_types';
}
