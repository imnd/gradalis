<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Apr 2019 00:22:44 +0300.
 */

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model,
    Spatie\MediaLibrary\HasMedia\HasMedia,
    Spatie\MediaLibrary\HasMedia\HasMediaTrait,
    App\Traits\MediaConversion,
    Spatie\Translatable\HasTranslations;

/**
 * Class FranchiseReview
 *
 * @property int $id
 * @property array $name
 * @property string $company
 * @property string $position
 * @property string $email
 * @property string $video_url
 * @property array $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class FranchiseReview extends Model implements HasMedia
{
    use HasTranslations, HasMediaTrait, MediaConversion;

    // В ожидании
    public const STATUS_AWAIT = 0;
    // Прошел модерацию
    public const STATUS_MODERATED = 1;
    // Одобрен
    public const STATUS_APPROVED = 2;
    // Отклонен
    public const STATUS_DECLINED = 4;

    public $translatable = ['name', 'description'];

    protected $fillable = [
        'name',
        'company',
        'position',
        'email',
        'video_url',
        'description',
        'franchise_id',
        'status',
    ];

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('franchise_reviews_photo')
            ->registerMediaConversions(function () {
                $this->addThumb();
                $this->addCard();
                $this->addOriginal();
            });

        $this->addMediaCollection('franchise_reviews_materials');
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_AWAIT     => 'В ожидании',
            self::STATUS_MODERATED => 'Прошел модерацию',
            self::STATUS_APPROVED => 'Одобрен',
            self::STATUS_DECLINED  => 'Отклонен',
        ];
    }

    public function franchise()
    {
        return $this->belongsTo('App\Models\Franchise\Franchise');
    }
}
