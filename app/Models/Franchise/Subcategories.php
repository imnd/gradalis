<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class Subcategories extends ListModel
{
    protected $table = 'franchise_subcategories';

    public $translatable = ['translation'];

    protected $fillable = ['name', 'translation', 'category_id', 'icon' ];

    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(FranchiseCategory::class, 'category_id', 'id');
    }

    public function franchise()
    {
        return $this->belongsTo(Franchise::class, 'subcat_id', 'id');
    }
}
