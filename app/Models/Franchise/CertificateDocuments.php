<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class CertificateDocuments extends ListModel
{
    protected $table = 'franchise_certificate_documents';

    public function certificate()
    {
        return $this->belongsTo(Certificates::class);
    }
    public function type()
    {
        return $this->belongsTo(CertificateTypes::class, 'certificate_type_id', 'id' );
    }

}
