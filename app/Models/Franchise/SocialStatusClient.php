<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class SocialStatusClient extends ListModel
{
    protected $table = 'social_status_clients';
}
