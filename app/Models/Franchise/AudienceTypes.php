<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class AudienceTypes extends ListModel
{
    protected $table = 'franchise_audience_types';

    public $timestamps = false;
}
