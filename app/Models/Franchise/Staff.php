<?php

namespace App\Models\Franchise;

use App\Traits\HasTranslations,
    App\Models\PostTypes,
    Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasTranslations;

    protected $table = 'franchise_staff';

    protected $fillable = ['franchise_id', 'monthly_wages', 'tax_amount_per_month', 'post_id'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public $timestamps = false;

    protected $casts = [
        'family_status_clients' => 'array',
        'social_status_clients' => 'array',
        'main_category_business_partners' => 'array',
        'main_attract_client_adv_src' => 'array',
    ];

    public function post()
    {
        return $this->belongsTo(PostTypes::class);
    }

    public function document()
    {
        return $this->hasOne(CertificateDocuments::class);
    }

    public function type()
    {
        return $this->belongsTo(CertificateTypes::class);
    }
}
