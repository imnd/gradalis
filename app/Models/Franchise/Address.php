<?php

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Address extends Model
{
    use HasTranslations;

    protected $table = 'alt_address_busines_for_franchise';

    protected $fillable = ['index', 'address', 'number_house', 'housing_house', 'number_office', 'franchise_id'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function franchise()
    {
        return $this->belongsTo(Franchise::class, 'franchise_id', 'id');
    }
}
