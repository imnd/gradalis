<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class PropertyCategories extends ListModel
{
    protected $table = 'franchise_property_categories';
}
