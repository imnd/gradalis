<?php

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class FranchisePackage extends Model
{
    use HasTranslations;

    public $timestamps = true;
    public $translatable = ['name', 'benefits'];

    protected $table = 'franchise_packages';
    
    protected $fillable = array(
        'options',
        'name',
        'franchise_id',
        'price',
        'vat',
        'include',
        'benefits',
        'terms',
        'royalty_rate',
        'royalty_rate_period'
    );

    protected $casts = [
        //'include' => 'array',
    ];

    public function franchise()
    {
        return $this->belongsTo('App\Models\Franchise\Franchise');
    }
}
