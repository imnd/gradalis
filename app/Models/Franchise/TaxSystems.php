<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class TaxSystems extends ListModel
{
    protected $table = 'franchise_tax_systems';
}
