<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class Penalties extends ListModel
{
    protected $table = 'franchise_penalties';

    public $timestamps = false;
}
