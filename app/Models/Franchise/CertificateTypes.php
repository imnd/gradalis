<?php

namespace App\Models\Franchise;

use App\Models\ListModel;

class CertificateTypes extends ListModel
{
    protected $table = 'franchise_certificate_types';

    public function franchises()
    {
        return $this->hasMany(Certificates::class);
    }

    public function documents()
    {
        return $this->hasMany(CertificateDocuments::class);
    }

}
