<?php

namespace App\Models\Franchise;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class FranchiseCategory extends Model
{
    use HasTranslations;

    public $timestamps = false;
    public $translatable = ['translation'];

    protected $table = 'franchise_categories';
    protected $fillable = ['parent_id', 'name', 'translation', 'icon'];

    # Relations

    public function franchises()
    {
        return $this->hasMany(Franchise::class);
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }
}
