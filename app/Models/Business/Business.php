<?php

namespace App\Models\Business;

use App\Models\Business\Traits\BusinessTranslatableAttributes,
    App\Models\Buyer,
    App\Models\ObjectRequest,
    App\Models\Referral\Campaign,
    App\Models\Task,
    App\Models\Auth\User,
    App\Traits\HasTranslations,
    App\Traits\Pricable,
    Carbon\Carbon,
    ChristianKuri\LaravelFavorite\Traits\Favoriteable,
    Illuminate\Database\Eloquent\Model,
    Spatie\MediaLibrary\HasMedia\HasMedia,
    Spatie\MediaLibrary\HasMedia\HasMediaTrait,
    App\Traits\MediaConversion;

/**
 * Class Business
 *
 * @property int $id
 * @property int $user_id
 * @property array $name
 * @property array $description
 * @property array $seo_title
 * @property array $seo_description
 * @property array $seo_keywords
 * @property int $price
 * @property int $profitability
 * @property int $profit
 * @property int $payback
 * @property int $status
 * @property int $city_id
 * @property int $category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $commission
 * @property int $call_count
 * @property float $percent
 * @property string $metrics
 * @property int $login_count
 * @property array $url
 * @property int $revenue
 * @property int $weight
 * @property int $discount
 * @property int $part
 * @property int $part_amount
 * @property array $options
 * @property int $show_count
 *
 * @property \App\Models\Business\BusinessCategory $business_category
 * @property \App\Models\City $city
 * @property \App\Models\Auth\User $user
 */
class Business extends Model implements HasMedia
{
    use BusinessTranslatableAttributes, Pricable, Favoriteable, HasMediaTrait, MediaConversion, HasTranslations;

    // В ожидании
    const STATUS_NEW = 0;
    // В ожидании
    const STATUS_AWAIT = 1;
    // Прошел модерацию
    const STATUS_MODERATED = 2;
    // Отклонен
    const STATUS_DECLINED = 3;
    // Продан
    const STATUS_SOLD = 4;
    // Зарезервирован
    const STATUS_RESERVED = 5;

    //Весь Бизнес
    const PART_ALL = 1;
    //Доля Бизнеса
    const PART_QUOTA = 2;
    //Акции
    const PART_STOCK = 3;

    public $timestamps = true;
    public $pricable = [
        'price',
        'profitability',
        'revenue',
        'options'
    ];
    public $translatable = ['url', 'name', 'description', 'seo_title', 'seo_description', 'seo_keywords',
        'reason_sale', 'link_video_review', 'name_legal_entity', 'name_video_review', 'additional_information_traffic',
        'additional_information_content_site', 'additional_information_expenses_site',
        'metrics_mail','metrics_google','metrics_fb','metrics_vk','metrics_ok',
    ];
    protected $table = 'businesses';
    protected $appends = ['type', 'euro', 'bitcoin', 'card', 'published_at',
        'reason_sale', 'link_video_review', 'name_legal_entity', 'name_video_review', 'additional_information_traffic',
        'additional_information_content_site', 'additional_information_expenses_site',
    ];
    protected $fillable = [
        'part',
        'url',
        'discount',
        'revenue',
        'weight',
        'price',
        'show_count',
        'percent',
        'call_count',
        'metrics_mail','metrics_google','metrics_fb','metrics_vk','metrics_ok',
        'user_id',
        'profitability',
        'profit',
        'payback',
        'status',
        'commission',
        'name',
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'country_id',
        'region_id',
        'city_id',
        'category_id',
        'options',
    ];
    protected $casts = [
        'options' => 'array',
        'metrics' => 'array',
    ];

    public static function getStatuses()
    {
        return [
            self::STATUS_AWAIT     => 'В ожидании',
            self::STATUS_MODERATED => 'Прошел модерацию',
            self::STATUS_SOLD  => 'Продан',
            self::STATUS_DECLINED  => 'Отклонен',
            self::STATUS_RESERVED  => 'Зарезервирован',
        ];
    }

    public static function getParts()
    {
        return [
            self::PART_ALL   => 'Весь бизнес',
            self::PART_QUOTA => 'Доля бизнеса',
            self::PART_STOCK => 'Акции'
        ];
    }

    public function getAttributeValue($key)
    {
        $pricable = $this->getPricableAttributeValue($key);
        $translatable = $this->getTranslatableAttributeValue($key);
        if ($pricable) {
            return $pricable;
        }
        return $translatable;
    }

    public function setAttribute($key, $value)
    {
        $pricable = $this->setPricableAttribute($key, $value);
        if (!$pricable) {
            return $this->setTranslationAttribute($key, $value);
        } else {
            return $pricable;
        }
    }

    public function getTypeAttribute()
    {
        return "business";
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('business')
            ->registerMediaConversions(function () {
                $this->addCard();
                $this->addOriginal();
                $this->addWatermark();
                $this->addThumb();
            });
        $this->addMediaCollection('docs');
    }

    public function getEuroAttribute()
    {
        return $this->price * config('currency.PLN_EUR');
    }

    public function getPublishedAtAttribute()
    {
        return $this->created_at->diffInDays(Carbon::now());
    }

    public function getBitcoinAttribute()
    {
        return $this->price * config('currency.PLN_BTC');
    }

    public function getCardAttribute()
    {
        if ($this->media->isEmpty()) {
            return 'default.jpg';
        } else {
            return $this->media->first()->getUrl('card');
        }
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Business\BusinessCategory');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Referral\Campaign', 'target_id')->where('type', '=', Campaign::TYPE_BUSINESS);
    }

    public function buyer()
    {
        return $this->morphMany(Buyer::class, 'target', 'target_type', 'target_id', 'id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'target_id', 'id');
    }

    public function viewRequest()
    {
        return $this->morphMany(ObjectRequest::class, 'object')->where('type', ObjectRequest::TYPE_VIEW);
    }

    public function docRequest()
    {
        return $this->morphMany(ObjectRequest::class, 'object')->where('type', ObjectRequest::TYPE_DOC);
    }

    public function request()
    {
        return $this->morphMany(ObjectRequest::class, 'object');
    }

    public function broker()
    {
        return $this->belongsTo(User::class, 'broker_id', 'id');
    }
}
