<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class BusinessCategory extends Model
{
    use HasTranslations;

    public $timestamps = true;
    public $translatable = ['translation'];
    protected $table = 'business_categories';
    protected $fillable = array('parent_id', 'name', 'translation', 'icon');

    # Relations

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }
}
