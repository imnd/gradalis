<?php
/**
 * Created by PhpStorm.
 * User: MIKHAIL
 * Date: 25.03.2019
 * Time: 14:11
 */

namespace App\Models\Business\Traits;


trait BusinessTranslatableAttributes
{

    public function getReasonSaleAttribute()
    {
        return json_encode($this->options['step1']['reasonSale']['val']);
    }

    public function setReasonSaleAttribute($value, $locale)
    {
        $options = $this->options;
        $options['step1']['reasonSale']['val'][$locale] = $value;
        $this->options = array_merge($this->options, $options);
    }

    public function getLinkVideoReviewAttribute()
    {
        return json_encode($this->options['step1']['linkVideoReview']['val']);
    }

    public function setLinkVideoReviewAttribute($value, $locale)
    {
        $options = $this->options;
        $options['step1']['linkVideoReview']['val'][$locale] = $value;
        $this->options = array_merge($this->options, $options);
    }

    public function getNameLegalEntityAttribute()
    {
        return json_encode($this->options['step1']['nameLegalEntity']['val']);
    }

    public function setNameLegalEntityAttribute($value, $locale)
    {
        $options = $this->options;
        $options['step1']['nameLegalEntity']['val'][$locale] = $value;
        $this->options = array_merge($this->options, $options);
    }

    public function getNameVideoReviewAttribute()
    {
        return json_encode($this->options['step1']['nameVideoReview']['val']);
    }

    public function setNameVideoReviewAttribute($value, $locale)
    {
        $options = $this->options;
        $options['step1']['nameVideoReview']['val'][$locale] = $value;
        $this->options = array_merge($this->options, $options);
    }

    public function getAdditionalInformationTrafficAttribute()
    {
        return json_encode($this->options['step6']['additionalInformationTraffic']['val']);
    }

    public function setAdditionalInformationTrafficAttribute($value, $locale)
    {
        $options = $this->options;
        $options['step6']['additionalInformationTraffic']['val'][$locale] = $value;
        $this->options = array_merge($this->options, $options);
    }

    public function getAdditionalInformationContentSiteAttribute()
    {
        return json_encode($this->options['step6']['additionalInformationContentSite']['val']);
    }

    public function setAdditionalInformationContentSiteAttribute($value, $locale)
    {
        $options = $this->options;
        $options['step6']['additionalInformationContentSite']['val'][$locale] = $value;
        $this->options = array_merge($this->options, $options);
    }

    public function getAdditionalInformationExpensesSiteAttribute()
    {
        return json_encode($this->options['step6']['additionalInformationExpensesSite']['val']);
    }

    public function setAdditionalInformationExpensesSiteAttribute($value, $locale)
    {
        $options = $this->options;
        $options['step6']['additionalInformationExpensesSite']['val'][$locale] = $value;
        $this->options = array_merge($this->options, $options);
    }

}
