<?php

namespace App\Models\Auth;

use
    App\Traits\HasTranslations,
    App\Traits\Pricable,
    ChristianKuri\LaravelFavorite\Traits\Favoriteability,
    Illuminate\Contracts\Auth\MustVerifyEmail,
    Illuminate\Database\Eloquent\SoftDeletes,
    Illuminate\Foundation\Auth\User as Authenticatable,
    Illuminate\Notifications\Notifiable,
    Lab404\Impersonate\Models\Impersonate,
    Spatie\Permission\Traits\HasRoles,
    Illuminate\Support\Facades\Auth,
    Illuminate\Support\Facades\Storage,

    App\Models\Referral\Partner,
    App\Models\Roles,
    App\Models\Chat\Dialog,
    App\Models\Business\Business,
    App\Models\Franchise\Franchise,
    App\Models\Task,
    App\Models\City,
    App\Models\Country,
    App\Models\Brokers,
    App\Models\ObjectRequest
;

class User extends Authenticatable implements MustVerifyEmail
{
    use
        Impersonate,
        Notifiable,
        HasRoles,
        Favoriteability,
        SoftDeletes,
        Pricable,
        HasTranslations;

    const DISK_NAME = 'user';

    // TODO: убрать "email_verified_at" сделано для теста
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'email_verified_at',
        'password',
        'balance',
        'phone',
        'country_id',
        'city_id',
        'subscribes',
        'code',
        'login_count',
        'api_token',
        'spoken_langs',
        'sum_from',
        'sum_to',
        'purchase_date_from',
        'purchase_date_to',
        'verified',
        'company_activity_id',
        'soc_network',
        'company_name',
        'secret_key',
    ];

    public $pricable = ['balance'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'full_name',
        'avatarUrl',
    ];

    protected $casts = [
        'subscribes' => 'array'
    ];

    public function getAttributeValue($key)
    {
        if ($pricable = $this->getPricableAttributeValue($key)) {
            return $pricable;
        }
        return $this->getTranslatableAttributeValue($key);
    }

    public function setAttribute($key, $value)
    {
        if ($pricable = $this->setPricableAttribute($key, $value)) {
            return $pricable;
        }
        $this->setTranslationAttribute($key, $value);
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @return mixed
     */
    public function getRegionId()
    {
        if ($city = City::find($this->city_id)) {
            return $city->region_id;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getMessageAddressees()
    {
        // С конкретными языками, то есть должны быть привязки так же к языкам
        $query = self::where('country_id', '=', $this->country_id)->with('roles');
        $role = $this->getRoleNames()[0];
        switch ($role) {
            case Roles::ROLE_REGION_BROKER:
            case Roles::ROLE_SENIOR_CONTENT_MANAGER:
            case Roles::ROLE_NEWS_EDITOR:
            case Roles::ROLE_OPERATIONAL_EDITOR:
            case Roles::ROLE_CLAIM_EDITOR:
                $query = $query->whereHas('roles', function($q) {
                    $q->whereIn('name', [
                        // Маркетологам
                        Roles::ROLE_MARKETER,
                        // Сео редактору
                        Roles::ROLE_SEO_EDITOR,
                        // Медиабайерам (по своей стране) от подразделения в Рф или БУрж
                        Roles::ROLE_MEDIA_BUYER,
                        // Аккаунт менеджерам (по своей стране) от подразделения в Рф или БУрж
                        Roles::ROLE_ACCOUNT_MANAGER,
                    ]);
                });
                break;
            case Roles::ROLE_BUYER:
                $userRegionId = $this->getRegionId();
                $query = $query
                    ->whereHas('roles', function($q) {
                        $q->whereIn('name', [
                            // Отдел контента и переводов
                            Roles::ROLE_TRANSLATE_MANAGER,
                            // Тех. отдел
                            Roles::ROLE_TECH_DEPARTMENT,
                            // Покупателю (Физ лицу)
                            Roles::ROLE_BUYER,
                            // Исполнителю
                            Roles::ROLE_EXECUTIVE,
                            // Отдел жалоб и проблем
                            Roles::ROLE_COMPLAINTS_MANAGER,
                        ]);
                    })
                    ->with('city')
                    ->orWhereHas('city', function($q) use($userRegionId) {
                        // Брокеру по данному региону, стране и языковой версии.
                        $q->where('region_id', $userRegionId);
                    })
                    ->whereHas('roles', function($q) {
                        $q->where('name', Roles::ROLE_BROKER_SELLER);
                    });
                break;
            case Roles::ROLE_SELLER:
                $userRegionId = $this->getRegionId();
                $query = $query
                    ->whereHas('roles', function($q) {
                        $q->whereIn('name', [
                            // Отдел контента и переводов
                            Roles::ROLE_TRANSLATE_MANAGER,
                            // Тех. отдел
                            Roles::ROLE_TECH_DEPARTMENT,
                            // Покупателю (Физ лицу)
                            Roles::ROLE_BUYER,
                            // Исполнителю
                            Roles::ROLE_EXECUTIVE,
                            // Отдел жалоб и проблем
                            Roles::ROLE_COMPLAINTS_MANAGER,]);
                    })
                    ->with('city')
                    ->orWhereHas('city', function($q) use($userRegionId) {
                        // Брокеру по данному региону, стране и языковой версии.
                        $q->where('region_id', $userRegionId);
                    })
                    ->whereHas('roles', function($q) {
                        $q->where('name', Roles::ROLE_BROKER_BUYER);
                    });
                break;

            case Roles::ROLE_EXECUTIVE:
                $query = $query->whereHas('roles', function($q) {
                    $q->whereIn('name', [
                        Roles::ROLE_SENIOR_CONTENT_MANAGER,
                        Roles::ROLE_SENIOR_REGION_ADMIN,
                    ]);
                });
                break;

            case Roles::ROLE_MEDIA_BUYER:
                $query = $query
                    ->whereHas('roles', function($q) {
                        // Старшему админу страны
                        $q->where('name', Roles::ROLE_SENIOR_COUNTRY_ADMIN);
                    })
                    ->with('partners')
                    ->orWhereHas('roles', function($q) {
                        // Брокеру (по реф ссылке, которого зарегился)
                        $q->where('name', Roles::ROLE_BROKER_SELLER);
                    })
                    ->whereHas('partners', function($q) {
                        // Старшему админу страны
                        $q->where('user_id', Auth::id());
                    })
                ;
                break;

            case Roles::ROLE_JUNIOR_CONTENT_MANAGER:
                $query = $query->whereHas('roles', function($q) {
                    $q->whereIn('name', [
                        // Старший менеджер контент группы
                        Roles::ROLE_SENIOR_CONTENT_MANAGER,
                    ]);
                });
                break;

            default:
                break;
        }
        return $query->get();
    }

    public function isActive()
    {
        return $this->active;
    }

    public function canModerateMessages()
    {
        return $this->hasAnyRole([Roles::ROLE_ADMIN, Roles::ROLE_MAIN_BROKER]);
    }

    public function getAvatarUrlAttribute()
    {
        if ($avatar = $this->avatar) {
            return Storage::disk('user')->url($avatar);
        }
        return '/img/void-avatar.png';
    }

    # Relations

    public function broker()
    {
        return $this->hasOne(Brokers::class, 'user_id');
    }

    public function business()
    {
        return $this->hasMany(Business::class);
    }

    public function franchise()
    {
        return $this->hasMany(Franchise::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'id', 'worker_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function partner()
    {
        return $this->hasOne(Partner::class, 'id', 'user_id');
    }

    /**
     * Это может быть или менеджер, или брокер, в зависимости от роли пользователя
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function users()
    {
        $this->hasMany(User::class, 'id', 'user_id');
    }

    public function dialog()
    {
        return $this->belongsTo(Dialog::class);
    }

    /* Диалоги чата в которые добавлен текущий пользователь */
    public function dialogs()
    {
        return $this->belongsToMany(Dialog::class);
    }

    public function objectRequests()
    {
        return $this->hasMany(ObjectRequest::class);
    }

    public function viewRequest()
    {
        return $this->objectRequests()->where('type', ObjectRequest::TYPE_VIEW);
    }

    public function docRequest()
    {
        return $this->objectRequests()->where('type', ObjectRequest::TYPE_DOC);
    }

    public function requestedDoc($objectId)
    {
        return $this->docRequest()->where('object_id', $objectId)->first();
    }
}
