<?php

namespace App\Models\Referral;

use App\Traits\HasTranslations;
use App\Traits\Pricable;
use Illuminate\Database\Eloquent\Model;

class CampaignTarget extends Model
{
    use HasTranslations , Pricable;

    public $timestamps = true;
    protected $table = 'campaign_targets';
    protected $pricable = [
        'cpl',
        'cps',
        'cpa',
    ];
    protected $translatable = ['name'];
    protected $fillable = array('name', 'country_id', 'campaign_id', 'cpa', 'cpl', 'cps');

    public function getAttributeValue($key)
    {
        $pricable = $this->getPricableAttributeValue($key);
        $translatable = $this->getTranslatableAttributeValue($key);
        if ($pricable) {
            return $pricable;
        }
        return $translatable;
    }

    public function setAttribute($key, $value)
    {
        $pricable = $this->setPricableAttribute($key, $value);
        if (!$pricable) {
            return $this->setTranslationAttribute($key, $value);
        } else {
            return $pricable;
        }
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Referral\Campaign');
    }

    public function counters()
    {
        return $this->belongsToMany(
            'App\Models\Referral\InvitationCounter',
            'counter_target',
            'target_id',
            'counter_id'
        )->withPivot('sum', 'status', 'type')->withTimestamps();
    }

}
