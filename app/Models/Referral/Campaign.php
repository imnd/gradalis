<?php

namespace App\Models\Referral;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Translatable\HasTranslations;

class Campaign extends Model implements HasMedia
{
    use HasTranslations, HasMediaTrait;

    const TYPE_BUSINESS = 1;
    const TYPE_FRANCHISE = 2;
    const TYPE_SERVICE = 3;

    const CONDITION_STATUS_DISABLED = 0;
    const CONDITION_STATUS_ENABLED = 1;

    const STATUS_AWAIT = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DECLINED = 2;

    const CAMPAIGN_STATUS_BASE = 1;
    const CAMPAIGN_STATUS_RECOMMENDED = 2;

    const USER_TYPE_SELLER = 1;
    const USER_TYPE_BUYER = 2;

    public $translatable = ['name', 'description'];
    public $timestamps = true;
    protected $table = 'campaigns';
    protected $fillable = array(
        'name', 'site', 'country_id', 'campaign_status', 'description', 'clt_days', 'approve_days', 'pay_days',
        'user_type',
        'target_id', 'type', 'status'
    );

    public static function getTypes()
    {
        return [
            self::TYPE_BUSINESS  => 'Бизнес',
            self::TYPE_FRANCHISE => 'Франшиза',
            self::TYPE_SERVICE   => 'Услуга',
        ];
    }

    public static function getType($type)
    {
        switch ($type) {
            case self::TYPE_BUSINESS:
                return 'Бизнес';
            case self::TYPE_FRANCHISE:
                return 'Франшиза';
            case self::TYPE_SERVICE:
                return 'Услуга';
        }
    }

    public static function getConditionStatuses()
    {
        return [
            self::CONDITION_STATUS_DISABLED => 'Активен',
            self::CONDITION_STATUS_ENABLED  => 'Неактивен',
        ];
    }

    public static function getConditionStatus($status)
    {
        switch ($status) {
            case self::CONDITION_STATUS_DISABLED:
                return 'Активен';
            case self::CONDITION_STATUS_ENABLED:
                return 'Неактивен';
        }
    }

    public static function getCampaignStatuses()
    {
        return [
            self::CAMPAIGN_STATUS_BASE        => 'Базовый',
            self::CAMPAIGN_STATUS_RECOMMENDED => 'Рекомендуемый'
        ];
    }

    public static function getCampaignStatus($status)
    {
        switch ($status) {
            case self::CAMPAIGN_STATUS_BASE:
                return 'Базовый';
            case self::CAMPAIGN_STATUS_RECOMMENDED:
                return 'Рекомендуемый';
        }
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_AWAIT    => 'В ожидании',
            self::STATUS_APPROVED => 'Подтвержден',
            self::STATUS_DECLINED => 'Отклонен'
        ];
    }

    public static function getStatus($status)
    {
        switch ($status) {
            case self::STATUS_AWAIT:
                return 'В ожидании';
            case self::STATUS_APPROVED:
                return 'Подтвержден';
            case self::STATUS_DECLINED:
                return 'Отклонен';
        }
    }

    public static function getUserTypes()
    {
        return [
            self::USER_TYPE_SELLER => 'Продавец',
            self::USER_TYPE_BUYER  => 'Покупатель',
        ];
    }

    public static function getUserType($status)
    {
        switch ($status) {
            case self::USER_TYPE_SELLER:
                return 'Продавец';
            case self::USER_TYPE_BUYER:
                return 'Покупатель';
        }
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('campaigns')
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('thumb')
                    ->width(100)
                    ->height(100);

                $this->addMediaConversion('card')
                    ->width(214)
                    ->height(250);

                $this->addMediaConversion('original')
                    ->width(1024)
                    ->height(768);
            });
    }

    public function referable()
    {
        return $this->morphTo(null, 'type', 'target_id');
    }

    public function conditions()
    {
        return $this->belongsToMany('App\Models\Referral\Condition')->withPivot('status');
    }

    public function targets()
    {
        return $this->hasMany('App\Models\Referral\CampaignTarget');
    }

    public function resources()
    {
        return $this->hasMany('App\Models\Referral\CampaignResource');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function invitations()
    {
        return $this->hasMany('App\Models\Referral\Invitation');
    }

    public function bookmarker()
    {
        return $this->belongsToMany('App\Models\Referral\Partner')->wherePivot('partner_id', auth()->user()->id);
    }

}
