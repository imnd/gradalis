<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model,
    Spatie\Translatable\HasTranslations;

class Tariffs extends Model
{
    use HasTranslations;

    const VISIBLE = 1;

    public $timestamps = false;

    public $translatable = ['name', 'description', 'price'];

    protected $guarded = ['id'];

    protected $fillable = ['name', 'description', 'price', 'icon', 'visible', 'recommended', 'validity', 'discount', 'sales_success_percent'];
    
    /**
     * json поля
     */
    protected $casts = [
        'name' => 'array',
        'description' => 'array',
        'price' => 'array',
    ];

    /**
     * Scope a query to only include users of a given type.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible($query)
    {
        return $query->where('visible', self::VISIBLE);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'tariffs_to_services', 'tariff_id', 'service_id');
    }
}
