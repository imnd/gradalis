<?php

namespace App\Models\Service;

use
    \Storage,
    Illuminate\Database\Eloquent\Model,
    Spatie\Translatable\HasTranslations;

class UsersServiceCategory extends Model
{
    use HasTranslations;

    public $timestamps = false;
    public $translatable = ['name'];

    protected $fillable = ['name', 'icon'];

    public function getIconUrlAttribute()
    {
        return Storage::disk('users_service')->url($this->icon);
    }

    public function services()
    {
        return $this->hasMany(UsersService::class, 'category_id');
    }
}
