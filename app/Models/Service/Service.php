<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model,
    Spatie\Translatable\HasTranslations;

class Service extends Model
{
    use HasTranslations;

    const STATUS_NEW = 0;
    const STATUS_AWAIT = 1;
    const STATUS_MODERATED = 2;
    const STATUS_DECLINED = 3;

    /** @const Для покупателя */
    const TYPE_FOR_SELLER = 1;
    /** @const Для продавца */
    const TYPE_FOR_BUYER = 2;

    public $timestamps = false;

    public $translatable = ['name', 'description', 'price_for'];

    protected $guarded = ['id'];

    protected $fillable = ['name', 'description', 'icon', 'visible', 'price'];

    /**
     * json поля для кастомных услуг
     */
    protected $casts = [
        'promo_video' => 'array',
        'price' => 'array',
    ];

    public static function getStatusLabels()
    {
        return [
            self::STATUS_NEW => __('fields.status_draft'),
            self::STATUS_AWAIT => __('fields.status_moderation'),
            self::STATUS_DECLINED => __('fields.status_disabled'),
            self::STATUS_MODERATED => __('fields.status_active'),
        ];
    }

    /**
     * Дефолтные значения для json полей
     * 
     * @param string $value
     * @return array
     */
    public function getPromoVideoAttribute($value)
    {
        return !$value ? [
            'link' => '',
            'description' => ''
        ] : json_decode($value);
    }

    # Relations

    public function category()
    {
        return $this->belongsTo(ServiceCategory::class);
    }

    public function tariffs()
    {
        return $this->belongsToMany(Tariffs::class, 'tariffs_to_services', 'service_id', 'tariff_id');
    }
}
