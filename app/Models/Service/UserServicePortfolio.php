<?php

namespace App\Models\Service;

use
    Spatie\Translatable\HasTranslations,
    Illuminate\Database\Eloquent\Model;

class UserServicePortfolio extends Model
{
    use HasTranslations;

    const DISK_NAME = UsersService::DISK_NAME;

    protected $table = 'users_service_portfolio';
    protected $fillable = ['service_id', 'description'];
    protected $translatable = ['description'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $casts = [
        'description' => 'array',
    ];

    public function getPhotoUrlAttribute()
    {
        return Storage::disk(self::DISK_NAME)->url($this->photos()[0]);
    }

    public function service()
    {
        return $this->belongsTo(UsersService::class, 'service_id');
    }

    public function photos()
    {
        return $this->hasMany(UserServicePortfolioPhotos::class, 'case_id');
    }
}
