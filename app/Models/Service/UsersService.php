<?php

namespace App\Models\Service;

use
    App,
    Storage,
    Carbon\Carbon,
    Illuminate\Database\Eloquent\Model,
    Illuminate\Support\Str,
    Spatie\Translatable\HasTranslations,
    App\Models\Auth\User,
    App\Traits\HasImage
;

class UsersService extends Model
{
    use HasTranslations, HasImage;

    const STATUS_NEW = 0;
    const STATUS_AWAIT = 1;
    const STATUS_MODERATED = 2;
    const STATUS_DECLINED = 3;

    const DISK_NAME = 'users_service';

    public $translatable = ['name', 'description', 'annotation', 'details', 'price', 'price_for', 'video_description'];

    protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'description',
        'annotation',
        'annotation_img',
        'details',
        'category_id',
        'performer_id',
        'price',
        'price_for',
        'slug',
        'country_id',
        'region_id',
        'city_id',
        'icon',
        'photo',
        'video_url',
        'video_description',
        'shows_count',
        'bids_count',
    ];
    protected $appends = ['iconUrl', 'countryTranslation', 'cityTranslation', 'discountPrice', 'published_at'];

    /**
     * json поля
     */
    protected $casts = [
        'name' => 'array',
        'description' => 'array',
        'annotation' => 'array',
        'details' => 'array',
        'price' => 'array',
        'price_for' => 'array',
        'video_description' => 'array',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->setSlug();
        });
        self::updating(function($model){
            $model->setSlug();
        });
    }

    public static function getStatusLabels()
    {
        return [
            self::STATUS_NEW => __('fields.status_draft'),
            self::STATUS_AWAIT => __('fields.status_moderation'),
            self::STATUS_DECLINED => __('fields.status_disabled'),
            self::STATUS_MODERATED => __('fields.status_active'),
        ];
    }

    # Attributes

    public function getIconUrlAttribute()
    {
        return Storage::disk(self::DISK_NAME)->url($this->icon);
    }

    public function getAnnotationImgUrlAttribute()
    {
        return Storage::disk(self::DISK_NAME)->url($this->annotation_img);
    }

    public function getPerformerAvatarUrlAttribute()
    {
        if ($performer = $this->performer) {
            return $performer->avatarUrl;
        }
    }

    public function getCityNameAttribute()
    {
        if ($city = $this->getCity()) {
            return $city->name;
        }
    }

    public function getCityTranslationAttribute()
    {
        if ($city = $this->getCity()) {
            return $city->translation;
        }
    }

    public function getCountryNameAttribute()
    {
        if ($country = $this->getCountry()) {
            return $country->name;
        }
    }

    public function getCountryTranslationAttribute()
    {
        if ($country = $this->getCountry()) {
            return $country->translation;
        }
    }

    public function getCountryIconAttribute()
    {
        if ($country = $this->getCountry()) {
            return [
                1 => 'russian',
                146 => 'poland',
                666 => 'uk',
            ][$country->id];
        }
    }

    public function getCountry()
    {
        if ($city = $this->getCity()) {
            return $city->country;
        }
    }

    public function getCity()
    {
        if ($performer = $this->performer) {
            return $performer->city;
        }
    }

    public function setSlug()
    {
        $this->slug = Str::slug($this->name);
    }

    public function getDiscountPriceAttribute()
    {
        if (!empty($this->discount)) {
            return (int)$this->price * (100 - $this->discount) / 100;
        }
        return $this->price;
    }

    public function getPublishedAtAttribute()
    {
        return Carbon::parse($this->created_at)->diffInDays(Carbon::now());
    }

    # Relations

    public function processes()
    {
        return $this->hasMany(UserServiceProcesses::class, 'service_id');
    }

    public function workers()
    {
        return $this
            ->hasMany(UserServiceWorkers::class, 'service_id')
            ->orderBy('id');
    }

    public function portfolio()
    {
        return $this->hasMany(UserServicePortfolio::class, 'service_id');
    }

    public function performer()
    {
        return $this->belongsTo(User::class, 'performer_id');
    }

    public function category()
    {
        return $this->belongsTo(UsersServiceCategory::class, 'category_id');
    }
}
