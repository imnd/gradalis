<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;

class UserServicePortfolioPhotos extends Model
{
    public $timestamps = false;

    protected $table = 'users_service_portfolio_photos';
    protected $fillable = ['file_name', 'case_id', 'is_main'];
    protected $guarded = ['id'];

    public function serviceCase()
    {
        return $this->belongsTo(UserServicePortfolio::class, 'service_id');
    }
}
