<?php

namespace App\Models\Service;

use Spatie\Translatable\HasTranslations,
    Illuminate\Database\Eloquent\Model,
    App\Traits\HasImage
;

class UserServiceWorkers extends Model
{
    use HasTranslations, HasImage;

    const DISK_NAME = UsersService::DISK_NAME;

    protected $table = 'users_service_workers';
    protected $fillable = ['name', 'position', 'photo', 'service_id'];
    protected $translatable = ['name', 'position'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $casts = [
        'name' => 'array',
    ];

    public function service()
    {
        return $this->belongsTo(UsersService::class, 'service_id');
    }
}
