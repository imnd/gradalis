<?php

namespace App\Models\Service;

use
    Spatie\Translatable\HasTranslations,
    Illuminate\Database\Eloquent\Model,
    App\Traits\HasImage
;

class UserServiceProcesses extends Model
{
    use HasTranslations, HasImage;

    const DISK_NAME = UsersService::DISK_NAME;

    protected $table = 'users_service_processes';
    protected $fillable = ['service_id', 'name', 'description', 'icon'];
    protected $translatable = ['name', 'description'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $casts = [
        'name' => 'array',
        'description' => 'array',
    ];

    public function service()
    {
        return $this->belongsTo(UsersService::class, 'service_id');
    }
}
