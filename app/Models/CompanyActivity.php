<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class CompanyActivity extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    protected $table = 'company_activities';
    protected $fillable = ['name'];

}
