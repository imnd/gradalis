<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Terms extends Model implements HasMedia
{
    use HasMediaTrait;
    
    public function language()
    {
        return $this->belongsTo('App\Models\Language');
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('documents');
    }
}
