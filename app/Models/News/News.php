<?php

namespace App\Models\News;

use
    Illuminate\Database\Eloquent\Model,
    Spatie\MediaLibrary\HasMedia\HasMedia,
    Spatie\MediaLibrary\HasMedia\HasMediaTrait,
    Spatie\Translatable\HasTranslations,
    App\Traits\MediaConversion;

class News extends Model implements HasMedia
{
    use
        HasTranslations,
        HasMediaTrait,
        MediaConversion;

    // В ожидании
    public const STATUS_AWAIT = 0;
    // Прошел модерацию
    public const STATUS_MODERATED = 1;
    // Одобрен
    public const STATUS_APPROVED = 2;
    // Отклонен
    public const STATUS_DECLINED = 4;

    const NEWS_COLLECTION_NAME = 'news';
    const FRANCHISE_NEWS_COLLECTION_NAME = 'franchise_news';
    const NEWS_COLLECTION_DISC_NAME = 'public';
    const FRANCHISE_NEWS_COLLECTION_DISC_NAME = 'franchise_news';

    public $timestamps = true;
    public $translatable = [
        'title',
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'vk',
        'ok',
        'fb',
        'google',
        'mail',
        'url',
        'video'
    ];
    protected $table = 'news';
    protected $fillable = [
        'title',
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'category_id',
        'vk',
        'ok',
        'fb',
        'google',
        'mail',
        'status',
        'url',
        'video',
        'owner_type',
        'owner_id',
    ];
    protected $appends = [
        'images',
    ];

    public function getImagesAttribute()
    {
        $images = [];
        foreach ($this->getMedia() as $media) {
            $images[] = $media->getUrl();
        }
        return $images;
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_AWAIT => 'В ожидании',
            self::STATUS_MODERATED => 'Прошел модерацию',
            self::STATUS_APPROVED => 'Одобрен',
            self::STATUS_DECLINED => 'Отклонен',
        ];
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('news')
            ->registerMediaConversions(function () {
                $this->addWatermark();
            });
    }

    public function newsMedia()
    {
        return $this->media()->where('collection_name', 'news');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\News\NewsCategory');
    }

    public function owner()
    {
        return $this->morphTo();
    }
}
