<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Translatable\HasTranslations;

class NewsCategory extends Model implements HasMedia
{
    use HasTranslations, HasMediaTrait;

    public $timestamps = true;
    public $translatable = [
        'title',
        'url',
    ];
    protected $table = 'news_category';
    protected $fillable = array(
        'title',
        'icon',
        'parent_id',
        'name',
        'url',
    );
    public function news()
    {
        return $this->hasMany('App\Models\News\News', 'category_id');
    }
    
    public function approved_news()
    {
        return $this->hasMany('App\Models\News\News', 'category_id', 'id')->where
        ('status', News::STATUS_APPROVED);
        
    }
    
    public function children()
    {
        return $this->hasMany($this, 'parent_id', 'id');
    }
    
    public function children_news()
    {
        return $this->hasManyThrough('App\Models\News\News', $this, 'parent_id', 'id');
    }
    
    public function app_ch_news()
    {
        return $this->hasManyThrough('App\Models\News\News', $this, 'parent_id', 'category_id', 'id', 'id')
                    ->where('status', News::STATUS_APPROVED);
    }
    
    public function parent()
    {
        return $this->belongsTo($this, 'parent_id', 'id')->where('parent_id', '=', null);
    }

    public static function franchiseCategoryId()
    {
        $franchiseCategory = self::where('name', 'franchises')->first();
        if($franchiseCategory){
            return $franchiseCategory->id;
        }
        return null;
    }

}
