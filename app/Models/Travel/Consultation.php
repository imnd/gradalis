<?php

namespace App\Models\Travel;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    protected $guarded = [];
    
    public function travel()
    {
        return $this->belongsTo('App\Models\Travel\Travel');
    }

    public function object()
    {
        return $this->morphTo();
    }

    protected $casts = [
        'date' => 'datetime'
    ];
}
