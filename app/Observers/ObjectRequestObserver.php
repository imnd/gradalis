<?php

namespace App\Observers;

use App\Models\ObjectRequest;

class ObjectRequestObserver
{
    /**
     * Handle the object request "created" event.
     *
     * @param  \App\Models\ObjectRequest  $objectRequest
     * @return void
     */
    public function created(ObjectRequest $objectRequest)
    {
        $objectRequest->object->user->notify(new \App\Notifications\NewObjectRequest($objectRequest));
    }

}
