<?php

namespace App\Observers;

use App\Models\Franchise\Franchise,
    App\Notifications\ObjectStatusChanged;

class FranchiseObserver
{
    public function saved(Franchise $franchise)
    {
        if ($franchise->wasChanged('status')) {
            $franchise->user->notify(new ObjectStatusChanged($franchise));
        }
    }
}
