<?php

namespace App\Observers;

use App\Models\Business\Business,
    App\Notifications\ObjectStatusChanged;

class BusinessObserver
{
    public function saved(Business $business)
    {
        if ($business->wasChanged('status')) {
            $business->user->notify(new ObjectStatusChanged($business));
        }
    }
}
