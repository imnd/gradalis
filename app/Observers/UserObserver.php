<?php

namespace App\Observers;

use App\Models\Auth\User;

class UserObserver
{
    public function saved(User $user)
    {
        if($user->wasChanged('balance')){
            $user->notify(new \App\Notifications\UserBalanceChanged($user->balance));
        }
    }
}
