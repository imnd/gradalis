<?php
namespace App\Helpers\Marketplace;

class Core
{
    /**
     * @param $price
     * @param null $format
     * @return string
     */
    public static function getPrice($price, $format = null)
    {
        return number_format($price, 0, '.', ' ');
    }
}
