<?php
namespace App\Helpers;

/**
 * class ArrayHelper
 * Содержит полезные функции для работы с массивами
 * 
 * @author Андрей Сердюк
 * @copyright (c) 2018 IMND
 */
class ArrayHelper
{
    public static function objectToArray($object)
    {
        $tmpArr = array();
        $object = (array)$object;
        foreach ($object as $key => $value) {
            if (is_object($value)) {
                $tmpArr[$key] = self::objectToArray($value);    
            } elseif (is_array($value)){
                $tmpArr[$key] = self::objectToArray($value);
            } else {
                $tmpArr[$key] = $value;
            }
        }
        return $tmpArr;
    }

    /**
     * Суммирует значения, извлекаемые из ассоциативного массива по ключу $key
     */
    public static function sum($array, $key)
    {
        $result = 0;
        foreach ($array as $item)
            $result += is_array($item) ? $item[$key] : $item->$key;

        return $result;
    }

    /**
     * Превращает массив ассоциативных массивов в обычный, извлекая из ассоциативных
     * массивов значения по ключу $key
     */
    public static function flatten($array, $key)
    {
        $result = array();
        if (is_array($key)) {
            $ind = array_keys($key)[0];
            $key = array_values($key)[0];
        } else
            $i = 0;

        foreach ($array as $item)
            if ($value = self::_extractValue($item, $key)) {
                $resultKey = isset($i) ? $i++ : self::_extractValue($item, $ind);
                $result[$resultKey] = $value;
            }

        return $result;
    }

    private static function _extractValue($item, $key)
    {
        return is_array($item) ? $item[$key] : $item->$key;
    }

    /**
     * @param mixed $from
     * @param mixed $to
     * @return void
     */
    public static function changeKey(&$arr, $from, $to)
    {
        $arr[$to] = $arr[$from];
        unset($arr[$from]);
    }

    /**
     * Выбирает максимальное значение поля $key массива $array
     */
    public static function max($array, $key)
    {
        $array = self::flatten($array, $key);
        return max($array);
    }

    /**
     * Выбирает минимальное значение поля $key массива $array
     */
    public static function min($array, $key)
    {
        $array = self::flatten($array, $key);
        return min($array);
    }

    public static function transpose($array)
    {
        $transposed = array();
        foreach ($array as $key1 => $params) {
            foreach ($params as $key2 => $val) {
                $transposed[$key2][$key1] = $val;
            }
        }
        return $transposed;
    }

    public static function isEmpty($array)
    {
        $isEmpty = true;
        foreach ($array as $element) {
            if (is_array($element)) {
                if (!empty(array_filter($element))) {
                    $isEmpty = $isEmpty && self::isEmpty($element);
                }
            } else {
                if (!empty($element)) {
                    return false;
                }
            }
        }
        return $isEmpty;
    }

    public static function filter($array)
    {
        foreach ($array as $ind => &$item) {
            foreach ($item as $key => &$data) {
                if (empty(array_filter($data))) {
                    unset($item[$key]);
                }
            }
            if (empty(array_filter($item))) {
                unset($item);
            }
        }
        return $array;
    }
}
