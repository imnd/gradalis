<?php
/**
 * Created by PhpStorm.
 * User: MIKHAIL
 * Date: 17.03.2019
 * Time: 18:37
 */

namespace App\Factories;


use App\Models\Business\Business;
use App\Models\Task;
use App\Services\BusinessTask;

class TaskFactory
{
    protected $target_type;
    protected $target_id;
    protected $tasker;

    public function __construct($target_type, $target_id)
    {
        $this->target_type = (int)$target_type;
        $this->target_id = (int)$target_id;
        $this->make();
    }

    protected function make()
    {
        switch ($this->target_type) {
            case Task::TARGET_TYPE_BUSINESS:
                $this->tasker = new BusinessTask(Business::find($this->target_id));
        }
    }

    public function getTasker(): \App\AbstractClasses\Task
    {
        return $this->tasker;
    }
}
