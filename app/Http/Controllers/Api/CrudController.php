<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller,
    Illuminate\Http\Request,
    Illuminate\Http\Response,
    Illuminate\Database\Eloquent\Model;

class CrudController extends Controller
{
    /**
     * Repository instance
     * @var Model $model
     */
    public $model;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json($this->model->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return response()->json($this->_findModel($id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'success' => $this->model->create($request->all())
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit($id)
    {
         return View::make('edit')
            ->with('model', $this->_findModel($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $success = $this->_findModel($id)->update($request->all());
        return response()->json(compact('success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $success = $this->_findModel($id)->delete();
        return response()->json(compact('success'));
    }

    /**
     * @param integer $id
     * @return Model
     */
    protected function _findModel($id)
    {
        if (!$model = $this->model->find($id)) {
            throw new HttpException('Нет такой модели.');
        }
        return $model;
    }
}
