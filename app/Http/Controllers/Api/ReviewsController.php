<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    public function addReview(Request $request)
    {
        $reviewData = $request->all();
        $data = json_decode($reviewData['form'], true);
        $review = Review::create($data);
    
        $files = $request->file('files');
        if ($files) {
            foreach ($files as $file) {
                $review->addMedia($file)->toMediaCollection('review');
            }
        }
    
        if ( $review ) {
            return response()->json(['status' => 'ok']);
        }
        
        return response()->json(['status' => 'error']);
    }
    
    public function showReview(Request $request)
    {
        $id = $request['id'];
        $review = Review::where([
            ['id', $id],
            ['status', 4]
        ])->first();
        
        if ($review) {
            $media = $review->getFirstMediaUrl('review', 'watermark');
            $allMedia = $review->getMedia('review')->map(function ($media) {
                return [
                    'thumb' => $media->getUrl('thumb'),
                    'full' => $media->getUrl('watermark'),
                ];
            });
            // TODO: заменить на нормальное дефолтное фото
            $review->main = $media ?: 'https://avatars.mds.yandex.net/get-pdb/25978/51b72f68-915e-4759-b9c6-b9d4246eb992/s1200';
            $review->allMedia = $allMedia;
            return response()->json($review);
        }
        
        return response()->json(['status' => 'error']);
    }
}
