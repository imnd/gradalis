<?php

namespace App\Http\Controllers\Api\Services;

use
    App\Http\Controllers\Controller,
    App\Models\Service\Tariffs
;

class ServiceController extends Controller
{
    public function getTariffs()
    {
        return Tariffs::with('services')->visible()->get();
    }
}
