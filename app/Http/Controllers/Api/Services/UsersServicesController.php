<?php

namespace App\Http\Controllers\Api\Services;

use
    App,
    Auth,
    Storage,
    Illuminate\Http\Request,
    Illuminate\Support\Facades\Validator,
    App\Helpers\ArrayHelper,

    App\Http\Controllers\Controller,
    App\Models\Service\UsersService,
    App\Models\Service\UsersServiceCategory
    /*,\Symfony\Component\DomCrawler\Crawler
    ,Illuminate\Support\Facades\Storage
    ,Illuminate\Http\File*/
;

class UsersServicesController extends Controller
{
    const RULES = [
        'name' => 'required',
        'category_id' => 'required',
        'price' => 'required',
        'description' => 'sometimes',
        'annotation' => 'sometimes',
        'details' => 'sometimes',
        'country_id' => 'numeric',
        'region_id' => 'sometimes',
        'city_id' => 'sometimes',
        'price_for' => 'sometimes',
        'video_url' => 'sometimes',
        'video_description' => 'sometimes',
    ];

    public function getList()
    {
        return UsersService::where('performer_id', Auth::id())->get();
    }

    public function getItem(int $id)
    {
        return UsersService::with(['processes', 'workers', 'portfolio'])->find($id);
    }

    public function store(Request $request)
    {
        $data = $request->validate(self::RULES);
        $data['performer_id'] = Auth::id();
        $service = UsersService::create($data);
        return [
            'status' => 'ok',
            'id' => $service->id,
            'relations' => $this->_saveAllRelations($request, $service),
        ];
    }

    public function update(Request $request, UsersService $service)
    {
        /*$data = $request->validate(self::RULES);
        $service->fill($data);
        $service->setSlug();
        $service->save();*/

        return [
            'status' => 'ok',
            'relations' => $this->_saveAllRelations($request, $service),
        ];
    }

    /**
     * @param Request $request
     * @param UsersService $service
     * @return array
     */
    private function _saveAllRelations(Request $request, UsersService $service)
    {
        return [
//            'processes' => $this->_saveRelations($request, $service, 'processes'),
            'workers' => $this->_saveRelations($request, $service, 'workers'),
            'portfolio' => $this->_saveRelations($request, $service, 'portfolio'),
        ];
    }
 
    /**
     * @param array $dataItems
     * @param UsersService $service
     * @return array
     */
    private function _saveRelations(Request $request, UsersService $service, $relationName)
    {
        if (!$dataItems = $this->_extractData($request, $relationName)) {
            return;
        }
        $ids = [];
        foreach ($dataItems as $ind => $dataItem) {
            if (isset($dataItem['id'])) {
                $relation = $service->$relationName()->where('id', $dataItem['id'])->first();
            } else {
                $relation = $service->$relationName()->create([
                    'service_id' => $service->id
                ]);
            }
            $fieldNames = array_flip($relation->getTranslatableAttributes());
            $data = array_intersect_key($dataItem, $fieldNames);
            $relation->update($data);
            $ids[$ind] = $relation->id;
        }
        $service->$relationName()
            ->where('service_id', $service->id)
            ->whereNotIn('id', $ids)
            ->delete();

        return $ids;
    }

    /**
     * @param array $dataTabs
     * @param string $key
     * @return array
     */
    private function _extractData(Request $request, $name)
    {
        $ret = array();
        $dataTabs = $request->post('tabs');
        $dataTabs = ArrayHelper::transpose($dataTabs);
        $dataTabs = $dataTabs[$name];
        if (ArrayHelper::isEmpty($dataTabs)) {
            return $ret;
        }
        $dataItems = ArrayHelper::transpose($dataTabs);
        $fieldNames = array_keys($dataItems[0][App::getLocale()]);
        foreach ($dataItems as $ind => $dataItem) {
            $ret[$ind] = array();
            foreach ($dataItem as $lang => $data) {
                foreach ($fieldNames as $fieldName) {
                    if (!isset($ret[$ind][$fieldName])) {
                        $ret[$ind][$fieldName] = [$lang => null];
                    }
                    if (isset($data[$fieldName])) {
                        if (empty($data[$fieldName]))
                            $data[$fieldName] = '';
                        $ret[$ind][$fieldName][$lang] = $data[$fieldName];
                    }
                }
            }
        }
        return ArrayHelper::filter($ret);
    }

    public function saveFile(Request $request, $fieldName, UsersService $service)
    {
        $request->validate(['file' => 'required|file|max:2048']);
        $image = $request->file('file');
        $fileName = md5(microtime()) . '.' . $image->getClientOriginalExtension();
        $filePath = Storage::disk(UsersService::DISK_NAME)->getAdapter()->getPathPrefix();
        $image->move($filePath, $fileName);
        $service->update([$fieldName => $fileName]);
        return response()->json([
            'name' => Storage::disk(UsersService::DISK_NAME)->url($fileName)
        ]);
    }

    public function saveRelationFile(
        Request $request,
        string $fieldName,
        string $relationName,
        UsersService $service,
        int $id
    )
    {
        $request->validate(['file' => 'required|file|max:2048']);
        $file = $request->file('file');
        $fileName = md5(microtime()) . '.' . $file->getClientOriginalExtension();
        $filePath = Storage::disk(UsersService::DISK_NAME)->getAdapter()->getPathPrefix();
        $file->move($filePath, $fileName);
        $relation = $service->$relationName()->get()->find($id);
        $relation->update([$fieldName => $fileName]);
        return response()->json([
            'name' => Storage::disk(UsersService::DISK_NAME)->url($fileName)
        ]);
    }

    public function saveRelationFiles(
        Request $request,
        string $fieldName,
        string $relationName,
        UsersService $service,
        int $id
    )
    {
        if (!$files = $request->file('files')) {
            return;
        }
        $validator = Validator::make($files, [
            'image_file.*' => 'required|mimes:jpg,jpeg,png,bmp|max:20000'
        ], [
            'image_file.*.required' => 'Please upload an image',
            'image_file.*.mimes' => 'Only jpeg,png and bmp images are allowed',
            'image_file.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray()
            ], 422);
        }
        $relation = $service->$relationName()->get()->find($id);
        $relation->files()->delete([
            'case_id' => $relation->id
        ]);
        foreach ($files as $file) {
            $fileName = md5(microtime()) . '.' . $file->getClientOriginalExtension();
            $filePath = Storage::disk(UsersService::DISK_NAME)->getAdapter()->getPathPrefix();
            $file->move($filePath, $fileName);
            $relation
                ->files()
                ->create([
                    'case_id' => $relation->id,
                    $fieldName => $fileName,
                ])
                ->save();
        }

        return response()->json([
            'status' => 'ok',
        ]);
    }

    public function delete(UsersService $service)
    {
        $service->delete();
        return response()->json(['status' => 'ok']);
    }

    public function getCategories()
    {
        $categories =  UsersServiceCategory::all()
            ->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'icon' => $item->translation,
                    'iconUrl' => $item->iconUrl,
                ];
            });
        return response()->json($categories);
    }

    public function getStatusLabels()
    {
        return UsersService::getStatusLabels();
    }

    /*public function handleQuillContent(Request $request)
    {
         $desc = $request->input('some_html'); // POST with html
         $domDesc = new Crawler($desc);
         $images = $domDesc->filterXPath('//img')->extract(array('src')); // extract images
         foreach ($images as $key => $value) {
             if (strpos($value, 'base64') !== false) { // leave alone not base64 images
                 $data = explode(',', $value); // split image mime and body
                 $tmpFile = tempnam('/tmp', 'items'); // create tmp file path
                 file_put_contents($tmpFile, base64_decode($data[1])); // fill temp file with image
                 $path = Storage::putFile('public/items', new File($tmpFile)); // put file to final destination
                 $desc = str_replace($value, $path, $desc); // replace src of converted file to fs path
                 unlink($tmpFile); // delete temp file
             }
         }
     }*/
}
