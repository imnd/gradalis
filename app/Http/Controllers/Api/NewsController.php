<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News\News;
use App\Models\News\NewsCategory;
use DB;
use Illuminate\Http\Request;


class NewsController extends Controller
{

    public function get(Request $request)
    {
        $query = $request->all();

        if (!empty($query['newsCategory'])) {
            $categorySlug = $query['newsCategory'];
            //$news->where('category_id', (int)$query['category_id']);
            $newsCategory = NewsCategory::query();
            foreach (config('app.locales') as $lang => $val) {
                $newsCategory->orWhere('url->' . $lang, $categorySlug);
            }
            $newsCategory = $newsCategory->firstOrFail();


            $ids = NewsCategory::where('parent_id', $newsCategory->id)
                ->get()->pluck('id');
            $ids[] = $newsCategory->id;

            $news = News::whereIn('category_id', $ids)->where('status',
                [News::STATUS_APPROVED])->with(['category']);

        } else {
            $news = News::whereIn('status', [News::STATUS_APPROVED])->with(['category']);
        }

        $response = $news->paginate(4);
        return response()->json($response);
    }

    public function getCategories()
    {
        return NewsCategory::where('parent_id', null)
            ->withCount('approved_news')
            ->withCount('app_ch_news')
            ->with([
                'children' => function ($query) {
                    $query->withCount('approved_news');
                }
            ])
            ->get()
            ->map(function ($news) {
                return [
                    'id' => $news->id,
                    'icon' => $news->icon,
                    'title' => $news->title,
                    'url' => $news->url,
                    'count' => $news->approved_news_count +
                        $news->app_ch_news_count,
                    'children' => $news->children,
                ];
            });
    }

    public function getParentCategories()
    {
        return NewsCategory::where('parent_id', null)->get()
            ->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->title,
                ];
            });
    }

    public function hpNews()
    {
        return News::where('category_id', '!=', NewsCategory::franchiseCategoryId())->where('status', News::STATUS_APPROVED)
            ->with('category')
            ->limit(6)
            ->orderBy('id', 'desc')
            ->get();
    }

}
