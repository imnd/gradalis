<?php

namespace App\Http\Controllers\Api;

use
    Illuminate\Http\Request,
    App\Http\Controllers\Controller,
    App\Models\Buyer,
    App\Models\News\News,
    App\Models\News\NewsCategory,
    App\Models\PostTypes,
    App\Models\Franchise\Franchise,
    App\Models\Franchise\FranchiseObject,
    App\Models\Franchise\FranchiseReview,
    App\Models\Franchise\AudienceTypes,
    App\Models\Franchise\CertificateTypes,
    App\Models\Franchise\CertificateDocuments,
    App\Models\Franchise\FranchiseCategory,
    App\Models\Franchise\Subcategories,
    App\Models\Franchise\Penalties,
    App\Models\Franchise\PropertyCategories,
    App\Models\Franchise\PropertyTypes,
    App\Models\Franchise\TransferCustomerBaseTypes,
    App\Models\Franchise\TrainingMaterialTypes,
    App\Models\Franchise\Equipments,
    App\Models\Franchise\OperationRestrictions,
    App\Models\Franchise\GenderTargetAudience,
    App\Models\Franchise\FamilyStatusClients,
    App\Models\Franchise\SocialStatusClient,
    App\Models\Franchise\MainAdvertisingSourcesAttractClients,
    App\Models\Franchise\ConditionEquipments,
    App\Models\Franchise\LegalStatuses,
    App\Models\Franchise\TaxSystems
;

class FranchiseController extends Controller
{
    public function reserve(Request $request)
    {
        Buyer::where('status', 0)
            ->where('target_id', $request->get('id'))
            ->where('target_type', Franchise::class)
            ->delete();

        $franchise = Franchise::find($request->get('id'));
        $buyer = Buyer::create([
            'user_id' => auth()->user()->id,
            'status'  => 0
        ]);
        $franchise->buyer()->save($buyer);
        $user = auth()->user();
        $user->balance -= 50;
        $user->save();
        return response()->json(['url' => '/profile/reserved-franchise']);
    }

    public function getReferences()
    {
        return response()->json([
            'audienceTypes' => AudienceTypes::all(),
            'certificateTypes' => CertificateTypes::all(),
            'certificateDocuments' => CertificateDocuments::all(),
            'categories' => FranchiseCategory::all(),
            'subcategories' => Subcategories::all(),
            'penalties' => Penalties::all(),
            'postTypes' => PostTypes::all(),
            'propertyCategories' => PropertyCategories::all(),
            'propertyTypes' => PropertyTypes::all(),
            'transferCustomerBaseTypes' => TransferCustomerBaseTypes::all(),
            'trainingMaterialTypes' => TrainingMaterialTypes::all(),
            'equipments' => Equipments::all(),
            'operationRestrictions' => OperationRestrictions::all(),
            'genderTargetAudiences' => GenderTargetAudience::all(),
            'clientFamilyStatuses' => FamilyStatusClients::all(),
            'clientSocialStatuses' => SocialStatusClient::all(),
            'clientMainAdvertisingSourceAttracts' => MainAdvertisingSourcesAttractClients::all(),
            'conditionEquipments' => ConditionEquipments::all(),
            'legalStatuses' => LegalStatuses::all(),
            'taxSystems' => TaxSystems::all(),
        ]);
    }

    public function getList(Request $request)
    {
        $queryData = $request->all();

        $franchises = Franchise::whereIn('status', [Franchise::STATUS_MODERATED])
            ->with(['city', 'country', 'region', 'category']);
        if (auth()->check()) {
            $franchises->with([
                'favorites' => function ($q) {
                    $q->where('user_id', auth()->user()->id);
                }
            ]);
        }
        if (!empty($queryData['country'])) {
            $franchises->whereHas('country', function ($q) use ($queryData) {
                $q->where('id', (int) $queryData['country']);
            });
        }
        if (!empty($queryData['region'])) {
            $franchises->whereHas('region', function ($q) use ($queryData) {
                $q->where('id', (int) $queryData['region']);
            });
        }
        if (!empty($queryData['city'])) {
            $franchises->whereHas('city', function ($q) use ($queryData) {
                $q->where('id', (int) $queryData['city']);
            });
        }
        if (!empty($queryData['query'])) {
            $franchises->whereRaw('LOWER(`name`) LIKE ? ', ['%'.trim(mb_strtolower($queryData['query'])).'%']);
        }
        if (isset($queryData['freeRoyalty']) && $queryData['freeRoyalty'] == "true") {
            $franchises->where('condition', '!=', Franchise::CONDITION_ROYALTY);
        }
        if (isset($queryData['freeContribution']) && $queryData['freeContribution'] == "true") {
            $franchises->where('condition', '!=', Franchise::CONDITION_LUMP_SUM);
        }
        if (!empty($queryData['category'])) {
            $franchises->where('category_id', (int) $queryData['category']);
        }
        if (!empty($queryData['price'])) {
            $price = $queryData['price'];
            $franchises->where('price', '>', (int) $price[0]);
            $franchises->where('price', '<', (int) $price[1]);
        }
        if (!empty($queryData['profit'])) {
            $profit = $queryData['profit'];
            $franchises->where('profit', '>', (int) $profit[0]);
            $franchises->where('profit', '<', (int) $profit[1]);
        }
        if (!empty($queryData['payback'])) {
            $payback = $queryData['payback'];
            $franchises->where('payback', '>', (int) $payback[0]);
            $franchises->where('payback', '<', (int) $payback[1]);
        }
        if (!empty($queryData['limit'])) {
            $paginate = $queryData['limit'];
        } else {
            $paginate = 6;
        }
        $response = $franchises->latest()->paginate($paginate);
        return response()->json($response);
    }

    public function getById(Franchise $franchise)
    {
        $media = $franchise->getMedia('franchise');
        $logo = $franchise->getMedia('logo');
        $frn = $franchise->toArray();
        $frn['b2c'] =  $franchise->b2c;
        $frn['b2c']['age_men'] = array('from'=> $frn['b2c']['age_men_from'], 'to' => $frn['b2c']['age_men_to']);
        $frn['b2c']['age_women'] = array('from'=> $frn['b2c']['age_women_from'], 'to' => $frn['b2c']['age_women_to']);
        $frn['b2c']['sex_ratio'] = array('from'=> $frn['b2c']['sex_ratio_from'], 'to' => $frn['b2c']['sex_ratio_to']);
        $frn['b2c']['average_income_target_clients'] = array('from'=> $frn['b2c']['average_income_target_clients_from'], 'to' => $frn['b2c']['average_income_target_clients_to']);


        $frn['b2b'] =  $franchise->b2b->load('contracts');
        $frn['staff'] = $franchise->staffs;

        $data['franchise'] = $frn;
        $data['packages'] = $franchise->packages;

        $data['certificates'] = $franchise->certificates;

        foreach ($franchise->objects as $object) {
            $object->getMedia('franchise_objects');
            $data['objects'][] = $object;
        }

        $data['media'] = $media;
        $data['logo'] = $logo;

        return $data;
    }

    public function getCategories()
    {
        return FranchiseCategory::with('children')
            ->where('parent_id', null)
            ->get();
    }

    public function imageRemove(Request $request, Franchise $franchise)
    {
        $franchise->deleteMedia($request->get('id'));
        return response()->json(['message' => 'Изображение удалено'], 201);
    }

    public function objectImageRemove(Request $request, FranchiseObject $franchiseObject)
    {
        $franchiseObject->deleteMedia($request->get('id'));
        return response()->json(['message' => 'Изображение удалено'], 201);
    }

    public function addObject(Request $request, $id, $objectId = null)
    {
        $data = json_decode($request->get('object'), true);
        $data['franchise_id'] = $id;
        $data['status'] = FranchiseObject::STATUS_AWAIT;
        if($objectId){
            $object = FranchiseObject::findOrFail($objectId);
            $object->fill($data)->save();
        }else{
            $object = FranchiseObject::create($data);
        }

        $this->_saveFiles($request, $object, 'files', 'franchise_objects');
        return response()->json(['status' => 'ok']);
    }

    public function addNews(Request $request, $id, $newsId = null)
    {
        $data = json_decode($request->get('news'), true);
        $data['owner_id'] = $id;
        $data['status'] = News::STATUS_AWAIT;
        $data['owner_type'] = Franchise::POLYMORPHIC_NAME;
        $data['category_id'] = NewsCategory::franchiseCategoryId();
        if ($newsId) {
            $object = News::findOrFail($newsId);
            $object->fill($data)->save();
        } else {
            $object = News::create($data);
        }
        $this->_saveFiles($request, $object, 'photos', 'franchise_news');

        return response()->json(['status' => 'ok']);
    }

    public function addReview(Request $request, $id, $reviewId = null)
    {
        $data = json_decode($request->get('review'), true);
        $data['franchise_id'] = $id;
        $data['status'] = FranchiseReview::STATUS_AWAIT;
        if ($reviewId) {
            $object = FranchiseReview::findOrFail($reviewId);
            $object->fill($data)->save();
        } else {
            $object = FranchiseReview::create($data);
        }
        $this->_saveFiles($request, $object, 'photos', 'franchise_reviews_photo');
        $this->_saveFiles($request, $object, 'materials', 'franchise_reviews_material');

        return response()->json(['status' => 'ok']);
    }

    /**
     * @param string $key
     * @param string $collectionName
     * @return void
     */
    private function _saveFiles(Request $request, $object, $key, $collectionName)
    {
        if ($files = $request->file($key)) {
            foreach ($files as $file) {
                $object
                    ->addMedia($file)
                    ->toMediaCollection($collectionName, $collectionName);
            }
        }
    }

    /**
     * @param Request $request
     * @param Franchise $franchise
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePrice(Request $request, $id)
    {
        $franchise = Franchise::findOrFail($id);
        $franchise->price = $request->post('price');
        $franchise->discount = $request->get('discount');
        $status = $franchise->save() ? 'ok' : 'fail';
        return response()->json(compact('status'));
    }

    public function sellWithBroker($id)
    {
        Franchise::findOrFail($id)
            ->update([
                'sell_with_broker' => Franchise::SELL_WITH_BROKER,
                'status' => Franchise::STATUS_AWAIT,
            ]);

        return response()->json(['status' => 'ok']);
    }
}
