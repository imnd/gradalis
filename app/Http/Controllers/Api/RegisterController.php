<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Buyer;
use App\Models\CompanyActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Nexmo\Laravel\Facade\Nexmo;
use App\Models\Auth\User;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    /*public function startVerify(Request $request)
    {
        $data = $request['form'];
        $phone = (int)$data['phone'];
        $code = rand(1111, 9999);
        $data['password'] = Hash::make('secret');
        $data['email_verified_at'] = Carbon::now();
        $data['code'] = $code;

        $user = $this->checkOrCreateUser($data);
        if ($user) {
            $message = Nexmo::message()->send(
                [
                    'to' => $phone,
                    'from' => '37477684655',
                    'text' => __('reg.sms.verify_code') . $code,
                    'type' => 'unicode'
                ]
            );
            $response = $message->getResponseData();

            if ($response['messages'][0]['status'] == 0) {
                return response()->json(['status' => 'ok']);
            }
            $errorMessage =
                'Невозможно отправить смс на номер: ' . $phone . ' Статус: '
                . $response['messages'][0]['status'];
            Log::error($errorMessage);

            return response()->json(
                [
                    'status' => 'error',
                    'msg' => __('reg.sms.verify_code_not_send'),
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'msg' => __('reg.sms.user_duplicate_error'),
            ]
        );
    }*/

    public function codeVerify(Request $request)
    {
        $data = $request['form'];
        $code = $data['code'];
        $phone = $data['phone'];

        $user = User::where([
            ['code', $code],
            ['phone', $phone]
        ])->first();

        if ($user) {
            $user->update(['code' => null]);
            return response()->json([
                'status' => 'ok',
                'url' => route('login')
            ]);
        }
        return response()->json(['status' => 'error']);
    }

    /* private function checkOrCreateUser($data)
     {
         $user = User::where('email', $data['email'])->orWhere('phone', $data['phone'])
             ->first();
         if ($user) {
             if ($user->code !== null) {
                 $user->update(['code' => $data['code']]);
                 return $user;
             }
             return false;
         }
         $data['api_token'] = substr(base64_encode(openssl_random_pseudo_bytes(60)), 0, 60);
         $user = User::create($data);
         $user->assignRole(Roles::ROLE_SELLER);

         return $user;
     }*/

    protected function validatorBuyer(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

    protected function validatorSeller(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'country_id' => ['integer'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

    protected function validatorExecutor(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'company_name' => ['string', 'max:255'],
            'soc_network' => ['string', 'max:255'],
            'country_id' => ['integer'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

    public function registerBuyer(Request $request)
    {
        $data = $request['form'];
        if (isset($data['timePurchase'][0]) && isset($data['timePurchase'][1])) {
            $data['purchase_date_from'] = date('Y-m-d H:i:s', time() +
                strtotime('+' . (int)$data['timePurchase'][0] . ' month', strtotime(time())));
            $data['purchase_date_to'] = date('Y-m-d H:i:s', time() +
                strtotime('+' . (int)$data['timePurchase'][1] . ' month', strtotime(time())));
        }
        $this->validatorBuyer($data)->validate();
        $this->createUser($data, Roles::ROLE_BUYER);
    }

    public function registerSeller(Request $request)
    {
        $data = $request['form'];
        $this->validatorSeller($data)->validate();
        $this->createUser($data, Roles::ROLE_SELLER);
    }

    public function registerExecutor(Request $request)
    {
        $data = $request['form'];
        $this->validatorExecutor($data)->validate();
        $this->createUser($data, Roles::ROLE_EXECUTIVE);

    }

    protected function createUser($data, $role)
    {
        $code = rand(1111, 9999);
        //$data['email_verified_at'] = Carbon::now();
        $data['code'] = $code;
        $data['api_token'] = substr(base64_encode(openssl_random_pseudo_bytes(60)), 0, 60);
        $data['secret_key'] = str_replace('/', '_', substr(base64_encode(openssl_random_pseudo_bytes(60)), 0, 60));
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        $user->assignRole($role);
        \Session::flash('success_message_title',
            app('translator')->getFromJson('profile.products.welcome_notify_title'));
        \Session::flash('success_message',
            str_replace('*firstName*', $user->first_name,
                app('translator')->getFromJson('reg.successfullyRegistered')));

        \Mail::send('emails.email-confirmation', compact('user'), function ($m) use ($user) {
            $m->from('info@gradalis.com', 'Gradalis Group');
            $m->to($user->email)->subject(app('translator')->getFromJson('reg.email.emailSubject'));
        });
        $this->guard()->login($user);
        return 1;
        // return $this->sendSms($user);
    }

    protected function sendSms($user)
    {
        if ($user) {
            $message = Nexmo::message()->send(
                [
                    'to' => (int)$user->phone,
                    'from' => '88005556699',
                    'text' => __('reg.sms.verify_code') . $user->code,
                ]
            );
            $response = $message->getResponseData();

            if ($response['messages'][0]['status'] == 0) {
                return response()->json(['status' => 'ok']);
            }
            $errorMessage =
                'Невозможно отправить смс на номер: ' . $user->phone . ' Статус: '
                . $response['messages'][0]['status'];
            Log::error($errorMessage);

            return response()->json(
                [
                    'status' => 'error',
                    'msg' => __('reg.sms.verify_code_not_send'),
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'msg' => __('reg.sms.user_duplicate_error'),
            ]
        );
    }

    public function companyActivities()
    {
        return CompanyActivity::all()
            ->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                ];
            });
    }
}
