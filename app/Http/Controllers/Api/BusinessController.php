<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Business\Business;
use App\Models\Business\BusinessCategory;
use App\Models\Buyer;
use Illuminate\Http\Request;


class BusinessController extends Controller
{
    public function reserve(Request $request)
    {
        Buyer::where('status', 0)
            ->where('target_id', $request->get('id'))
            ->where('target_type', Business::class)
            ->delete();

        $business = Business::find($request->get('id'));
        $business->status = Business::STATUS_RESERVED;
        $business->save();
        $buyer = Buyer::create([
            'user_id' => auth()->user()->id,
            'status'  => 0
        ]);
        $business->buyer()->save($buyer);
        $user = auth()->user();
        $user->balance -= 300;
        $user->save();
        return response()->json(['url' => '/profile/reserved-business']);
    }

    public function get(Request $request)
    {
        $query = $request->all();

        $businesses = Business::whereIn('status', [Business::STATUS_SOLD, Business::STATUS_MODERATED])->with(['city', 'country', 'region', 'category']);
        if (auth()->check()) {
            $businesses->with(['favorites' => function ($q) {
                $q->where('user_id', auth()->user()->id);
            }]);
        }
        if (!empty($query['country'])) {
            $businesses->whereHas('country', function ($q) use ($query) {
                $q->where('id', (int)$query['country']);
            });
        }
        if (!empty($query['region'])) {
            $businesses->whereHas('region', function ($q) use ($query) {
                $q->where('id', (int)$query['region']);
            });
        }
        if (!empty($query['city'])) {
            $businesses->whereHas('city', function ($q) use ($query) {
                $q->where('id', (int)$query['city']);
            });
        }

        if (!empty($query['query'])) {
            $businesses->whereRaw('LOWER(`name`) LIKE ? ', ['%' . trim(mb_strtolower($query['query'])) . '%']);
        }

        if (isset($query['saled']) && $query['saled'] == "false") {
            $businesses->where('status', Business::STATUS_MODERATED);
        }

        if (!empty($query['category'])) {
            $businesses->where('category_id', (int)$query['category']);
        }
        if (!empty($query['price'])) {
            $price = $query['price'];
            $businesses->where('price', '>', (int)$price[0]);
            $businesses->where('price', '<', (int)$price[1]);
        }
        if (!empty($query['profit'])) {
            $profit = $query['profit'];
            $businesses->where('profit', '>', (int)$profit[0]);
            $businesses->where('profit', '<', (int)$profit[1]);
        }
        if (!empty($query['payback'])) {
            $payback = $query['payback'];
            $businesses->where('payback', '>', (int)$payback[0]);
            $businesses->where('payback', '<', (int)$payback[1]);
        }


        $responce = $businesses->latest()->paginate(6);
        return response()->json($responce);
    }

    public function getRanges()
    {
        $data['maxPrice'] = Business::max('price');

        $data['maxProfit'] = Business::max('profit');

        $data['maxPayback'] = Business::max('payback');

        return response()->json($data);
    }

    public function getById(Business $business)
    {
        //TODO Оптимизировать
        $business->getMedia('business');
        $bs = $business->toArray();
        $data['options'] = $business->options;
        $bs['price'] = $business->price;
        $bs['profitability'] = $business->profitability;
        $bs['revenue'] = $business->revenue;
        $data['business'] = $bs;
        return $data;
    }

    public function getCategories()
    {
        return BusinessCategory::with('children')->where('parent_id', null)->get();
    }

    public function imageRemove(Request $request, Business $business)
    {
        $business->deleteMedia($request->get('id'));
        return response(['message' => 'Изображение удалено'], 201);
    }
}
