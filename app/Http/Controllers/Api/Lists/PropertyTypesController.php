<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\PropertyTypes,
    App\Http\Controllers\Api\CrudController;

class PropertyTypesController extends CrudController
{
    /**
     * Создание нового экземпляра контроллера.
     *
     * @param PropertyTypes $model
     * @return void
     */
    public function __construct(PropertyTypes $model)
    {
        $this->model = $model;
    }
}
