<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\TrainingMaterialTypes,
    App\Http\Controllers\Api\CrudController;

class TrainingMaterialTypesController extends CrudController
{
    /**
     * Создание нового экземпляра контроллера.
     *
     * @param TrainingMaterialTypes $model
     * @return void
     */
    public function __construct(TrainingMaterialTypes $model)
    {
        $this->model = $model;
    }
}
