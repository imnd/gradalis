<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\FamilyStatusClients;

class FamilyStatusClientController extends CrudController
{
    public function __construct(FamilyStatusClients $model)
    {
        $this->model = $model;
    }
}
