<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\Subcategories,
    App\Http\Controllers\Api\CrudController;

class SubcategoriesController extends CrudController
{
    /**
     * @param Subcategories $model
     * @return void
     */
    public function __construct(Subcategories $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json($this->model->all());
    }
}
