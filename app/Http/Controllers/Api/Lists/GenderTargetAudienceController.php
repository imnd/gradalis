<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\GenderTargetAudience;

class GenderTargetAudienceController extends CrudController
{
    public function __construct(GenderTargetAudience $model)
    {
        $this->model = $model;
    }
}
