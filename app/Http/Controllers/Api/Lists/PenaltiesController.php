<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\Penalties,
    App\Http\Controllers\Api\CrudController;

class PenaltiesController extends CrudController
{
    /**
     * Создание нового экземпляра контроллера.
     *
     * @param Penalties $model
     * @return void
     */
    public function __construct(Penalties $model)
    {
        $this->model = $model;
    }
}
