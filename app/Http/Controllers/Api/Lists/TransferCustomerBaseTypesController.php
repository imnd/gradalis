<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\TransferCustomerBaseTypes,
    App\Http\Controllers\Api\CrudController;

class TransferCustomerBaseTypesController extends CrudController
{
    /**
     * Создание нового экземпляра контроллера.
     *
     * @param TransferCustomerBaseTypes $model
     * @return void
     */
    public function __construct(TransferCustomerBaseTypes $model)
    {
        $this->model = $model;
    }
}
