<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\OperationRestrictions,
    App\Http\Controllers\Api\CrudController;

class OperationRestrictionsController extends CrudController
{
    public function __construct(OperationRestrictions $model)
    {
        $this->model = $model;
    }
}
