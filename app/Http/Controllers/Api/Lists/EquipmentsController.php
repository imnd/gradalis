<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\Equipments;

class EquipmentsController extends CrudController
{
    /**
     * Создание нового экземпляра контроллера.
     *
     * @param Equipments $model
     * @return void
     */
    public function __construct(Equipments $model)
    {
        $this->model = $model;
    }
}
