<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\AltAddressBusines;

class AltAddressBusinesController extends CrudController
{
    /**
     * @param AltAddressBusines $model
     * @return void
     */
    public function __construct(AltAddressBusines $model)
    {
        $this->model = $model;
    }
}
