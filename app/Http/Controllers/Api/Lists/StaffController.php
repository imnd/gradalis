<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\Staff,
    App\Http\Controllers\Api\CrudController;

class StaffController extends CrudController
{
    /**
     * @param Staff $model
     * @return void
     */
    public function __construct(Staff $model)
    {
        $this->model = $model;
    }
}
