<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\B2BContractsList;

class B2BContractsListController extends CrudController
{
    /**
     * @param B2BContractsList $model
     * @return void
     */
    public function __construct(B2BContractsList $model)
    {
        $this->model = $model;
    }
}
