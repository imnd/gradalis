<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\B2B;

class B2BController extends CrudController
{
    /**
     * @param B2B $model
     * @return void
     */
    public function __construct(B2B $model)
    {
        $this->model = $model;
    }
}
