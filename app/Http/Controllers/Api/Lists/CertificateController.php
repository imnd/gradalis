<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\Certificate;

class CertificateController extends CrudController
{
    /**
     * @param Certificate $model
     * @return void
     */
    public function __construct(Certificate $model)
    {
        $this->model = $model;
    }
}
