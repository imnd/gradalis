<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\CertificateTypes;

class CertificateTypesController extends CrudController
{
    /**
     * Создание нового экземпляра контроллера.
     *
     * @param CertificateTypes $model
     * @return void
     */
    public function __construct(CertificateTypes $model)
    {
        $this->model = $model;
    }
}
