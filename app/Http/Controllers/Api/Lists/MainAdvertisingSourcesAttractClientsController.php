<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\MainAdvertisingSourcesAttractClients,
    App\Http\Controllers\Api\CrudController;

class MainAdvertisingSourcesAttractClientsController extends CrudController
{
    public function __construct(MainAdvertisingSourcesAttractClients $model)
    {
        $this->model = $model;
    }
}
