<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\AudienceTypes;

class AudienceTypesController extends CrudController
{
    /**
     * @param AudienceTypes $model
     * @return void
     */
    public function __construct(AudienceTypes $model)
    {
        $this->model = $model;
    }
}
