<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\CertificateDocuments;

class CertificateDocumentsController extends CrudController
{
    /**
     * Создание нового экземпляра контроллера.
     *
     * @param CertificateDocuments $model
     * @return void
     */
    public function __construct(CertificateDocuments $model)
    {
        $this->model = $model;
    }
}
