<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\PostTypes,
    App\Http\Controllers\Api\CrudController;

class PostTypesController extends CrudController
{
    /**
     * @param PostTypes $model
     * @return void
     */
    public function __construct(PostTypes $model)
    {
        $this->model = $model;
    }
}
