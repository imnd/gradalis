<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\PropertyCategories,
    App\Http\Controllers\Api\CrudController;

class PropertyCategoriesController extends CrudController
{
    /**
     * Создание нового экземпляра контроллера.
     *
     * @param PropertyCategories $model
     * @return void
     */
    public function __construct(PropertyCategories $model)
    {
        $this->model = $model;
    }
}
