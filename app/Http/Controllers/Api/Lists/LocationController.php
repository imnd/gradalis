<?php

namespace App\Http\Controllers\Api\Lists;

use
    App\Http\Controllers\Controller,
    App\Models\Country,
    App\Models\City,
    App\Models\Region
;

class LocationController extends Controller
{
    public function getCountries()
    {
        return Country::all()
            ->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->translation,
                ];
            });
    }

    public function getRegions($country)
    {
        return Region::where('country_id', $country)
            ->get()
            ->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->translation,
                ];
            });
    }

    public function getCities($region)
    {
        return City::where('region_id', $region)
            ->get()
            ->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->translation,
                ];
            });
    }
}
