<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\FranchiseCategory;

class CategoriesController extends CrudController
{
    /**
     * @param FranchiseCategory $model
     * @return void
     */
    public function __construct(FranchiseCategory $model)
    {
        $this->model = $model;
    }
}
