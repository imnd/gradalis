<?php

namespace App\Http\Controllers\Api\Lists;

use App\Models\Franchise\SocialStatusClient,
    App\Http\Controllers\Api\CrudController;

class SocialStatusClientController extends CrudController
{
    public function __construct(SocialStatusClient $model)
    {
        $this->model = $model;
    }
}
