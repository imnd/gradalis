<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Chat\Message;
use App\Models\Referral\InvitationCounter;
use App\Models\Referral\Partner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function partnerStatusChange(Request $request)
    {
        $partner = Partner::find($request->get('partner_id'));
        $partner->status = [
            'unblock' => 1,
            'block' => 2,
        ][$request->get('status')] ?? null;
        $partner->save();
        return response()->json(['status' => 'ok']);
    }

    public function getSummary(Request $request)
    {
        $partners = $request->get('partners');

        $invitations = InvitationCounter::whereHas('invitation', function ($q) use ($partners) {
            $q->whereIn('partner_id', $partners);
        })->get();

        $data['views'] = 0;
        $data['clicks'] = 0;
        $data['regs'] = 0;
        foreach ($invitations as $invitation) {
            $data['views'] += $invitation->count;
            $data['clicks']++;
            if ($invitation->status == InvitationCounter::STATUS_REGISTERED) {
                $data['regs']++;
            }
        }
        $data['hits'] = $data['views'];

        return response()->json($data);
    }

    public function getAddressees()
    {
        if ($user = Auth::user()) {
            $addressees = $user->getMessageAddressees();
            return response()->json($addressees);
        }
    }

    public function getPartners(Request $request)
    {
        $blocked = $request->get('blocked');
        $await = $request->get('await');
        $approved = $request->get('approved');
        $query = Partner::where('apa_id', auth()->user()->id)
            ->with('user')
            ->with('user.country')
            ->moderatedMessagesCount();

        $query->where(function ($q) use ($blocked, $approved, $await) {
            if ($blocked) {
                $q->orWhere('status', Partner::STATUS_BLOCKED);
            }
            if ($await) {
                $q->orWhere('status', Partner::STATUS_AWAIT);
            }
            if ($approved) {
                $q->orWhere('status', Partner::STATUS_MODERATED);
            }
        });

        $partners = $query->get();
        $partners->map(function ($partner) {
            $data = Partner::partnerSummary($partner->id)->first();
            $partner->hits = $data->hits;
            $partner->approved_leads = $data->approved_leads;
            $partner->approved_commission = $data->approved_commission;
            return $partner;
        });
        return response()->json($partners);
    }

    public function getPartnerMessages(Partner $partner)
    {
        $data['messages'] = Message::where('from', $partner->user_id)
            ->where('status', Message::STATUS_MODERATION)
            ->get();

        return response()->json($data);
    }

    public function changeMessageStatus(Request $request)
    {
        $message = Message::find($request->get('id'));
        $status = Message::STATUS_ACTIVE;
        if ($request->get('status') == 'block') {
            $status = Message::STATUS_BLOCKED;
        }
        if (Partner::where('user_id', $message->from)->first()->apa_id == auth()->user()->id) {
            $message->status = $status;
            $message->save();
            return response()->json(['status' => 'ok']);
        }
        return response('401 Unauthorized', 401)->json(['status' => 'unauthorized']);
    }

    public function getPartner(Partner $partner)
    {
        $data['user'] = $partner->user;
        $data['partner'] = $partner;
        return response()->json($data);
    }

    public function updatePartner(Partner $partner)
    {
        $data['user'] = $partner->user;
        $data['partner'] = $partner;
        return response()->json($data);
    }

    public function getChartData(Request $request)
    {
        $req = $request->get('data');

        $invitations = InvitationCounter::whereHas('invitation', function ($q) use ($req) {
            $q->whereIn('partner_id', $req['partners']);
        });
        if (isset($req['from']) && isset($req['to'])) {
            $invitations->where('created_at', '>=', Carbon::parse($req['from']));
            $invitations->where('created_at', '<=', Carbon::parse($req['to']));
        } else {
            $invitations->where('created_at', '>=', Carbon::now()->subMonth());
        }
        $dates = $invitations->get();
        if ($req['dateType'] == 'week') {
            $labels = $dates->groupBy(function ($invitation) {
                return $invitation->created_at->startOfWeek()->format('d.m.Y').'-'.$invitation->created_at->endOfWeek()->format('d.m.Y');
            });

        } else {
            $labels = $dates->groupBy(function ($invitation) {
                return $invitation->created_at->format('d-m-Y');
            });
        }

        $data['result'] = $labels->transform(function ($invitation, $date) {
            $views = $clicks = $registered = 0;
            foreach ($invitation as $item) {
                $views += $item->count;
                $clicks++;
                if ($item->status == InvitationCounter::STATUS_REGISTERED) {
                    $registered++;
                }
            }
            return compact('date', 'views', 'clicks', 'registered');
        });
        return response()->json($data);
    }


}
