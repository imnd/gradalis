<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUnreadNotifications()
    {
        return Auth::user()->getUnreadNotifications();
    }

    public function delete($id)
    {
        $notification = \Auth::user()->notifications()->where('id', $id)->first();
        if($notification) {
            $notification->delete();
        }
        return response()->json(['status' => 'ok']);
    }


}
