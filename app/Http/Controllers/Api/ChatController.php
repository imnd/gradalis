<?php

namespace App\Http\Controllers\Api;

use
    Auth,
    Illuminate\Http\Request,
    Notification,
    App\Http\Controllers\Controller,
    App\Models\Business\Business,
    App\Models\Franchise\Franchise,
    App\Models\Roles,
    App\Models\Chat\Message,
    App\Models\Chat\Dialog,
    App\Models\Brokers,
    App\Notifications\NewMessage
;

class ChatController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        // TODO Или выдать permission нужным ролям?
        $user->canModerateMessages = $user->canModerateMessages();

        $roles = $user->roles->pluck('name')->all();
        $user = $user->toArray();
        $user['roles'] = $roles;
        return view('chat', compact('user'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDialogs(Request $request)
    {
        // TODO Проверка на возможность смотреть диалоги, пока отдаются все существующие
        $dialogs = Dialog::with('user', 'object')
            ->whereHas('users', function($query) {
                $query->where('users.id', Auth::id());
            })
            ->when(request('search'), function ($query, $search) {
                return $query->where('theme', 'LIKE', "%$search%");
            })
            ->get()
            ->map(function ($item) {
                // TODO get notifications from db
                $item->notifications = 0;
                return $item;
            });

        return $dialogs;
    }

    /**
     * @return \App\Models\Chat\Dialog
     */
    public function getDialog(Request $request, Dialog $dialog)
    {
        return $dialog->load([
            'messages' => function($query) use ($request) {
                if ($search = $request->get('search', null)) {
                    $query->where('text', 'LIKE', "%$search%");
                }
                $query->with(['from', 'media']);
                // Если пользователь может модерировать сообщения, или это его сообшения
                if (!Auth::user()->canModerateMessages()) {
                    $query->where('status', '!=', 0)->orWhere('from', Auth::user()->id);
                }
            }
        ]);
    }

    /**
     * @return \App\Models\Chat\Message
     */
    public function getUserDialog(Request $request)
    {
        $authUserId = Auth::id();
        $userId = $request->get('userId', null);
        $query = Message::with('dialog');
        if ($message = $query
            ->where('from', $authUserId)
            ->where('to', $userId)
            ->first()
        ) {
            return response()->json($message->dialog);
        }
        if ($message = $query
            ->where('to', $authUserId)
            ->where('from', $userId)
            ->first()
        ) {
            return response()->json($message->dialog);
        }
    }

    public function newDialog(Request $request)
    {
        $user = Auth::user();

        $request->validate([
            'theme' => 'required',
            'text' => 'required',
            'object_id' => 'required',
            'type' => 'required'
        ]);

        switch ($type = $request->get('type')) {
            case 'broker':
                if (
                       !$brokerId = $request->get('to')
                    or !$broker = Brokers::find($brokerId)
                ) {
                    $type = 'support';
                } else {
                    $type =
                        $user->hasRole(Roles::ROLE_BUYER) ?
                            'buyer' : $user->hasRole(Roles::ROLE_SELLER) ?
                                'seller' : null;
                }
                // TODO обработать случаи, если топик стартер не входит в группы выше
                break;
            case 'support':
            case 'manager':
                break;
            default:
                return response('Wrong dialog type', 422);
        }

        $objectType = $request->get('object_type') == 'business' ? Business::class : Franchise::POLYMORPHIC_NAME;

        $dialog = Dialog::where('type', $type)
            ->where('user_id', $user->id)
            ->where('object_id', $request->get('objectId'))
            ->where('object_type', $objectType)
            ->first();
        if (!$dialog) {
            $dialog = Dialog::create([
                'theme' => $request->get('theme'),
                'user_id' => $user->id,
                'type' => $type,
                'object_id' => $request->get('objectId'),
            ]);
        }

        $user->dialogs()->save($dialog);

        // TODO прикрепить нужных юзеров к диалогу
        // TODO выслать уведомления
        Message::create([
            'dialog_id' => $dialog->id,
            'from' => $user->id,
            'to' => $request->get('to'),
            // TODO если это пишет не юзер, у которого включена премодерация сообщений
            'status' => Message::STATUS_ACTIVE,
            'text' => $request->get('text')
        ]);

        return $dialog;
    }

    /**
     * @return \App\Models\Chat\Message
     */
    public function newMessage(Request $request)
    {
        // TODO Привязывать юзера к диалогу?
        $request->validate([
            'dialogId' => 'required',
            'text' => 'required_without:file',
            'file' => 'file|required_if:text,==""'
        ]);
        $dialogId = $request->input('dialogId');
        $message = Message::create([
            'dialog_id' => $dialogId,
            'from' => Auth::id(),
            'text' => $request->input('text', ''),
            'status' => Auth::user()->hasAnyRole([Roles::ROLE_BROKER_SELLER, Roles::ROLE_BROKER_BUYER]) ? 0 : 1
        ]);

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $message->addMedia($request->file('file'))->toMediaCollection();
        }
        // Уведомляем юзеров, которые прикреплены к диалогу кроме отправляющего
        $dialog = Dialog::where('id',  $dialogId)->firstOrFail();

        $users = $dialog
            ->users()
            ->where('dialog_user.user_id', '!=', Auth::id())
            ->get();

        Notification::send($users, new NewMessage($message));

        return $message->load('from');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getMessages(Dialog $dialog)
    {
        $messages = Message::where('dialog__id', $dialog->id);

        if (!Auth::user()->canModerateMessages()) {
            $messages->where('status', '!=', 0)->orWhere('from', Auth::user()->id);
        }

         /*$messages = $messages->map(function ($item){
             foreach ($item->media as $k => $media){
                 $item->media[$k]->url =  [
                     'origin' => $media->getUrl(),
                     'thumb' =>$media->getUrl('thumb')
                 ];
             }
             return $item;
         });*/

        return $messages;
    }

    /**
     * @return mixed
     */
    public function acceptMessage(Message $message, Request $request)
    {
        if (!$request->user()->canModerateMessages()) {
            return response('401 Unauthorized', 401);
        }
        $message->status = 1;
        $message->save();
        // TODO Броадкаст ивент на смену статуса / новое сообщение
        return $message;
    }

    /**
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    public function deleteMessage(Message $message, Request $request)
    {
        $request->validate([
            'delete_reason' => 'required'
        ]);

        if (!Auth::user()->canModerateMessages()) {
            return response('401 Unauthorized', 401);
        }
        $message->delete_reason = $request->get('delete_reason');
        $message->save();
        // TODO Броадкаст ивент на удаление
        return response('OK');
    }
}
