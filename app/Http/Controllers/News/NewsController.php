<?php

namespace App\Http\Controllers\News;

use App\Models\News\News;
use App\Http\Controllers\Controller;
use App\Models\News\NewsCategory;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NewsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(NewsCategory $newsCategory, News $news)
    {
        if ($news->category_id != $newsCategory->id) {
            throw new NotFoundHttpException();
        }
        return view('news.show', compact('news'));
    }

    /**
     * Новости для главной.
     *
     * @return \App\Models\News\News
     */
    public static function latestNews()
    {
        return News::where('category_id', 1)
                    ->latest()
                    ->take(10)
                    ->get();
    }

    public function index(NewsCategory $newsCategory = null)
    {
        if (!$newsCategory) {
            return view('news.index', compact('newsCategory'));
        }
        return view('news.index', ['category' => $newsCategory, 'newsCategory' => $newsCategory->url]);

    }
}
