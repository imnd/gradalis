<?php

namespace App\Http\Controllers\Services;

use
    Illuminate\Http\Request,
    App\Http\Controllers\Controller,
    App\Models\Service\UsersService,
    App\Models\Service\UsersServiceCategory
;

class UsersServicesController extends Controller
{
    public function index(Request $request)
    {
        $query = UsersService::where('status', UsersService::STATUS_MODERATED)
            ->when(request('category'), function ($q, $category) {
                return $q->where('category_id', $category);
            })
            ->when(request('country'), function ($q, $country) {
                return $q->where('country_id', $country);
            })
            ->when(request('region'), function ($q, $region) {
                return $q->where('region_id', $region);
            })
            ->when(request('city'), function ($q, $city) {
                return $q->where('city_id', $city);
            })
            ->when(request('term'), function ($q, $term) {
                return $q->where('name', 'LIKE', "%$term%");
            })
        ;
        return view('users-services.index', [
            'categories' => UsersServiceCategory::all(),
            'categoryId' => $request->category,
            'country' => $request->country,
            'region' => $request->region,
            'city' => $request->city,
            'term' => $request->term,
            'services' => $query->paginate(9)
        ]);
    }

    public function show($id)
    {
        $service = UsersService
            ::with(['processes', 'workers', 'portfolio'])
            ->findOrFail($id);

        $service->increment('shows_count');
        return view('users-services.show', compact('service'));
    }

    public function processes($id)
    {
        return $this->_relation($id, 'processes');
    }

    public function portfolio($id)
    {
        return $this->_relation($id, 'portfolio');
    }

    public function workers($id)
    {
        return $this->_relation($id, 'workers');
    }

    private function _relation($id, $relation)
    {
        $service = UsersService
            ::with($relation)
            ->findOrFail($id);

        return view("users-services.$relation", [
            'service' => $service,
            $relation => $service->$relation,
        ]);
    }

    public function details($id)
    {
        return view('users-services.details', [
            'service' => UsersService::findOrFail($id),
        ]);
    }
}
