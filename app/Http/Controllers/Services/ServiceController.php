<?php

namespace App\Http\Controllers\Services;

use
    Illuminate\Http\Request,
    App\Models\Service\ServiceCategory,
    App\Models\Service\Service
;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->category) {
            $services = Service::where('category_id', (int)$request->category)->get();
        } else {
            $services = Service::all();
        }
        
        return view('services.index', [
            'categories' => ServiceCategory::all(),
            'categoryId' => $request->category,
            'services' => $services
        ]);
    }

    public function list()
    {
        return Service::with('category')->get();
    }
}
