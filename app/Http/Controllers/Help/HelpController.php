<?php

namespace App\Http\Controllers\Help;

use App\Models\Help\Help;
use App\Models\Help\HelpCategory;
use App\Models\Help\HelpSection;
use App\Http\Controllers\Controller;

class HelpController extends Controller
{

    public function index(HelpSection $helpSection = null, HelpCategory $helpCategory = null, Help $help = null)
    {
        $sections = HelpSection::with('categories.approvedHelps')->get();
        //Первая категория выбрана при открытии страницы помощи
        $select = $parent  = $currentLevel = null;
        if ($help) {
            $select = $help->id;
            $parent = $helpCategory->id;
            $currentLevel = $help;
        } elseif ($helpCategory) {
            $select = $helpCategory->id;
            $currentLevel = $helpCategory;
        } elseif ($helpSection) {
            $currentLevel = $helpSection;
        }

        return view('help')->with(compact('sections', 'currentLevel', 'select', 'parent', 'helpSection', 'helpCategory',
            'help'));
    }

}
