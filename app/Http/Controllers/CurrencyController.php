<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CurrencyController extends Controller
{
    public function switchCurrency($currency)
    {
        $currencies = Config::get('currency.currencies');
        if (array_key_exists($currency, Config::get('currency.currencies'))) {
            Session::put('currency', $currencies[$currency]);
        }
        return Redirect::back();
    }
}
