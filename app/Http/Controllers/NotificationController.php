<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class NotificationController extends Controller
{
    public function markAsRead()
    {
        Auth::user()->unreadNotifications->markAsRead();

        return ['message' => __('notifications.marked_as_read'), 200];
    }

    public function getUnread()
    {
        return Auth::user()->unreadNotifications;
    }
}
