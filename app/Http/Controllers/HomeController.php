<?php

namespace App\Http\Controllers;

use App\Http\Controllers\News\NewsController;
use App\Models\Business\Business;
use App\Models\Franchise\Franchise;
use App;
use App\Models\News\News;
use App\Models\News\NewsCategory;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
