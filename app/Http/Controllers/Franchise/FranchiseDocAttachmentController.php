<?php

namespace App\Http\Controllers\Franchise;

use App\Models\Franchise\DocAttachment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FranchiseDocAttachmentController extends Controller
{
    public function store(Request $request, DocAttachment $attachment)
    {
        $this->validate($request, [
            'franchise_id' => 'required',
            'name' => 'required|min:6',
            'file' => 'file|required|mimes:jpeg,bmp,png,jpg,doc,docx,pdf'
        ]);

        $franchiseId = $request->get('franchise_id');
        if (!Storage::disk('franchise_documents')->exists($franchiseId . '/')) {
            Storage::disk('franchise_documents')->makeDirectory($franchiseId . '/');
        }
        
        $fileName = $request->get('name') . '-' . time() . '.' . $request->file('file')->getClientOriginalExtension();

        $file = $request
            ->file('file')
            ->storeAs($franchiseId, $fileName, 'franchise_documents');

        $store = $attachment->create([
            'franchise_id' => $request->get('franchise_id'),
            'name' => $request->get('name'),
            'file' => $file,
        ]);

        return response([
            'message' => trans('franchises.create.documents.success_added' ),
            'doc'=> $store
        ], 200);
    }

}


