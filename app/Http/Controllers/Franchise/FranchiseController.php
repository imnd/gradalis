<?php

namespace App\Http\Controllers\Franchise;

use
    Log,
    Illuminate\Http\Request,
    App\Http\Controllers\Controller,
    App\Models\Franchise\Franchise,
    App\Models\Franchise\FranchiseObject,
    App\Models\Franchise\FranchisePackage,
    App\Models\Franchise\B2BContractsList,
    App\Http\Requests\FranchiseStoreRequest,
    App\Models\Franchise\Certificates
;

class FranchiseController extends Controller
{
    public function show($id)
    {
        $franchise = Franchise::
              with('broker')
            ->with('certificates')
            ->findOrFail($id);

        if (sizeof($franchise->packages)) {
            foreach ($franchise->packages as $index => $package) {
                $franchise->packages[$index]->include = array_map('trim', explode("\n", $package->include) );
            }
        }
        $franchise->increment('show_count');
        $latestNews = $franchise->getLatestNews();
        $objects = $franchise->getLatestObjects();
        $latestReviews = $franchise->getLatestReviews();
        return view('franchise.show', compact('franchise', 'latestNews', 'objects', 'latestReviews'));
    }

    public function store(Request $request)
    {
        $data = json_decode($request->all()['object'], true);
        $data['user_id'] = auth()->user()->id;
        $data['status'] = Franchise::STATUS_AWAIT;
        $franchise = Franchise::create($data);
        $this->_saveRelated($data, $franchise, $request);
        return response()->json(['status' => 'ok']);
    }

    public function edit(Franchise $franchise)
    {
        return view('franchise.edit', compact('franchise'));
    }

    public function update(Request $request, $id)
    {
        $data = json_decode($request->all()['object'], true);
        $franchise = Franchise::find($id);
        // Если у видео изменена ссылка то соответсвено видео помечаем как на модерации
        if ($data['video_exists'] && ($franchise->video_url != $data['video_url'])) {
            $data['video_status'] = Franchise::VIDEO_ON_MODERATION;
        }
        $franchise->update($data);
        $this->_saveRelated($data, $franchise, $request);
        return response()->json(['status' => 'ok']);
    }

    /**
     * @param Franchise $franchise
     * @param array $data
     * @param Request $request
     * @return void
     */
    private function _saveRelated(array $data, Franchise $franchise, Request $request)
    {
        $this->_saveB2c($franchise, $data);
        $this->_saveB2b($franchise, $data);
        $this->_savePackages($franchise, $data);
        // Обекты отныне не добавляются в форме. Шаг с обектами убран из формы
        //$this->_saveObjects($franchise, $data, $request);
        $this->_saveStaff($franchise, $data);
        // Надо разобратся с таблицей документов сертификатов
        $this->_saveCertificates($franchise, $data);
        $this->_saveFiles($franchise, $request);
    }

    private function _saveB2c(Franchise $franchise, $data)
    {
        if (empty($data['b2c'])) {
            return;
        }
        $data = $data['b2c'];
        foreach (['age_men', 'age_women', 'sex_ratio', 'average_income_target_clients'] as $key) {
            $data[$key . '_from'] = $data[$key]['from'];
            $data[$key . '_to'] = $data[$key]['to'];
            unset($data[$key]);
        }
        $object = $franchise->b2c()->updateOrCreate([
            'franchise_id' => $franchise->id
        ]);
        $object->fill($data);
        $object->save();
    }

    private function _saveB2b(Franchise $franchise, $data)
    {
        if (empty($data['b2b'])) {
            return;
        }
        $data = $data['b2b'];
        $b2b = $franchise->b2b()->updateOrCreate([
            'franchise_id' => $franchise->id
        ]);
        $b2b->fill($data);
        $b2b->save();

        if (!$data['have_existing_contracts']) {
            $contracts = $franchise->b2b->contracts;
            if (sizeof($contracts))
                foreach ($contracts as $contract) {
                    $contract->delete();
                }
        } else {
            if (!empty($data['contracts'])) {
                $exists = [];
                foreach ($data['contracts'] as $contract) {
                    if (empty($contract['id']))
                        $b2b_contract = B2BContractsList::firstOrCreate([
                            'franchise_b2b_id' => $b2b->id,
                            'name' => $contract['name'],
                            'date' => $contract['date']
                        ]);
                    else
                        $b2b_contract = B2BContractsList::updateOrCreate([
                            'id' => $contract['id'],
                            'franchise_b2b_id' => $b2b->id
                        ]);
                    $exists[] = $b2b_contract->id;
                    $b2b_contract->fill($contract);
                    $b2b_contract->save();
                }
                // Удаляем контракты которые были удалены в форме на фронте
                if (sizeof($exists))
                    B2BContractsList::where('franchise_b2b_id', '=', $b2b->id)->whereNotIn('id', $exists)->delete();

            }
        }
    }

    /**
     * @param Franchise $franchise
     * @param $data
     */
    private function _savePackages(Franchise $franchise, $data): void
    {
        if (empty($data['packages'])) {
            return;
        }

        // Если нет пакетов то удаляем все пакеты франшизы если они были ранее это для случай если польшователь редактирует франшизу!
        if (!$data['have_packages']) {
            $packages = $franchise->packages;
            if (sizeof($packages))
                foreach ($packages as $package) {
                    $package->delete();
                }
        } else {
            $exists = [];
            foreach ($data['packages'] as $item) {
                if (empty($item['id']))
                    $package = FranchisePackage::firstOrCreate([
                        'name' => $item['name'],
                        'franchise_id' => $franchise->id
                    ]);
                else
                    $package = FranchisePackage::updateOrCreate([
                        'id' => $item['id'],
                        'franchise_id' => $franchise->id
                    ]);

                $exists[] = $package->id;
                $package->fill($item);
                $package->save();
            }
            // Удаляем пакеты которые были удалены в форме на фронте
            if (sizeof($exists))
                FranchisePackage::where('franchise_id', '=', $franchise->id)->whereNotIn('id', $exists)->delete();
        }
    }

    /**
     * @param Franchise $franchise
     * @param $objects
     * @param Request $request
     */
    private function _saveObjects(Franchise $franchise, $data, Request $request): void
    {
        if (empty($data['objects'])) {
            return;
        }
        $files = $request->file('objects_photos');
        foreach ($data['objects'] as $item) {
            if (empty($item['id']))
                $object = FranchiseObject::firstOrCreate([
                    'title' => $item['title'],
                    'franchise_id' => $franchise->id
                ]);
            else
                $object = FranchiseObject::updateOrCreate([
                    'id' => $item['id'],
                    'franchise_id' => $franchise->id
                ]);

            $object->fill($item);
            $object->save();
            if (!empty($files[$item['idx']])) {
                foreach ($files[$item['idx']] as $file) {
                    $object
                        ->addMedia($file)
                        ->toMediaCollection('franchise_objects', 'franchise_objects');
                }
            }
        }
        Log::debug('objects saved');
    }

    private function _saveCertificates(Franchise $franchise, $data)
    {
        if (empty($data['certificates'])) {
            return;
        }
        // Если нет сертификатов то удаляем все сертификаты франшизы если они были ранее это для случай если польшователь редактирует франшизу!
        if (!$data['have_certificates']) {
            $certificates = $franchise->certificates;
            if (sizeof($certificates))
                foreach ($certificates as $certificate) {
                    $certificate->delete();
                }
        } else {
            $exists = [];
            foreach ($data['certificates'] as $item) {
                if (empty($item['id']))
                    $certificate = Certificates::firstOrCreate([
                        'franchise_id' => $franchise->id,
                        'certificate_document_id' => $item['certificate_document_id'],
                        'certificate_type_id' => $item['certificate_type_id'],
                        'license_period' => $item['license_period'],
                    ]);
                else
                    $certificate = Certificates::updateOrCreate([
                        'id' => $item['id'],
                        'franchise_id' => $franchise->id
                    ]);
                $exists[] = $certificate->id;
                $certificate->fill($item);
                $certificate->save();
            }
            // Удаляем сертификаты которые были удалены в форме на фронте
            if (sizeof($exists))
                Certificates::where('franchise_id', '=', $franchise->id)->whereNotIn('id', $exists)->delete();

        }
        Log::debug('certificates saved');
    }

    private function _saveStaff(Franchise $franchise, $data)
    {
        if (empty($data['staff'])) {
            return;
        }
        foreach ($data['staff'] as $item) {
            if (empty($item['id']))
                $staff = $franchise->staffs()->firstOrCreate([
                    'tax_amount_per_month' => $item['tax_amount_per_month'],
                    'monthly_wages' => $item['post_id'],
                    'post_id' => $item['post_id'],
                    'franchise_id' => $franchise->id
                ]);
            else
                $staff = $franchise->staffs()->updateOrCreate([
                    'id' => $item['id'],
                    'post_id' => $item['post_id'],
                    'franchise_id' => $franchise->id
                ]);

            $staff->fill($item);
            $staff->save();
        }
        Log::debug('staff saved');
    }

    private function _saveFiles(Franchise $franchise, Request $request)
    {
        $cover = json_decode($request->all()['cover'], true);
        if ($files = $request->file('files')) {
            foreach ($files as $index => $file) {
                $is_cover = $cover['set'] && $cover['new'] && ($cover['id'] == $index);
                $franchise
                    ->addMedia($file)
                    ->withCustomProperties(['cover' => $is_cover])
                    ->toMediaCollection('franchise', 'franchise');
            }
        }
        if ($logo = $request->file('logo')) {
            $franchise->addMedia($logo)->toMediaCollection('logo', 'franchise');
        }

        // Обнвляем главную картинку в случай сохранение франшизы или же устанавливаем по умолчанию
        if ($cover['set'] && !$cover['new']) {
            $franchise = Franchise::find($franchise->id);
            $franchise->getMedia('franchise')->each(function ($media, $key) use ($cover) {
                $media->setCustomProperty('cover',$media->id == $cover['id'] );
                $media->save();
            });
        }

        Log::debug('images saved');
    }

    public function index()
    {
        $currency = session()->get('currency');

        $currency = empty($currency['val']) ? 'EUR' : $currency['val'];
        $priceRanges = config('currency.priceRanges');

        $tempRanges = [];

        $i = 1;

        foreach ($priceRanges[$currency] as $priceRange) {
            $tempRanges['add' . $i++] = $priceRange;
        }

        $priceRanges = json_encode($tempRanges);

        return view('franchise.index', compact('priceRanges'));
    }

}
