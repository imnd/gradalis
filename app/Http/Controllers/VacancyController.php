<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VacancyResponse;
use App\Models\Vacancy;

class VacancyController extends Controller
{
    public function list()
    {
        $vacancies = Vacancy::with('city.region.country')->paginate(4);
    
        return view('vacancy.index', compact('vacancies'));
    }

    public function show(Vacancy $vacancy)
    {
        return view('vacancy.show', compact('vacancy'));
    }

    public function createResponse(Request $request)
    {
        $data = $request->validate([
            "name" => "required",
            "vacancy_id" => "required",
            "phone" => "required",
            "email" => "required",
            "cv_link" => "required",
            "text" => "required",
        ]);

        VacancyResponse::create($data);

        return ['message' => __('vacancy.response_send')];
    }
}
