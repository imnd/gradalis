<?php

namespace App\Http\Controllers;

use
    Illuminate\Http\Request,
    Illuminate\Support\Facades\Auth,
    Illuminate\Support\Facades\Hash,
    Illuminate\Support\Facades\Validator,
    Illuminate\Support\Facades\Storage,
    App\Models\Business\Business,
    App\Models\Auth\User,
    App\Models\Buyer,
    App\Models\City,
    App\Models\Country,
    App\Models\Franchise\Franchise,
    App\Models\Language,
    App\Models\ObjectRequest,
    App\Models\PaymentTransaction,
    App\Models\Service\OrderedService,
    App\Models\Terms,
    App\Models\Franchise\FranchiseObject,
    App\Models\Franchise\FranchiseReview,
    Carbon\Carbon,
    App\Models\News\News
;

class ProfileController extends Controller
{
    /**
     * Обновление настроек профиля
     */
    public function update(Request $request)
    {
        $data = $request->validate([
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email'      => 'email|unique:users,email,' . Auth::id(),
            'phone'      => 'required|string',
            'city_id'    => 'sometimes',
            'subscribes' => 'sometimes'
        ]);

        Auth::user()->fill($data)->save();

        return response(['message' => 'Профиль обновлен'], 200);
    }

    public function uploadAvatar(Request $request)
    {
        $request->validate([
            'file' => 'required|file|image|max:2048'
        ]);
        $image = $request->file('file');
        $fileName = md5(microtime()) . '.' . $image->getClientOriginalExtension();
        $filePath = Storage::disk(User::DISK_NAME)
            ->getAdapter()
            ->getPathPrefix();
        $image->move($filePath, $fileName);
        Auth::user()->avatar = $fileName;
        Auth::user()->save();

        return response([
            'message' => 'Аватар добавлен',
            'avatar' => Storage::disk(User::DISK_NAME)->url($fileName),
        ], 201);
    }

    /**
     * Смена пароля.
     * Проверка старого пароля, проверка нового пароля на совпадение со старым.
     */
    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password'          => 'required',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
        ]);

        $validator->after(function ($validator) use ($request) {
            if (Hash::check($request->get('password'), Auth::user()->password)) {
                $validator->errors()->add('password', __('validation.new_password'));
            }

            if (!Hash::check($request->get('old_password'), Auth::user()->password)) {
                $validator->errors()->add('old_password', __('validation.old_password'));
            }
        });

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        //TODO Эвент на смену пароля

        Auth::user()->password = Hash::make($request->get('password'));
        Auth::user()->save();

        return response(['message' => 'Новый пароль установлен'], 200);

    }

    /**
     * Список городов для селекта
     */
    public function getCities(Request $request)
    {
        $cities = City::query();
        $country_id = $request->get('country_id', null);

        if ($country_id) {
            return Country::find($country_id)->cities;
        }

        return $cities->get();
    }

    public function getReservedBusinesses()
    {
        $ids = Buyer::where('user_id', auth()->user()->id)->where('target_type',
            Buyer::TYPE_BUSINESS)->get()->pluck('target_id')->toArray();
        $businesses = Business::whereIn('id', $ids)->with([
            'city', 'city.country', 'category', 'viewRequest', 'docRequest'
        ])->withCount([
            'viewRequest',
            'docRequest',
        ])->get()->map(function ($item) {
            $item->favorites_count = $item->favoritesCount;
            $item->status_labels = $item->getStatuses();
            $item->type = 'franchise';
            $item->image = $item->getFirstMedia('business')->getUrl('card');
            $item->docs_count = count($item->getMedia('docs'));
            $item->docs = $item->getMedia('docs');
            return $item;
        });
        return response()->json($businesses);
    }

    public function getReservedFranchises()
    {
        $ids = Buyer::where('user_id', auth()->user()->id)->where('target_type',
            Buyer::TYPE_FRANCHISE)->get()->pluck('target_id')->toArray();
        $franchises = Franchise::whereIn('id', $ids)->with([
            'city', 'city.country', 'category', 'viewRequest', 'docRequest'
        ])->withCount([
            'viewRequest',
            'docRequest',
        ])->get()->map(function ($item) {
            $item->favorites_count = $item->favoritesCount;
            $item->status_labels = $item->getStatuses();
            $item->type = 'franchise';
            $item->image = $item->getFirstMedia('franchise')->getUrl('card');
            $item->docs_count = count($item->getMedia('docs'));
            $item->docs = $item->getMedia('docs');
            return $item;
        });
        return response()->json($franchises);
    }

    public function getBusinesses()
    {
        $business = Business::where('user_id', Auth::id())
            ->with([
            'city', 'city.country', 'category', 'viewRequest', 'docRequest'
        ])
            ->withCount([
                'viewRequest',
                'docRequest',
            ])
            ->get()
            ->map(function ($item) {
                $item->favorites_count = $item->favoritesCount;
                $item->status_labels = $item->getStatuses();
                $item->type = 'business';
                $item->image = $item->getFirstMedia('business')->getUrl('card');
                $item->docs_count = count($item->getMedia('docs'));
                $item->docs = $item->getMedia('docs');
                return $item;
            });
        return response()->json($business);
    }

    public function getFranchises()
    {
        $franchises = Franchise::
        where('user_id', Auth::id())
            ->with([
                'city',
                'city.country',
                'category',
                'viewRequest',
                'docRequest',
                'docs',
                'broker',
            ])
            ->withCount([
                'viewRequest',
                'docRequest',
                'docNewRequest',
                'reviews',
                'objects',
                'news',
                'docs',
                'approvedNews',
                'approvedObjects',
                'approvedReviews',
                'docs',
            ])
            ->latest()
            ->get()
            ->map(function ($franchise) {
                $franchise->favorites_count = $franchise->favoritesCount;
                $franchise->status_labels = $franchise->getStatuses();
                $franchise->type = 'franchise';
                $franchise->discountPrice = $franchise->getDiscountPriceAttribute();
                if ($image = $franchise->getFirstMedia('franchise')) {
                    $franchise->image = $image->getUrl('card');
                }
                return $franchise;
            });

        return response()->json($franchises);
    }

    /**
     * Список избранных объектов
     */
    public function getFavorites()
    {
        return Auth::user()
            ->favorites()
            ->with('favoriteable.category')
            ->get();
    }

    public function toggleFavoriteBusiness(Business $object)
    {
        $object->toggleFavorite();

        return ['message' => $object->isFavorited() ? 'Бизнес добавлен в избранное' : 'Бизнес удален из избранного'];
    }

    public function toggleFavoriteFranchise(Franchise $object)
    {
        $object->toggleFavorite();

        return ['message' => $object->isFavorited() ? 'Франшиза добавлена в избранное' : 'Франшиза удалена из избранного'];
    }

    public function getPurchasedServices()
    {
        return OrderedService::where('user_id', Auth::id())->with('service')->get();
    }

    public function getPurchasedService(OrderedService $service)
    {
        return $service;
    }

    /**
     * Список транзакций на странице баланса
     */
    public function getPaymentTransactions()
    {
        return PaymentTransaction::where('user_id', Auth::id())->orderBy('created_at')->get();
    }

    public function getObjectRequests($type)
    {
        switch ($type) {
            case 'view':
                $type_id = ObjectRequest::TYPE_VIEW;
                $relationName = 'viewRequest';
                break;
            case 'doc':
                $type_id = ObjectRequest::TYPE_DOC;
                $relationName = 'docRequest';
                break;
        }
        $business = Business::where('user_id', Auth::id())->with([
            $relationName . '.user.city',
            $relationName . '.object'
        ])->get()->toArray();
        $franchise = Franchise::where('user_id', Auth::id())->with([
            $relationName . '.user.city',
            $relationName . '.object'
        ])->get()->toArray();
        $view_requests = collect(array_merge($business, $franchise))->map(function ($item) {
            return $item['view_request'];
        })->reject(function ($item) {
            return empty($item);
        });

        $view_requests_mapped = [];
        foreach ($view_requests->values()->all() as $array) {
            $view_requests_mapped = array_merge($view_requests_mapped, $array);
        }

        $view_requests_mapped = array_filter($view_requests_mapped, function ($item) use ($type_id) {
            return $item['type'] == $type_id;
        });

        return array_values($view_requests_mapped);
    }

    public function getBuyerObjectRequests($type)
    {
        $relationName = $type == 'view' ? 'viewRequest' : 'docRequest';
        $user = User::where('id', Auth::id())->with([
            $relationName . '.user.city',
            $relationName . '.object.acceptedDocs'
        ])->first();
        return $user->$relationName;
    }

    public function getFranchiseRequests($type, $id)
    {
        $relationName = $type == 'view' ? 'viewRequest' : 'docRequest';
        $franchise = Franchise::where('user_id', Auth::id())
            ->where('id', $id)
            ->has($relationName)
            ->with([
                $relationName . '.user.city',
                $relationName . '.object',
            ])->withCount('docs')->first();
        return [
            'items' => $franchise ? $franchise->$relationName : [],
            'docs_count' => $franchise->docs_count
        ];
    }

    public function setObjectRequestStatus(ObjectRequest $viewRequest, $status)
    {
        $viewRequest->status = $status;
        $viewRequest->save();

        //TODO translate message
        return response(['message' => 'Статус запроса изменен'], 200);
    }

    public function getObjects()
    {
        $business = Business::where('user_id', Auth::id())->with([
            'city.country',
            'viewRequest',
            'docRequest',
        ])->withCount([
            'viewRequest',
            'docRequest',
        ])->get()->map(function ($item) {
            $item->favorites_count = $item->favoritesCount;
            $item->status_labels = $item->getStatuses();
            $item->type = 'business';
            return $item;
        });

        $franchise = Franchise::where('user_id', Auth::id())->with([
            'city.country',
            'viewRequest',
            'docRequest'
        ])->withCount([
            'viewRequest',
            'docRequest',
        ])->get()->map(function ($item) {
            $item->favorites_count = $item->favoritesCount;
            $item->status_labels = $item->getStatuses();
            $item->type = 'franchise';
            return $item;
        });

        return $business->concat($franchise);
    }

    public function setObjectStatus($type, $id, $status)
    {
        $modelNames = [
            'franchise' => '\App\Models\Franchise\Franchise',
            'business' => '\App\Models\Business\Business',
        ];
        $modelName = $modelNames[$type];
        if (!$object = $modelName::where([['id', $id], ['user_id', Auth::id()]])->first()) {
            return response(['message' => 'Object not found'], 404);
        }

        $object->status = $status;
        $object->save();

        return response(['message' => 'Статус объекта изменен'], 200);
    }

    public function rejectObjectViewRequest(ObjectRequest $viewRequest, Request $request)
    {
        $data = $request->validate([
            'reject_reason' => 'required'
        ]);
        if ($viewRequest->object->user_id != Auth::id()) {
            return response(['message' => 'Object not found'], 404);
        }
        $viewRequest->status = ObjectRequest::STATUS_REJECTED;
        $viewRequest->reject_reason = $data['reject_reason'];

        $viewRequest->save();

        return response(['message' => 'Статус объекта изменен'], 200);
    }


    public function acceptObjectViewRequest(ObjectRequest $viewRequest)
    {
        if ($viewRequest->object->user_id != Auth::id()) {
            return response(['message' => 'Object not found'], 404);
        }
        $viewRequest->status = ObjectRequest::STATUS_ACCEPTED;
        $viewRequest->save();

        return response(['message' => 'Статус объекта изменен'], 200);
    }

    public function requestShowDocs(ObjectRequest $viewRequest)
    {

        $docs = [];
        if (($viewRequest->status != ObjectRequest::STATUS_ACCEPTED) || $viewRequest->user_id != Auth::id()) {
            return response(['message' => app('translator')->getFromJson('reg.successfullyVerified')], 404);
        }
        foreach ($viewRequest->object->acceptedDocs as $doc) {
            $docs[] = [
                'name' => $doc->name,
                'path' => Storage::disk('franchise_documents')->url($doc->file),
            ];
        }
        return $docs;
    }


    public function getTermsOfUse()
    {
        $lang = Language::whereLang(app()->getLocale())->firstOrFail();

        $term = Terms::where('language_id', $lang->id)->firstOrFail();

        foreach ($term->media as $file) {
            $file->url = $file->getUrl();
        }
        return $term;
    }

    public function removeFranchiseDocument(Request $request, Franchise $franchise)
    {
        $document = $franchise->docs()->find($request->get('id'));
        if ($document) {
            $file = $document->file;
            $document->delete();
            Storage::disk('franchise_documents')->delete($file);
            return response(['message' => trans('profile.franchises.documents.success_deleted')], 200);
        }
        return response(['message' => trans('profile.franchises.documents.not_found')], 404);

    }

    public function getFranchiseeReviews($id)
    {
        $franchise = Franchise::findOrFail($id);
        $data = $franchise->reviews;
        $items = [];
        $statuses = FranchiseReview::getStatuses();

        foreach ($data as $item) {
            $items[] = [
                'id' => $item->id,
                'date' => date('d.m.Y', strtotime($item->created_at)),
                'title' => $item->name,
                'status' => $statuses[$item->status] ?? '',
                'statusCode' => $item->status
            ];
        }
        $columns = ['add_date', 'review_text', 'status', 'actions'];
        $columnClasses = ['is-3', 'is-5', 'is-2', 'is-2'];
        $header = 'reviews';
        return response()->json(compact('items', 'columns', 'columnClasses', 'header'));
    }

    public function getFranchiseeObjects($id)
    {
        $franchise = Franchise::findOrFail($id);
        $data = $franchise->objects;
        $items = [];
        $statuses = FranchiseObject::getStatuses();
        foreach ($data as $item) {
            $items[] = [
                'id' => $item->id,
                'date' => date('d.m.Y', strtotime($item->created_at)),
                'title' => $item->title,
                'status' => isset($statuses[$item->status]) ? $statuses[$item->status] : '',
                'statusCode' => $item->status
            ];
        }
        $columns = ['add_date', 'object_name', 'status', 'actions'];
        $columnClasses = ['is-3', 'is-5', 'is-2', 'is-2'];
        $header = 'objects';
        return response()->json(compact('items', 'columns', 'columnClasses', 'header'));
    }

    public function getFranchiseeNews($id)
    {
        $franchise = Franchise::findOrFail($id);
        $data = $franchise->news;
        $items = [];
        $statuses = News::getStatuses();
        foreach ($data as $item) {
            $items[] = [
                'id' => $item->id,
                'date' => date('d.m.Y', strtotime($item->created_at)),
                'title' => $item->title,
                'status' => isset($statuses[$item->status]) ? $statuses[$item->status] : '',
                'statusCode' => $item->status
            ];
        }
        $columns = ['add_date', 'news_title', 'status', 'actions'];
        $columnClasses = ['is-3', 'is-5', 'is-2', 'is-2'];
        $header = 'news';
        return response()->json(compact('items', 'columns', 'columnClasses', 'header'));
    }

    public function deleteReviewsItem($id)
    {
        $model = FranchiseReview::findOrFail($id);
        $status = 'error';
        if ($model->franchise && $model->franchise->user_id == Auth::id()) {
            $model->delete();
            $status = 'ok';
        }
        return response()->json(['status' => $status]);
    }

    public function deleteObjectsItem($id)
    {
        $model = FranchiseObject::findOrFail($id);
        $status = 'error';
        if ($model->franchise && $model->franchise->user_id == Auth::id()) {
            $model->delete();
            $status = 'ok';
        }
        return response()->json(['status' => $status]);
    }

    public function deleteNewsItem($id)
    {
        $model = News::findOrFail($id);
        $status = 'error';
        if ($model->owner && $model->owner->user_id == Auth::id()) {
            $model->delete();
            $status = 'ok';
        }
        return response()->json(['status' => $status]);
    }

    public function reviewDetails($id)
    {
        $model = FranchiseReview::findOrFail($id);
        if ($model->franchise && $model->franchise->user_id == Auth::id()) {
            $data = [
                'id' => $model->id,
                'name' => $model->getTranslations('name'),
                'company' => $model->company,
                'email' => $model->email,
                'position' => $model->position,
                'description' => $model->getTranslations('name'),
                'video_url' => $model->video_url,
            ];
            return response()->json([
                'formData' => $data,
                'methodName' => 'showModalAddReview',
                'label' => 'franchises.add_review.edit_title'
            ]);
        }
        return response()->json(['status' => 'error']);
    }

    public function objectDetails($id)
    {
        $model = FranchiseObject::findOrFail($id);
        if ($model->franchise && $model->franchise->user_id == Auth::id()) {
            $data = [
                'id' => $model->id,
                'title' => $model->getTranslations('title'),
                'phone' => $model->phone,
                'email' => $model->email,
                'address' => $model->address,
                'description' => $model->getTranslations('description'),
                'lng' => $model->lng,
                'lat' => $model->lat,
            ];
            return response()->json([
                'formData' => $data,
                'methodName' => 'showModalAddObject',
                'label' => 'franchises.add_object.edit_title'
            ]);
        }
        return response()->json(['status' => 'error']);
    }

    public function newsDetails($id)
    {
        $model = News::findOrFail($id);
        if ($model->owner && $model->owner->user_id == Auth::id()) {
            $data = [
                'id' => $model->id,
                'title' => $model->getTranslations('title'),
                'description' => $model->getTranslations('description'),
            ];
            return response()->json([
                'formData' => $data,
                'methodName' => 'showModalAddNews',
                'label' => 'franchises.add_news.edit_title'
            ]);
        }
        return response()->json(['status' => 'error']);
    }


    public function confirmEmail($key)
    {
        $user = User::where('secret_key', $key)->first();
        if (!$user) {
            throw new \HttpException();
        }
        $user->email_verified_at = Carbon::now();
        $user->verified = 1;
        $user->save();

        \Session::flash('success_message', app('translator')->getFromJson('reg.successfullyVerified'));
        \Session::flash('success_message_title', app('translator')->getFromJson('reg.thankYou'));

        return redirect('/profile');

    }

    public function createRequestDoc(Request $request)
    {
        $data = $request->validate([
            'object_id' => 'required',
            'object_type' => 'required',
            'buyer_comment' => 'required'
        ]);

        switch ($data['object_type']) {
            case 'business':
                $data['object_type'] = 'App\Models\Business\Business';
                break;

            case 'franchise':
                $data['object_type'] = Franchise::POLYMORPHIC_NAME;
                break;

            default:
                return response(['message' => 'wrong object type'], 400);
                break;
        }
        $data['user_id'] = Auth::id();
        $data['type'] = ObjectRequest::TYPE_DOC;

        if (!Auth::user()->requestedDoc($data['object_id'])) {
            ObjectRequest::create($data);
        }
        return response()->json(['status' => 'ok']);
    }
}
