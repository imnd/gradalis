<?php

namespace App\Http\Controllers;

use Auth,
    Session,
    Route,
    Illuminate\Support\Facades\Storage,
    App\Models\Referral\Partner
;

class CoreController extends Controller
{
    public const MODIFIED_ITEMS_FILE = 'js/modified_items_md5.txt';

    public function globalVariables()
    {
        // Текущий язык
        $lang = Session::get('applocale') ? Session::get('applocale') : app()->getLocale();
        $cacheFile = "/js/global_variables_$lang.js";
        $gzCacheFile = "/js/global_variables_$lang.js.gz";

        // Если есть закешированный файл то просто редиректим на него иначе создаем его а потом уже редиректим
        if (Storage::disk('public-web')->exists($cacheFile)) {
            header("Location: $cacheFile", true);
            header("Connection: close", true);
            // Обрываем связь с клиентом но продолжаем работать в фоне чтобы пересоздать кэш в случай если есть изменения в файлах сущностей
            session_write_close();
            if (function_exists('fastcgi_finish_request')) {
                fastcgi_finish_request();
            }
        }
        // Берем закешированное состояние сущностей
        $cacheModifiedItems = $this->collectModifiedItems('cache');
        // Берем текущее состояние сущностей
        $currentModifiedItems = $this->collectModifiedItems();

        // Делаем сверку, если есть разница, значит в сущностях есть изменения и потому надо пересозадть кэш и сохранить его, а далее уже соответсвено выдать этот сохраненный кэш
        $createCache =
               !sizeof($cacheModifiedItems)
            || !isset($cacheModifiedItems['lang'][$lang])
            || $cacheModifiedItems['lang'][$lang] != $currentModifiedItems['lang'][$lang]
            || $cacheModifiedItems['locale'] != $currentModifiedItems['locale']
            || $cacheModifiedItems['routes'] != $currentModifiedItems['routes']
            || $cacheModifiedItems['currency_rates'] != $currentModifiedItems['currency_rates']
        ;

        // Если надо создать кэш то создаем его
        if ($createCache /*= true*/) {
            // Создаём кэщ локали и языка
            $cacheContent = '';
            $files = glob(resource_path('lang/'.$lang.'/*.php'));
            $strings = [];

            foreach ($files as $file) {
                $name = basename($file, '.php');
                $strings[$name] = require $file;
            }

            $data = [
                'locales' => config('translatable.locales'),
                'indexLocale' => $lang
            ];
            $cacheContent .= 'window.i18n = ' . json_encode($strings) . ';';
            $cacheContent .= 'window.locales = ' . json_encode($data) . ';';

            // Создаем кэш для роутов
            $cacheContent .= 'window.Laravel = ' . json_encode([
                'csrfToken' => csrf_token(),
                'baseUrl' => url('/'),
                'routes' => collect(Route::getRoutes())->mapWithKeys(function ($route) {
                    return [$route->getName() => $route->uri()];
                })
            ]).';';

            $cacheContent .= 'window.rates = '.json_encode(config('currency.rates')).';';

            // Сохраняем в кэш
            $cacheModifiedItems['lang'][$lang] = $currentModifiedItems['lang'][$lang];
            $cacheModifiedItems['locale'] = $currentModifiedItems['locale'];
            $cacheModifiedItems['routes'] = $currentModifiedItems['routes'];
            $cacheModifiedItems['currency_rates'] = $currentModifiedItems['currency_rates'];
            Storage::disk('public-web')->put($cacheFile, $cacheContent);
            Storage::disk('public-web')->put(self::MODIFIED_ITEMS_FILE, json_encode($cacheModifiedItems));
            // Создаем сжатую версию на лету. Не надо забыть добавить gzip_static on; в настройках nginx чтобы nginx выдавал сжатую версию файла в место полноразмерного файла
            Storage::disk('public-web')->put($gzCacheFile, gzencode($cacheContent,9));
        }

        header('Location: ' . $cacheFile);
        exit();
    }

    public function collectModifiedItems($source = 'current')
    {
        $modifiedItems = array();

        // Если сбор информации из кэша
        if ($source == 'cache') {
            // Файл где будем хранить md5 для каждой сущности которого мы хотим закешировать
            if (Storage::disk('public-web')->exists(self::MODIFIED_ITEMS_FILE)) {
                $modifiedItems = json_decode(Storage::disk('public-web')->get(self::MODIFIED_ITEMS_FILE),true);
                if (is_null($modifiedItems))
                    $modifiedItems = array();
            }

            return $modifiedItems;
        }

        //Собираем информацию об измененых файлах в языках
        $lang = Session::get('applocale') ? Session::get('applocale') : app()->getLocale();
        $modifiedItems['lang'] = array();
        $modifiedItems['lang'][$lang] = '';

        $files = glob(resource_path("lang/$lang/*.php"));

        $tmp = '';
        foreach ($files as $file)
            $tmp .= filemtime($file); //filesize($file);

        $modifiedItems['lang'][$lang] = md5($tmp);

        // Собираем информацию об локале
        $locales = implode(',', config('translatable.locales'));
        $modifiedItems['locale'] = md5($lang . $locales);

        // Собираем информацию об роутах
        $modifiedItems['routes'] = md5(filemtime(base_path() . '/routes/web.php'));
        // Собираем информацию об курсах
        $modifiedItems['currency_rates'] = md5(filemtime(base_path() . '/config/currency.php'));

        return $modifiedItems;
    }

    public function lang()
    {
        // $strings = Cache::rememberForever('lang.js', function () {
        $lang = config('app.locale');

        $files = glob(resource_path('lang/'.$lang.'/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name = basename($file, '.php');
            $strings[$name] = require $file;
        }

        $locales = array_map(function ($value) {
            return __($value);
        }, config('translatable.locales'));

        $languages = [];
        foreach ($locales as $lang => $locale) {
            $languages[$lang] = $locale;
            /*if ($lang == app()->getLocale()) {
                $languages[$lang] = $locale;
            } elseif (auth()->check() && auth()->user()->hasPermissionTo("translate-{$lang}")) {
                $languages[$lang] = $locale;
            }*/
        }
        $data = [
            'locales'     => $languages,
            'indexLocale' => app()->getLocale()
        ];

        header('Content-Type: text/javascript');
        echo('window.i18n = '.json_encode($strings).';');
        echo('window.locales = '.json_encode($data).';');
        exit();
    }

    public function laravelRoutes()
    {
        header('Content-Type: text/javascript');
        echo('window.Laravel = ' . json_encode([
            'csrfToken' => csrf_token(),
            'baseUrl'   => url('/'),
            'routes'    => collect(Route::getRoutes())->mapWithKeys(function ($route) {
                return [$route->getName() => $route->uri()];
            })
        ]) . ';');
        exit();
    }

    public function user()
    {
        if (Auth::check()) {
            $user = Auth::user()->load(['roles']);
            $user->canModerateMessages = $user->canModerateMessages();
            $user->permissions = $user->getPermissionsViaRoles();
            $user->notifications = $user->unreadNotifications;
            $partner = Partner::where('user_id', $user->id)->first();
        } else {
            $user = null;
            $partner = null;
        }
        header('Content-Type: text/javascript');
        echo('window.currency = ' . json_encode(session()->get('currency')) . ';');
        echo('window.user = ' . json_encode($user) . ';');
        echo('window.partner = ' . json_encode($partner) . ';');
        echo('window.Laravel.apiToken = "' . ($user->api_token ?? '') . '";');
        exit();
    }
}
