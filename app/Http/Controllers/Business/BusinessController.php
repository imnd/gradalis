<?php

namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\Business\Business;
use App\Models\Task;
use App\Services\BusinessTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class BusinessController extends Controller
{
    public function show($id)
    {
        $business = Business::findOrFail($id);
        $business->increment('show_count');
        return view('business.show', compact('business'));
    }

    public function create()
    {
        return view('business.create');
    }

    public function store(Request $request)
    {
        $businessData = json_decode($request->get('business'), true);
        $businessData['user_id'] = auth()->user()->id;

        $business = Business::create($businessData);

        $files = $request->file('files');
        if ($files) {
            foreach ($files as $file) {
                $business->addMedia($file)->toMediaCollection('business', 'business');
            }
        }
        $business_task = new BusinessTask();
        $business_task->approveTask($business->id, Task::TARGET_TYPE_BUSINESS, 0, Task::TASK_TYPE_BROKER_REVIEW);
        return response()->json(['status' => 'ok']);
    }

    public function edit(Business $business)
    {
        $data['business_id'] = $business->id;
        return view('business.edit', $data);
    }

    public function update(Request $request, Business $business)
    {
        Schema::disableForeignKeyConstraints();
        $businessData = json_decode($request->get('business'), true);
        $business->update($businessData);
        $files = $request->file('files');
        if ($files) {
            foreach ($files as $file) {
                $business->addMedia($file)->toMediaCollection('business', 'business');
            }
        }
        Schema::enableForeignKeyConstraints();
        return response()->json(['status' => 'ok']);
    }

    private function getBroker(): User
    {
        return User::permission('object-moderate')->with('tasks')->get()->sortBy(function ($user) {
            return $user->tasks->count();
        })->first();
    }
}
