<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FranchiseStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $data = json_decode($this->get('object'), true);
        $locale = app()->getLocale();
        foreach (['name', 'description', 'name_legal_entity', 'name_business', 'justification_financial_indicators'] as $key) {
            $data[$key] = $data[$key][$locale];
        }
        $data['user_id'] = auth()->user()->id;
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price'         => 'required|numeric',
            'name'          => 'required',
            'description'   => 'required',
            'category_id'   => 'required',
            'country_id'    => 'required',
            'city_id'       => 'required',
            //'return_payback'=> 'required',
            'profitability' => 'required',
            'category_property_id' => 'required',
            'property_type_id' => 'required',
            'name,description,seo_description,seo_title' => 'string',
//            'commission'    => 'numeric',
        ];
    }

    public function messages()
    {
        return [
            'category_id.required'   => 'Категория не выбранна',
            'country_id.required'    => 'Страна не выбранна',
            'city_id.required'       => 'Город не выбран',
            'name.required'          => 'Название франшизы не указано',
            'name.string'            => 'Название франшизы должно быть строкой',
            'description.required'   => 'Описание франшизы не указано',
            //'return_payback.required'=> 'Окупаемость франшизы не указана',
            'price.required'         => 'Цена франшизы не указана',
            'revenue.required'       => 'Доход франшизы не указан',
            'profitability.required' => 'Рентабельность франшизы не указана',
            'district_id.required'   => 'Регион не выбран',
            'category_property_id.required' => 'Тип собственности объекта не выбран',
            'property_type_id.required'   => 'Тип недвижимости не выбран',
        ];
    }
}
