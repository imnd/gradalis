<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Session;

class NovaUser
{
    /**
     * set current lang for request
     */
    public function handle($request, Closure $next)
    {
        $main = Config::get('currency.main');
        $currencies = Config::get('currency.currencies');

        if (!Session::has('currency')) {
            Session::put('currency', $currencies[$main]);
        } elseif (Session::get('currency')['val'] != $main) {
            Session::put('currency', $currencies[$main]);
        }
        return $next($request);
    }
}
