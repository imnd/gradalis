<?php

namespace App\Http\Middleware;

use Closure,
    Illuminate\Support\Facades\Session,
    GuzzleHttp\Client,
    Exception,
    App;

class GeoLocation
{
    const URL = 'https://ipstack.com/product';

    /**
     * Определяем гео-локацию по IP. Можно было использовать популярный https://github.com/Torann/laravel-geoip,
     * но он тянет странные зависимости, пока не стал ставить. Пока обычный реквест на http://ip-api.com
     * Еще вот этот сервис выглядит неплохо https://ipstack.com/product
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('ip_info')) {
            // заглушка ибо тормоза
            Session::put('ip_info', [
                'zip' => '454000',
                'lon' => 61.43119812011719,
                'as' => 'AS8369 Intersvyaz-2 JSC',
                'countryCode' => 'RU',
                'city' => 'Chelyabinsk',
                'timezone' => 'Asia/Yekaterinburg',
                'isp' => 'Intersvyaz-2 JSC',
                'org' => 'Intersvyaz-2 JSC',
                'query' => '77.222.98.94',
                'country' => 'Russia',
                'region' => 'CHE',
                'regionName' => 'Chelyabinsk',
                'lat' => 55.15800094604492,
                'status' => 'success',
            ]);
            return $next($request);

            /* TODO добавить дефолтный локэйшн? */
            /*$ip = App::environment('local') ? '77.222.98.94' : $_SERVER['REMOTE_ADDR'];
            try {
                $response = (new Client)->request('GET', "http://ip-api.com/php/$ip");
                Session::put('ip_info', unserialize($response->getBody()->getContents()));
            } catch (Exception $e) {}*/
        }
        return $next($request);
    }
}
