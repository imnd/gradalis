<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Currency
{
    /**
     * set current lang for request
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('currency')) {
            $main = Config::get('currency.main');
            $currencies = Config::get('currency.currencies');
            Session::put('currency', $currencies[$main]);
        }
        return $next($request);
    }
}
