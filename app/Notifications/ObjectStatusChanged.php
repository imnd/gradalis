<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Business\Business;
use App\Models\Franchise\Franchise;

class ObjectStatusChanged extends Notification
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($object)
    {
        switch (get_class($object)) {
            case 'App\Models\Business\Business':
                $type = 'business';
                $status_text = Business::getStatuses()[$object->status];
                break;
            case 'App\Models\Franchise\Franchise':
                $type = 'franchise';
                $status_text = Franchise::getStatuses()[$object->status];
                break;
        }

        $this->data = [
            'text' => __("notifications.${type}_status_changed", ['name' => $object->name, 'status' => $status_text])
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->data
        ];
    }
}
