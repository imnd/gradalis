<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewMessage extends Notification
{
    use Queueable;

    public $message;
    public $data;
    
    public function __construct($message)
    {
        $object = $message->dialog->object;
        $dialog = $message->dialog;
        if($object && $object->getFirstMediaUrl('business', 'thumb')){
            $thumb = $object->getFirstMediaUrl('business', 'thumb');
        }else{
            $thumb = false;
        }

        $text = __('notifications.new_message', ['link' => "<a href='/profile/chat/dialog/".$dialog->id."'>".strip_tags($dialog->theme)."</a>"]);

        $this->message = [
            'id' => $message->id,
        ];

        $this->data = [
            'dialog' => $dialog,
            'thumb' => $thumb,
            'text' => $text
        ];

    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'data' => $this->data
        ];
    }
}
