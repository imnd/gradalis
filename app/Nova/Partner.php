<?php

namespace App\Nova;

use App\Models\Referral\Partner as PartnerModel;
use Arsenaltech\NovaTab\NovaTab;
use Arsenaltech\NovaTab\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Treestoneit\BelongsToField\BelongsToField;

class Partner extends Resource
{
    use Tabs;

    public static $category = 'Referral';
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Referral\Partner';

    public static $with = ['user', 'accountManager'];

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Partners');
    }

    public function title()
    {
        return $this->user->full_name;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Number::make('Баланс', 'balance'),

            Number::make('Холд', 'hold'),

            Number::make('Открытая коммиссия', 'open_commission'),

            new NovaTab('Статус', [
                Select::make('Статус', 'status')
                    ->options(PartnerModel::getStatuses())
                    ->displayUsingLabels()
                    ->rules('required'),
            ]),

            new NovaTab('Связи', [
                BelongsToField::make('Пользователь', 'user', 'App\Nova\User'),
                BelongsToField::make('Акаунт менеджр', 'accountManager', 'App\Nova\User'),
            ]),
        ];
    }
}
