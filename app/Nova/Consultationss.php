<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Treestoneit\BelongsToField\BelongsToField;

class Consultationss extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Travel\Consultation';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    public static function label()
    {
        return __('Consultations');
    }

    public static function singularLabel()
    {
        return __('Consultation');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            DateTime::make(__('fields.date_time'), 'date'),
            Text::make('тема', 'theme'),

            BelongsToField::make('Поездка', 'travel', 'App\Nova\Travel'),

            MorphTo::make('Объект', 'object')->types([
                Business::class,
                Franchise::class,
            ])
        ];
    }
}
