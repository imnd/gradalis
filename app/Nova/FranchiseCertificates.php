<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID,
    Illuminate\Http\Request,
    Laravel\Nova\Fields\Text;

class FranchiseCertificates extends Resource
{
    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\Certificates';

    /**
     * @inheritdoc
     */
    public static $search = [
        'id'
    ];

    /**
     * @inheritdoc
     */
    public static function availableForNavigation(Request $request)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('Certificates');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('Certificates');
    }

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            BelongsTo::make('Документ', 'type', CertificateDocuments::class)
                ->rules('required'),

            BelongsTo::make('Тип', 'type', CertificateTypes::class)
                ->rules('required'),

            Date::make('Срок действия лицензии', 'license_period')
                ->rules('required'),
        ];
    }
}
