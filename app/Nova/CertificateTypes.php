<?php

namespace App\Nova;

class CertificateTypes extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\CertificateTypes';

    public static function label()
    {
        return __('CertificateTypes');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('CertificateType');
    }
}
