<?php

namespace App\Nova;

class LegalStatuses extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\LegalStatuses';

    public static function label()
    {
        return __('LegalStatuses');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('LegalStatuses');
    }
}
