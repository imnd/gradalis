<?php

namespace App\Nova;

use
    Laravel\Nova\Http\Requests\NovaRequest,
    App\Models\Roles
;

class Brokers extends User
{
    public static $category = 'Broker';
    public static $with = ['city', 'country', 'roles'];

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query
            ->whereHas('roles', function($query) {
                $query->where('roles.name', Roles::ROLE_BROKER);
            });
    }

    public static function label()
    {
        return __('Brokers');
    }

    public static function singularLabel()
    {
        return  __('Broker');
    }
}
