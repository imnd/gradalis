<?php

namespace App\Nova;

use DmitryBubyakin\NovaMedialibraryField\Fields\Medialibrary;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Marketplace\Translatable\Translatable;
use App\Models\News\News AS NewsModel;
use App\Models\News\NewsCategory;

class FranchiseNews extends ListModelResource
{

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('News');
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\News\News';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('News');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('News');
    }


    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('category_id', NewsCategory::franchiseCategoryId());
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $locale = app()->getLocale();
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Translatable::make(__('Title'),'title')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            Translatable::make(__('fields.description'),'description')
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:7000'),

            Text::make('ID франшизы', function(){
                return '<a href="/franchise/'.$this->owner_id.'" target="_blank">'.$this->owner_id.'</a>';
            })->asHtml()->onlyOnIndex(),

            Medialibrary::make('Изображение', 'franchise_news')
                ->mediaOnIndex(),

            Image::make('Изображение', 'image')
                // FIX for svg
                ->store(function (Request $request, $model) {
                    $file = $request->file('image');
                    $model
                        ->addMedia($file)
                        ->toMediaCollection(NewsModel::FRANCHISE_NEWS_COLLECTION_NAME,
                            NewsModel::FRANCHISE_NEWS_COLLECTION_DISC_NAME);
                    return true;
                }),

            Select::make('Статус','status')
                ->options(NewsModel::getStatuses())
                ->hideWhenCreating()
                ->displayUsingLabels()
                ->rules('required'),

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(Request $request)
    {
        return [
            new Actions\StatusAwait(),
            new Actions\StatusModerated,
            new Actions\StatusApproved,
            new Actions\StatusDeclined
        ];
    }
}
