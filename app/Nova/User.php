<?php

namespace App\Nova;

use
    Session,
    Illuminate\Http\Request,
    App\Nova\CustomPermission as Permission,
    App\Nova\CustomRole as Role,
    KABBOUCHI\NovaImpersonate\Impersonate,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Avatar,
    Laravel\Nova\Fields\Date,
    Laravel\Nova\Fields\MorphToMany,
    Laravel\Nova\Fields\Password,
    Laravel\Nova\Fields\Select,
    Laravel\Nova\Fields\Text,
    Treestoneit\BelongsToField\BelongsToField,
    Vyuldashev\NovaMoneyField\Money
;

/**
 * @property string $first_name
 * @property string $last_name
 */
class User extends Resource
{
    /**
     * @inheritdoc
     */
    public static $model = 'App\\Models\\Auth\\User';
    /**
     * @inheritdoc
     */
    public static $search = [
        'id', 'first_name', 'last_name', 'email', 'phone', 'balance', 'active', 'sum_from', 'sum_to', 'purchase_date_from', 'purchase_date_to'
    ];
    /**
     * @inheritdoc
     */
    public static $with = ['city'];

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('Users');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('User');
    }

    /**
     * @inheritdoc
     */
    public function title()
    {
        return $this->full_name;
    }

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Avatar::make(__('Avatar'), 'avatar')
                ->maxWidth(100)
                ->disk('user'),

            Text::make(__('First name'), 'first_name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Last name'), 'last_name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Phone'), 'phone')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Email'), 'email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Date::make(__('Registration date'), 'created_at')
                ->sortable()
                ->hideWhenCreating()
                ->hideWhenUpdating(),

            Select::make(__('Active'), 'active')
                ->options([
                    'Заблокирован',
                    __('fields.status_active'),
                ])
                ->displayUsingLabels()
                ->sortable(),

            Money::make(__('Balance'), Session::get('currency')['val'], 'balance'),

            Password::make(__('Password'), 'password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:6')
                ->updateRules('nullable', 'string', 'min:6'),

            BelongsToField::make(__('City'), 'city', City::class)->searchable(),

            Impersonate::make($this),

            MorphToMany::make('Роль', 'roles', Role::class),

            MorphToMany::make('Разрешение', 'permissions', Permission::class),
        ];
    }
}
