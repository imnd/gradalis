<?php

namespace App\Nova\Actions;

use App\Models\News\News;

class NewsStatusModerated extends StatusModerated
{
    protected $statusVal = News::STATUS_MODERATED;
}
