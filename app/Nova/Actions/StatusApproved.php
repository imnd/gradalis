<?php

namespace App\Nova\Actions;

class StatusApproved extends SetStatus
{
    // Одобрен
    public const STATUS_APPROVED = 2;

    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Установить статус "Одобрен"';

    protected $statusVal = self::STATUS_APPROVED;
}
