<?php

namespace App\Nova\Actions;

use App\Models\Franchise\Franchise;

class FranchiseStatusModerated extends SetStatus
{
    protected $statusVal = Franchise::STATUS_MODERATED;
}
