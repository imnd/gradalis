<?php

namespace App\Nova\Actions;

class StatusDeclined extends SetStatus
{
    // Отклонен
    public const STATUS_DECLINED = 4;

    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Установить статус "Отклонен"';

    protected $statusVal = self::STATUS_DECLINED;
}
