<?php

namespace App\Nova\Actions;

use
    Laravel\Nova\Actions\Action,
    Illuminate\Support\Collection,
    Laravel\Nova\Fields\ActionFields,
    Illuminate\Queue\SerializesModels,
    App\Models\Franchise\Franchise;

class FranchiseStatusAwait extends Action
{
    use SerializesModels;

    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Установить статус "В ожидании"';

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model) {
            try {
                $model->status = Franchise::STATUS_AWAIT;
                $model->save();
                $this->markAsFinished($model);
            } catch (\Exception $e) {
                $this->markAsFailed($model, $e);
            }
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
