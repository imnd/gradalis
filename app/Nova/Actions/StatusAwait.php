<?php

namespace App\Nova\Actions;

class StatusAwait extends SetStatus
{
    // В ожидании
    public const STATUS_AWAIT = 0;

    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Установить статус "В ожидании"';

    protected $statusVal = self::STATUS_AWAIT;
}
