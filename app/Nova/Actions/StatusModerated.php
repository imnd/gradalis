<?php

namespace App\Nova\Actions;

class StatusModerated extends SetStatus
{
    // Прошел модерацию
    public const STATUS_MODERATED = 1;
    public $name = 'Установить статус "Прошел модерацию"';

    protected $statusVal = self::STATUS_MODERATED;
}
