<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Treestoneit\BelongsToField\BelongsToField;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Trix;
use DmitryBubyakin\NovaMedialibraryField\Fields\Medialibrary;

class Terms extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Terms';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    public static function label()
    {
        return __('Terms');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsToField::make('Язык', 'language', 'App\Nova\Language'),

            Text::make('header'),

            Text::make('Byuer text', 'buyer_text')->onlyOnIndex(),
            Text::make('Seller text', 'seller_text')->onlyOnIndex(),

            Trix::make('Byuer text', 'buyer_text')->hideFromIndex(),
            Trix::make('Seller text', 'seller_text')->hideFromIndex(),

            Medialibrary::make('Документы для покупателя', 'byuer_documents')->mediaOnIndex(20),
            Medialibrary::make('Документы для продавца', 'seller_documents')->mediaOnIndex(20)
        ];
    }
}
