<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID,
    Illuminate\Http\Request,
    Laravel\Nova\Fields\Text;

class Objects extends Resource
{
    public static $category = 'Franchise';

    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\FranchiseObject';

    /**
     * @inheritdoc
     */
    public static $title = 'title';

    /**
     * @inheritdoc
     */
    public static $search = [
        'id', 'title'
    ];

    /**
     * @inheritdoc
     */
    public static function availableForNavigation(Request $request)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function group()
    {
        return __('Franchise');
    }

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('Objects');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('Object');
    }

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make(__('fields.description'), 'description')
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make('Телефон', 'phone')
                ->sortable()
                ->rules('required', 'max:10'),
            Text::make('Email', 'email')
                ->sortable()
                ->rules('required', 'max:128'),
            Text::make('Адрес', 'address')
                ->sortable()
                ->rules('required', 'max:255'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions(Request $request)
    {
        return [];
    }
}
