<?php

namespace App\Nova;

class Penalties extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\Penalties';

    public static function label()
    {
        return __('Penalties');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('Penalty');
    }
}
