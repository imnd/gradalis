<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID,
    Illuminate\Http\Request,
    Laravel\Nova\Fields\Text,
    Marketplace\IconSelect\IconSelect,
    Marketplace\Translatable\Translatable;

class FranchiseCategory extends Resource
{
    public static $category = 'Franchise';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\FranchiseCategory';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'translation';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Franchise');
    }

    public static function label()
    {
        return __('Categories');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('Category') . ' ' . __('of franchise');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            Text::make(__('Title'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),
            IconSelect::make(__('Icon'), 'icon')->init(),
            Translatable::make('Перевод', 'translation')
                ->singleLine()
                ->hideFromIndex()
                ->rules('required', 'max:255'),
        ];
    }
}
