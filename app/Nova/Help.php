<?php

namespace App\Nova;


use App\Models\Language;
use App\Nova\Fields\CustomCkeditor;
use DmitryBubyakin\NovaMedialibraryField\Fields\Medialibrary;
use Treestoneit\BelongsToField\BelongsToField;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Select;
use Marketplace\Translatable\Translatable;
use App\Models\Help\Help AS HelpModel;

class Help extends Resource
{

    //public static $displayInNavigation = false;

    public static $category = "Help";
    public static $group = 'Help';

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Help');
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Help\Help';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['category'];

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Help');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Help');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $locale = app()->getLocale();
        $allLangs = Language::all();
        $fields = [
            ID::make()->sortable()->onlyOnIndex(),
        
            Translatable::make(__('Title'),'title')
                        ->singleLine()
                        ->indexLocale($locale)
                        ->rules('required', 'max:255'),

            CustomCkeditor::make(__('fields.description'), 'description')->hideFromIndex(),

            Select::make('Статус','status')
                  ->options(HelpModel::getStatuses())
                  ->hideWhenCreating()
                  ->displayUsingLabels()
                  ->rules('required'),
        
            BelongsToField::make('Категория', 'category', 'App\Nova\HelpCategory'),

            Translatable::make('SEO_Url (Наименование транслитом, маленькими буквами, пробелы заменяем на дефисы)',
                'url')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Title')
                ->singleLine()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Description')
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Keywords')
                ->singleLine()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),
        ];

        return $fields;
    }
}
