<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    DmitryBubyakin\NovaMedialibraryField\Fields\Medialibrary,
    Treestoneit\BelongsToField\BelongsToField,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Select,
    Marketplace\Translatable\Translatable,
    App\Models\News\News as NewsModel,
    Laravel\Nova\Fields\Image,
    App\Nova\Fields\CustomCkeditor
;

class News extends Resource
{
    public static $category = 'News';

    public static $model = 'App\Models\News\News';

    public static $title = 'title';

    public static $search = [
        'title',
    ];

    public static $with = ['category'];

    public static function group()
    {
        return __('News');
    }

    public static function label()
    {
        return __('News');
    }

    public static function singularLabel()
    {
        return __('News');
    }

    public function fields(Request $request)
    {
        $locale = app()->getLocale();
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Translatable::make(__('fields.title'), 'title')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            Translatable::make('SEO_Url (Наименование транслитом, маленькими буквами, пробелы заменяем на дефисы)', 'url')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            CustomCkeditor::make(__('fields.description'), 'description')
                ->hideFromIndex(),

            /*Translatable::make(__('fields.description'), 'description')
                ->trix()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:7000'),*/

            /*Translatable::make('Видео', 'video')
                ->indexLocale($locale)
                ->hideFromIndex(),*/

            Medialibrary::make('Изображение', 'news')
                ->relation('newsMedia')
                ->mediaOnIndex(),

            Image::make('Изображение', 'image')
                // FIX for svg
                ->store(function (Request $request, $model) {
                    $file = $request->file('image');
                    $model
                        ->addMedia($file)
                        ->toMediaCollection(NewsModel::NEWS_COLLECTION_NAME, NewsModel::NEWS_COLLECTION_DISC_NAME);
                    return true;
                }),

            Select::make('Статус', 'status')
                ->options(NewsModel::getStatuses())
                ->hideWhenCreating()
                ->displayUsingLabels()
                ->rules('required'),

            Translatable::make('Seo_Title')
                ->singleLine()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Description')
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Keywords')
                ->singleLine()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            BelongsToField::make('Категория', 'category', 'App\Nova\NewsCategory')
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(Request $request)
    {
        return [
            new Actions\NewsStatusModerated,
        ];
    }
}
