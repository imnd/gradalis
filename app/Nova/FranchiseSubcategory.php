<?php

namespace App\Nova;

use
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\BelongsTo,
    Laravel\Nova\Fields\Text,
    Illuminate\Http\Request,
    Marketplace\IconSelect\IconSelect,
    Marketplace\Translatable\Translatable
;

class FranchiseSubcategory extends ListModelResource
{
    public static $title = 'name';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\Subcategories';

    public static function label()
    {
        return __('Subcategories');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('Subcategory');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            BelongsTo::make(__('Category'), 'category', 'App\Nova\FranchiseCategory'),
            Text::make(__('Title'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),
            IconSelect::make('Иконка', 'icon')->init(),
            Translatable::make('Перевод', 'translation')
                ->singleLine()
                ->hideFromIndex()
                ->rules('required', 'max:255'),

        ];
    }
}
