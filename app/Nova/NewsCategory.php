<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Marketplace\Translatable\Translatable;
use Treestoneit\BelongsToField\BelongsToField;
use Marketplace\IconSelect\IconSelect;

class NewsCategory extends Resource
{
    public static $category = "News";


    public static $model = 'App\Models\News\NewsCategory';

    public static $title = 'title';

    public static $search = [
        'title',
    ];

    public static $with = ['parent'];

    public static function group()
    {
        return __('News');
    }

    public static function label()
    {
        return __('NewsCategory');
    }

    public static function singularLabel()
    {
        return __('NewsCategories');
    }

    public function fields(Request $request)
    {
        $locale = app()->getLocale();
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Translatable::make(__('Title'),'title')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            Translatable::make('SEO_Url (Наименование транслитом, маленькими буквами, пробелы заменяем на дефисы)',
                'url')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            IconSelect::make(__('Icon'), 'icon')
                ->setPath('/svg/icons/')
                ->init(),

            BelongsToField::make('Родитель', 'parent', 'App\Nova\NewsCategory')
                ->nullable(),
        ];
    }
}
