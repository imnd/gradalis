<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Illuminate\Support\Facades\Cache,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Boolean,
    Laravel\Nova\Fields\Image,
    Laravel\Nova\Fields\Number,
    Laravel\Nova\Fields\BelongsToMany,
    Marketplace\Translatable\Translatable,
    Vyuldashev\NovaMoneyField\Money
;

class Tariff extends Resource
{
    public static $category = 'Services';
    public static $model = 'App\Models\Service\Tariffs';
    public static $title = 'name';
    public static $search = ['id'];

    public static function label()
    {
        return __('Tariffs');
    }

    public static function singularLabel()
    {
        return __('Tariff');
    }

    public static function group()
    {
        return __('Services');
    }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            BelongsToMany::make(__('Service'), 'services', Service::class),

            Translatable::make(__('fields.title'), 'name')
                ->sortable()
                ->rules('required'),

            Translatable::make(__('fields.description'), 'description'),

            Image::make(__('Icon'), 'icon')
                // FIX for svg
                ->storeAs(function (Request $request) {
                    return sha1($request->icon->getClientOriginalName()) . '.' . $request->icon->getClientOriginalExtension();
                }),

            Translatable::make(__('Price'), 'price'),

            Boolean::make(__('Visible'), 'visible'),
            Boolean::make(__('Recommended'), 'recommended'),

            Number::make(__('Validity'), 'validity'),
            Number::make(__('Discount'), 'discount'),
            Number::make(__('Views count'), 'views'),
            Number::make(__('Sales success percent'), 'sales_success_percent'),
        ];
    }

    /**
     * Возвращает курс btc к указанной валюте
     * @param string $currency
     * @return mixed
     */
    public function getBtc($currency = 'EUR')
    {
        $currencies = Cache::remember('btc', 60, function () use ($currency) {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://blockchain.info/ticker');
            $currencies = json_decode($response->getBody()->getContents(), true);
            return $currencies;
        });

        return $currencies[$currency]['last'];
    }

}
