<?php

namespace App\Nova;

use
    Treestoneit\BelongsToField\BelongsToField,
    Laravel\Nova\Fields\ID,
    Illuminate\Http\Request,
    Marketplace\Translatable\Translatable;

class FranchisePackage extends Resource
{
    public static $displayInNavigation = false;

    public static $category = 'Franchise';

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Franchise');
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\FranchisePackage';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['franchise'];

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Packages');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('Package') . ' ' . __('of franchise');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            Translatable::make(__('Title'),'name')
                ->singleLine()
                ->indexLocale('ru')
                ->rules('required', 'max:255'),

            Translatable::make('Опции','options')
                ->singleLine()
                ->hideFromIndex()
                ->indexLocale('ru')
                ->rules('required', 'max:255'),

            BelongsToField::make('Франшиза', 'franchise', 'App\Nova\Franchise')->searchable(),
        ];
    }
}
