<?php

namespace App\Nova;

class Equipments extends ListModelResource
{
    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\Equipments';

    public static function label()
    {
        return __('Equipments');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('Equipment');
    }
}
