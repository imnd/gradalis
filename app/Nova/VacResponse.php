<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Treestoneit\BelongsToField\BelongsToField;
use App\Models\Roles;

class VacResponse extends Resource
{
    public static $category = "Vacancy";
    public static $group = 'Vacancy';

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Vacancy');
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\VacancyResponse';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['id'];

    public static function label()
    {
        return 'Заявки на вакансии';
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        $user = $request->user();
        if($user->hasRole([Roles::ROLE_TECH_BRANCH_MANAGER])){
            $query->whereHas('vacancy', function($query) use ($user){
                return $query->where('city_id', $user->city_id);
            });
        } else {
            return $query;
        }
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            BelongsToField::make('Вакансия', 'vacancy', 'App\Nova\Vacancy')->searchable(),
            Text::make('Имя', 'name'),
            Text::make('Телефон', 'phone'),
            Text::make('Email', 'email'),
            Text::make('Ссылка на резюме', 'cv_link'),
            Text::make('Текст', 'text'),
        ];
    }
}
