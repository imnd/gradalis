<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request,
    Laravel\Nova\Filters\BooleanFilter,
    App\Models\Roles;

class UserType extends BooleanFilter
{
    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $values
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        $query->whereHas('roles', function ($query) use ($value) {
            foreach ($value as $id) {
                $query->where('id', $id);
            }
        });
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            Roles::ROLE_BUYER => Roles::ROLE_BUYER_ID,
            Roles::ROLE_SELLER => Roles::ROLE_SELLER_ID,
        ];
    }
}
