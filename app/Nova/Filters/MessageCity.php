<?php

namespace App\Nova\Filters;

use App\Models\Auth\User;
use App\Models\Chat\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Filters\Filter;

class MessageCity extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Фильтр по городу';


    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('ip_info->city', $value );
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        //Список уникальных городов пользователй
        $cities = array();

        $cities_ = DB::table('messages')->select('ip_info->city as name')->groupBy('ip_info->city')->orderBy('ip_info->city')->get();

        foreach( $cities_ as $city )
            $cities[trim($city->name,'"')] = trim($city->name,'"');


        return $cities;
    }
}
