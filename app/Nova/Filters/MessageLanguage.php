<?php

namespace App\Nova\Filters;

use App\Models\Auth\User;
use App\Models\Chat\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Filters\Filter;

class MessageLanguage extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Фильтр по языку';


    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('locale', $value );
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $languages = array(
            'Русский' =>'ru',
            'Английский' => 'en',
            'Польский' =>'pl'
        );
        return $languages;
    }
}
