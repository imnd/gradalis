<?php

namespace App\Nova\Filters;

use App\Models\Auth\User;
use App\Models\Chat\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Filters\Filter;

class MessageCountry extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Фильтр по странам';


    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('ip_info->country', $value );
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        //Список уникальных стран пользователй
        $countries= array();

        $countries_ = DB::table('messages')->select('ip_info->country as name')->groupBy('ip_info->country')->orderBy('ip_info->country')->get();

        foreach( $countries_ as $country )
            $countries[trim($country->name,'"')] = trim($country->name,'"');


        return $countries;
    }
}
