<?php

namespace App\Nova\Filters;

use App\Models\Auth\User;
use App\Models\Chat\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Filters\Filter;

class MessageObjectType extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The filter's component.
     *
     * @var string
     */
    public $name = 'Фильтр по типу объекта';


    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->join('dialogs', 'dialogs.id', '=', 'messages.dialog_id')
            ->select('messages.*')->where('dialogs.object_type', '=', $value );
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $object_types = array(
            'Бизнес' =>'App\Models\Business\Business',
            'Франшиза' => 'App\Models\Business\Franchise'
        );
        return $object_types;

    }
}
