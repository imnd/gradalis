<?php

namespace App\Nova\Fields;


use Laravel\Nova\Http\Requests\NovaRequest;
use Waynestate\Nova\CKEditor;

class CustomCkeditor extends CKEditor
{

    /**
     * Resolve the given attribute from the given resource.
     *
     * @param  mixed  $resource
     * @param  string  $attribute
     * @return mixed
     */
    protected function resolveAttribute($resource, $attribute)
    {


        $results = $resource->getTranslations($attribute);

        /*$translations = $resource->translations()
            ->get(['locale', $attribute])
            ->toArray();
        foreach ( $translations as $translation ) {
            $results[$translation['locale']] = $translation[$attribute];
        }*/

        return $results;
    }

    /**
     * Hydrate the given attribute on the model based on the incoming request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  string  $requestAttribute
     * @param  object  $model
     * @param  string  $attribute
     * @return void
     */
    protected function fillAttributeFromRequest(NovaRequest $request, $requestAttribute, $model, $attribute)
    {
        if ( is_array($request[$requestAttribute]) ) {
            $model->setTranslations($attribute, $request[$requestAttribute]);
            /*foreach ( $request[$requestAttribute] as $lang => $value ) {
                $model->translateOrNew($lang)->{$attribute} = $value;
            }*/
        }
    }




}