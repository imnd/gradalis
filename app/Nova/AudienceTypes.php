<?php

namespace App\Nova;

class AudienceTypes extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\AudienceTypes';

    public static function label()
    {
        return __('AudienceTypes');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('AudienceType');
    }
}
