<?php

namespace App\Nova;

use App\Models\Referral\CampaignResource as CampaignResourceModel;
use Arsenaltech\NovaTab\NovaTab;
use Arsenaltech\NovaTab\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Marketplace\Translatable\Translatable;
use Treestoneit\BelongsToField\BelongsToField;

class CampaignResource extends Resource
{
    use Tabs;

    public static $category = "Referral";
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Referral\CampaignResource';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('CampaignResources');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new NovaTab('Информация', [
                ID::make()->sortable()->onlyOnIndex(),
                Translatable::make('Наимнование', 'name'),
            ]),
            Text::make('Url', 'url'),
            new NovaTab('Статус', [
                Select::make('Тип', 'type')
                    ->options(CampaignResourceModel::getTypes())
                    ->displayUsingLabels()
                    ->rules('required'),
                Select::make('Тип ресурса', 'resource_type')
                    ->options(CampaignResourceModel::getResourceTypes())
                    ->displayUsingLabels()
            ]),
            new NovaTab('Параметры', [
                Text::make('Ширина', 'width'),
                Text::make('Высота', 'height'),
            ]),
            new NovaTab('Связи', [
                BelongsToField::make('Страна', 'country', 'App\Nova\Country')->searchable(),
                BelongsToField::make('Оффер', 'campaign', 'App\Nova\Campaign')->searchable(),
            ])
        ];
    }
}
