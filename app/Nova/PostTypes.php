<?php

namespace App\Nova;

class PostTypes extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\PostTypes';

    public static function label()
    {
        return __('PostTypes');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('PostType');
    }
}
