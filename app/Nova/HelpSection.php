<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Marketplace\Translatable\Translatable;

class HelpSection extends Resource
{

    //public static $displayInNavigation = false;

    public static $category = "Help";
    public static $group = 'Help';

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Help');
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Help\HelpSection';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];



    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('HelpSections');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('HelpSection');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $locale = app()->getLocale();
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Translatable::make(__('Title'),'title')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            Translatable::make('SEO_Url (Наименование транслитом, маленькими буквами, пробелы заменяем на дефисы)',
                'url')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Title')
                ->singleLine()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Description')
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Keywords')
                ->singleLine()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),
        ];
    }
}
