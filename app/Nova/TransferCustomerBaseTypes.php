<?php

namespace App\Nova;

class TransferCustomerBaseTypes extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\TransferCustomerBaseTypes';

    public static function label()
    {
        return __('TransferCustomerBaseTypes');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('TransferCustomerBaseType');
    }
}
