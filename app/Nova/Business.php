<?php

namespace App\Nova;

use Session,
    Illuminate\Http\Request,
    App\Models\Business\Business as BusinessModel,

    Arsenaltech\NovaTab\NovaTab,
    Arsenaltech\NovaTab\Tabs,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Number,
    Laravel\Nova\Fields\Select,
    Marketplace\Translatable\Translatable,
    Treestoneit\BelongsToField\BelongsToField,
    Vyuldashev\NovaMoneyField\Money,
    DmitryBubyakin\NovaMedialibraryField\Fields\Medialibrary
;

class Business extends Resource
{
    use Tabs;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Business\Business';
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];
    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['category', 'country', 'region', 'city', 'user'];

    public static $category = "Business";

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Business');
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Businesses');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Business');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new NovaTab('Информация', [
                ID::make()->sortable()->onlyOnIndex(),

                Medialibrary::make('Business'),

                Money::make('Цена', Session::get('currency')['val'], 'price')
                    ->rules('required'),

                Number::make('Рентабельность', 'profitability')
                    ->hideFromIndex()
                    ->rules('required'),

                Money::make('Прибыль', Session::get('currency')['val'], 'profit')
                    ->hideFromIndex()
                    ->rules('required'),

                Number::make('Окупаемость', 'payback')
                    ->hideFromIndex()
                    ->rules('required'),


                Number::make('Комиссия', 'commission')
                    ->hideWhenCreating()
                    ->rules('required'),
            ]),
            new NovaTab('Переводы', [
                Translatable::make(__('Title'), 'name')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->rules('required', 'max:255'),

                Translatable::make(__('fields.description'), 'description')
                    ->trix()
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),

                Translatable::make('Причина продажи', 'reason_sale')
                    ->singleLine()
                    ->hideFromIndex()
                    ->indexLocale('ru'),

                Translatable::make('Название юр. лица', 'name_legal_entity')
                    ->singleLine()
                    ->hideFromIndex()
                    ->indexLocale('ru'),

                Translatable::make('Название видеообзора', 'name_video_review')
                    ->singleLine()
                    ->hideFromIndex()
                    ->indexLocale('ru'),

                Translatable::make('Ссылка видеообзора', 'link_video_review')
                    ->singleLine()
                    ->hideFromIndex()
                    ->indexLocale('ru'),

                Translatable::make('Дополнительная информация по трафику', 'additional_information_traffic')
                    ->singleLine()
                    ->hideFromIndex()
                    ->indexLocale('ru'),

                Translatable::make('Дополнительная информация по контенту и дизайну',
                    'additional_information_content_site')
                    ->singleLine()
                    ->hideFromIndex()
                    ->indexLocale('ru'),

                Translatable::make('Дополнительная информация по расходам', 'additional_information_expenses_site')
                    ->singleLine()
                    ->hideFromIndex()
                    ->indexLocale('ru'),
            ]),
            new NovaTab('Статус', [
                Select::make('Статус', 'status')
                    ->options(BusinessModel::getStatuses())
                    ->hideWhenCreating()
                    ->displayUsingLabels()
                    ->rules('required'),
            ]),
            new NovaTab('SEO', [
                Translatable::make('Seo_Title')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),

                Translatable::make('Seo_Description')
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),

                Translatable::make('Seo_Keywords')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),
            ]),
            new NovaTab('Маркетинг', [

                Translatable::make('Вк таргет', 'metrics_vk')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),
                Translatable::make('Однокласники', 'metrics_ok')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),
                Translatable::make('Фейсбук', 'metrics_fb')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),
                Translatable::make('Гугл', 'metrics_google')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),
                Translatable::make('Мейлрутаргет', 'metrics_mail')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),
            ]),
            new NovaTab('Связи', [
                BelongsToField::make(__('Category'), 'category', 'App\Nova\BusinessCategory'),
                BelongsToField::make('Страна', 'country', 'App\Nova\Country')->searchable(),
                BelongsToField::make('Регион', 'region', 'App\Nova\Region')->searchable(),
                BelongsToField::make('Город', 'city', 'App\Nova\City')->searchable(),
                BelongsToField::make('Пользователь', 'user', 'App\Nova\User')->searchable(),
                Medialibrary::make('Фото', 'business')
            ])
        ];
    }
}
