<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Illuminate\Support\Facades\Cache,

    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Select,
    Laravel\Nova\Fields\Image,
    Laravel\Nova\Fields\BelongsToMany,

    Inspheric\Fields\Indicator,
    Benjaminhirsch\NovaSlugField\Slug,
    Benjaminhirsch\NovaSlugField\TextWithSlug,
    Marketplace\Translatable\Translatable,
    Vyuldashev\NovaMoneyField\Money
;

class Service extends Resource
{
    public static $category = 'Services';

    public static $model = 'App\Models\Service\Service';

    public static $title = 'name';

    public static $search = [
        'id',
    ];

    public static function label()
    {
        return __('Services');
    }

    public static function singularLabel()
    {
        return __('Service');
    }

    public static function group()
    {
        return __('Services');
    }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            BelongsToMany::make(__('Tariff'), 'tariffs', Tariff::class),

            Indicator::make(__('fields.status'), 'status')
                ->labels([
                    __('fields.status_draft'),
                    __('fields.status_moderation'),
                    __('fields.status_disabled'),
                    __('fields.status_active'),
                ])
                ->colors([
                    'orange',
                    'green',
                    'red',
                    'green',
                ])
                ->onlyOnIndex()
                ->sortable(),

            Select::make(__('fields.status'), 'status')
                ->options([
                    __('fields.status_draft'),
                    __('fields.status_moderation'),
                    __('fields.status_disabled'),
                    __('fields.status_active')
                ])
                ->displayUsingLabels()
                ->onlyOnForms(),

            TextWithSlug::make(__('fields.title'), 'name')
                ->sortable()
                ->slug(__('Permalink'))
                ->rules('required'),

            Slug::make(__('Permalink'), 'slug')
                ->rules('required'),

            Translatable::make(__('fields.description'), 'description'),

            Image::make(__('Icon'), 'icon')
                // FIX for svg
                ->storeAs(function (Request $request) {
                    return sha1($request->icon->getClientOriginalName()) . '.' . $request->icon->getClientOriginalExtension();
                }),

            Money::make(__('Price'), 'EUR', 'price'),
            
            Translatable::make('Цена за', 'price_for')->hideFromIndex(),

            /*Select::make(__('Type'), 'type')
                ->options([
                    1 => 'Для покупателя',
                    2 => 'Для продавца'
                ])
                ->displayUsingLabels()
                ->sortable()
                ->rules('required')*/

        ];
    }

    /**
     * Возвращает курс btc к указанной валюте
     * @param string $currency
     * @return mixed
     */
    public function getBtc($currency = 'EUR')
    {
        $currencies = Cache::remember('btc', 60, function () use ($currency) {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://blockchain.info/ticker');
            $currencies = json_decode($response->getBody()->getContents(), true);
            return $currencies;
        });

        return $currencies[$currency]['last'];
    }
}
