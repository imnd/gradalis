<?php

namespace App\Nova;

use
    Marketplace\Translatable\Translatable,
    Illuminate\Http\Request,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Text;

abstract class ListModelResource extends Resource
{
    public static $category = 'Franchise';
    /**
     * @inheritdoc
     */
    public static $title = 'translation';
    /**
     * @inheritdoc
     */
    public static $search = ['name'];

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Franchise');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Text::make(__('Title'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            Translatable::make('Перевод', 'translation')
                ->singleLine()
                ->hideFromIndex()
                ->rules('required', 'max:255'),
        ];
    }
}
