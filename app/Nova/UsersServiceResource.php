<?php

namespace App\Nova;

use
    Illuminate\Http\Request,

    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Select,
    Laravel\Nova\Fields\BelongsTo,
    Laravel\Nova\Fields\Image,
    Laravel\Nova\Fields\Text,

    Inspheric\Fields\Indicator,
    Marketplace\Translatable\Translatable,
    Vyuldashev\NovaMoneyField\Money,

    App\Models\Service\UsersService
;

class UsersServiceResource extends Resource
{
    public static $category = 'UsersServices';
    public static $model = UsersService::class;
    public static $title = 'name';
    public static $search = ['id'];

    public static function group()
    {
        return __('UsersServices');
    }

    public static function label()
    {
        return __('UsersServices');
    }

    public static function singularLabel()
    {
        return __('UsersService');
    }

    public function fields(Request $request)
    {
        return [
            ID::make()
                ->sortable()
                ->onlyOnIndex(),

            BelongsTo::make(__('UsersServiceCategory'), 'category', UsersServiceCategory::class),

            BelongsTo::make(__('Performer'), 'performer', User::class),

            Indicator::make(__('fields.status'), 'status')
                ->labels([
                    __('fields.status_draft'),
                    __('fields.status_moderation'),
                    __('fields.status_disabled'),
                    __('fields.status_active'),
                ])
                ->colors([
                    'orange',
                    'green',
                    'red',
                    'green',
                ])
                ->onlyOnIndex()
                ->sortable(),

            Select::make(__('fields.status'), 'status')
                ->options([
                    __('fields.status_draft'),
                    __('fields.status_moderation'),
                    __('fields.status_disabled'),
                    __('fields.status_active')
                ])
                ->displayUsingLabels()
                ->onlyOnForms(),

            Translatable::make(__('fields.title'), 'name')
                ->rules('required'),

            Text::make(__('Permalink'), 'slug')
                ->rules('required'),

            Translatable::make(__('fields.description'), 'description')
                ->onlyOnForms(),

            Image::make(__('Icon'), 'icon')
                ->maxWidth(38)
                ->disk(UsersService::DISK_NAME)
                ->storeAs(function(Request $request) {
                    return sha1($request->icon->getClientOriginalName()) . '.' . $request->icon->getClientOriginalExtension();
                }),

            Money::make(__('Price'), 'EUR', 'price'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(Request $request)
    {
        return [
            new Actions\StatusAwait,
            new Actions\StatusModerated,
            new Actions\StatusApproved,
            new Actions\StatusDeclined
        ];
    }
}
