<?php

namespace App\Nova;

class TaxSystems extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\TaxSystems';

    public static function label()
    {
        return __('TaxSystems');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('TaxSystems');
    }
}
