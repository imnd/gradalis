<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\BelongsTo,
    Marketplace\Translatable\Translatable,
    Laravel\Nova\Fields\Text;

class CertificateDocuments extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\CertificateDocuments';

    public static function label()
    {
        return __('CertificateDocuments');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('CertificateDocument');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Text::make(__('Title'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            BelongsTo::make('Тип документа', 'type', 'App\Nova\CertificateTypes'),

            Translatable::make('Перевод', 'translation')
                ->singleLine()
                ->hideFromIndex()
                ->rules('required', 'max:255'),
        ];
    }


}
