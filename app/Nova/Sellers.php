<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Text,
    Laravel\Nova\Fields\Textarea,
    Laravel\Nova\Http\Requests\NovaRequest,
    App\Models\Roles;

class Sellers extends Resource
{
    public static $model = 'App\Models\Auth\User';

    public static $title = 'id';

    public static $search = [
        'id', 'first_name','last_name', 'email','phone','balance','active','sum_from','sum_to','purchase_date_from','purchase_date_to'
    ];

    /**
     * @var array
     */
    public static $with = ['city'];

    public static function label()
    {
        return __('Sellers');
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->whereHas('roles', function ($query){
            $query->where('name', Roles::ROLE_SELLER);
        })->where('user_id', $request->user()->id);
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Text::make(__('fields.full_name'), function(){
                return $this->full_name;
            })->onlyOnIndex(),

            Text::make(__('fields.email'), 'email')->sortable()->onlyOnIndex(),

            Text::make(__('fields.phone'), 'phone')->onlyOnIndex(),

            Text::make(__('fields.business_category'), function (){
                $categories = $this->business->map(function($item){
                    return $item->category->translation;
                })->unique()->values();
                return implode(', ', $categories->toArray());
            })->onlyOnIndex(),

            // Кто привел / ответственный
            // TODO Добавить миграцию в юзера
            Text::make(__('fields.user_ref_id'), function (){
                return 'Ответственный';
            })->canSee(function ($request) {
                //TODO Добавить главного брокера к миграциям
                return $request->user()->hasAnyRole([Roles::ROLE_ADMIN, Roles::ROLE_MAIN_BROKER]);
            })->onlyOnIndex(),

            Text::make(__('fields.chat_link'), 'chat_link', function (){
                return "<a href=''>ссылка на чат с продавцом</a>";
            })->asHtml(),

            Text::make(__('fields.note'), 'note')->onlyOnIndex(),

            Textarea::make(__('fields.note'), 'note')
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\UserByBusinessCategory,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [
            //new Lenses\SellersList
        ];
    }
}
