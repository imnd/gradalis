<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Text;

class B2C extends Resource
{
    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\B2C';

    /**
     * @inheritdoc
     */
    public static $search = [
        'id'
    ];

    /**
     * @inheritdoc
     */
    public static function availableForNavigation(Request $request)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('B2C');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('B2C');
    }

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Text::make('', 'age_men_from')
                ->rules('required', 'max:255'),

            Text::make('', 'age_men_to')
                ->rules('required', 'max:255'),

            Text::make('', 'age_women_from')
                ->rules('required', 'max:255'),

            Text::make('', 'age_women_to')
                ->rules('required', 'max:255'),

            Text::make('', 'sex_ratio_from')
                ->rules('required', 'max:255'),

            Text::make('', 'sex_ratio_to')
                ->rules('required', 'max:255'),

            Text::make('', 'alone_clients')
                ->rules('required', 'max:255'),

            Text::make('', 'child_clients')
                ->rules('required', 'max:255'),

            Text::make('', 'couples_clients')
                ->rules('required', 'max:255'),

            Text::make('', 'students_clients')
                ->rules('required', 'max:255'),

            Text::make('', 'pensioners_clients')
                ->rules('required', 'max:255'),

            Text::make('', 'average_check_clients')
                ->rules('required', 'max:255'),

            Text::make('', 'family_status_clients')
                ->rules('required', 'max:255'),

            Text::make('', 'social_status_clients')
                ->rules('required', 'max:255'),

            Text::make('', 'gender_target_audience')
                ->rules('required', 'max:255'),

            Text::make('Доля семейных пар с детьми', 'families_with_children_clients')
                ->rules('required', 'max:255'),

            Text::make('Средний уровень доходов целевых клиентов', 'average_income_target_clients_to')
                ->rules('required', 'max:255'),

            Text::make('Средний уровень доходов целевых клиентов', 'average_income_target_clients_from')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('', 'average_income_target_clients_from_to')
                ->rules('required', 'max:255'),

            Text::make('Основная категория партнёров по бизнесу', 'main_category_business_partners')
                ->rules('required', 'max:10'),

            Text::make('Основные рекламные источники привлечение клиентов', 'main_attract_client_adv_src')
                ->rules('required', 'max:255'),
        ];
    }
}
