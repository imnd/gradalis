<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Text;

class B2B extends Resource
{
    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\B2B';

    /**
     * @inheritdoc
     */
    public static $search = [
        'id'
    ];

    /**
     * @inheritdoc
     */
    public static function availableForNavigation(Request $request)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('B2B');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('B2B');
    }

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Text::make('Средний чек клиента', 'average_check_clients')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Наличие действующих контрактов', 'have_existing_contracts')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Основная категория партнёров по бизнесу', 'main_category_business_partners')
                ->sortable()
                ->rules('required', 'max:10'),

            Text::make('Количество бессрочных договров на оказания улсуг', 'count_perpetual_service_contracts')
                ->sortable()
                ->rules('required', 'max:128'),

            Text::make('Основные рекламные источники привлечение клиентов', 'main_attract_client_adv_src')
                ->sortable()
                ->rules('required', 'max:255'),
        ];
    }
}
