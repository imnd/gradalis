<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID,
    Illuminate\Http\Request,
    Laravel\Nova\Fields\Text;
use Money\Number;

class FranchiseStaff extends Resource
{
    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\Staff';

    /**
     * @inheritdoc
     */
    public static $search = [
        'id'
    ];

    /**
     * @inheritdoc
     */
    public static function availableForNavigation(Request $request)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('Staff');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('Staff');
    }

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Number::make('Должность', 'certificate_document_id')
                ->rules('required'),

            Number::make('Зарплата', 'monthly_wages')
                ->rules('required'),

            Number::make('Налоги за месяц', 'tax_amount_per_month')
                ->rules('required'),
        ];
    }
}
