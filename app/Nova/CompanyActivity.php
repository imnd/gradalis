<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Marketplace\IconSelect\IconSelect;
use Marketplace\Translatable\Translatable;
use Treestoneit\BelongsToField\BelongsToField;

class CompanyActivity extends Resource
{

    /**
     * The model the resource corresponds to.
     * @var string
     */
    public static $model = 'App\Models\CompanyActivity';
    /**
     * The single value that should be used to represent the resource when being displayed.
     * @var string
     */
    public static $title = 'name';
    /**
     * The columns that should be searched.
     * @var array
     */
    public static $search = [
        'id',
    ];


    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Company Activity');
    }


    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Translatable::make('Перевод', 'name')
                ->singleLine()
                ->hideFromIndex()
                ->rules('required', 'max:255'),

        ];
    }
}
