<?php

namespace App\Nova;

use App\Nova\Fields\CustomCkeditor;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Marketplace\Translatable\Translatable;

class Vacancy extends Resource
{
    public static $category = "Vacancy";
    public static $group = 'Vacancy';

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Vacancy');
    }


    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Vacancy';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    public static function label()
    {
        return __('Vacancy');
    }

    /**
     * @inheritdoc
     */
    public static $with = ['city'];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $locale = app()->getLocale();

        return [
            ID::make()->sortable()->onlyOnIndex(),
            Translatable::make('Название','name')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            Translatable::make('Зар.плата', 'salary')
                ->singleLine()
                ->indexLocale($locale)
                ->sortable()
                ->rules('required', 'max:255'),

            BelongsTo::make(__('City'), 'city', City::class)->searchable(),

            Translatable::make('Тип занятости', 'work_time')
                ->singleLine()
                ->indexLocale($locale)
                ->sortable()
                ->rules( 'max:255'),

            Translatable::make('Язык', 'language')
                ->singleLine()
                ->indexLocale($locale)
                ->sortable()
                ->rules( 'max:255'),

            CustomCkeditor::make('Описание деятельности', 'description')->hideFromIndex(),

            Translatable::make('SEO_Url (Наименование транслитом, маленькими буквами, пробелы заменяем на дефисы)',
                'url')
                ->singleLine()
                ->indexLocale($locale)
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Title')
                ->singleLine()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Description')
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            Translatable::make('Seo_Keywords')
                ->singleLine()
                ->indexLocale($locale)
                ->hideFromIndex()
                ->rules('required', 'max:255'),
        ];
    }
}
