<?php

namespace App\Nova;

class ConditionEquipments extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\ConditionEquipments';

    public static function label()
    {
        return __('ConditionEquipments');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('ConditionEquipments');
    }
}
