<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID,
    Illuminate\Http\Request,
    Laravel\Nova\Fields\Text,
    Laravel\Nova\Fields\Select,
    Marketplace\Translatable\Translatable,
    Treestoneit\BelongsToField\BelongsToField,
    App\Models\Franchise\FranchiseReview as FranchiseReviewModel;

class FranchiseReview extends ListModelResource
{

    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\FranchiseReview';

    /**
     * @inheritdoc
     */
    public static $search = [
        'id'
    ];

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('Reviews');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('Reviews');
    }

    /**
     * @inheritdoc
     */
    public static $title = 'name';

    /**
     * @inheritdoc
     */
    public static $with = ['franchise'];

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Translatable::make(__('Title'), 'name')
                ->singleLine()
                ->sortable()
                ->indexLocale('ru')
                ->rules('required', 'max:255'),

            Text::make('ID франшизы', function(){
                return '<a href="/franchise/'.$this->franchise_id.'" target="_blank">'.$this->franchise_id.'</a>';
            })->asHtml()->onlyOnIndex(),

            Select::make('Статус','status')
                ->options(FranchiseReviewModel::getStatuses())
                ->hideWhenCreating()
                ->displayUsingLabels()
                ->rules('required'),

            Text::make('Компания', 'company')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Должность', 'position')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('E-mail', 'email')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('URL видео', 'video_url')
                ->sortable()
                ->rules('required', 'max:255'),

            Translatable::make(__('fields.description'), 'description')
                ->trix()
                ->indexLocale('ru')
                ->hideFromIndex()
                ->rules('required', 'max:255'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(Request $request)
    {
        return [
            new Actions\StatusAwait(),
            new Actions\StatusModerated,
            new Actions\StatusApproved,
            new Actions\StatusDeclined
        ];
    }
}
