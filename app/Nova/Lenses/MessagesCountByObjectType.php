<?php

namespace App\Nova\Lenses;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Lenses\Lens;
use Laravel\Nova\Http\Requests\LensRequest;
use DB;

class MessagesCountByObjectType extends Lens
{
    public $name = 'Сообщения по типам обектов';

    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query->join('dialogs', 'dialogs.id', '=', 'messages.dialog_id')
                ->select('dialogs.id', DB::raw(' count( dialogs.id ) AS messages_count'),
                    DB::raw(' if( STRCMP( dialogs.object_type, "App\\\Models\\\Business\\\Business") = 0, "Бизнес", "Франшиза" )  AS object_type_text'),
                    'dialogs.object_type'
                    )->whereNotNull('dialogs.object_type')->groupBy('dialogs.object_type' )
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make('ID', 'id')->sortable(),
            Text::make('Количество сообщений', 'messages_count')->sortable(),
            Text::make('Тип объекта', 'object_type_text')->sortable()//,
            //Text::make('Тип объекта', 'object_type')
        ];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return parent::actions($request);
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'messages-count-by-object-type';
    }
}
