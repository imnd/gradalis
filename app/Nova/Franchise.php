<?php

namespace App\Nova;

use
    Session,
    Illuminate\Http\Request,
    App\Models\Franchise\Franchise as FranchiseModel,
    App\Models\Franchise\AudienceTypes,
    App\Models\Franchise\PropertyCategories,
    App\Models\Franchise\PropertyTypes,
    App\Models\Franchise\OperationRestrictions,
    App\Models\Franchise\TrainingMaterialTypes,
    App\Models\Franchise\Equipments,
    App\Models\Franchise\Penalties,

    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Number,
    Laravel\Nova\Fields\Select,
    Laravel\Nova\Fields\Text,
    Laravel\Nova\Fields\HasOne,
    Laravel\Nova\Fields\HasMany,
    Laravel\Nova\Fields\BelongsTo,
    Arsenaltech\NovaTab\NovaTab,
    Arsenaltech\NovaTab\Tabs,
    Marketplace\Translatable\Translatable,
    Treestoneit\BelongsToField\BelongsToField,
    Vyuldashev\NovaMoneyField\Money,
    Epartment\NovaDependencyContainer\HasDependencies,
    Epartment\NovaDependencyContainer\NovaDependencyContainer
;

class Franchise extends Resource
{
    use Tabs, HasDependencies;

    /**
     * @inheritdoc
     */
    public static $category = 'Franchise';
    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\Franchise';
    /**
     * @inheritdoc
     */
    public static $title = 'name';
    /**
     * @inheritdoc
     */
    public static $search = [
        'name',
    ];
    /**
     * @inheritdoc
     */
    public static $with = ['category', 'city', 'user', 'packages'];

    /**
     * @inheritdoc
     */
    public static function group()
    {
        return __('Franchise');
    }

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('Franchises');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('Franchise');
    }

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        $yesNo = [
            'нет',
            'да',
        ];
        $currency = Session::get('currency')['val'];
        $conditions = [
            [
                'title' => 'Без ничего',
                'value' => 0,
            ],
            [
                'title' => 'Роялти',
                'value' => 1,
            ],
            [
                'title' => 'Паушальный взнос',
                'value' => 2,
            ],
        ];
        $propertyCategories = PropertyCategories::all()->pluck('name', 'id');
        $propertyTypes = PropertyTypes::all()->pluck('name', 'id');
        $operationRestrictions = OperationRestrictions::all()->pluck('name', 'id');
        $trainingMaterialsTypes = TrainingMaterialTypes::all()->pluck('name', 'id');
        $typeTransferCustomerBase = [
            [
                'id' => 1,
                'name' => 'Excel',
            ],
            [
                'id' => 2,
                'name' => 'Бумажная',
            ],
        ];
        $equipmentList = Equipments::all()->pluck('name', 'id');
        $equipmentCondition = [
            [
                'id' => 1,
                'name' => 'Item1',
            ],
            [
                'id' => 2,
                'name' => 'Item2',
            ],
            [
                'id' => 3,
                'name' => 'Item3',
            ],
            [
                'id' => 4,
                'name' => 'Item4',
            ],
        ];
        $audienceTypes = AudienceTypes::all()->pluck('name', 'id');
        $legalStatuses = [
            [
                'id' => 1,
                'name' => 'Item',
            ],
            [
                'id' => 2,
                'name' => 'Item',
            ],
            [
                'id' => 3,
                'name' => 'Item',
            ],
        ];
        $taxSystems = [
            [
                'id' => 1,
                'name' => 'Item',
            ],
            [
                'id' => 2,
                'name' => 'Item',
            ],
            [
                'id' => 3,
                'name' => 'Item',
            ],
        ];
        $penalties = Penalties::all()->pluck('name', 'id');
        $disputable = [
            [
                'id' => 1,
                'name' => 'Item',
            ],
            [
                'id' => 2,
                'name' => 'Item',
            ],
            [
                'id' => 3,
                'name' => 'Item',
            ],
        ];
        return [
            new NovaTab('Шаг 1', [
                ID::make()->sortable()->onlyOnIndex(),

                Translatable::make(__('Title'), 'name')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->rules('required', 'max:255'),

                BelongsToField::make('Категория', 'category', FranchiseCategory::class),

                BelongsToField::make('Подкатегория (направление)', 'subCategory', FranchiseSubcategory::class),

                Number::make('Год основания', 'foundation_year')
                    ->max(date('Y'))
                    ->min(1900)
                    ->hideFromIndex()
                    ->rules('required'),

                Select::make('Условия покупки', 'condition')
                    ->options($conditions)
                    ->hideFromIndex()
                    ->rules('required'),

                Number::make('Роялти', 'roality')
                    ->hideFromIndex()
                    ->rules('required'),

                Money::make('Цена', $currency, 'price')
                    ->rules('required'),

                Select::make('Цена с учетом НДС', 'price_including_vat_bool')
                    ->options($yesNo)
                    ->rules('required'),

                Number::make('Скидка', 'discount')
                    ->hideFromIndex()
                    ->rules('required'),

                Select::make('Имеет ли франшиза разные пакеты при продаже', 'have_packages')
                    ->options($yesNo)
                    ->hideFromIndex()
                    ->rules('required'),

                # if have_packages
                HasMany::make(__('Packages'), 'packages', FranchisePackage::class)
                    ->withMeta([
                        'indexName' => __('Packages'),
                        'name'      => __('Packages')
                    ])
                    ->singularLabel(__('Package')),
                Select::make('Имеет ли франшиза видеообзор?', 'video_exists')
                    ->options($yesNo)
                    ->hideFromIndex()
                    ->rules('required'),
                Select::make('Статус видеообзора', 'video_status')
                    ->options(FranchiseModel::getVideoStatuses())
                    ->hideFromIndex()
                    ->rules('required'),
                Text::make('Название видеообзора', 'video_title')
                    ->hideFromIndex(),
                Text::make('Ссылка на видеообзор', 'video_url')
                    ->hideFromIndex(),
                BelongsToField::make('Страна', 'country', Country::class)
                    ->searchable(),

                BelongsToField::make('Регион', 'region', Region::class)
                    ->searchable(),

                BelongsToField::make('Город', 'city', City::class)
                    ->searchable(),

                Translatable::make(__('fields.description'), 'description')
                    ->trix()
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),

                // не появляется
                HasMany::make(__('FranchiseAddresses'), 'addresses', FranchiseAddresses::class),

                Text::make('Название юридического лица', 'name_legal_entity')
                    ->hideFromIndex(),

                Text::make('Название бизнеса', 'name_business')
                    ->hideFromIndex(),
            ]),
            new NovaTab('Шаг 2', [
                Number::make('Рентабельность', 'profitability')
                    ->hideFromIndex()
                    ->rules('required'),

                Money::make('Прибыль', $currency, 'profit')
                    ->hideFromIndex()
                    ->rules('required'),

                Money::make('Обороты в месяц', $currency, 'turnover_month')
                    ->hideFromIndex(),

                Money::make('Обороты в год', $currency, 'turnover_year')
                    ->hideFromIndex(),

                Money::make('Общие расходы', $currency, 'general_expenses')
                    ->hideFromIndex(),

                Money::make('Расходы в месяц', $currency, 'month_expenses')
                    ->hideFromIndex(),

                Money::make('Затраты на персонал', $currency, 'staff_costs')
                    ->hideFromIndex(),

                Money::make('Затраты на коммерческую деятельность в год', $currency, 'cost_business_per_year')
                    ->hideFromIndex(),

                Money::make('Затраты на коммерческую деятельность в квартал', $currency, 'cost_business_per_quarter')
                    ->hideFromIndex(),

                Number::make('Сроки запуска', 'launch_dates')
                    ->hideFromIndex(),

                Number::make('Выход на окупаемость', 'return_payback')
                    ->hideFromIndex(),

                Number::make('Выход на операционную прибыль', 'operating_profit')
                    ->hideFromIndex(),

                /*// ЭТО ВЫЗЫВАЕТ ОШИБКУ
                Translatable::make('Обоснование финансовых показателей', 'justification_financial_indicators')
                    ->trix()
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),*/
            ]),
            new NovaTab('Шаг 3', [
                Select::make('Тип собственности объекта', 'category_property_id')
                    ->options($propertyCategories)
                    ->hideFromIndex(),

                Select::make('Тип недвижимости объекта', 'property_type_id')
                    ->options($propertyTypes)
                    ->hideFromIndex(),

                Number::make('Количество кв. метров', 'number_square_meters')
                    ->hideFromIndex(),

                Money::make('Цена за кв. метр', $currency, 'price_square_meters')
                    ->hideFromIndex(),

                Money::make('Цена с учетом НДС', $currency, 'price_including_vat')
                    ->hideFromIndex(),

                Select::make('Правоустанавливающие документы', 'title_documents')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Ограничения по эксплуатации', 'restrictions_operation')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Укажите ограничения', 'list_restrictions_operation')
                    ->options($operationRestrictions)
                    ->hideFromIndex(),

                Select::make('Справка из регистра недв. и зданий', 'ref_register_estate')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Технический план недвижимости', 'technical_property_plan')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Согласование перепланировок', 'coordination_redevelopment')
                    ->options($yesNo)
                    ->hideFromIndex(),
            ]),
            new NovaTab('Шаг 4', [
                Select::make('Передаете ли схемы работы?', 'transfer_work_schemes')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Number::make('Сколько времени будете обучать?', 'month_teach')
                    ->hideFromIndex(),

                Select::make('Готовы ли обучающие материалы?', 'ready_training_materials')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Тип обучающих материалов', 'type_training_materials')
                    ->options($trainingMaterialsTypes)
                    ->hideFromIndex(),

                Select::make('Передаёте базы клиентов?', 'transfer_customer_base')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Тип передачи базы', 'type_transfer_customer_base')
                    ->options($typeTransferCustomerBase)
                    ->hideFromIndex(),

                HasMany::make(__('Staff'), 'staffs', FranchiseStaff::class)
                    ->singularLabel(__('Staff')),

                HasMany::make(__('Certificates'), 'certificates', FranchiseCertificates::class)
                    ->singularLabel(__('Certificates')),
            ]),

            new NovaTab('Шаг 5', [
                Select::make('Входит ли в стоимость покупки оборудование', 'included_equipment')
                    ->options($yesNo)
                    ->hideFromIndex(),

                NovaDependencyContainer::make([
                    # included_equipment == нет
                    Select::make('Перечень оборудования входящее в стоимость покупки', 'list_equipment')
                        ->options($equipmentList)
                        ->hideFromIndex(),

                    Select::make('Состояние имущества (по оценке собственника)', 'condition_equipment_id')
                        ->options($equipmentCondition)
                        ->hideFromIndex(),
                ])->dependsOn('included_equipment', 0),
                NovaDependencyContainer::make([
                    # included_equipment == да
                    Select::make('Скидка на покупку оборудования', 'discount_equipment')
                        ->options($yesNo)
                        ->hideFromIndex(),

                    NovaDependencyContainer::make([
                        # franchise.discount_equipment === 0
                        Number::make('Общая стоимость с учетом скидки', 'total_amount_equipment_with_discount')
                            ->hideFromIndex(),
                    ])->dependsOn('discount_equipment', 0),

                    Number::make('Общая сумма на оборудование', 'total_amount_equipment')
                        ->hideFromIndex(),
                ])->dependsOn('included_equipment', 1),

                Select::make('Обучение персонала работе на оборудовании', 'staff_training_equipment')
                    ->options($yesNo)
                    ->hideFromIndex(),
            ]),
            new NovaTab('Шаг 6', [
                HasMany::make(__('Objects'), 'objects', Objects::class)
                    ->hideFromIndex(),
            ]),
            new NovaTab('Шаг 7', [
                Select::make('Тип аудитории на которую расчитан бизнес', 'type_audience')
                    ->options($audienceTypes)
                    ->hideFromIndex(),

                # franchise.type_audience == 1 || franchise.type_audience == 3
                HasOne::make(__('B2C'), 'b2c', B2C::class)
                    ->hideFromIndex(),

                # franchise.type_audience === 2 || franchise.type_audience === 3
                HasOne::make(__('B2B'), 'b2b', B2B::class)
                    ->hideFromIndex(),
            ]),
            new NovaTab('Шаг 8', [
                Select::make('Юридический статус', 'legal_status')
                    ->options($legalStatuses)
                    ->hideFromIndex(),

                Select::make('Система налогообложения', 'tax_system')
                    ->options($taxSystems)
                    ->hideFromIndex(),

                /*
                Select::make('Были изменения профиля деятельности в юридическом лице?', 'changes_profile_legal_entity')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Были непокрытые штрафные санкции, наложенные фискальными органами?', 'have_penalties')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Укажите штрафные санкции', 'list_penalties')
                    ->options($penalties)
                    ->hideFromIndex(),

                Select::make('Вовлечение в спорные коммерческие/административные ситуации?', 'have_disputable_situations')
                    ->options($yesNo)
                    ->hideFromIndex(),

                Select::make('Укажите типы спорных ситуаций', 'list_disputable_situations')
                    ->options($disputable)
                    ->hideFromIndex(),
                */
            ]),
            new NovaTab('Seo', [
                Translatable::make('Seo_Title')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),

                Translatable::make('Seo_Description')
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),

                Translatable::make('Seo_Keywords')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex()
                    ->rules('required', 'max:255'),
            ]),
            new NovaTab('Маркетинг', [

                Translatable::make('Вк таргет', 'metrics_vk')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),

                Translatable::make('Однокласники', 'metrics_ok')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),

                Translatable::make('Фейсбук', 'metrics_fb')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),

                Translatable::make('Гугл', 'metrics_google')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),

                Translatable::make('Мейлрутаргет', 'metrics_mail')
                    ->singleLine()
                    ->indexLocale('ru')
                    ->hideFromIndex(),
            ]),

            new NovaTab('Остальное', [
                Number::make('Окупаемость', 'payback')
                    ->hideFromIndex()
                    ->rules('required'),

                Number::make('Год старта', 'start_year')
                    ->hideFromIndex()
                    ->rules('required'),

                Number::make('Количество предприятий', 'own_enterprices')
                    ->hideFromIndex()
                    ->rules('required'),

                Number::make('Комиссия', 'commission')
                    ->hideWhenCreating()
                    ->rules('required'),

                Select::make('Статус', 'status')
                    ->options(FranchiseModel::getStatuses())
                    ->hideWhenCreating()
                    ->displayUsingLabels()
                    ->rules('required'),

                BelongsTo::make(__('Broker'), 'broker', Brokers::class),

                BelongsToField::make(__('Owner'), 'owner', User::class)
                    ->searchable(),
            ]),
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(Request $request)
    {
        return [
            new Actions\FranchiseStatusAwait,
            new Actions\FranchiseStatusModerated,
            new Actions\FranchiseStatusApproved,
            new Actions\FranchiseStatusDeclined
        ];
    }
}
