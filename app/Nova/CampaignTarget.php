<?php

namespace App\Nova;

use Arsenaltech\NovaTab\NovaTab;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Marketplace\Translatable\Translatable;
use Session;
use Treestoneit\BelongsToField\BelongsToField;
use Vyuldashev\NovaMoneyField\Money;

class CampaignTarget extends Resource
{
    public static $category = "Referral";
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Referral\CampaignTarget';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            Translatable::make(__('Title'), 'name'),
            Money::make('CPL', Session::get('currency')['val'], 'cpl'),
            Money::make('CPS', Session::get('currency')['val'], 'cps'),
            Money::make('CPA', Session::get('currency')['val'], 'cpa'),
            new NovaTab('Связи', [
                BelongsToField::make('Страна', 'country', 'App\Nova\Country')->searchable(),
                BelongsToField::make('Оффер', 'campaign', 'App\Nova\Campaign')->searchable(),
            ])
        ];
    }
}
