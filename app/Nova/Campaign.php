<?php

namespace App\Nova;

use App\Models\Referral\Campaign as CampaignModel;
use Arsenaltech\NovaTab\NovaTab;
use Arsenaltech\NovaTab\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Marketplace\Translatable\Translatable;

class Campaign extends Resource
{
    use Tabs;

    public static $category = "Referral";

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Referral');
    }
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Referral\Campaign';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Campaigns');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            Translatable::make(__('Title'), 'name')
                ->singleLine()
                ->indexLocale('ru')
                ->rules('required', 'max:255'),
            Translatable::make(__('fields.description'), 'description')
                ->trix()
                ->indexLocale('ru')
                ->hideFromIndex()
                ->rules('required', 'max:255'),
            Text::make('Сайт', 'site'),
            Number::make('Clt', 'clt_days'),
            Number::make('Одобрение', 'approve_days'),
            Number::make('Выплата', 'pay_days'),
            new NovaTab('Статус', [
                Select::make('Статус', 'status')
                    ->options(CampaignModel::getStatuses())
                    ->displayUsingLabels()
                    ->rules('required'),
                Select::make('Тип', 'campaign_status')
                    ->options(CampaignModel::getCampaignStatuses())
                    ->displayUsingLabels()
                    ->rules('required'),
                Select::make('Направление', 'user_type')
                    ->options(CampaignModel::getUserTypes())
                    ->displayUsingLabels()
                    ->rules('required'),
            ]),
            new NovaTab('Связи', [
                MorphTo::make('Взаимодействует','referable')->types([
                    Franchise::class,
                    Business::class,
                ]),
                BelongsToMany::make('Условия', 'conditions', 'App\Nova\Condition')->fields(function () {
                    return [
                        Select::make('Статус', 'status')
                            ->options(CampaignModel::getConditionStatuses())
                            ->displayUsingLabels(),
                    ];
                }),
            ]),
//            'target_id', 'type',
        ];
    }
}
