<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID,
    Illuminate\Http\Request,
    Laravel\Nova\Fields\Text,
    Laravel\Nova\Fields\Select,
    Marketplace\Translatable\Translatable,
    Laravel\Nova\Fields\Number,
    Treestoneit\BelongsToField\BelongsToField,
    App\Models\Franchise\FranchiseObject as FranchiseObjectModel;

class FranchiseObject extends ListModelResource
{
    public static $category = 'Franchise';

    /**
     * @inheritdoc
     */
    public static $model = 'App\Models\Franchise\FranchiseObject';

    /**
     * @inheritdoc
     */
    public static $search = [
        'id'
    ];

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Franchise');
    }

    /**
     * @inheritdoc
     */
    public static function label()
    {
        return __('Objects');
    }

    /**
     * @inheritdoc
     */
    public static function singularLabel()
    {
        return __('Objects');
    }

    /**
     * @inheritdoc
     */
    public static $title = 'title';

    /**
     * @inheritdoc
     */
    public static $with = ['franchise'];

    /**
     * @inheritdoc
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Translatable::make(__('Title'), 'title')
                ->singleLine()
                ->sortable()
                ->indexLocale('ru')
                ->rules('required', 'max:255'),

            Text::make('ID франшизы', function(){
                return '<a href="/franchise/'.$this->franchise_id.'" target="_blank">'.$this->franchise_id.'</a>';
            })->asHtml()->onlyOnIndex(),

            Select::make('Статус','status')
                ->options(FranchiseObjectModel::getStatuses())
                ->hideWhenCreating()
                ->displayUsingLabels()
                ->rules('required'),

            Text::make('Адрес', 'address')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Телефон', 'phone')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('E-mail', 'email')
                ->sortable()
                ->rules('required', 'max:255'),

            Number::make('Latitude', 'lat')
                ->hideFromIndex()
                ->rules('required'),

            Number::make('Longitude', 'lng')
                ->hideFromIndex()
                ->rules('required'),

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(Request $request)
    {
        return [
            new Actions\StatusAwait(),
            new Actions\StatusModerated,
            new Actions\StatusApproved,
            new Actions\StatusDeclined
        ];
    }
}
