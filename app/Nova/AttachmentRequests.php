<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Laravel\Nova\Fields\BelongsTo,
    Laravel\Nova\Fields\DateTime,
    App\Models\Franchise\Franchise,
    Laravel\Nova\Http\Requests\NovaRequest
;

class AttachmentRequests extends Resource
{
    public static $category = 'Broker';
    public static $model = Franchise::class;
    public static $title = '';
    public static $search = [
        'id',
    ];
    public static $with = ['owner'];

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query
            ->whereNull('broker_id')
            ->where('sell_with_broker', Franchise::SELL_WITH_BROKER)
        ;
    }

    public static function label()
    {
        return __('AttachmentRequests');
    }

    public static function singularLabel()
    {
        return  __('AttachmentRequest');
    }

    public function fields(Request $request)
    {
        return [
            BelongsTo::make(__('Owner'), 'owner', User::class)
                ->hideWhenUpdating(),
            BelongsTo::make(__('Broker'), 'broker', User::class)
                ->hideFromIndex(),

            DateTime::make(trans('fields.created_at'), 'created_at')
                ->onlyOnIndex()->sortable(),
        ];
    }
}
