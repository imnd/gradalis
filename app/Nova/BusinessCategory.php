<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Marketplace\IconSelect\IconSelect;
use Marketplace\Translatable\Translatable;
use Treestoneit\BelongsToField\BelongsToField;

class BusinessCategory extends Resource
{
    public static $category = "Business";
    /**
     * The model the resource corresponds to.
     * @var string
     */
    public static $model = 'App\Models\Business\BusinessCategory';
    /**
     * The single value that should be used to represent the resource when being displayed.
     * @var string
     */
    public static $title = 'translation';
    /**
     * The columns that should be searched.
     * @var array
     */
    public static $search = [
        'id',
    ];
    public static $with = ['parent'];

    /**
     * @return array|string|null
     */
    public static function group()
    {
        return __('Business');
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Business categories');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Business category');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Text::make(__('Title'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            IconSelect::make(__('Icon'), 'icon')->init(),

            Translatable::make('Перевод', 'translation')
                ->singleLine()
                ->hideFromIndex()
                ->rules('required', 'max:255'),

            BelongsToField::make('Родитель', 'parent', 'App\Nova\BusinessCategory')->nullable(),
        ];
    }
}
