<?php

namespace App\Nova;

use Arsenaltech\NovaTab\NovaTab;
use Arsenaltech\NovaTab\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Treestoneit\BelongsToField\BelongsToField;

class Invitation extends Resource
{
    use Tabs;
    public static $category = "Referral";
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Referral\Invitation';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Invitations');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            Text::make('Суб аккаунт', 'sub_account'),
            Text::make('Токен', 'token'),
            Number::make('Статус', 'status'),
            new NovaTab('Связи', [
                BelongsToField::make('Оффер', 'campaign', 'App\Nova\Campaign')->searchable(),
                BelongsToField::make('Ресурс', 'resource', 'App\Nova\CampaignResource')->searchable(),
                BelongsToField::make('Партнер', 'partner', 'App\Nova\Partner')->searchable(),
            ]),
        ];
    }
}
