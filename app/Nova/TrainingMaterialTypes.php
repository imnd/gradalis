<?php

namespace App\Nova;

class TrainingMaterialTypes extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\TrainingMaterialTypes';

    public static function label()
    {
        return __('TrainingMaterialTypes');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('TrainingMaterialType');
    }
}
