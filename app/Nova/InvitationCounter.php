<?php

namespace App\Nova;

use App\Models\Referral\CounterTarget;
use Arsenaltech\NovaTab\NovaTab;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Session;
use Treestoneit\BelongsToField\BelongsToField;
use Vyuldashev\NovaMoneyField\Money;

class InvitationCounter extends Resource
{
    public static $category = "Referral";
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Referral\InvitationCounter';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('InvitationCounters');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            Text::make('Ip'),
            Text::make('Http Referrer'),
            Text::make('Токен', 'token'),
            Text::make('Колличество переходов', 'count'),
            Boolean::make('Одобренно', 'approved'),
            new NovaTab('Связи', [
                BelongsToField::make('Приглашение', 'invitation', 'App\Nova\Invitation'),
                BelongsToField::make('Пользователь', 'user', 'App\Nova\User'),
                BelongsToMany::make('Таргеты', 'targets', 'App\Nova\CampaignTarget')->fields(function () {
                    return [
                        Select::make('Статус', 'status')
                            ->options(CounterTarget::getStatuses())
                            ->displayUsingLabels(),
                        Select::make('Тип', 'type')
                            ->options(CounterTarget::getTypes())
                            ->displayUsingLabels(),
                        Money::make('Сумма', Session::get('currency')['val'], 'sum')
                    ];
                }),
            ]),
        ];
    }
}
