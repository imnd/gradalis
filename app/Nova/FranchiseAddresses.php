<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Text,
    Laravel\Nova\Fields\Number
;

class FranchiseAddresses extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\Address';

    /**
     * @inheritdoc
     */
    public static function availableForNavigation(Request $request)
    {
        return false;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),
            Text::make('Адрес', 'address')
                ->rules('required', 'max:255'),
            Text::make('Номер дома', 'number_house')
                ->rules('required', 'max:20'),
            Text::make('Корпус, строение', 'housing_house')
                ->rules('required', 'max:20'),
            Text::make('Номер офиса', 'number_office')
                ->rules('required', 'max:20'),
            Number::make('Индекс', 'index')
                ->rules('required', 'max:20'),
        ];
    }

    public static function label()
    {
        return __('FranchiseAddresses');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('FranchiseAddress');
    }
}
