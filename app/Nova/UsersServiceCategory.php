<?php

namespace App\Nova;

use
    Illuminate\Http\Request,
    Laravel\Nova\Fields\ID,
    Laravel\Nova\Fields\Image,
    App\Models\Service\UsersService,
    Marketplace\Translatable\Translatable
;

class UsersServiceCategory extends Resource
{
    public static $category = 'UsersServices';
    public static $model = 'App\Models\Service\UsersServiceCategory';
    public static $title = 'name';
    public static $search = [
        'id', 'name',
    ];

    public static function group()
    {
        return __('UsersServices');
    }

    public static function label()
    {
        return __('Categories');
    }

    public static function singularLabel()
    {
        return  __('Category of services');
    }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->onlyOnIndex(),

            Translatable::make(__('Title'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            Image::make(__('Icon'), 'icon')
                ->maxWidth(38)
                ->disk(UsersService::DISK_NAME)
                ->storeAs(function (Request $request) {
                    return sha1($request->icon->getClientOriginalName()) . '.' . $request->icon->getClientOriginalExtension();
                }),
        ];
    }
}
