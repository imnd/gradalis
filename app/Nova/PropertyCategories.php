<?php

namespace App\Nova;

class PropertyCategories extends ListModelResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Franchise\PropertyCategories';

    public static function label()
    {
        return __('PropertyCategories2');
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return  __('PropertyCategories2');
    }
}
