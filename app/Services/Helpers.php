<?php
/**
 * Created by PhpStorm.
 * User: ipitchkhadze
 * Date: 22.01.2019
 * Time: 17:41
 */

namespace App\Services;


use Carbon\Carbon;

class Helpers
{
    public static function transliterate($string)
    {
        $str = mb_strtolower($string, 'UTF-8');

        $leter_array = array(
            'a'    => 'а',
            'b'    => 'б',
            'v'    => 'в',
            'g'    => 'г',
            'd'    => 'д',
            'e'    => 'е,э',
            'jo'   => 'ё',
            'zh'   => 'ж',
            'z'    => 'з',
            'i'    => 'и,i',
            'j'    => 'й',
            'k'    => 'к',
            'l'    => 'л',
            'm'    => 'м',
            'n'    => 'н',
            'o'    => 'о',
            'p'    => 'п',
            'r'    => 'р',
            's'    => 'с',
            't'    => 'т',
            'u'    => 'у',
            'f'    => 'ф',
            'kh'   => 'х',
            'ts'   => 'ц',
            'ch'   => 'ч',
            'sh'   => 'ш',
            'shch' => 'щ',
            ''     => 'ъ',
            'y'    => 'ы',
            ''     => 'ь',
            'yu'   => 'ю',
            'ya'   => 'я',
        );

        foreach ($leter_array as $leter => $kyr) {
            $kyr = explode(',', $kyr); // кирилические строки разобьем в массив с разделителем запятая.
            // в строке $str мы пытаемся отыскать символы кирилицы $kyr и все найденные совпадения заменяем на ключи $leter
            $str = str_replace($kyr, $leter, $str);
        }

        // теперь необходимо учесть правильность формирования URL
        // поиск и замена по регулярному выражению.
        // перв. выраж. указываем рег выражение. втор.выраж. строка или массив строк для замены
        //   //  регуляр выражение  ()+  может повторяться 1 и более раз.,   \s пробельный символ сразу же заменяется на '-'
        // | Логическое или. либо то условие либо что указано справа от |  притом справа укажем диапазон [A-Za-z0-9-]
        //  ^ Логическое отрицание. т.е. заменяем либо пробельный символ на тире, либо любой другой символ, что не входит в указанный диапазон.
        $str = preg_replace('/(\s|[^A-Za-z0-9-])+/', '-', $str);
        $str = trim($str, '-'); // если в конце появится тире, то его удаляем.

        return $str;
    }

    public function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public function isJson($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    public function formatPrice($price, $sign = true, $cur = null)
    {
        $user_currency = session()->get('currency');
        $dec = (($user_currency['val'] == 'BTC' && $cur == null) || $cur == 'BTC') ? 3 : 0;
        $currencies = config('currency.currencies');
        $rates = config('currency.rates');

        if ($cur !== null) {
            $price *= $rates[$user_currency['val'] . '_EUR'];
            $price *= $rates['EUR_' . $cur];
        } else {

        }
        if ($cur !== null) {
            $user_currency['sign'] = $currencies[$cur]['sign'];
        }
        if ($sign) {
            return number_format($price, $dec, ',', ' ') . ' ' . $user_currency['sign'];
        } else {
            return number_format($price, $dec, ',', ' ');
        }

    }

    public function stepsGenerator($options, $pKey = null)
    {
        $str = '<div class="object__info__list is-size-875 has-text-dark-blue">';
        foreach ($options as $key => $value) {

            $v = self::getValue($value, $key, $pKey);
            $key = ($pKey !== null && ($pKey == 'staff' || $pKey == 'contractors' || $pKey == 'certificates' || $pKey == 'leavingStaff')) ? $pKey . '.' . $key : $key;
            if ($v === false) continue;
            $str .= '<div class="object__info__list__item">' .
                '<span>' . __('business.create.' . $key . '.title') . ': </span>' .
                '<span>' . $v . '</span>' .
                ' </div > ';

        }
        $str .= '</div><hr>';
        return $str;

    }

    private function getValue($value, $key, $pKey = null)
    {
        if (!isset($value['type'])) {
            return false;
        }

        if ($value['type'] == 'object') {
            foreach ($value['val'] as $val) {
                return $this->stepsGenerator($val, $key);
            }
        } elseif ($value['type'] == 'translation') {
            return $value['val'][app()->getLocale()];
        } elseif ($value['type'] == 'select') {
            if ($pKey !== null && $pKey !== 'objects') {
                $key = $pKey . '.' . $key;
            }
            $options = __('business.create.' . $key . '.options');
            foreach ($options as $option) {
                if ($option['id'] == $value['val']) {
                    return $option['name'];
                }
            }
        } elseif ($value['type'] == 'mselect') {
            if ($pKey !== null && $pKey !== 'objects') $key = $pKey . '.' . $key;
            $str = '';
            $options = __('business.create.' . $key . '.options');
            foreach ($options as $option) {
                foreach ($value['val'] as $i) {
                    if ($option['id'] == $i) {
                        $str .= ' / ' . $option['name'];
                    }
                }
            }
            return $str;
        } elseif ($value['type'] == 'radio') {
            return false;
        } elseif ($value['type'] == 'price') {
            return format_price($value['val']);
        } elseif ($value['type'] == 'percent') {
            return false;
        } elseif ($value['type'] == 'fromto') {
            dd($value);
            return false;
        } elseif ($value['type'] == 'yn') {
            if ($value['val'] == 1) {
                return 'Да';
            } else {
                return 'Нет';
            }
        } elseif ($value['type'] == 'have') {
            if ($value['val'] == 1) {
                return 'Есть';
            } else {
                return 'Нет';
            }
        } elseif ($value['type'] == 'date') {
            return Carbon::parse($value['val'])->format('d.m.Y');
        } elseif ($value['type'] == 'number' || $value['type'] == 'string') {
            return $value['val'];
        } elseif ($value['type'] == 'checkbox') {
            return ($value['val'] != null) ? 'Да' : 'Нет';
        } else {
            \Log::info($value);
            \Log::info($key);
            \Log::info($pKey);

        }
    }
}
