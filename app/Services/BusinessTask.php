<?php
/**
 * Created by PhpStorm.
 * User: MIKHAIL
 * Date: 14.03.2019
 * Time: 15:25
 */

namespace App\Services;

use App\AbstractClasses\Task;
use App\Models\Language;
use App\Models\Roles;

class BusinessTask extends Task
{
    protected $schema;

    public function __construct($object = null)
    {
        parent::__construct();
        $languages = Language::all();

        $this->schema = [
            1 => [
                Roles::ROLE_SELLERS_BROKER,
            ]
        ];

        foreach ($languages as $language) {
            $lang = $language->lang;
            if ($object !== null && $object->lang !== $lang) {
                $this->schema[2][] = Roles::ROLE_TRANSLATOR . " $lang";
            }
        }

        foreach ($languages as $language) {
            $this->schema[3][] = Roles::ROLE_SEO . " {$language->lang}";
        }

        foreach ($languages as $language) {
            $this->schema[4][] = Roles::ROLE_MARKETER . " {$language->lang}";
        }

        $this->schema[5] = [
            Roles::ROLE_MAIN_BROKER,
        ];
        $this->schema[6] = [
            Roles::ROLE_REGION_ADMIN,
        ];
    }

    public function typeToStep($taskType): array
    {
        return $this->schema[$taskType];
    }
}
