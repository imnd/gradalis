<?php
/**
 * Created by PhpStorm.
 * User: MIKHAIL
 * Date: 14.03.2019
 * Time: 15:19
 */

namespace App\AbstractClasses;

use App\Models\Auth\User;
use App\Models\Task as TaskModel;


abstract class Task
{
    protected $schema;
    protected $automated;

    protected function __construct()
    {
        $this->automated = false;
    }

    public function assignTask($target_id, $target_type, $step, $task_type, $worker_id = null)
    {
        if ($this->automated) {
            $st = $this->getNext($this->schema, $step);
        } else {
            $st = $this->typeToStep($task_type);
        }
        foreach ($st as $role) {
            if ($this->automated) {
                $worker = (object)$this->getWorker($role)->id;
            } else {
                $worker = $worker_id;
            }
            $data = [
                'task_type'   => $task_type,
                'target_type' => $target_type,
                'target_id'   => $target_id,
                'status'      => TaskModel::STATUS_AWAIT,
                'from_id'     => auth()->user()->id,
            ];
            if ($worker !== null) {
                $data['to_id'] = $worker;
            }
            $task = TaskModel::create($data);
            if ($task) {
                return true;
            }
        }
    }

    /**
     * @param $array
     * @param $key
     * @return mixed
     */
    private function getNext($array, $key)
    {
        $currentKey = key($array);
        while ($currentKey !== null && $currentKey != $key) {
            next($array);
            $currentKey = key($array);
        }
        return next($array);
    }

    abstract protected function typeToStep($task_type): array;

    /**
     * @param $role
     * @return User
     */
    protected function getWorker($role): User
    {
        return User::role($role)->with('tasks')->get()->sortBy(function ($user) {
            return $user->tasks->count();
        })->first();
    }

    public function approveTask($target_id, $target_type, $step, $task_type, $task_id = null)
    {
        $this->closeTask($task_id);

        $tasks = TaskModel::where('target_id', $target_id)
            ->where('target_type', $target_type)
            ->whereIn('status', [TaskModel::STATUS_AWAIT])
            ->get();

        if ($tasks->isEmpty()) {
            $st = $this->getNext($this->schema, $step);
            foreach ($st as $role) {
                $worker = (object)$this->getWorker($role);
                TaskModel::create([
                    'task_type'   => $task_type,
                    'target_type' => $target_type,
                    'target_id'   => $target_id,
                    'status'      => TaskModel::STATUS_AWAIT,
                    'from_id'     => auth()->user()->id,
                    'to_id'       => $worker->id
                ]);
            }
        }
    }

    /**
     * @param $task_id
     */
    private function closeTask($task_id): void
    {
        if ($task_id !== null) {
            $task = TaskModel::find($task_id);
            $task->status = TaskModel::STATUS_APPROVED;
            $task->save();
        }
    }

    /**
     * @param $target_id
     * @param $target_type
     * @param $step
     */
    public function rejectTask($target_id, $target_type, $step)
    {
        $step = $this->schema[$step];
    }
}
