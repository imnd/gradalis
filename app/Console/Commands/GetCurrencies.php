<?php

namespace App\Console\Commands;

use Config;
use File;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class GetCurrencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marketplace:get-currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Currencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $currencies_array = Config::get('currency.currencies');
        $main_currency = Config::get('currency.main');
        unset($currencies_array[$main_currency]);

        $q = "";
        foreach ($currencies_array as $currency => $data) {
            $q .= $main_currency . "_" . $currency . ',';
            $q .= $currency . "_" . $main_currency;
            if ($data !== end($currencies_array)) {
                $q .= ",";
            }
        }
        $client = new Client([
            'base_uri' => 'https://free.currencyconverterapi.com/api/v6/',
            'timeout'  => 2.0,
        ]);
        $api_response = $client->request('GEt', 'convert', [
            'query' => [
                'q'       => $q,
                'compact' => 'ultra',
                'apiKey'  => config('currency.cca_app_id')
            ]
        ]);
        $currencies = json_decode($api_response->getBody()->getContents(), true);
        $currencies['EUR_EUR'] = 0;
        $rates = [
            'rates' => $currencies
        ];
        $config_currencies = Config::get('currency');
        $array = array_merge($config_currencies, $rates);
        $data = var_export($array, 1);
        if (File::put(base_path() . '/config/currency.php', "<?php\n return $data ;")) {
            return true;
        }
        return false;

    }
}
