<?php

namespace App\Traits;

use Storage;

trait HasImage
{
    public function getPhotoUrlAttribute()
    {
        return Storage::disk(self::DISK_NAME)->url($this->photo);
    }

    public function getIconUrlAttribute()
    {
        return Storage::disk(self::DISK_NAME)->url($this->icon);
    }
}
