<?php

namespace App\Traits;


use App\Models\Referral\CounterTarget;
use App\Models\Referral\InvitationCounter;
use App\Models\Referral\Partner;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait PartnerSummary
{
    public function getTargetsAttribute()
    {
        if (!array_key_exists('targets', $this->relations)) {
            $this->targets();
        }

        return $this->getRelation('targets');
    }

    public function targets()
    {
        $targets = CounterTarget::join('invitation_counter', 'counter_target.counter_id', '=', 'invitation_counter.id')
            ->join('invitations', 'invitation_counter.invitation_id', '=', 'invitations.id')
            ->where('invitations.partner_id', $this->getkey())
            ->get();

        $hasMany = new HasMany(Partner::query(), $this, 'invitations.partner_id', 'id');

        $hasMany->matchMany(array($this), $targets, 'targets');

        return $this;
    }

//    public function getCountersAttribute()
//    {
//        if (!array_key_exists('counters', $this->relations)) {
//            $this->targets();
//        }
//
//        return $this->getRelation('counters');
//    }
//
//    public function counters()
//    {
//        $counters = InvitationCounter::join('invitations', 'invitation_counter.invitation_id', '=', 'invitations.id')
//            ->where('invitations.partner_id', $this->getkey())
//            ->get();
//
//        $hasMany = new HasMany(Partner::query(), $this, 'invitations.partner_id', 'id');
//
//        $hasMany->matchMany(array($this), $counters, 'counters');
//
//        return $this;
//    }
}
