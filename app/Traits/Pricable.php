<?php

namespace App\Traits;

use Config;

trait Pricable
{
    public function getPricableAttributeValue($key)
    {
        if (!$this->isPricableAttribute($key)) {
            return false;
        }
        return $this->getPrice($key);
    }

    public function isPricableAttribute(string $key): bool
    {
        return in_array($key, $this->getPricableAttributes());
    }

    public function getPricableAttributes(): array
    {
        return is_array($this->pricable)
            ? $this->pricable
            : [];
    }

    public function getPrice(string $key)
    {
        if (is_json($this->attributes[$key])) {
            $json = json_decode($this->attributes[$key], true);
            return $this->formatJson($json);
        }
        $price = $this->attributes[$key];
        $user_currency = session()->get('currency');
        if (empty($user_currency['val']) || $user_currency['val'] == 'EUR') {
            return $price;
        }
        $rates = Config::get('currency.rates');
        $price = $price * $rates['EUR_' . $user_currency['val']];
        return $price;
    }

    private function formatJson($el, $rev = null)
    {
        $main = Config::get('currency.main');
        $rates = Config::get('currency.rates');
        $user_currency = session()->get('currency');
        if (!$user_currency || ($user_currency['val'] === $main)) {
            return $el;
        }

        foreach ($el as $key => $value) {
            if (isset($value['type']) && $value['type'] == 'object') {
                $el[$key] = $this->formatJson($value, $rev);
            } else if (isset($value['type']) && $value['type'] == 'price') {
                if ($user_currency['val'] !== $main) {
                    $el[$key]['val'] = ($rev !== null) ?
                        $value['val'] * $rates[$user_currency['val'] . '_EUR'] :
                        $value['val'] * $rates['EUR_' . $user_currency['val']];
                    $el[$key]['val'] = ($user_currency['val'] != 'BTC') ? round($el[$key]['val']) : round($el[$key]['val'], 6);
                }
            } else if (!isset($value['type']) && is_array($value)) {
                $el[$key] = $this->formatJson($value, $rev);
            }
        }
        return $el;
    }

    public function setPricableAttribute($key, $value)
    {
        if (!$this->isPricableAttribute($key)) {
            return false;
        }
        return $this->setPrice($key, $value);
    }

    public function setPrice($key, $value)
    {
        if (is_array($value)) {
            $val = $this->getOptions($value, true);
            $this->attributes[$key] = json_encode($val);
        } else {
            $user_currency = session()->get('currency');
            if ($user_currency['val'] == 'EUR' || empty($user_currency['val'])) {
                $this->attributes[$key] = $value;
            } else {
                $rates = Config::get('currency.rates');
                $price = $value * $rates[$user_currency['val'] . '_EUR'];
                $this->attributes[$key] = ($user_currency['val'] !== 'BTC') ? round($price) : round($price, 9);
            }
        }
        return $this;
    }

    public function getDiscountPriceAttribute()
    {
        if (!empty($this->discount)) {
            return $this->price * (100 - $this->discount) / 100;
        }
        return $this->price;
    }

    private function getOptions($options, $rev = null)
    {
        $main = Config::get('currency.main');
        $rates = Config::get('currency.rates');
        $user_currency = session()->get('currency');
        if (!$user_currency || ($user_currency['val'] === $main)) {
            return $options;
        }
        foreach ($options as $key => $option) {
            if (isset($option['type']) && $option['type'] == 'object') {
                $options[$key] = $this->getOptions($option, $rev);
            } else if (isset($option['type']) && $option['type'] == 'price') {
                if ($user_currency['val'] !== $main) {
                    $options[$key]['val'] = ($rev === null) ?
                        $option['val'] * $rates[$user_currency['val'] . '_EUR'] :
                        $option['val'] * $rates['EUR_' . $user_currency['val']];
                    $options[$key]['val'] = ($user_currency['val'] != 'BTC') ? round($options[$key]['val']) : round($options[$key]['val'], 6);
                }
            } else if (!isset($option['type']) && is_array($option)) {
                $options[$key] = $this->getOptions($option, $rev);
            }
        }
        return $options;
    }
}
