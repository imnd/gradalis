<?php

namespace App\Traits;

use Spatie\Image\Manipulations;

trait MediaConversion
{
    /**
     * @return \Spatie\MediaLibrary\Conversion\Conversion
     */
    public function addWatermark()
    {
        $this->addMediaConversion('watermark')
            /*->watermark(
                base_path('/resources/img/watermarks/2.png')
            )
           ->watermarkPosition(Manipulations::POSITION_CENTER)
           ->watermarkOpacity(30)
           ->watermarkFit(Manipulations::FIT_FILL)
           ->apply()*/
            ->watermark(
                base_path('/resources/img/watermarks/3.png')
            )
            ->watermarkPosition(Manipulations::POSITION_BOTTOM_RIGHT)
            ->watermarkOpacity(50)
            ->watermarkHeight(20, Manipulations::UNIT_PERCENT)
            ->keepOriginalImageFormat();
    }

    /**
     * @return \Spatie\MediaLibrary\Conversion\Conversion
     */
    public function addThumb($width = 100, $height = 100)
    {
        $this->addMediaConversion('thumb')
            ->width($width)
            ->height($height);
    }

    /**
     * @return \Spatie\MediaLibrary\Conversion\Conversion
     */
    public function addCard($width = 214, $height = 250)
    {
        $this->addMediaConversion('card')
            ->width($width)
            ->height($height);
    }

    /**
     * @return \Spatie\MediaLibrary\Conversion\Conversion
     */
    public function addOriginal($width = 1024, $height = 768)
    {
        $this->addMediaConversion('original')
            ->width($width)
            ->height($height);
    }
}
