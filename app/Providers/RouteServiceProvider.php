<?php

namespace App\Providers;

use App\Models\Help\Help;
use App\Models\Help\HelpCategory;
use App\Models\Help\HelpSection;
use App\Models\News\News;
use App\Models\News\NewsCategory;
use App\Models\Vacancy;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('news', function ($url) {
            $news = News::query();
            foreach (config('app.locales') as $lang => $val) {
                $news->orWhere("url->$lang", $url);
            }
            return $news->firstOrFail();
        });

        Route::bind('newsCategory', function ($url) {
            $newsCategory = NewsCategory::query();
            foreach (config('app.locales') as $lang => $val) {
                $newsCategory->orWhere("url->$lang", $url);
            }
            return $newsCategory->firstOrFail();
        });

        Route::bind('helpSection', function ($url) {
            $model = HelpSection::query();
            foreach (config('app.locales') as $lang => $val) {
                $model->orWhere("url->$lang", $url);
            }
            return $model->firstOrFail();
        });

        Route::bind('helpCategory', function ($url) {
            $model = HelpCategory::query();
            foreach (config('app.locales') as $lang => $val) {
                $model->orWhere("url->$lang", $url);
            }
            return $model->firstOrFail();
        });

        Route::bind('help', function ($url) {
            $model = Help::query();
            foreach (config('app.locales') as $lang => $val) {
                $model->orWhere("url->$lang", $url);
            }
            return $model->firstOrFail();
        });

        Route::bind('vacancyUrl', function ($url) {
            $model = Vacancy::query();
            foreach (config('app.locales') as $lang => $val) {
            //$model->orWhere('url->' . $lang, $url);
                $model->orWhere('url', 'like', '%' . $url . '%'); //TODO remove
            }
            return $model->firstOrFail();
        });

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
