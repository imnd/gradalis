<?php

namespace App\Providers;

use
    DmitryBubyakin\NovaMedialibraryField\Resources\Media,
    Illuminate\Support\Facades\Gate,
    Laravel\Nova\Cards\Help,
    Laravel\Nova\Nova,
    Laravel\Nova\NovaApplicationServiceProvider,
    Marketplace\Chat\Chat,
    Marketplace\Referrals\Referrals,
    PhpJunior\NovaLogViewer\Tool
;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Nova::resources([
            Media::class,
        ]);
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            new Chat,
            new Referrals,
            new Tool,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return true;/*in_array($user->email, [
                //
            ]);*/
        });
    }

    /**
     * Get the cards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new Help,
        ];
    }
}
