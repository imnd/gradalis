<?php

namespace App\Providers;

use
    Illuminate\Support\ServiceProvider,
    Illuminate\Support\Facades\Auth,
    Illuminate\Support\Facades\View,
    Illuminate\Support\Carbon,
    Illuminate\Support\Facades\Schema,
    Illuminate\Database\Eloquent\Relations\Relation,

    App\Models\Auth\User,
    App\Models\Business\Business,
    App\Models\Franchise\Franchise,
    App\Models\ObjectRequest,
    App\Models\Referral\InvitationCounter,

    App\Observers\InvitationObserver,
    App\Observers\UserObserver,
    App\Observers\BusinessObserver,
    App\Observers\FranchiseObserver,
    App\Observers\ObjectRequestObserver
;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // TODO почему то не работает
        $locale = config('app.locale');
        setlocale(LC_TIME, $locale . '_' . strtoupper($locale) . '.UTF-8');
        Carbon::setLocale($locale);
        InvitationCounter::observe(InvitationObserver::class);
        User::observe(UserObserver::class);
        Business::observe(BusinessObserver::class);
        Franchise::observe(FranchiseObserver::class);
        ObjectRequest::observe(ObjectRequestObserver::class);

        View::composer('*', function ($view) {
            if ($user = Auth::user()) {
                $view->with('user', $user);
            }
        });

        Relation::morphMap([
            'franchises' => 'App\Models\Franchise\Franchise',
            'businesses' => 'App\Models\Business\Business',
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() === 'local') {
            // Генератор моделей
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
        }
    }
}
