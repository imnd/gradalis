<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class MarketplaceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application users-services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application users-services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/Core.php';
    }
}
