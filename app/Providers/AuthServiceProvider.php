<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider,
    Illuminate\Support\Facades\Gate,
    App\Models\Roles;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Auth\User'         => 'App\Policies\UserPolicy',
        'App\Models\AdminNotification' => 'App\Policies\AdminNotificationPolicy',
        'App\Models\Tech'              => 'App\Policies\TechPolicy',
        'App\Models\VacancyResponse'   => 'App\Policies\VacancyResponsePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Дает админу все права без прямого назначения в бд
        Gate::before(function ($user) {
            if ($user->hasRole(Roles::ROLE_ADMIN)) {
                return true;
            }
        });
    }
}
