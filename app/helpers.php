<?php

use App\Services\Helpers;

/*
 * Global helpers file with misc functions.
 */
if (!function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (!function_exists('format_price')) {
    /**
     *
     * @return mixed
     */
    function format_price($price, $sign = true, $cur = null)
    {
        return resolve(Helpers::class)->formatPrice($price, $sign, $cur);
    }
}

if (!function_exists('steps_generator')) {
    /**
     *
     * @return mixed
     */
    function steps_generator($options)
    {
        return resolve(Helpers::class)->stepsGenerator($options);
    }
}

if (!function_exists('is_assoc')) {
    /**
     *
     * @return mixed
     */
    function is_assoc(array $arr)
    {
        return resolve(Helpers::class)->isAssoc($arr);
    }
}

if (!function_exists('is_json')) {
    /**
     *
     * @return mixed
     */
    function is_json($str)
    {
        return resolve(Helpers::class)->isJson($str);
    }
}


