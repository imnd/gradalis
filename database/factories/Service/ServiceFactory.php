<?php

use Faker\Generator as Faker;
use App\Models\Language;

$factory->define(\App\Models\Service\Service::class, function (Faker $faker) {

    return [
        'category_id' => rand(1,3),
        'type' => 1,
        'status' => 1,
        'name' => ['en' => 'Service', 'pl' => 'Usługa', 'ru' => 'Услуга'],
        'price' => ['en' => 10000, 'pl' => 2300, 'ru' => 0],
        'price_for' => [
            'en' => 'за единицу услуги',
            'pl' => 'за единицу услуги',
            'ru' => 'за единицу услуги'
        ],
        'description' => $faker->text(),
        'icon' => '/svg/icons/services/ic_translate.svg',
        'slug' => 'link-to-service'
    ];
});
