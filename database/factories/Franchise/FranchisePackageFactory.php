<?php

use App\Models\Franchise\FranchisePackage;
use Faker\Generator as Faker;

$factory->define(FranchisePackage::class, function (Faker $faker) {
    return [
        'price' => 10000,
        'options'=>json_decode('{1,2}'),
        'profit' => 100,
        'lump' => 100,
        'investments' => 1998,
        'roality' => 2000,
        'marketing_fee' => 5,
        'payback' => 100,
    ];
});


//$table->json('options');
//$table->json('name');
//$table->integer('franchise_id')->unsigned();
//$table->bigInteger('price');
//$table->tinyInteger('profitability');
//$table->tinyInteger('price_including_vat_bool')->default(0);
//$table->json('packageInclude')->nullable();
//$table->json('packageBenefits')->nullable();
//$table->json('purchaseTerms')->default(1);
//$table->integer('royalty_rate')->nullable();
//$table->tinyInteger('royalty_rate_period')->nullable();
//$table->timestamps();
