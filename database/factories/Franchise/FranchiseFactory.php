<?php

use App\Models\Franchise\Franchise;
use Faker\Generator as Faker;

$factory->define(Franchise::class, function (Faker $faker) {
    $countries = [1, 146];
    $country_id = $countries[rand(0, 1)];
    if ($country_id == 1) {
        $city_id = 1;
        $region_id = 1096;
    } else {
        $city_id = 13984;
        $region_id = 1062;
    }
    return [
        'price'           => 1000,
        'percent'         => 10,
        'profitability'   => 1000,
        'condition'       => rand(0, 2),
        'user_id'         => rand(1, 3),
        'broker_id'       => rand(7,8),
        'profit'          => 100,
        'roality'         => 1000,
        'foundation_year' => 1998,
        'start_year'      => 2000,
        'own_enterprices' => 5,
        'status'          => rand(0, 2),
        'commission'      => 15,
        'city_id'         => $city_id,
        'country_id'      => $country_id,
        'region_id'       => $region_id,
        'revenue'         => 1000,
        'discount'        => rand(0, 15),
        'weight'          => array_rand(array(1, 2, 3, 4, null)),
        'category_id'     => rand(1, 2),
    ];
});
