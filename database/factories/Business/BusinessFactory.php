<?php

use App\Models\Business\Business;
use Faker\Generator as Faker;

$factory->define(Business::class, function (Faker $faker) {
    $countries = [1, 146];
    $country_id = $countries[rand(0, 1)];
    if ($country_id == 1) {
        $city_id = 1;
        $region_id = 1096;
    } else {
        $city_id = 13984;
        $region_id = 1062;
    }
    $price = rand(100000, 10000000);
    return [
        'price'         => 1000,
        'percent'       => 10,
        'profitability' => 1000,
        'user_id'       => rand(1, 3),
        'broker_id'     => rand(7,8),
        'profit'        => 30,
        'payback'       => rand(12, 36),
        'status'        => rand(2, 3),
        'city_id'       => $city_id,
        'country_id'    => $country_id,
        'region_id'     => $region_id,
        'commission'    => 15,
        'revenue'       => 500,
        'discount'      => rand(0, 15),
        'weight'        => array_rand(array(1, 2, 3, 4, null)),
        'category_id'   => rand(1, 4)
    ];
});
