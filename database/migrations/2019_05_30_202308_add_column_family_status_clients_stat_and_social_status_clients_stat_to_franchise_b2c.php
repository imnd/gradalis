<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFamilyStatusClientsStatAndSocialStatusClientsStatToFranchiseB2c extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->json('family_status_clients_stat')->nullable();
            $table->json('social_status_clients_stat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->dropColumn('family_status_clients_stat');
            $table->dropColumn('social_status_clients_stat');
        });
    }
}
