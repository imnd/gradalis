<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PerformersServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('performers_services', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->text('annotation')->comment('аннотация');
            $table->text('annotation_img')->comment('картинка к аннотации');
            $table->text('video_url')->comment('ссылка на промо видео');
            $table->text('video_description')->comment('описание к видео');
            $table->json('price_for')->nullable()->after('price');
        });
        Schema::table('performers_services', function (Blueprint $table) {
            $table->json('price');
        });
        Schema::rename('performers_services', 'users_services');
        Schema::rename('performers_service_categories', 'users_service_categories');
        Schema::create('users_service_steps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->string('name', 255);
            $table->text('description');
            $table->string('icon', 255)->nullable();
        });
        Schema::create('users_service_cases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->text('description');
        });
        Schema::create('users_service_cases_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('case_id');
            $table->integer('is_main');
            $table->string('file_name', 255)->nullable();
        });
        Schema::create('users_service_workers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->integer('position_id');
            $table->string('name', 255);
            $table->string('icon', 255)->nullable();
        });
        Schema::rename('franchise_posts', 'positions');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('users_services', 'performers_services');
        Schema::table('performers_services', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('annotation');
            $table->dropColumn('annotation_img');
        });
        Schema::table('performers_services', function (Blueprint $table) {
            $table->double('price');
        });
    }
}
