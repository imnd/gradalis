<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFranchiseReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchise_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->json('name');
            $table->string('company')->nullable();
            $table->string('position')->nullable();
            $table->string('email');
            $table->string('video_url')->nullable();
            $table->json('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchise_reviews');
    }
}
