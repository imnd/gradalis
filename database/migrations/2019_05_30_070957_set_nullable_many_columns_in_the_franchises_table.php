<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableManyColumnsInTheFranchisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('franchises', function (Blueprint $table) {
            $table->unsignedInteger('lump_sum')->default(0)->nullable()->change();
            $table->unsignedInteger('city_id')->default(0)->nullable()->change();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('franchises', function (Blueprint $table) {
            $table->unsignedInteger('lump_sum')->default(0)->change();
            $table->unsignedInteger('city_id')->default(0)->change();
        });
        Schema::enableForeignKeyConstraints();
    }
}
