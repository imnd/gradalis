<?php

use Illuminate\Support\Facades\Schema,
    Illuminate\Database\Schema\Blueprint,
    Illuminate\Database\Migrations\Migration;

class RenameFranchiseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audience_types', function (Blueprint $table) {
            $table->rename('franchise_audience_types');
        });
        Schema::table('directions', function (Blueprint $table) {
            $table->rename('franchise_directions');
        });
        Schema::table('certificate_types', function (Blueprint $table) {
            $table->rename('franchise_certificate_types');
        });
        Schema::table('penalties', function (Blueprint $table) {
            $table->rename('franchise_penalties');
        });
        Schema::table('certificate_documents', function (Blueprint $table) {
            $table->rename('franchise_certificate_documents');
        });
        Schema::table('operation_restrictions', function (Blueprint $table) {
            $table->rename('franchise_operation_restrictions');
        });
        Schema::table('training_material_types', function (Blueprint $table) {
            $table->rename('franchise_training_material_types');
        });
        Schema::table('property_categories', function (Blueprint $table) {
            $table->rename('franchise_property_categories');
        });
        Schema::table('list_contracts_for_franchise', function (Blueprint $table) {
            $table->rename('franchise_b2b_contracts_list');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_audience_types', function (Blueprint $table) {
            $table->rename('audience_types');
        });
        Schema::table('franchise_directions', function (Blueprint $table) {
            $table->rename('directions');
        });
        Schema::table('franchise_certificate_types', function (Blueprint $table) {
            $table->rename('certificate_types');
        });
        Schema::table('franchise_penalties', function (Blueprint $table) {
            $table->rename('penalties');
        });
        Schema::table('franchise_certificate_documents', function (Blueprint $table) {
            $table->rename('certificate_documents');
        });
        Schema::table('franchise_operation_restrictions', function (Blueprint $table) {
            $table->rename('operation_restrictions');
        });
        Schema::table('franchise_training_material_types', function (Blueprint $table) {
            $table->rename('training_material_types');
        });
        Schema::table('franchise_property_categories', function (Blueprint $table) {
            $table->rename('property_categories');
        });
        Schema::table('franchise_b2b_contracts_list', function (Blueprint $table) {
            $table->rename('list_contracts_for_franchise');
        });
    }
}
