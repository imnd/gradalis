<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseToBrokerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brokers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->tinyInteger('is_company')->comment('компания или физ лицо');
            $table->string('company')->comment('название компании')->nullable();
            $table->integer('business_category_id')->comment('категория бизнеса в которой работает брокер');
        });
        Schema::table('users', function (Blueprint $table) {
            $table
                ->addColumn('json', 'spoken_langs')
                ->comment('на каких языках говорит')
                ->nullable();
        });
        Schema::table('franchises', function (Blueprint $table) {
            $table
                ->addColumn('tinyInteger', 'sell_with_broker')
                ->comment('хочет продавать через брокера');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchises_to_brokers');
        Schema::dropIfExists('brokers');
    }
}
