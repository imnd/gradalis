<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('positions', function (Blueprint $table) {
            $table->renameColumn('slug', 'translation');
        });
        Schema::table('positions', function (Blueprint $table) {
            $table->json('translation')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('positions', function (Blueprint $table) {
            $table->renameColumn('translation', 'slug');
        });
        Schema::table('positions', function (Blueprint $table) {
            $table->string('slug')->change();
        });
    }
}
