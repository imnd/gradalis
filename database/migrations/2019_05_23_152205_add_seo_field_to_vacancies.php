<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFieldToVacancies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('vacancies')->truncate();
        Schema::table('vacancies', function (Blueprint $table) {
                $table->json('url')->nullable();
                $table->json('seo_description')->nullable();
                $table->json('seo_title')->nullable();
                $table->json('seo_keywords')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacancies', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('seo_description');
            $table->dropColumn('seo_title');
            $table->dropColumn('seo_keywords');
        });
    }
}
