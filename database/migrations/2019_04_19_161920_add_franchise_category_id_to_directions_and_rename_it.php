<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFranchiseCategoryIdToDirectionsAndRenameIt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_directions', function (Blueprint $table) {
            $table->rename('franchise_subcategories');
        });
        Schema::table('franchise_subcategories', function (Blueprint $table) {
            $table->integer('category_id')->nullable();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
        Schema::table('franchises', function (Blueprint $table) {
            $table->dropColumn('direction_id');
            $table->unsignedInteger('subcat_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_subcategories', function (Blueprint $table) {
            $table->dropColumn('category_id');
            $table->timestamps();
        });
        Schema::table('franchise_subcategories', function (Blueprint $table) {
            $table->rename('franchise_directions');
        });
        Schema::table('franchises', function (Blueprint $table) {
            $table->dropColumn('subcat_id');
            $table->unsignedInteger('direction_id');
        });
    }
}
