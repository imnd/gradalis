<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // тариф
        Schema::create('tariffs', function (Blueprint $table) {
            $table->increments('id');
            $table->json('name')->comment('Название');
            $table->json('description')->comment('Описание');
            $table->tinyInteger('visible')->comment('Статус видимости');
            $table->tinyInteger('recommended')->comment('Рекомендуемы');
            $table->string('icon')->comment('Иконка');
            $table->tinyInteger('views')->comment('Кол-во просмотров');
            $table->smallInteger('price')->comment('Цена за 1 день');
            $table->smallInteger('validity')->comment('Срок действия');
            $table->tinyInteger('discount')->comment('Скидка');
            $table->tinyInteger('sales_success_percent')->comment('Процент к успеху продажи');
        });
        // услуги входящие в тариф
        Schema::create('tariffs_to_services', function (Blueprint $table) {
            $table->integer('tariff_id');
            $table->integer('service_id');
        });
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->tinyInteger('visible')->comment('Статус видимости');
            $table->renameColumn('price_eur', 'price');
            $table->dropColumn('price_pln');
            //$table->dropColumn('price_btc');
            $table->renameColumn('small_text', 'description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');
        Schema::dropIfExists('tariffs_to_services');
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('visible');
            $table->renameColumn('price', 'price_eur');
            $table->smallInteger('price_pln');
            //$table->smallInteger('price_btc');
            $table->timestamps();
        });
    }
}
