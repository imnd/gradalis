<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_service_cases', function (Blueprint $table) {
            $table->rename('users_service_portfolio');
        });
        Schema::table('users_service_cases_photos', function (Blueprint $table) {
            $table->rename('users_service_portfolio_photos');
        });
        Schema::table('users_service_steps', function (Blueprint $table) {
            $table->rename('users_service_processes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_service_portfolio', function (Blueprint $table) {
            $table->rename('users_service_cases');
        });
        Schema::table('users_service_portfolio_photos', function (Blueprint $table) {
            $table->rename('users_service_cases_photos');
        });
        Schema::table('users_service_processes', function (Blueprint $table) {
            $table->rename('users_service_steps');
        });
    }
}
