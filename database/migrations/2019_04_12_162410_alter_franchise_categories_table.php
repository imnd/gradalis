<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFranchiseCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_categories', function (Blueprint $table) {
            $table->softDeletes();
            $table->string('icon')->default('ic_1.svg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
            $table->dropColumn('icon');
        });
    }
}
