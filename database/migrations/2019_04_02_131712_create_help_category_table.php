<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_category', function (Blueprint $table) {
            $table->increments('id');
            $table->json('title');
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('section_id')->nullable();
            $table->json('seo_description')->nullable();
            $table->json('seo_title')->nullable();
            $table->json('seo_keywords')->nullable();
            $table->json('vk')->nullable();
            $table->json('ok')->nullable();
            $table->json('google')->nullable();
            $table->json('mail')->nullable();
            $table->json('fb')->nullable();
            $table->json('yandex')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_category');
    }
}
