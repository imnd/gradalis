<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchise_certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('franchise_id')->nullable();
            $table->unsignedInteger('certificate_document_id')->nullable();
            $table->unsignedInteger('certificate_type_id')->nullable();
            $table->date('license_period')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchise_certificates');
    }
}
