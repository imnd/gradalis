<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFranchiseStaffPostId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_staff', function (Blueprint $table) {
            $table->dropColumn('franchise_post_id');
        });
        Schema::table('franchise_staff', function (Blueprint $table) {
            $table->integer('post_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_staff', function (Blueprint $table) {
            $table->dropColumn('post_id');
        });
        Schema::table('franchise_staff', function (Blueprint $table) {
            $table->integer('franchise_post_id');
        });
    }
}
