<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsToUsersServiceSteps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_service_steps', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('users_service_cases', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('users_service_workers', function (Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_service_steps', function (Blueprint $table) {
            Schema::table('users_service_steps', function (Blueprint $table) {
                $table->dropTimestamps();
            });
            Schema::table('users_service_cases', function (Blueprint $table) {
                $table->dropTimestamps();
            });
            Schema::table('users_service_workers', function (Blueprint $table) {
                $table->dropTimestamps();
            });
        });
    }
}
