<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconToCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->string('icon')->nullable();
            $table->dropTimestamps();
            $table->dropColumn('deleted_at');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropTimestamps();
            $table->dropColumn('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('icon');
            $table->timestamps();
            $table->timestamp('deleted_at');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->timestamps();
            $table->timestamp('deleted_at');
        });
    }
}
