<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertRefactoringOptionsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchises', function (Blueprint $table) {
            $table->unsignedInteger('direction_id');
            $table->boolean('have_packages')->default(false)->nullable();
            $table->json('name_business')->nullable();
            $table->json('name_legal_entity')->nullable();
            $table->json('name_video_review')->nullable();
            $table->string('price_including_vat')->nullable();
            $table->string('royalty_rate_period')->nullable();
            $table->json('franchise_video_review')->nullable();
            $table->string('staff_costs')->nullable();
            $table->integer('launch_dates')->comment('Дней для запуска')->nullable();
            $table->string('turnover_year')->comment('Оборот в год')->nullable();
            $table->string('turnover_month')->comment('Оборот в месяц')->nullable();
            $table->string('month_expenses')->comment('Рассходы в месяц')->nullable();
            $table->string('property_costs')->nullable();
            $table->integer('return_payback')->nullable();
            $table->string('general_expenses')->comment('Всего расходов')->nullable();
            $table->string('cost_business_per_year')->comment('год')->nullable();
            $table->string('cost_business_per_quarter')->comment('квартал')->nullable();
            $table->json('justification_financial_indicators')->nullable();
            $table->boolean('title_documents')->default(false)->nullable();
            $table->unsignedInteger('property_type_id')->nullable();
            $table->unsignedInteger('category_property_id')->nullable();
            $table->boolean('price_including_vat_bool')->default(false)->nullable();
            $table->string('price_square_meters')->nullable();
            $table->boolean('ref_register_estate')->default(false)->nullable();
            $table->string('number_square_meters')->nullable();
            $table->boolean('restrictions_operation')->default(false)->nullable();
            $table->boolean('technical_property_plan')->default(false)->nullable();
            $table->boolean('coordination_redevelopment')->default(false)->nullable();
            $table->json('list_restrictions_operation')->comment('list needed')->nullable();
            $table->integer('month_teach')->nullable();
            $table->boolean('needed_licenses')->default(false)->nullable();
            $table->boolean('transfer_work_schemes')->default(false)->nullable();
            $table->boolean('transfer_customer_base')->default(false)->nullable();
            $table->json('type_training_materials')->comment('list needed')->nullable();
            $table->boolean('ready_training_materials')->default(false)->nullable();
            $table->json('type_transfer_customer_base')->comment('list needed')->nullable();
            $table->json('list_equipment')->comment('list needed')->nullable();
            $table->boolean('discount_equipment')->default(false)->nullable();
            $table->boolean('included_equipment')->default(false)->nullable();
            $table->unsignedInteger('condition_equipment_id')->nullable();
            $table->string('total_amount_equipment')->nullable();
            $table->boolean('staff_training_equipment')->default(false)->nullable();
            $table->string('total_amount_equipment_with_discount')->nullable();
            $table->unsignedInteger('type_audience')->nullable();
            $table->string('tax_system')->nullable();
            $table->string('legal_status')->nullable();
            $table->boolean('have_penalties')->nullable();
            $table->json('list_penalties')->comment('list needed')->nullable();
            $table->boolean('have_disputable_situations')->default(false)->nullable();
            $table->json('list_disputable_situations')->nullable();
            $table->boolean('changes_profile_legal_entity')->default(false)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchises', function (Blueprint $table) {
            $table->dropColumn('direction_id');
            $table->dropColumn('have_packages');
            $table->dropColumn('name_business');
            $table->dropColumn('name_legal_entity');
            $table->dropColumn('name_video_review');
            $table->dropColumn('price_including_vat');
            $table->dropColumn('royalty_rate_period');
            $table->dropColumn('franchise_video_review');
            $table->dropColumn('staff_costs');
            $table->dropColumn('launch_dates');
            $table->dropColumn('turnover_year');
            $table->dropColumn('turnover_month');
            $table->dropColumn('month_expenses');
            $table->dropColumn('property_costs');
            $table->dropColumn('return_payback');
            $table->dropColumn('general_expenses');
            $table->dropColumn('cost_business_per_year');
            $table->dropColumn('cost_business_per_quarter');
            $table->dropColumn('justification_financial_indicators');
            $table->dropColumn('title_documents');
            $table->dropColumn('property_type_id');
            $table->dropColumn('category_property_id');
            $table->dropColumn('price_including_vat_bool');
            $table->dropColumn('price_square_meters');
            $table->dropColumn('ref_register_estate');
            $table->dropColumn('number_square_meters');
            $table->dropColumn('restrictions_operation');
            $table->dropColumn('technical_property_plan');
            $table->dropColumn('coordination_redevelopment');
            $table->dropColumn('list_restrictions_operation');
            $table->dropColumn('month_teach');
            $table->dropColumn('needed_licenses');
            $table->dropColumn('transfer_work_schemes');
            $table->dropColumn('transfer_customer_base');
            $table->dropColumn('type_training_materials');
            $table->dropColumn('ready_training_materials');
            $table->dropColumn('type_transfer_customer_base');
            $table->dropColumn('list_equipment');
            $table->dropColumn('discount_equipment');
            $table->dropColumn('included_equipment');
            $table->dropColumn('condition_equipment_id');
            $table->dropColumn('total_amount_equipment');
            $table->dropColumn('staff_training_equipment');
            $table->dropColumn('total_amount_equipment_with_discount');
            $table->dropColumn('type_audience');
            $table->dropColumn('tax_system');
            $table->dropColumn('legal_status');
            $table->dropColumn('have_penalties');
            $table->dropColumn('list_penalties');
            $table->dropColumn('have_disputable_situations');
            $table->dropColumn('list_disputable_situations');
            $table->dropColumn('changes_profile_legal_entity');
        });
    }
}
