<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorHelpTablesFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('help')->truncate();
        DB::table('help_category')->truncate();
        DB::table('help_section')->truncate();

        Schema::table('help_category', function (Blueprint $table) {
            $table->dropColumn('vk');
            $table->dropColumn('ok');
            $table->dropColumn('google');
            $table->dropColumn('mail');
            $table->dropColumn('fb');
            $table->dropColumn('yandex');
            $table->dropColumn('parent_id');
            $table->json('url')->nullable();
        });

        Schema::table('help', function (Blueprint $table) {
            $table->json('url')->nullable();
            $table->json('seo_description')->nullable();
            $table->json('seo_title')->nullable();
            $table->json('seo_keywords')->nullable();
        });
        Schema::table('help_section', function (Blueprint $table) {
            $table->json('url')->nullable();
            $table->json('seo_description')->nullable();
            $table->json('seo_title')->nullable();
            $table->json('seo_keywords')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('help_category', function (Blueprint $table) {
            $table->json('vk')->nullable();
            $table->json('ok')->nullable();
            $table->json('google')->nullable();
            $table->json('mail')->nullable();
            $table->json('fb')->nullable();
            $table->json('yandex')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->dropColumn('url');
        });
        Schema::table('help', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('seo_description');
            $table->dropColumn('seo_title');
            $table->dropColumn('seo_keywords');
        });
        Schema::table('help_section', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('seo_description');
            $table->dropColumn('seo_title');
            $table->dropColumn('seo_keywords');
        });
    }
}
