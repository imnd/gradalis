<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchise_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('franchise_id')->nullable();
            $table->unsignedInteger('franchise_post_id')->nullable();
            $table->string('monthly_wages')->nullable();
            $table->string('tax_amount_per_month')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchise_staff');
    }
}
