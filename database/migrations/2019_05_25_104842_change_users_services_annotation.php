<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersServicesAnnotation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_services', function (Blueprint $table) {
            $table->dropColumn('annotation');
            $table->dropColumn('video_description');
        });
        Schema::table('users_services', function (Blueprint $table) {
            $table->json('annotation');
            $table->json('video_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_services', function (Blueprint $table) {
            $table->dropColumn('annotation');
            $table->dropColumn('video_description');
        });
        Schema::table('users_services', function (Blueprint $table) {
            $table->text('annotation');
            $table->text('video_description');
        });
    }
}
