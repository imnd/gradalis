<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCertificateTypeIdToFranchiseCertificateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_certificate_documents', function (Blueprint $table) {
            // Внешний ключ на тип сертификата
            $table->integer('certificate_type_id')->unsigned()->nullable();
            $table->foreign('certificate_type_id')->references('id')->on('franchise_certificate_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('franchise_certificate_documents', function (Blueprint $table) {
            $table->dropForeign('franchise_certificate_documents_certificate_type_id_foreign');
            $table->dropColumn('certificate_type_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
