<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->tinyInteger('user_type')->default(0);
        });
        Schema::table('campaign_targets', function (Blueprint $table) {
            $table->tinyInteger('cpa')->default(0);
            $table->json('name')->change()->nullable();
        });
        Schema::table('campaign_resources', function (Blueprint $table) {
            $table->integer('country_id')->unsigned();
            $table->string('url')->change();
            $table->foreign('country_id')->references('id')->on('countries')
                ->onDelete('no action')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('user_type')->default(0);
        });
        Schema::table('campaign_targets', function (Blueprint $table) {
            $table->dropColumn('cpa')->default(0);
            $table->string('name')->change();
        });
        Schema::table('campaign_resources', function (Blueprint $table) {
            $table->json('url')->change();
            $table->dropForeign('country_id');
            $table->dropColumn('country_id')->unsigned();
        });
    }
}
