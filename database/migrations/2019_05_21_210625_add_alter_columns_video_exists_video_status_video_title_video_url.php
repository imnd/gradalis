<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlterColumnsVideoExistsVideoStatusVideoTitleVideoUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchises', function (Blueprint $table) {
            // Есть ли видеообзор у франшиз
            $table->tinyInteger('video_exists')->default(0)->after('name_legal_entity');
            // Статус видеообзора 0 - в модерации; 1 - одобрен; 2 - отклонен
            $table->tinyInteger('video_status')->default(0)->after('video_exists');

            $table->string('name_video_review',255)->default('')->change();
            $table->string('franchise_video_review',255)->default('')->change();
        });

        Schema::table('franchises', function (Blueprint $table) {
            $table->renameColumn('name_video_review', 'video_title');
            $table->renameColumn('franchise_video_review', 'video_url');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchises', function (Blueprint $table) {
            $table->dropColumn('video_exists');
            $table->dropColumn('video_status');

            $table->json('video_title')->change();
            $table->json('video_url')->change();
        });

        Schema::table('franchises', function (Blueprint $table) {
            $table->renameColumn('video_title', 'name_video_review');
            $table->renameColumn('video_url', 'franchise_video_review');
        });

    }
}
