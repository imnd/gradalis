<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToConsultations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('consultations');
        Schema::create('consultations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('travel_id')->nullable()->unsigned();
            $table->foreign('travel_id')->references('id')->on('travels');
            $table->morphs('object');
            $table->string('theme');
            $table->datetime('date');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultations');
    }
}
