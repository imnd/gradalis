<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCounterTargetDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('counter_target', function (Blueprint $table) {
            $table->integer('status')->default(1)->change();
            $table->integer('type')->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('counter_target', function (Blueprint $table) {
            $table->tinyInteger('status')->default(0)->change();
            $table->tinyInteger('type')->default(0)->change();
        });
    }
}
