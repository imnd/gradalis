<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountersToUsersService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_services', function (Blueprint $table) {
            $table
                ->addColumn('integer', 'shows_count')
                ->after('status')
                ->default(0)
                ->comment('просмотров');
            $table
                ->addColumn('integer', 'bids_count')
                ->after('shows_count')
                ->default(0)
                ->comment('заявок');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_services', function (Blueprint $table) {
            $table->dropColumn('shows_count');
            $table->dropColumn('bids_count');
        });
    }
}
