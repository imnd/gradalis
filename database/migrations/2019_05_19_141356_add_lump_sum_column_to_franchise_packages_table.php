<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLumpSumColumnToFranchisePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_packages', function (Blueprint $table) {
            $table->unsignedInteger('lump_sum')->default(0)->after('terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_packages', function (Blueprint $table) {
            $table->dropColumn('lump_sum');
        });
    }
}
