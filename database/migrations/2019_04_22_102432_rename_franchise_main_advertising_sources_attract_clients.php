<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFranchiseMainAdvertisingSourcesAttractClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_b2b', function (Blueprint $table) {
            $table->dropColumn('main_advertising_sources_attract_clients');
        });
        Schema::table('franchise_b2b', function (Blueprint $table) {
            $table->json('main_attract_client_adv_src')->nullable();
        });
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->dropColumn('main_advertising_sources_attract_clients');
        });
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->json('main_attract_client_adv_src')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_b2b', function (Blueprint $table) {
            $table->dropColumn('main_attract_client_adv_src');
        });
        Schema::table('franchise_b2b', function (Blueprint $table) {
            $table->json('main_advertising_sources_attract_clients')->nullable();
        });
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->dropColumn('main_attract_client_adv_src');
        });
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->json('main_advertising_sources_attract_clients')->nullable();
        });
    }
}
