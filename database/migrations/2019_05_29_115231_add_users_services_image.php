<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersServicesImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_services', function (Blueprint $table) {
            $table->string('annotation_img', 128)->change();
            $table->string('photo', 128)->after('annotation_img');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_services', function (Blueprint $table) {
            $table->text('annotation_img')->change();
            $table->dropColumn('photo');
        });
    }
}
