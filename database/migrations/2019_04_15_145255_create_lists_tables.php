<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsTables extends Migration
{
    private $tables = [
        'operation_restrictions',
        'training_material_types',
        'transfer_customer_base_types',
        'equipments',
        'audience_types',
        'penalties',
    ];
    private $fields = [
        'list_restrictions_operation',
        'type_training_materials',
        'type_transfer_customer_base',
        'list_equipment',
        'type_audience',
        'list_penalties',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $tableName) {
            Schema::create($tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug');
            });
        }
        Schema::table('franchises', function (Blueprint $table) {
            foreach ($this->fields as $fieldName) {
                $table->dropColumn($fieldName);
            }
        });
        Schema::table('franchises', function (Blueprint $table) {
            foreach ($this->fields as $fieldName) {
                $table->unsignedInteger($fieldName)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $tableName) {
            Schema::dropIfExists($tableName);
        }
        Schema::table('franchises', function (Blueprint $table) {
            foreach ($this->fields as $fieldName) {
                $table->dropColumn($fieldName);
            }
        });
        Schema::table('franchises', function (Blueprint $table) {
            foreach ($this->fields as $fieldName) {
                $table->json($fieldName)->comment('list needed');
            }
        });
    }
}
