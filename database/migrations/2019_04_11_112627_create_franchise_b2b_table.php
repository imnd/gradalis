<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseB2bTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchise_b2b', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('franchise_id');
            $table->string('average_check_clients')->nullable();
            $table->boolean('have_existing_contracts')->nullable();
            $table->json('main_category_business_partners')->comment('list needed')->nullable();
            $table->string('count_perpetual_service_contracts')->nullable();
            $table->json('main_advertising_sources_attract_clients')->comment('list needed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchise_b2b');
    }
}
