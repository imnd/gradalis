<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersServiceWorkers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_service_workers', function (Blueprint $table) {
            $table->dropColumn('position_id');
            $table->string('position', 128)->after('name');
            $table->renameColumn('icon', 'photo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_service_workers', function (Blueprint $table) {
            $table->dropColumn('position');
            $table->integer('position_id');
            $table->renameColumn('photo', 'icon');
        });
    }
}
