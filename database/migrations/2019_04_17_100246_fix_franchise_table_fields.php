<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixFranchiseTableFields extends Migration
{
    private $fields = [
        'list_restrictions_operation',
        'type_training_materials',
        'type_transfer_customer_base',
        'list_equipment',
        'list_penalties',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchises', function (Blueprint $table) {
            foreach ($this->fields as $fieldName) {
                $table->dropColumn($fieldName);
            }
        });
        Schema::table('franchises', function (Blueprint $table) {
            foreach ($this->fields as $fieldName) {
                $table->json($fieldName)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchises', function (Blueprint $table) {
            foreach ($this->fields as $fieldName) {
                $table->dropColumn($fieldName);
            }
        });
        Schema::table('franchises', function (Blueprint $table) {
            foreach ($this->fields as $fieldName) {
                $table->unsignedInteger($fieldName);
            }
        });
    }
}
