<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Franchise\FranchiseObject;
use App\Models\Franchise\FranchiseReview;

class AddStatusFieldToFranchiseReviewsAndObjectsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_objects', function (Blueprint $table) {
            $table->unsignedSmallInteger('status')->default(FranchiseObject::STATUS_AWAIT);
        });
        Schema::table('franchise_reviews', function (Blueprint $table) {
            $table->unsignedSmallInteger('status')->default(FranchiseReview::STATUS_AWAIT);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_objects', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('franchise_reviews', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
