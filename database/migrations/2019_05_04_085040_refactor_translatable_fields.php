<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorTranslatableFields extends Migration
{
    private $tables = [
        'franchise_audience_types',
        'franchise_certificate_documents',
        'franchise_certificate_types',
        'franchise_operation_restrictions',
        'franchise_penalties',
        'franchise_property_categories',
        'franchise_training_material_types',
        'equipments',
        'family_status_clients',
        'main_advertising_sources_attract_clients',
        'property_type',
        'social_status_clients',
        'transfer_customer_base_types',
        'gender_target_audiences',
        'franchise_subcategories',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('slug');
            });
            Schema::table($tableName, function (Blueprint $table) {
                $table->json('translation');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('translation');
            });
            Schema::table($tableName, function (Blueprint $table) {
                $table->string('slug');
            });
        }
    }
}
