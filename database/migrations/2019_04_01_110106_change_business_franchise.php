<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBusinessFranchise extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->dropColumn('metrics');
            $table->json('metrics_mail');
            $table->json('metrics_google');
            $table->json('metrics_vk');
            $table->json('metrics_ok');
            $table->json('metrics_fb');
        });

        Schema::table('franchises', function (Blueprint $table) {
            $table->dropColumn('metrics');
            $table->json('metrics_mail');
            $table->json('metrics_google');
            $table->json('metrics_vk');
            $table->json('metrics_ok');
            $table->json('metrics_fb');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->text('metrics')->nullable();
            $table->dropColumn('metrics_mail');
            $table->dropColumn('metrics_google');
            $table->dropColumn('metrics_vk');
            $table->dropColumn('metrics_ok');
            $table->dropColumn('metrics_fb');
        });
        Schema::table('franchises', function (Blueprint $table) {
            $table->text('metrics')->nullable();
            $table->dropColumn('metrics_mail');
            $table->dropColumn('metrics_google');
            $table->dropColumn('metrics_vk');
            $table->dropColumn('metrics_ok');
            $table->dropColumn('metrics_fb');
        });
    }
}
