<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseB2cTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchise_b2c', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('franchise_id');
            $table->integer('age_men_to')->nullable();
            $table->integer('age_men_from')->nullable();
            $table->integer('age_women_to')->nullable();
            $table->integer('age_women_from')->nullable();
            $table->integer('sex_ratio_to')->nullable();
            $table->integer('sex_ratio_from')->nullable();
            $table->integer('alone_clients')->nullable();
            $table->integer('child_clients')->nullable();
            $table->integer('couples_clients')->nullable();
            $table->integer('students_clients')->nullable();
            $table->integer('pensioners_clients')->nullable();
            $table->string('average_check_clients')->nullable();
            $table->json('family_status_clients')->comment('list needed')->nullable();
            $table->json('social_status_clients')->comment('list needed')->nullable();
            $table->unsignedInteger('gender_target_audience')->nullable();
            $table->integer('families_with_children_clients')->nullable();
            $table->string('average_income_target_clients_to')->nullable();
            $table->json('main_category_business_partners')->comment('list needed')->nullable();
            $table->integer('average_income_target_clients_from_to')->nullable();
            $table->integer('average_income_target_clients_from_from')->nullable();
            $table->json('main_advertising_sources_attract_clients')->comment('list needed')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchise_b2c');
    }
}
