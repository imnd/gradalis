<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformersServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performers_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('performer_id')->unsigned();
            $table->string('icon')->nullable();
            $table->json('name');
            $table->string('slug', 255)->nullable();
            $table->json('description');
            $table->double('price')->unsigned();
            $table->tinyInteger('status')->default(0);
        });
        Schema::create('performers_service_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->json('name');
            $table->string('icon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performers_services');
        Schema::dropIfExists('performers_service_categories');
    }
}
