<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFranchiseTableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->dropColumn('average_income_target_clients_from_from');
        });
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->integer('average_income_target_clients_from');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->dropColumn('average_income_target_clients_from');
        });
        Schema::table('franchise_b2c', function (Blueprint $table) {
            $table->integer('average_income_target_clients_from_from');
        });
    }
}
