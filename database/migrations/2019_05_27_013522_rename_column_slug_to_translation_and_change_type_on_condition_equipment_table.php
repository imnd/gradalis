<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnSlugToTranslationAndChangeTypeOnConditionEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('condition_equipment', function (Blueprint $table) {
            $table->renameColumn('slug', 'translation');
        });
        Schema::table('condition_equipment', function (Blueprint $table) {
            $table->json('translation')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('condition_equipment', function (Blueprint $table) {
            $table->renameColumn('translation', 'slug');
        });
        Schema::table('condition_equipment', function (Blueprint $table) {
            $table->string('slug')->change();
        });
    }
}
