<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('verified')->default(0);
            $table->integer('company_activity_id')->unsigned()->nullable();
            $table->string('soc_network')->nullable();
            $table->string('company_name')->nullable();
            $table->string('secret_key');

            $table->foreign('company_activity_id')->references('id')->on('company_activities')
                ->onDelete('no action')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('verified');
            $table->integer('company_activity_id');
            $table->string('soc_network');
            $table->string('company_name');
            $table->string('secret_key');

            $table->dropForeign('company_activity_id');
        });
    }
}
