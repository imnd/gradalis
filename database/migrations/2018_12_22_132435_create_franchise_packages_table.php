<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFranchisePackagesTable extends Migration {

	public function up()
	{
		Schema::create('franchise_packages', function(Blueprint $table) {
			$table->increments('id');
            $table->json('name')->nullable();
			$table->integer('franchise_id')->unsigned();
			$table->bigInteger('price')->nullable();
			$table->tinyInteger('vat')->default(0)->nullable();
            $table->json('include')->nullable();
            $table->json('benefits')->nullable();
            $table->tinyInteger('terms')->nullable();
            $table->integer('royalty_rate')->nullable();
            $table->tinyInteger('royalty_rate_period')->nullable();
			$table->timestamps();

            $table->foreign('franchise_id')->references('id')->on('franchises')
                ->onDelete('cascade')
                ->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::drop('franchise_packages');
	}
}
