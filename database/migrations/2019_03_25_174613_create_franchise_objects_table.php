<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchise_objects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('franchise_id')->unsigned();
            $table->json('title');
            $table->json('description');
            $table->string('phone');
            $table->string('email');
            $table->string('address');
            $table->timestamps();

            $table->foreign('franchise_id')->references('id')->on('franchises')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchise_objects');
    }
}
