<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFranchiseReviewFranchiseId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_reviews', function (Blueprint $table) {
            $table->integer('franchise_id')->comment('Внешний ключ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_reviews', function (Blueprint $table) {
            $table->dropColumn('franchise_id');
        });
    }
}
