<?php

use App\Models\Service\Tariffs;

class TariffsSeeder extends SimpleSeeder
{
    protected $rows = [
        [
            'name' => [
                "en" => "Economy",
                "pl" => "Ekonomia",
                "ru" => "Эконом"
            ],
            'description' => [
                "en" => "25 567 users have chosen this tariff plan",
                "pl" => "25 567 użytkowników wybrało ten plan taryfowy",
                "ru" => "25 567 пользователей выбрали этот тарифный план"
            ],
            'visible' => 1,
            'recommended' => 1,
            'icon' => '/svg/icons/services/ic_flag.svg',
            'price' => 15,
            'validity' => 50,
            'discount' => 9,
            'sales_success_percent' => 19,
            'views' => 5,
        ],
        [
            'name' => [
                "en" => "Luxury sale",
                "pl" => "Sprzedaż luksusowa",
                "ru" => "Люкс продажа",
            ],
            'description' => [
                "en" => "×30 views",
                "pl" => "×30 widoków",
                "ru" => "×30 просмотров",
            ],
            'visible' => 1,
            'recommended' => 1,
            'icon' => '',
            'price' => 50,
            'validity' => 50,
            'discount' => 22,
            'sales_success_percent' => 28,
            'views' => 30,
        ],
    ];

    /**
     * @param Tariffs $model
     * @return void
     */
    public function __construct(Tariffs $model)
    {
        $this->model = $model;
    }
}
