<?php

use App\Models\Service\OrderedService;

class OrderedServicesSeeder extends SimpleSeeder
{
    protected $rows = [
        [
            'service_id' => 1,
            'status' => 1,
            'admin_comment' => 'admin comment',
            'user_comment' => 'user comment',
            'user_id' => 1
        ],
        [
            'service_id' => 1,
            'status' => 1,
            'admin_comment' => 'admin comment',
            'user_comment' => 'user comment',
            'user_id' => 4
        ]
    ];

    /**
     * @param OrderedService $model
     * @return void
     */
    public function __construct(OrderedService $model)
    {
        $this->model = $model;
    }
}
