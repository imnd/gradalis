<?php

use App\Models\Service\Service;

class ServicesSeeder extends SimpleSeeder
{
    protected $rows = [
        [
            'category_id' => 1,
            'user_id' => null,
            'type' => 1,
            'status' => 1,
            'slug' => 'link-to-service',
            'icon' => '/svg/icons/services/ic_vip.svg',
            'name' => [
                "en" => "Announcement «Bussines VIP»",
                "pl" => "Ogłoszenie „Bussines VIP”",
                "ru" => "Объявление «Bussines VIP»"
            ],
            'description' => [
                "ru" => "Объявление о продаже всегда находится в премиум разделе."
            ],
            'price' => 10000,
            'price_for' => [
                "ru" => "за единицу услуги",
            ],
            'banner' => null,
            'promo_video' => null,
            'preview_text' => null,
            'detail_text' => null,
            'preview_image' => null,
            'visible' => 0,
        ],
        [
            'category_id' => 1,
            'user_id' => null,
            'type' => 1,
            'status' => 1,
            'slug' => 'link-to-service',
            'icon' => '/svg/icons/services/ic_flag.svg',
            'name' => [
                "en" => "Noticeable announcement",
                "pl" => "Zauważalne ogłoszenie",
                "ru" => "Заметное объявление"
            ],
            'description' => [
                "ru" => "Размещение VIP объявления в общем каталоге франшиз на главной и в разделе франшизы."
            ],
            'price' => 10000,
            'price_for' => [
                "en" => "за единицу услуги",
                "pl" => "за единицу услуги",
                "ru" => "за единицу услуги"
            ],
            'banner' => null,
            'promo_video' => null,
            'preview_text' => null,
            'detail_text' => null,
            'preview_image' => null,
            'visible' => 0,
        ],
        [
            'category_id' => 1,
            'user_id' => null,
            'type' => 1,
            'status' => 1,
            'slug' => 'link-to-service',
            'icon' => '/svg/icons/services/ic_translate.svg',
            'name' => [
                "en" => "Перевод документов",
                "pl" => "Tłumaczenie dokumentów",
                "ru" => "Перевод документов"
            ],
            'description' => [
                "ru" => "Наши переводчики сделают перевод документов, необходимых для продажи на русском и английском языках;"
            ],
            'price' => 10000,
            'price_for' => [
                "en" => "за единицу услуги",
                "pl" => "за единицу услуги",
                "ru" => "за единицу услуги"
            ],
            'banner' => null,
            'promo_video' => null,
            'preview_text' => null,
            'detail_text' => null,
            'preview_image' => null,
            'visible' => 0,
        ],
        [
            'category_id' => 1,
            'user_id' => null,
            'type' => 1,
            'status' => 1,
            'slug' => 'link-to-service',
            'icon' => '/svg/icons/services/ic_mentorship.svg',
            'name' => [
                "en" => "Company valuation",
                "pl" => "Wycena firmy",
                "ru" => "Оценка компании"
            ],
            'description' => ["ru" => "За счёт оценки может выясниться, что ваша франшиза стоит дороже, чем вы за неё запрашиваете или дешевле, продайте выгодно!"],
            'price' => '',
            'price_for' => [
                "ru" => "за единицу услуги"
            ],
            'banner' => null,
            'promo_video' => null,
            'preview_text' => null,
            'detail_text' => null,
            'preview_image' => null,
            'visible' => 0,
        ],
        [
            'category_id' => 1,
            'user_id' => null,
            'type' => 1,
            'status' => 1,
            'slug' => 'link-to-service',
            'icon' => '/svg/icons/services/ic_create_presentation.svg',
            'name' => [
                "en" => "Making a presentation",
                "pl" => "Tworzenie prezentacji",
                "ru" => "Создание презентации"
            ],
            'description' => [
                "ru" => "Мы создадим презентацию для покупателя сразу на трех языках."
            ],
            'price' => 1000,
            'price_for' => [
                "ru" => "за единицу услуги"
            ],
            'banner' => null,
            'promo_video' => null,
            'preview_text' => null,
            'detail_text' => null,
            'preview_image' => null,
            'visible' => 0,
        ],
        [
            'category_id' => 1,
            'user_id' => null,
            'type' => 1,
            'status' => 1,
            'slug' => 'link-to-service',
            'icon' => '/svg/icons/services/ic_law.svg',
            'name' => [
                "en" => "«TURBO Sale Bussines»",
                "pl" => "«TURBO Sale Bussines»",
                "ru" => "«TURBO Sale Bussines»"
            ],
            'description' => ["ru" => "Объявление продажи компании будут появляться в Интернете по всему миру, в том числе, в странах бывшего СССР, Ближнего Востока и английский-говорящих странах."],
            'price' => 1000,
            'price_for' => [
                "ru" => "за единицу услуги"
            ],
            'banner' => null,
            'promo_video' => null,
            'preview_text' => null,
            'detail_text' => null,
            'preview_image' => null,
            'visible' => 0,
        ],
    ];

    /**
     * @param Service $model
     * @return void
     */
    public function __construct(Service $model)
    {
        $this->model = $model;
    }
}
