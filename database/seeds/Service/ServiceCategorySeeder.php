<?php

use Illuminate\Database\Seeder,
    App\Models\Service\ServiceCategory;

class ServiceCategorySeeder extends Seeder
{
    
    use TruncateTable;
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //TODO для теста, убрать
        $this->truncate('service_categories');
        
        $serviceCategories = [
            [
                'icon' => 'ic_briefcase.svg',
                'name' => [
                    'ru' => 'Для продавца',
                    'en' => '',
                    'pl' => '',
                ],
            ],
            [
                'icon' => 'ic_shopping-cart.svg',
                'name' => [
                    'ru' => 'Для покупателя',
                    'en' => '',
                    'pl' => '',
                ],
            ],
            [
                'icon' => 'ic_lawyer.svg',
                'name' => [
                    'ru' => 'Юридические',
                    'en' => '',
                    'pl' => '',
                ],
            ],
            [
                'icon' => 'ic_web-dev.svg',
                'name' => [
                    'ru' => 'Для программиста',
                    'en' => '',
                    'pl' => '',
                ],
            ],
            [
                'icon' => 'ic_idea.svg',
                'name' => [
                    'ru' => 'Для бухгалтера',
                    'en' => '',
                    'pl' => '',
                ],
            ],
            [
                'icon' => 'ic_flag.svg',
                'name' => [
                    'ru' => 'Для иностранца',
                    'en' => '',
                    'pl' => '',
                ],
            ],
            [
                'icon' => 'ic_paint-palette.svg',
                'name' => [
                    'ru' => 'Для дизайнера',
                    'en' => '',
                    'pl' => '',
                ],
            ],
            [
                'icon' => 'ic_petri-dish.svg',
                'name' => [
                    'ru' => 'Для кого-то',
                    'en' => '',
                    'pl' => '',
                ],
            ],
            //[
            //    'icon' => 'ic_lawyer.svg',
            //    'name' => [
            //        'ru' => 'Юридические',
            //        'en' => '',
            //        'pl' => '',
            //    ],
            //],
            //[
            //    'icon' => 'ic_lawyer.svg',
            //    'name' => [
            //        'ru' => 'Юридические',
            //        'en' => '',
            //        'pl' => '',
            //    ],
            //],
            //[
            //    'icon' => 'ic_lawyer.svg',
            //    'name' => [
            //        'ru' => 'Юридические',
            //        'en' => '',
            //        'pl' => '',
            //    ],
            //],
        ];
        
        foreach ( $serviceCategories as $serviceCategory ) {
            ServiceCategory::create($serviceCategory);
        }
    }
}
