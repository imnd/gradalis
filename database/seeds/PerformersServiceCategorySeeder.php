<?php

use
    Illuminate\Database\Seeder;
    App\Models\Service\UsersServiceCategory
;

class PerformersServiceCategorySeeder extends SimpleSeeder
{
    protected $rows = [
        [
            'id' => 1,
            'name' => '{"en": "For the seller", "pl": "Для продавца", "ru": "Для продавца"}',
            'icon' => 'ic_briefcase.svg',
        ],
        [
            'id' => 2,
            'name' => '{"en": "Legal", "pl": "Legal", "ru": "Юридические"}',
            'icon' => 'ic_lawyer.svg',
        ],
        [
            'id' => 3,
            'name' => '{"en": "For the programmer", "pl": "For the programmer", "ru": "Для программиста"}',
            'icon' => 'ic_web-dev.svg',
        ],
        [
            'id' => 4,
            'name' => '{"en": "For an accountant", "pl": "For an accountant", "ru": "Для бухгалтера"}',
            'icon' => 'ic_idea.svg',
        ],
        [
            'id' => 5,
            'name' => '{"en": "For buyer", "pl": "", "ru": "Для покупателя"}',
            'icon' => 'ic_shopping-cart.svg',
        ],
        [
            'id' => 6,
            'name' => '{"en": "For a foreigner", "pl": "", "ru": "Для иностранца"}',
            'icon' => 'ic_flag.svg',
        ],
        [
            'id' => 7,
            'name' => '{"en": "For the designer", "pl": "", "ru": "Для дизайнера"}',
            'icon' => 'ic_paint-palette.svg',
        ],
        [
            'id' => 8,
            'name' => '{"en": "For someone", "pl": "", "ru": "Для кого-то"}',
            'icon' => 'ic_petri-dish.svg',
        ],
    ];

    /**
     * @param UsersServiceCategory $model
     * @return void
     */
    public function __construct(UsersServiceCategory $model)
    {
        $this->model = $model;
    }
}
