<?php

use Illuminate\Database\Seeder,
    App\Models\Auth\User,
    App\Models\Roles;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Roles::NAMES as $role) {
            Roles::create(['name' => $role ,'guard_name' => 'web']);
        }
        User::find(1)->assignRole(Roles::ROLE_ADMIN);
        User::find(9)->assignRole(Roles::ROLE_TECH_BRANCH_MANAGER);
        User::find(10)->assignRole(Roles::ROLE_BROKER);
        User::find(11)->assignRole(Roles::ROLE_BROKER);
        User::find(12)->assignRole(Roles::ROLE_BROKER);
    }
}
