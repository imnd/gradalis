<?php

use App\Models\Auth\User;
use App\Models\Language;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use App\Models\Roles;

class PermissionSeeder extends Seeder
{
    /**
     * Все модели для которых нужны разрешения
     *
     */
    private $allModels = [
        'Business',
        'BusinessCategory',
        'City',
        'Consultation',
        'Flight',
        'Franchise',
        'FranchiseCategory',
        'FranchisePackage',
        'Hotel',
        'Meeting',
        'FranchisePackage',
        'Meeting',
        'OrderedService',
        'Service',
        'Travel',
        'User',
        'Tech',
    ];

    /**
     * Отдельные разрешения, которые не относятся к моделям
     *
     */
    private $allPerms = [
        'object-sell',
        'object-buy',
        'assign-tasks',
        'object-moderate',
        'object-reserve',
        //Чат: модерация сообщений
        'сhat_message-moderate',
        //Чат: модерация сообщений (свои пользователи)
        'сhat_message-moderate-my-users',

    ];

    private $translationPerms = [
        'translate-',
        'view-translation-',
        'seo-',
        'ads-'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        foreach ($this->allModels as $model) {
            $resourcePermissions = [
                "create $model"       => "create $model",
                "update $model"       => "update $model",
                "view $model"         => "view $model",
                "delete $model"       => "delete $model",
                "force delete $model" => "force delete $model",
                "restore $model"      => "restore $model",
            ];
            foreach ($resourcePermissions as $resourcePermission) {
                Permission::firstOrCreate(
                    ['name' => $resourcePermission],
                    ['guard_name' => 'web']
                );
            }
        }
        foreach ($this->allPerms as $perm) {
            Permission::firstOrCreate(
                ['name' => $perm],
                ['guard_name' => 'web']
            );

        }
        foreach ($this->translationPerms as $perm) {
            foreach (Language::all() as $lang) {
                Permission::firstOrCreate(
                    ['name' => $perm . $lang->lang],
                    ['guard_name' => 'web']
                );
            }

        }

        User::whereId(1)->first()->assignRole(Roles::ROLE_SELLER);
        User::where('email', 'seller@user.com')->first()->assignRole(Roles::ROLE_SELLER);
        User::whereId(4)->first()->assignRole(Roles::ROLE_BUYER);
        User::where('email', 'content@user.com')->first()->assignRole(Roles::ROLE_CONTENT_MANAGER);
        User::where('email', 'broker@seller.com')->first()->assignRole(Roles::ROLE_SELLERS_BROKER);

        Role::where('name', Roles::ROLE_ADMIN)->first()
            ->givePermissionTo('сhat_message-moderate')
            //->givePermissionTo('object-reserve')
            ->givePermissionTo('translate-ru')
            ->givePermissionTo('translate-pl')
            ->givePermissionTo('translate-en');

        Role::where('name', Roles::ROLE_BUYER)->first()
            ->givePermissionTo('object-reserve')
            ->givePermissionTo('object-buy');

        Role::where('name', Roles::ROLE_SELLER)->first()->givePermissionTo('object-sell');

        Role::where('name', Roles::ROLE_SELLERS_BROKER)->first()
            ->givePermissionTo('object-moderate')
            ->givePermissionTo('assign-tasks');

        Role::where('name', Roles::ROLE_CONTENT_MANAGER)->first()->givePermissionTo('translate-ru');
        Role::where('name', Roles::ROLE_CONTENT_MANAGER)->first()->givePermissionTo('translate-pl');
        Role::where('name', Roles::ROLE_CONTENT_MANAGER)->first()->givePermissionTo('translate-en');
    }
}
