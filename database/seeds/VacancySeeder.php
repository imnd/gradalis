<?php

use Illuminate\Database\Seeder;
use App\Models\Vacancy;
use Faker\Factory as Faker;

class VacancySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        for ($i = 1; $i <= 10; $i++) {
            Vacancy::create([
                "city_id" => 1,
                "name" => "Тестовая вакансия $i",
                "salary" => "От 800$",
                "work_time" => "Частичная/Полная занятость",
                "language" => "Русский и Английский",
                "description" => $faker->realText(500)
            ]);
        }
    }
}
