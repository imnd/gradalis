<?php

use Illuminate\Database\Seeder;

class SimpleSeeder extends Seeder
{
    protected $rows = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        foreach ($this->rows as $row) {
            $model = clone $this->model;
            $model->fill($row);
            $model->save();
        }
    }
}
