<?php

use Illuminate\Database\Seeder;
use App\Models\Terms;

class TermsOfUse extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Terms::create([
            'language_id' => 1,
            'header' => 'Заголовок',
            'buyer_text' => 'Условия договора для покупателя',
            'seller_text' => 'Условия договора для продавца',
        ]);
        
        Terms::create([
            'language_id' => 2,
            'header' => 'Заголовок',
            'buyer_text' => 'Условия договора для покупателя на польском',
            'seller_text' => 'Условия договора для продавца на польском',
        ]);

        Terms::create([
            'language_id' => 3,
            'header' => 'Заголовок',
            'buyer_text' => 'Условия договора для покупателя на англ',
            'seller_text' => 'Условия договора для продавца на англ',
        ]);
    }
}
