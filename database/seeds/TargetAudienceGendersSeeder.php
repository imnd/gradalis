<?php

use App\Models\Franchise\GenderTargetAudience;

class TargetAudienceGendersSeeder extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Мужской',
            'translation' => [
                'ru' => 'Мужской',
                'en' => 'Male',
                'pl' => 'Mężczyzna',
            ],
        ],
        [
            'name' => 'Женский',
            'translation' => [
                'ru' => 'Женский',
                'en' => 'Female',
                'pl' => 'Kobieta',
            ],
        ],
        [
            'name' => 'Не имеет значения',
            'translation' => [
                'ru' => 'Не имеет значения',
                'en' => 'Irrelevant',
                'pl' => 'Nie ma znaczenia',
            ],
        ],
    ];


    /**
     * @param GenderTargetAudience $model
     * @return void
     */
    public function __construct(GenderTargetAudience $model)
    {
        $this->model = $model;
    }
}
