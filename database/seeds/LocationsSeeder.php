<?php

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();

        Eloquent::unguard();

        $path = Storage::disk('data')->path('locations2.sql');
        $username = Config::get('database.connections.mysql.username');
        $password = Config::get('database.connections.mysql.password');
        $database = Config::get('database.connections.mysql.database');
        $host = Config::get('database.connections.mysql.host');
        DB::table('countries')->truncate();
        DB::table('regions')->truncate();
        DB::table('cities')->truncate();
        $command = "mysql -h $host -u $username -p$password $database < $path";
        exec($command);

        Schema::enableForeignKeyConstraints();
    }
}
