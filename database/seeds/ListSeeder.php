<?php

use Illuminate\Database\Seeder;

class ListSeeder extends Seeder
{
    protected $rows = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        foreach ($this->rows as $row) {
            $model = clone $this->model;
            $model->name = $row['name'];
            $model->setTranslations('translation', $row['translation']);
            $model->save();
        }
    }
}
