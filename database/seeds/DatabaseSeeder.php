<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '5000M');
        set_time_limit(3000);
        error_reporting(-1);
        ini_set('display_errors', 1);

        Model::unguard();

        Schema::disableForeignKeyConstraints();
        $this->truncateMultiple([
            'admin_notifications',
            //'businesses',
            //'business_categories',
            'counter_target',
            'countries',
            'cities',
            'campaigns',
            'campaign_resources',
            'conditions',
            'campaign_condition',
            'campaign_targets',
            'consultations',
            'dialogs',
            'dialog_user',
            'equipments',
            'family_status_clients',
            'flights',
            'franchise_penalties',
            'franchise_audience_types',
            //'franchises',
            //'franchise_categories',
            //'franchise_packages',
            //'franchise_objects',
            'gender_target_audiences',
            'help',
            'help_category',
            'help_section',
            'hotels',
            'invitations',
            'invitation_counter',
            'languages',
            'main_advertising_sources_attract_clients',
            'meetings',
            'model_has_roles',
            'model_has_permissions',
            'media',
            'news',
            'news_category',
            'object_requests',
            'ordered_services',
            'partners',
            'payment_transactions',
            'permissions',
            'property_type',
            'roles',
            'role_has_permissions',
            'regions',
            'reviews',
            'services',
            'service_categories',
            'social_status_clients',
            'tariffs',
            'tasks',
            'terms',
            'travels',
            'users',
            'vacancies',
        ]);

        Schema::enableForeignKeyConstraints();

        $this->call(UserTableSeeder::class);
        $this->call(LanguagesSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(LocationsSeeder::class);
        $this->call(BusinessSeeder::class);
        $this->call(FranchiseSeeder::class);
        $this->call(FrachiseEquipmentsSeeder::class);
        //$this->call(FranchisePackageSeeder::class);

        $this->call(ReferralUsersSeeder::class);
        $this->call(PartnerSeeder::class);
        //$this->call(CampaignSeeder::class);
        $this->call(ConditionSeeder::class);
        $this->call(ServiceCategorySeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(TariffsSeeder::class);
        $this->call(OrderedServicesSeeder::class);
        $this->call(TravelsSeeder::class);
        $this->call(FlightsSeeder::class);
        $this->call(HotelsSeeder::class);
        $this->call(MeetingsSeeder::class);
        $this->call(ConsultationsSeeder::class);
        $this->call(DialogSeeder::class);
        $this->call(AdminNofiticationSeeder::class);
        $this->call(PaymentTransactionSeeder::class);
        $this->call(ObjectRequestSeeder::class);

        $this->call(TermsOfUse::class);
        $this->call(VacancySeeder::class);

        //$this->call(SubCatFrachiseSeeder::class);
        $this->call(FrachisePropertyCategories::class);
        $this->call(FrachisePropertyTypes::class);
        //$this->call(FrachisePostTypes::class);
        $this->call(FrachisePenalties::class);
        $this->call(FrachiseOperationRestrictions::class);
        $this->call(FrachiseCertificateDocuments::class);
        $this->call(FrachiseCertificateTypes::class);
        $this->call(FrachiseAudienceTypes::class);
        $this->call(ClientFamilyStatuses::class);
        $this->call(TargetAudienceGendersSeeder::class);
        $this->call(MainAdvertisingSourcesAttractClientsSeeder::class);
        $this->call(ClientSocialStatuses::class);
        $this->call(ReviewsSeeder::class);
        $this->call(HelpSeeder::class);
        $this->call(NewsSeeder::class);

        Model::reguard();
    }
}
