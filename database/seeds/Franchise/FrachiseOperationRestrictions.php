<?php

use App\Models\Franchise\OperationRestrictions;

class FrachiseOperationRestrictions extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Не установлены',
            'translation' => [
                'ru' => 'Не установлены',
                'en' => 'Not set',
                'pl' => 'Nie są ustawione',
            ],
        ],
        [
            'name' => 'Назначенный(ые) франчайзером контрагент(ы)',
            'translation' => [
                'ru' => 'Назначенный(ые) франчайзером контрагент(ы)',
                'en' => 'Contractor(s) appointed by the franchisor(s)',
                'pl' => 'Wykonawca(y) wyznaczony przez franczyzodawcę',
            ],
        ],
        [
            'name' => 'Запрет на открытие конкурирующих организаций',
            'translation' => [
                'ru' => 'Запрет на открытие конкурирующих организаций',
                'en' => 'Ban on the opening of competing organizations',
                'pl' => 'Zakaz otwierania konkurencyjnych organizacji',
            ],
        ],
        [
            'name' => 'Строгий регламент бизнес-процесса',
            'translation' => [
                'ru' => 'Строгий регламент бизнес-процесса',
                'en' => 'Strict business process regulations',
                'pl' => 'Surowe przepisy dotyczące procesów biznesowych',
            ],
        ],
        [
            'name' => 'Согласованные рекламные и маркетинговые мероприятия',
            'translation' => [
                'ru' => 'Согласованные рекламные и маркетинговые мероприятия',
                'en' => 'Agreed advertising and marketing activities',
                'pl' => 'Uzgodnione działania reklamowe i marketingowe',
            ],
        ],
    ];


    /**
     * @param OperationRestrictions $model
     * @return void
     */
    public function __construct(OperationRestrictions $model)
    {
        $this->model = $model;
    }
}
