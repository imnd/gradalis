<?php

use App\Models\Franchise\CertificateDocuments;

class FrachiseCertificateDocuments extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Разрешения на продукцию (товар)',
            'translation' => [
                'ru' => 'Разрешения на продукцию (товар)',
                'en' => 'Product Permits (goods)',
                'pl' => 'Pozwolenia na produkt (towary)',
            ],
        ],
        [
            'name' => 'Разрешения на регулируемые виды деятельности',
            'translation' => [
                'ru' => 'Разрешения на регулируемые виды деятельности',
                'en' => 'Permits for regulated activities',
                'pl' => 'Zezwolenia na działalność regulowaną',
            ],
        ],
        [
            'name' => 'Разрешения для объектов общепита, торговли и услуг',
            'translation' => [
                'ru' => 'Разрешения для объектов общепита, торговли и услуг',
                'en' => 'Permits for catering, trade and services',
                'pl' => 'Zezwolenia na gastronomię, handel i usługi',
            ],
        ],
        [
            'name' => 'Разрешения на строительство и согласование проектов строительств',
            'translation' => [
                'ru' => 'Разрешения на строительство и согласование проектов строительств',
                'en' => 'Building permits and construction projects approval',
                'pl' => 'Pozwolenia budowlane i zatwierdzanie projektów budowlanych',
            ],
        ],
        [
            'name' => 'Сертификация систем управления качеством на предприятии',
            'translation' => [
                'ru' => 'Сертификация систем управления качеством на предприятии',
                'en' => 'Certification of quality management systems in the enterprise',
                'pl' => 'Certyfikacja systemów zarządzania jakością w przedsiębiorstwie',
            ],
        ],
        [
            'name' => 'Лицензии на торговлю алкоголем',
            'translation' => [
                'ru' => 'Лицензии на торговлю алкоголем',
                'en' => 'Licenses for the sale of alcohol',
                'pl' => 'Licencje na sprzedaż alkoholu',
            ],
        ],
        [
            'name' => 'Разрешение на игорный бизнес',
            'translation' => [
                'ru' => 'Разрешение на игорный бизнес',
                'en' => 'Permission to gambling',
                'pl' => 'Pozwolenie na hazard',
            ],
        ],
        [
            'name' => 'Разрешение на банковскую и страховую деятельность',
            'translation' => [
                'ru' => 'Разрешение на банковскую и страховую деятельность',
                'en' => 'Permission for banking and insurance activities',
                'pl' => 'Pozwolenie na działalność bankową i ubezpieczeniową',
            ],
        ],
        [
            'name' => 'Разрешение на манипуляции, связанные с коммунальными услугами',
            'translation' => [
                'ru' => 'Разрешение на манипуляции, связанные с коммунальными услугами',
                'en' => 'Permission to manipulate related utilities',
                'pl' => 'Zezwolenie na manipulowanie powiązanymi narzędziami',
            ],
        ],
        [
            'name' => 'Концессия на мероприятия по охране людей и имущества',
            'translation' => [
                'ru' => 'Концессия на мероприятия по охране людей и имущества',
                'en' => 'Concession for the protection of people and property',
                'pl' => 'Koncesja na ochronę ludzi i mienia',
            ],
        ],
        [
            'name' => 'Концессия OPC (оборот топливом и газом)',
            'translation' => [
                'ru' => 'Концессия OPC (оборот топливом и газом)',
                'en' => 'Fuel and gas turnover concession',
                'pl' => 'Koncesja na obrót paliwami i gazem',
            ],
        ],
        [
            'name' => 'Лицензия на международные грузовые автоперевозки',
            'translation' => [
                'ru' => 'Лицензия на международные грузовые автоперевозки',
                'en' => 'International Freight Trucking License',
                'pl' => 'Międzynarodowa licencja na przewóz ładunków',
            ],
        ],
        [
            'name' => 'Лицензия на международное экспедирование грузов',
            'translation' => [
                'ru' => 'Лицензия на международное экспедирование грузов',
                'en' => 'International freight forwarding license',
                'pl' => 'Międzynarodowa licencja spedycyjna',
            ],
        ],
        [
            'name' => 'Лицензия на международные пассажирские автоперевозки',
            'translation' => [
                'ru' => 'Лицензия на международные пассажирские автоперевозки',
                'en' => 'License for international passenger road transport',
                'pl' => 'Licencja na międzynarodowy pasażerski transport drogowy',
            ],
        ],
    ];


    /**
     * @param CertificateDocuments $model
     * @return void
     */
    public function __construct(CertificateDocuments $model)
    {
        $this->model = $model;
    }
}
