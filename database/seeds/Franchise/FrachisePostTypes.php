<?php

use App\Models\PostTypes;

class FrachisePostTypes extends ListSeeder
{
    protected $rows = [
        [
            'name' => '',
            'translation' => [
                'ru' => '',
                'en' => '',
                'pl' => '',
            ],
        ],
    ];

    /**
     * @param PostTypes $model
     * @return void
     */
    public function __construct(PostTypes $model)
    {
        $this->model = $model;
    }
}
