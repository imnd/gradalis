<?php

use App\Models\Franchise\PropertyCategories;

class FrachisePropertyCategories extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'офисное помещение',
            'translation' => [
                'ru' => 'офисное помещение',
                'en' => 'office',
                'pl' => 'przestrzeń biurowa',
            ],
        ],
        [
            'name' => 'торговое помещение',
            'translation' => [
                'ru' => 'торговое помещение',
                'en' => 'commercial premises',
                'pl' => 'powierzchnia handlowa',
            ],
        ],

    ];

    /**
     * @param PropertyCategories $model
     * @return void
     */
    public function __construct(PropertyCategories $model)
    {
        $this->model = $model;
    }
}
