<?php

use App\Models\Franchise\PropertyTypes;

class FrachisePropertyTypes extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'офисное помещение',
            'translation' => [
                'ru' => 'офисное помещение',
                'en' => 'office space',
                'pl' => 'przestrzeń biurowa',
            ],
        ],
        [
            'name' => 'торговое помещение',
            'translation' => [
                'ru' => 'торговое помещение',
                'en' => 'commercial premises',
                'pl' => 'powierzchnia handlowa',
            ],
        ],
        [
            'name' => 'гостиничное помещение',
            'translation' => [
                'ru' => 'гостиничное помещение',
                'en' => 'hotel room',
                'pl' => 'pokój hotelowy',
            ],
        ],
        [
            'name' => 'гаражное помещение',
            'translation' => [
                'ru' => 'гаражное помещение',
                'en' => 'garage room',
                'pl' => 'pokój garażowy',
            ],
        ],
        [
            'name' => 'складское помещение',
            'translation' => [
                'ru' => 'складское помещение',
                'en' => 'warehouse space',
                'pl' => 'pomieszczenie do przechowywania',
            ],
        ],
        [
            'name' => 'производственное помещение',
            'translation' => [
                'ru' => 'производственное помещение',
                'en' => 'production room',
                'pl' => 'zakład produkcyjny',
            ],
        ],
        [
            'name' => 'кухонное помещение',
            'translation' => [
                'ru' => 'кухонное помещение',
                'en' => 'kitchen room',
                'pl' => 'pokój kuchenny',
            ],
        ],
        [
            'name' => 'обустроенное нежилое помещение',
            'translation' => [
                'ru' => 'обустроенное нежилое помещение',
                'en' => 'furnished non-residential premises',
                'pl' => 'umeblowane pomieszczenia niemieszkalne',
            ],
        ],
        [
            'name' => 'земельный участок коммерческого назначения',
            'translation' => [
                'ru' => 'земельный участок коммерческого назначения',
                'en' => 'commercial land',
                'pl' => 'ziemia komercyjna',
            ],
        ],
        [
            'name' => 'земельный участок промышленного назначения',
            'translation' => [
                'ru' => 'земельный участок промышленного назначения',
                'en' => 'industrial land',
                'pl' => 'ziemia przemysłowa',
            ],
        ],
        [
            'name' => 'земельный участок административного назначения',
            'translation' => [
                'ru' => 'земельный участок административного назначения',
                'en' => 'administrative land',
                'pl' => 'ziemia administracyjna',
            ],
        ],
        [
            'name' => 'земельный участок, предназначенный под застройку',
            'translation' => [
                'ru' => 'земельный участок, предназначенный под застройку',
                'en' => 'land for development',
                'pl' => 'grunt pod zabudowę',
            ],
        ],
        [
            'name' => 'земельный участок сельскохозяйственного назначения',
            'translation' => [
                'ru' => 'земельный участок сельскохозяйственного назначения',
                'en' => 'agricultural land',
                'pl' => 'grunty rolne',
            ],
        ],
    ];


    /**
     * @param PropertyTypes $model
     * @return void
     */
    public function __construct(PropertyTypes $model)
    {
        $this->model = $model;
    }
}
