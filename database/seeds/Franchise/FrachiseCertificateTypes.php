<?php

use App\Models\Franchise\CertificateTypes;

class FrachiseCertificateTypes extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Лицензия',
            'translation' => [
                'ru' => 'Лицензия',
                'en' => 'License',
                'pl' => 'Licencja',
            ],
        ],
        [
            'name' => 'Сертификат',
            'translation' => [
                'ru' => 'Сертификат',
                'en' => 'Certificate',
                'pl' => 'Certyfikat',
            ],
        ],
        [
            'name' => 'Разрешение',
            'translation' => [
                'ru' => 'Разрешение',
                'en' => 'Resolution',
                'pl' => 'Rozdzielczość',
            ],
        ],
    ];


    /**
     * @param CertificateTypes $model
     * @return void
     */
    public function __construct(CertificateTypes $model)
    {
        $this->model = $model;
    }
}
