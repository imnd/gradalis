<?php

use App\Models\Franchise\AudienceTypes;

class FrachiseAudienceTypes extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'B2C',
            'translation' => [
                'ru' => 'B2C',
                'en' => 'B2C',
                'pl' => 'B2C',
            ],
        ],
        [
            'name' => 'B2B',
            'translation' => [
                'ru' => 'B2B',
                'en' => 'B2B',
                'pl' => 'B2B',
            ],
        ],
        [
            'name' => 'B2C и B2B',
            'translation' => [
                'ru' => 'B2C и B2B',
                'en' => 'B2C and B2B',
                'pl' => 'B2C i B2B',
            ],
        ],
    ];

    /**
     * @param AudienceTypes $model
     * @return void
     */
    public function __construct(AudienceTypes $model)
    {
        $this->model = $model;
    }
}
