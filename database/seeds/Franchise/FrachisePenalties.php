<?php

use App\Models\Franchise\Penalties;

class FrachisePenalties extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Оперативно-хозяйственные санкции за нарушение хозяйственных обязательств',
            'translation' => [
                'ru' => 'Оперативно-хозяйственные санкции за нарушение хозяйственных обязательств',
                'en' => 'Operational economic sanctions for violation of economic obligations',
                'pl' => 'Operacyjne sankcje ekonomiczne za naruszenie zobowiązań gospodarczych',
            ],
        ],
        [
            'name' => 'Оперативно-хозяйственные санкции за нарушение денежных обязательств',
            'translation' => [
                'ru' => 'Оперативно-хозяйственные санкции за нарушение денежных обязательств',
                'en' => 'Operational economic sanctions for violation of monetary obligations',
                'pl' => 'Operacyjne sankcje ekonomiczne za naruszenie zobowiązań pieniężnych',
            ],
        ],
        [
            'name' => 'Штрафные санкции за нарушение денежных обязательств',
            'translation' => [
                'ru' => 'Штрафные санкции за нарушение денежных обязательств',
                'en' => 'Penalties for violation of monetary obligations',
                'pl' => 'Kary za naruszenie zobowiązań pieniężnych',
            ],
        ],
        [
            'name' => 'Штрафные санкции за хозяйственное правонарушение',
            'translation' => [
                'ru' => 'Штрафные санкции за хозяйственное правонарушение',
                'en' => 'Penalties for a business offense',
                'pl' => 'Kary za wykroczenie służbowe',
            ],
        ],
        [
            'name' => 'Штрафные санкции за неправомерное пользование чужими средствами',
            'translation' => [
                'ru' => 'Штрафные санкции за неправомерное пользование чужими средствами',
                'en' => 'Penalties for improper use of other people\'s funds',
                'pl' => 'Kary za niewłaściwe wykorzystanie funduszy innych osób',
            ],
        ],
        [
            'name' => 'Штрафные санкции за просрочку исполнения хозяйственных обязательства',
            'translation' => [
                'ru' => 'Штрафные санкции за просрочку исполнения хозяйственных обязательства',
                'en' => 'Penalties for delay in execution of economic obligations',
                'pl' => 'Kary za opóźnienie w wykonaniu zobowiązań gospodarczych',
            ],
        ],
        [
            'name' => 'Административно-хозяйственные санкции за нарушение правил осуществления хозяйственной деятельности',
            'translation' => [
                'ru' => 'Административно-хозяйственные санкции за нарушение правил осуществления хозяйственной деятельности',
                'en' => 'Administrative and economic sanctions for violation of the rules of economic activity',
                'pl' => 'Sankcje administracyjne i gospodarcze za naruszenie zasad działalności gospodarczej',
            ],
        ],
        [
            'name' => 'Административно-хозяйственные санкции организационно-правового или имущественного характера',
            'translation' => [
                'ru' => 'Административно-хозяйственные санкции организационно-правового или имущественного характера',
                'en' => 'Administrative and economic sanctions of organizational legal or property nature',
                'pl' => 'Sankcje administracyjne i ekonomiczne organizacyjnej natury prawnej lub majątkowej',
            ],
        ],
        [
            'name' => 'Изъятие прибыли (дохода) полученной вследствие нарушения правил осуществления хозяйственной деятельности',
            'translation' => [
                'ru' => 'Изъятие прибыли (дохода) полученной вследствие нарушения правил осуществления хозяйственной деятельности',
                'en' => 'Withdrawal of profit (income) received due to violation of business rules',
                'pl' => 'Wycofanie zysku (dochodu) otrzymanego z powodu naruszenia zasad biznesowych',
            ],
        ],
        [
            'name' => 'Административно-хозяйственный штраф за нарушение правил осуществления хозяйственной деятельности',
            'translation' => [
                'ru' => 'Административно-хозяйственный штраф за нарушение правил осуществления хозяйственной деятельности',
                'en' => 'Administrative and economic penalty for violation of the rules of economic activity',
                'pl' => 'Kara administracyjna i ekonomiczna za naruszenie zasad działalności gospodarczej',
            ],
        ],
        [
            'name' => 'Изъятие имущества, продукции (товаров) неправомерно изготовленных или неправомерно использованных',
            'translation' => [
                'ru' => 'Изъятие имущества, продукции (товаров) неправомерно изготовленных или неправомерно использованных',
                'en' => 'Seizure of property, products (goods) illegally manufactured or illegally used',
                'pl' => 'Zajęcie mienia, produktów (towarów) nielegalnie wyprodukowanych lub nielegalnie używanych',
            ],
        ],
        [
            'name' => 'Остановка расходных операций за нарушение налогового законодательства',
            'translation' => [
                'ru' => 'Остановка расходных операций за нарушение налогового законодательства',
                'en' => 'Stopping of expenditure transactions for violation of tax laws',
                'pl' => 'Zatrzymywanie transakcji wydatków z powodu naruszenia przepisów podatkowych',
            ],
        ],
        [
            'name' => 'Прекращение экспортно-импортных операций в случаях недобросовестной конкуренции',
            'translation' => [
                'ru' => 'Прекращение экспортно-импортных операций в случаях недобросовестной конкуренции',
                'en' => 'Termination of export-import transactions in cases of unfair competition',
                'pl' => 'Zakończenie transakcji importowo-eksportowych w przypadku nieuczciwej konkurencji',
            ],
        ],
        [
            'name' => 'Прекращение экспортно-импортных операций за нанесение ущерба внешнеэкономической деятельностью экономике страны',
            'translation' => [
                'ru' => 'Прекращение экспортно-импортных операций за нанесение ущерба внешнеэкономической деятельностью экономике страны',
                'en' => 'Termination of export-import operations for damage to foreign economic activity of the country’s economy',
                'pl' => 'Zakończenie operacji importowo-eksportowych z powodu szkód w zagranicznej działalności gospodarczej gospodarki kraju',
            ],
        ],
        [
            'name' => 'Ограничение или приостановка хозяйственной деятельности за нарушение экологических требований',
            'translation' => [
                'ru' => 'Ограничение или приостановка хозяйственной деятельности за нарушение экологических требований',
                'en' => 'Restriction or suspension of economic activity for violation of environmental requirements',
                'pl' => 'Ograniczenie lub zawieszenie działalności gospodarczej z powodu naruszenia wymogów ochrony środowiska',
            ],
        ],
        [
            'name' => 'Штрафные санкции за нарушение экологических требований',
            'translation' => [
                'ru' => 'Штрафные санкции за нарушение экологических требований',
                'en' => 'Penalties for violation of environmental requirements',
                'pl' => 'Kary za naruszenie wymogów ochrony środowiska',
            ],
        ],

    ];

    /**
     * @param Penalties $model
     * @return void
     */
    public function __construct(Penalties $model)
    {
        $this->model = $model;
    }
}
