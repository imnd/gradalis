<?php

use App\Models\Franchise\TrainingMaterialTypes;

class FrachiseTrainingMaterialTypes extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Электронные обучающие модули',
            'translation' => [
                'ru' => 'Электронные обучающие модули',
                'en' => 'Electronic learning modules',
                'pl' => 'Elektroniczne moduły szkoleniowe',
            ],
        ],
        [
            'name' => 'Материал по тренингам',
            'translation' => [
                'ru' => 'Материал по тренингам',
                'en' => 'Training material',
                'pl' => 'Materiał treningowy',
            ],
        ],
        [
            'name' => 'Руководство по эксплуатации',
            'translation' => [
                'ru' => 'Руководство по эксплуатации',
                'en' => 'Manual',
                'pl' => 'Instrukcja obsługi',
            ],
        ],
        [
            'name' => 'Видеоролики',
            'translation' => [
                'ru' => 'Видеоролики',
                'en' => 'Videos',
                'pl' => 'Filmy',
            ],
        ],
        [
            'name' => 'Постоянный представитель',
            'translation' => [
                'ru' => 'Постоянный представитель',
                'en' => 'Permanent representative',
                'pl' => 'Stały przedstawiciel',
            ],
        ],
    ];

    /**
     * @param TrainingMaterialTypes $model
     * @return void
     */
    public function __construct(TrainingMaterialTypes $model)
    {
        $this->model = $model;
    }
}
