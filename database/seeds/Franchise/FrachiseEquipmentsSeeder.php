<?php

use App\Models\Franchise\Equipments;

class FrachiseEquipmentsSeeder extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Инженерное осветительное оборудование',
            'translation' => [
                'ru' => 'Инженерное осветительное оборудование',
                'en' => 'Engineering lighting equipment',
                'pl' => 'Sprzęt oświetleniowy dla inżynierów',
            ],
        ],
        [
            'name' => 'Инженерное отопительное оборудование',
            'translation' => [
                'ru' => 'Инженерное отопительное оборудование',
                'en' => 'Engineering heating equipment',
                'pl' => 'Inżynieryjne urządzenia grzewcze',
            ],
        ],
        [
            'name' => 'Измерительно-контрольное инженерное оборудование',
            'translation' => [
                'ru' => 'Измерительно-контрольное инженерное оборудование',
                'en' => 'Measuring and control engineering equipment',
                'pl' => 'Sprzęt pomiarowy i sterujący',
            ],
        ],
        [
            'name' => 'Интерьерное коммерческое оборудование',
            'translation' => [
                'ru' => 'Интерьерное коммерческое оборудование',
                'en' => 'Interior commercial equipment',
                'pl' => 'Wyposażenie wnętrz handlowych',
            ],
        ],
        [
            'name' => 'Интерьерное офисное оборудование',
            'translation' => [
                'ru' => 'Интерьерное офисное оборудование',
                'en' => 'Interior Office Equipment',
                'pl' => 'Wyposażenie wnętrz biurowych',
            ],
        ],
        [
            'name' => 'Специальное технологическое оборудование',
            'translation' => [
                'ru' => 'Специальное технологическое оборудование',
                'en' => 'Special technological equipment',
                'pl' => 'Specjalne wyposażenie technologiczne',
            ],
        ],
        [
            'name' => 'Вспомогательное технологическое оборудование',
            'translation' => [
                'ru' => 'Вспомогательное технологическое оборудование',
                'en' => 'Auxiliary process equipment',
                'pl' => 'Pomocnicze urządzenia procesowe',
            ],
        ],
        [
            'name' => 'Ортопедическое оборудование',
            'translation' => [
                'ru' => 'Ортопедическое оборудование',
                'en' => 'Orthopedic equipment',
                'pl' => 'Sprzęt ortopedyczny',
            ],
        ],
        [
            'name' => 'Оборудование для косметологических процедур',
            'translation' => [
                'ru' => 'Оборудование для косметологических процедур',
                'en' => 'Equipment for cosmetology procedures',
                'pl' => 'Sprzęt do zabiegów kosmetologicznych',
            ],
        ],
        [
            'name' => 'Оборудование для парикмахерских процедур',
            'translation' => [
                'ru' => 'Оборудование для парикмахерских процедур',
                'en' => 'Hairdressing equipment',
                'pl' => 'Sprzęt fryzjerski',
            ],
        ],
        [
            'name' => 'Компьютерное оборудование',
            'translation' => [
                'ru' => 'Компьютерное оборудование',
                'en' => 'Computer equipment',
                'pl' => 'Sprzęt komputerowy',
            ],
        ],
        [
            'name' => 'Офисная оргтехника',
            'translation' => [
                'ru' => 'Офисная оргтехника',
                'en' => 'Office equipment',
                'pl' => 'Sprzęt biurowy',
            ],
        ],
        [
            'name' => 'Лицензионное программное обеспечение',
            'translation' => [
                'ru' => 'Лицензионное программное обеспечение',
                'en' => 'Licensed software',
                'pl' => 'Licencjonowane oprogramowanie',
            ],
        ],
        [
            'name' => 'Цифровая вычислительная техника',
            'translation' => [
                'ru' => 'Цифровая вычислительная техника',
                'en' => 'Digital computing',
                'pl' => 'Komputery cyfrowe',
            ],
        ],
        [
            'name' => 'Аналоговая вычислительная техника',
            'translation' => [
                'ru' => 'Аналоговая вычислительная техника',
                'en' => 'Analog computing',
                'pl' => 'Obliczenia analogowe',
            ],
        ],
        [
            'name' => 'Гибридная вычислительная техника',
            'translation' => [
                'ru' => 'Гибридная вычислительная техника',
                'en' => 'Hybrid computing technology',
                'pl' => 'Hybrydowa technologia obliczeniowa',
            ],
        ],
        [
            'name' => 'Оборудование систем электронных расчетов и платежей',
            'translation' => [
                'ru' => 'Оборудование систем электронных расчетов и платежей',
                'en' => 'Equipment for electronic payment and payment systems',
                'pl' => 'Sprzęt do elektronicznych systemów płatności i płatności',
            ],
        ],
        [
            'name' => 'Коммерческое кухонное оборудование',
            'translation' => [
                'ru' => 'Коммерческое кухонное оборудование',
                'en' => 'Commercial Kitchen Equipment',
                'pl' => 'Handlowy sprzęt kuchenny',
            ],
        ],
        [
            'name' => 'Коммерческое ресторанное оборудование',
            'translation' => [
                'ru' => 'Коммерческое ресторанное оборудование',
                'en' => 'Commercial restaurant equipment',
                'pl' => 'Wyposażenie restauracji komercyjnych',
            ],
        ],
        [
            'name' => 'Тепловое коммерческое оборудование',
            'translation' => [
                'ru' => 'Тепловое коммерческое оборудование',
                'en' => 'Thermal Commercial Equipment',
                'pl' => 'Termiczne wyposażenie handlowe',
            ],
        ],
        [
            'name' => 'Обогревательное коммерческое оборудование',
            'translation' => [
                'ru' => 'Обогревательное коммерческое оборудование',
                'en' => 'Commercial Heating Equipment',
                'pl' => 'Komercyjne urządzenia grzewcze',
            ],
        ],
        [
            'name' => 'Холодильное коммерческое оборудование',
            'translation' => [
                'ru' => 'Холодильное коммерческое оборудование',
                'en' => 'Commercial refrigeration equipment',
                'pl' => 'Komercyjne urządzenia chłodnicze',
            ],
        ],
        [
            'name' => 'Микроволновое коммерческое оборудование',
            'translation' => [
                'ru' => 'Микроволновое коммерческое оборудование',
                'en' => 'Microwave Commercial Equipment',
                'pl' => 'Sprzęt kuchenny mikrofalowy',
            ],
        ],
        [
            'name' => 'Вакуумное коммерческое оборудование',
            'translation' => [
                'ru' => 'Вакуумное коммерческое оборудование',
                'en' => 'Vacuum Commercial Equipment',
                'pl' => 'Próżniowe wyposażenie handlowe',
            ],
        ],
        [
            'name' => 'Индукционное коммерческое оборудование',
            'translation' => [
                'ru' => 'Индукционное коммерческое оборудование',
                'en' => 'Induction Commercial Equipment',
                'pl' => 'Indukcyjne wyposażenie handlowe',
            ],
        ],
        [
            'name' => 'Электрическое тепловое коммерческое оборудование',
            'translation' => [
                'ru' => 'Электрическое тепловое коммерческое оборудование',
                'en' => 'Electric thermal commercial equipment',
                'pl' => 'Elektryczne urządzenia handlowe',
            ],
        ],
        [
            'name' => 'Огневое тепловое коммерческое оборудование',
            'translation' => [
                'ru' => 'Огневое тепловое коммерческое оборудование',
                'en' => 'Fire thermal commercial equipment',
                'pl' => 'Sprzęt do ognioodpornych urządzeń handlowych',
            ],
        ],
        [
            'name' => 'Паровое тепловое коммерческое оборудование',
            'translation' => [
                'ru' => 'Паровое тепловое коммерческое оборудование',
                'en' => 'Steam Heat Commercial Equipment',
                'pl' => 'Wyposażenie handlowe do ogrzewania parowego',
            ],
        ],
        [
            'name' => 'Газовое тепловое коммерческое оборудование',
            'translation' => [
                'ru' => 'Газовое тепловое коммерческое оборудование',
                'en' => 'Gas heat commercial equipment',
                'pl' => 'Sprzęt do handlu ciepłem gazowym',
            ],
        ],
        [
            'name' => 'Нейтральное коммерческое оборудование',
            'translation' => [
                'ru' => 'Нейтральное коммерческое оборудование',
                'en' => 'Neutral Commercial Equipment',
                'pl' => 'Neutralne wyposażenie komercyjne',
            ],
        ],
        [
            'name' => 'Электромеханическое коммерческое оборудование',
            'translation' => [
                'ru' => 'Электромеханическое коммерческое оборудование',
                'en' => 'Electromechanical commercial equipment',
                'pl' => 'Elektromechaniczny sprzęt handlowy',
            ],
        ],
        [
            'name' => 'Торговое инвентарное оборудование',
            'translation' => [
                'ru' => 'Торговое инвентарное оборудование',
                'en' => 'Trade Inventory Equipment',
                'pl' => 'Wyposażenie ekwipunku handlowego',
            ],
        ],
        [
            'name' => 'Торговое холодильное оборудование',
            'translation' => [
                'ru' => 'Торговое холодильное оборудование',
                'en' => 'Commercial refrigeration equipment',
                'pl' => 'Komercyjne urządzenia chłodnicze',
            ],
        ],
        [
            'name' => 'Холодильные прилавки и витрины',
            'translation' => [
                'ru' => 'Холодильные прилавки и витрины',
                'en' => 'Refrigerated counters and showcases',
                'pl' => 'Chłodzone lady i gabloty',
            ],
        ],
        [
            'name' => 'Торговое измерительное оборудование',
            'translation' => [
                'ru' => 'Торговое измерительное оборудование',
                'en' => 'Trade measuring equipment',
                'pl' => 'Sprzęt pomiarowy handlu',
            ],
        ],
        [
            'name' => 'Контрольно-кассовое оборудование',
            'translation' => [
                'ru' => 'Контрольно-кассовое оборудование',
                'en' => 'Cash register equipment',
                'pl' => 'Sprzęt kasowy',
            ],
        ],
        [
            'name' => 'Вендинговые аппараты',
            'translation' => [
                'ru' => 'Вендинговые аппараты',
                'en' => 'Vending machines',
                'pl' => 'Automaty sprzedające',
            ],
        ],
        [
            'name' => 'Коммерческое оборудование для бара',
            'translation' => [
                'ru' => 'Коммерческое оборудование для бара',
                'en' => 'Commercial Bar Equipment',
                'pl' => 'Wyposażenie barów komercyjnych',
            ],
        ],
        [
            'name' => 'Электромеханическое производственное оборудование',
            'translation' => [
                'ru' => 'Электромеханическое производственное оборудование',
                'en' => 'Electromechanical production equipment',
                'pl' => 'Elektromechaniczne urządzenia produkcyjne',
            ],
        ],
        [
            'name' => 'Механическое производственное оборудование',
            'translation' => [
                'ru' => 'Механическое производственное оборудование',
                'en' => 'Mechanical production equipment',
                'pl' => 'Sprzęt do produkcji mechanicznej',
            ],
        ],
        [
            'name' => 'Химическое производственное оборудование',
            'translation' => [
                'ru' => 'Химическое производственное оборудование',
                'en' => 'Chemical Production Equipment',
                'pl' => 'Sprzęt do produkcji chemicznej',
            ],
        ],
        [
            'name' => 'Термическое производственное оборудование',
            'translation' => [
                'ru' => 'Термическое производственное оборудование',
                'en' => 'Thermal production equipment',
                'pl' => 'Urządzenia do produkcji termicznej',
            ],
        ],
        [
            'name' => 'Оборудование общепромышленного назначения',
            'translation' => [
                'ru' => 'Оборудование общепромышленного назначения',
                'en' => 'General industrial equipment',
                'pl' => 'Ogólne urządzenia przemysłowe',
            ],
        ],
        [
            'name' => 'Производственное оборудование специального назначения',
            'translation' => [
                'ru' => 'Производственное оборудование специального назначения',
                'en' => 'Special purpose production equipment',
                'pl' => 'Sprzęt produkcyjny specjalnego przeznaczenia',
            ],
        ],
        [
            'name' => 'Инновационное производственное оборудование',
            'translation' => [
                'ru' => 'Инновационное производственное оборудование',
                'en' => 'Innovative production equipment',
                'pl' => 'Innowacyjny sprzęt produkcyjny',
            ],
        ],
        [
            'name' => 'Коммерческое прачечное оборудование',
            'translation' => [
                'ru' => 'Коммерческое прачечное оборудование',
                'en' => 'Commercial laundry equipment',
                'pl' => 'Komercyjny sprzęt do prania',
            ],
        ],
        [
            'name' => 'Оборудование бесперебойного электропитания',
            'translation' => [
                'ru' => 'Оборудование бесперебойного электропитания',
                'en' => 'Uninterruptible power supply equipment',
                'pl' => 'Urządzenia do zasilania awaryjnego',
            ],
        ],
        [
            'name' => 'Телекоммуникационное оборудование',
            'translation' => [
                'ru' => 'Телекоммуникационное оборудование',
                'en' => 'Telecommunication equipment',
                'pl' => 'Sprzęt telekomunikacyjny',
            ],
        ],
        [
            'name' => 'Сетевое оборудование',
            'translation' => [
                'ru' => 'Сетевое оборудование',
                'en' => 'network hardware',
                'pl' => 'Sprzęt sieciowy',
            ],
        ],
        [
            'name' => 'Серверное оборудование',
            'translation' => [
                'ru' => 'Серверное оборудование',
                'en' => 'Server equipment',
                'pl' => 'Sprzęt serwerowy',
            ],
        ],
        [
            'name' => 'Техническое оборудование сервисного назначения',
            'translation' => [
                'ru' => 'Техническое оборудование сервисного назначения',
                'en' => 'Technical equipment for service purposes',
                'pl' => 'Wyposażenie techniczne do celów serwisowych',
            ],
        ],
        [
            'name' => 'Погрузочный автотранспорт',
            'translation' => [
                'ru' => 'Погрузочный автотранспорт',
                'en' => 'Loading vehicles',
                'pl' => 'Ładowanie pojazdów',
            ],
        ],
        [
            'name' => 'Автомобильная спецтехника',
            'translation' => [
                'ru' => 'Автомобильная спецтехника',
                'en' => 'Automotive special equipment',
                'pl' => 'Specjalny sprzęt samochodowy',
            ],
        ],
        [
            'name' => 'Большегрузные автомобили',
            'translation' => [
                'ru' => 'Большегрузные автомобили',
                'en' => 'Heavy trucks',
                'pl' => 'Ciężkie ciężarówki',
            ],
        ],
        [
            'name' => 'Коммерческий автотранспорт',
            'translation' => [
                'ru' => 'Коммерческий автотранспорт',
                'en' => 'Commercial vehicles',
                'pl' => 'Pojazdy użytkowe',
            ],
        ],
        [
            'name' => 'Строительная спецтехника',
            'translation' => [
                'ru' => 'Строительная спецтехника',
                'en' => 'Строительная спецтехника',
                'pl' => 'Maszyny budowlane',
            ],
        ],
        [
            'name' => 'Оборудование по переработке и утилизации органических отходов',
            'translation' => [
                'ru' => 'Оборудование по переработке и утилизации органических отходов',
                'en' => 'Equipment for the processing and disposal of organic waste',
                'pl' => 'Sprzęt do przetwarzania i usuwania odpadów organicznych',
            ],
        ],
        [
            'name' => 'Оборудование по переработке и утилизации непищевых отходов',
            'translation' => [
                'ru' => 'Оборудование по переработке и утилизации непищевых отходов',
                'en' => 'Equipment for the processing and disposal of non-food waste',
                'pl' => 'Sprzęt do przetwarzania i usuwania odpadów nieżywnościowych',
            ],
        ],
        [
            'name' => 'Оборудование по переработке пищевых материалов',
            'translation' => [
                'ru' => 'Оборудование по переработке пищевых материалов',
                'en' => 'Food processing equipment',
                'pl' => 'Sprzęt do przetwarzania żywności',
            ],
        ],
        [
            'name' => 'Лабораторное медицинское оборудование',
            'translation' => [
                'ru' => 'Лабораторное медицинское оборудование',
                'en' => 'Laboratory Medical Equipment',
                'pl' => 'Laboratoryjny sprzęt medyczny',
            ],
        ],
        [
            'name' => 'Диагностическое медицинское оборудование',
            'translation' => [
                'ru' => 'Диагностическое медицинское оборудование',
                'en' => 'Diagnostic Medical Equipment',
                'pl' => 'Diagnostyczny sprzęt medyczny',
            ],
        ],
        [
            'name' => 'Стоматологическое оборудование',
            'translation' => [
                'ru' => 'Стоматологическое оборудование',
                'en' => 'Dental equipment',
                'pl' => 'Sprzęt dentystyczny',
            ],
        ],
        [
            'name' => 'Офтальмологическое оборудование',
            'translation' => [
                'ru' => 'Офтальмологическое оборудование',
                'en' => 'Ophthalmological equipment',
                'pl' => 'Sprzęt okulistyczny',
            ],
        ],
        [
            'name' => 'Медицинское оборудование по стерилизации и дезинфекции',
            'translation' => [
                'ru' => 'Медицинское оборудование по стерилизации и дезинфекции',
                'en' => 'Sterilization and disinfection medical equipment',
                'pl' => 'Sterylizacja i dezynfekcja sprzętu medycznego',
            ],
        ],
        [
            'name' => 'Медицинское оборудование по реанимации и анестезии',
            'translation' => [
                'ru' => 'Медицинское оборудование по реанимации и анестезии',
                'en' => 'Medical equipment for resuscitation and anesthesia',
                'pl' => 'Sprzęt medyczny do resuscytacji i znieczulenia',
            ],
        ],
        [
            'name' => 'Хирургическое оборудование',
            'translation' => [
                'ru' => 'Хирургическое оборудование',
                'en' => 'Surgical Equipment',
                'pl' => 'Sprzęt chirurgiczny',
            ],
        ],
        [
            'name' => 'Высокоточное измерительное оборудование',
            'translation' => [
                'ru' => 'Высокоточное измерительное оборудование',
                'en' => 'Precision measuring equipment',
                'pl' => 'Precyzyjny sprzęt pomiarowy',
            ],
        ],
        [
            'name' => 'Техническое сенсорное оборудование',
            'translation' => [
                'ru' => 'Техническое сенсорное оборудование',
                'en' => 'Technical sensor equipment',
                'pl' => 'Wyposażenie czujników technicznych',
            ],
        ],
        [
            'name' => 'Сигнализационное оборудование',
            'translation' => [
                'ru' => 'Сигнализационное оборудование',
                'en' => 'Signaling equipment',
                'pl' => 'Sprzęt sygnalizacyjny',
            ],
        ],
        [
            'name' => 'Оборудование для пожаротушения',
            'translation' => [
                'ru' => 'Оборудование для пожаротушения',
                'en' => 'Fire Fighting Equipment',
                'pl' => 'Sprzęt gaśniczy',
            ],
        ],

    ];

    /**
     * @param Equipments $model
     * @return void
     */
    public function __construct(Equipments $model)
    {
        $this->model = $model;
    }
}