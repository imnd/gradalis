<?php

use App\Models\Franchise\Franchise,
    App\Models\Franchise\FranchiseCategory,
    App\Models\Language,
    App\Services\Helpers,
    Faker\Factory as Faker,
    Illuminate\Database\Seeder
;

/**
 * Class FranchiseSeeder
 */
class FranchiseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        $files = Storage::allFiles('data/images');

        $franchiseCategory = new FranchiseCategory;
        $franchiseCategory->name = 'Недвижимость';
        $franchiseCategory->setTranslations('translation', [
            'ru' => 'Недвижимость',
            'en' => 'Realty',
            'pl' => 'Nieruchomości',
        ]);
        $franchiseCategory->save();

        $franchiseCategory = new FranchiseCategory;
        $franchiseCategory->name = 'Фармацевтика';
        $franchiseCategory->setTranslations('translation', [
            'ru' => 'Фармацевтика',
            'en' => 'Pharmaceuticals',
            'pl' => 'Farmaceutyki',
        ]);
        $franchiseCategory->save();
    
        factory(Franchise::class, 20)->make()->each(function ($franchise) use ($files) {
            foreach (Language::all() as $lang) {
                $faker = [
                    'ru' => Faker::create('ru_RU'),
                    'en' => Faker::create('en_EN'),
                    'pl' => Faker::create('pl_PL')
                ][$lang->lang];
                $name = "Франшиза - {$faker->company}";
                $url = Helpers::transliterate($name);
                $description = $faker->realText(160);
                $franchise->setTranslation('name', $lang->lang, $name);
                $franchise->setTranslation('description', $lang->lang, $description);
                $franchise->setTranslation('url', $lang->lang, $url);
                $franchise->setTranslation('seo_title', $lang->lang, $name);
                $franchise->setTranslation('seo_description', $lang->lang,$description);
                $franchise->setTranslation('seo_keywords', $lang->lang, $faker->words(4, true));
                $faker = null;
            }
            $franchise->save();
            foreach ($files as $file) {
                $franchise->addMedia(storage_path("app/$file"))
                    ->preservingOriginal()
                    ->toMediaCollection('franchise', 'franchise');
            }
        });

        Schema::enableForeignKeyConstraints();
    }
}
