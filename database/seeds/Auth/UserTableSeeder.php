<?php

use App\Models\Auth\User,
    Carbon\Carbon,
    Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        $subscribes = [
            'ordered_service_status' => false,
            'new_message'            => false,
            'personal_selection'     => false,
            'offer'                  => false,
        ];

        $cities = [1, 57]; //Москва и Варшава

        // Add the master administrator, user id of 1
        User::create([
            'first_name'        => 'Admin',
            'last_name'         => 'Istrator',
            'email'             => 'admin@admin.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => $cities[rand(0, 1)],
            'sum_from'          => 10000,
            'sum_to'            => 1000000,
            'balance'           => 2000,
            'api_token'         => substr(base64_encode(openssl_random_pseudo_bytes(60)), 0, 60),
        ]);

        User::create([
            'first_name'        => 'Executive',
            'last_name'         => 'User',
            'email'             => 'executive@executive.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => $cities[rand(0, 1)],
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        User::create([
            'first_name'        => 'Default',
            'last_name'         => 'User',
            'email'             => 'user@user.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => $cities[rand(0, 1)],
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        User::create([
            'first_name'        => 'Buyer',
            'last_name'         => 'Buyer',
            'email'             => 'buyer@user.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => $cities[rand(0, 1)],
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        User::create([
            'first_name'        => 'Seller',
            'last_name'         => 'Seller',
            'email'             => 'seller@user.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => $cities[rand(0, 1)],
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        User::create([
            'first_name'        => 'Content',
            'last_name'         => 'Manager',
            'email'             => 'content@user.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => $cities[rand(0, 1)],
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        User::create([
            'first_name'        => 'Broker',
            'last_name'         => 'Seller',
            'email'             => 'broker@seller.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => $cities[rand(0, 1)],
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        User::create([
            'first_name'        => 'Broker Two',
            'last_name'         => 'Seller',
            'email'             => 'broker_two@seller.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => $cities[rand(0, 1)],
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        //ID 9
        User::create([
            'first_name'        => 'Branch',
            'last_name'         => 'Manager',
            'email'             => 'branch@manager.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => 1,
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        //ID 10
        User::create([
            'first_name'        => 'Support',
            'last_name'         => 'Support',
            'email'             => 'support@support.com',
            'password'          => Hash::make('secret'),
            'email_verified_at' => Carbon::now(),
            'subscribes'        => $subscribes,
            'city_id'           => 1,
            'sum_from'          => 10000,
            'sum_to'            => 1000000
        ]);

        $this->enableForeignKeys();
    }
}
