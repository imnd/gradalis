<?php

use App\Models\Franchise\SocialStatusClient;

class ClientSocialStatuses extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Дети дошкольного возраста',
            'translation' => [
                'ru' => 'Дети дошкольного возраста',
                'en' => 'Preschool children',
                'pl' => 'Dzieci w wieku przedszkolnym',
            ],
        ],
        [
            'name' => 'Домохозяйки',
            'translation' => [
                'ru' => 'Домохозяйки',
                'en' => 'Housewives',
                'pl' => 'Gospodynie domowe',
            ],
        ],
        [
            'name' => 'Спортсмены-профессионалы',
            'translation' => [
                'ru' => 'Спортсмены-профессионалы',
                'en' => 'Professional athletes',
                'pl' => 'Zawodowi sportowcy',
            ],
        ],
        [
            'name' => 'Спортсмены-любители',
            'translation' => [
                'ru' => 'Спортсмены-любители',
                'en' => 'Amateur athletes',
                'pl' => 'Sportowcy amatorzy',
            ],
        ],
        [
            'name' => 'Служащие',
            'translation' => [
                'ru' => 'Служащие',
                'en' => 'Employees',
                'pl' => 'Pracownicy',
            ],
        ],
        [
            'name' => 'Школьники',
            'translation' => [
                'ru' => 'Школьники',
                'en' => 'School child',
                'pl' => 'Uczeń',
            ],
        ],
        [
            'name' => 'Студенты',
            'translation' => [
                'ru' => 'Студенты',
                'en' => 'Students',
                'pl' => 'Studenci',
            ],
        ],
        [
            'name' => 'Предприниматели',
            'translation' => [
                'ru' => 'Предприниматели',
                'en' => 'Businessmen',
                'pl' => 'Biznesmeni',
            ],
        ],
        [
            'name' => 'Научная интеллигенция',
            'translation' => [
                'ru' => 'Научная интеллигенция',
                'en' => 'Scientific intelligentsia',
                'pl' => 'Inteligencja naukowa',
            ],
        ],
        [
            'name' => 'Коллекционеры',
            'translation' => [
                'ru' => 'Коллекционеры',
                'en' => 'Collectors',
                'pl' => 'Kolekcjonerzy',
            ],
        ],
        [
            'name' => 'Представители креативного класса',
            'translation' => [
                'ru' => 'Представители креативного класса',
                'en' => 'Representatives of the creative class',
                'pl' => 'Przedstawiciele klasy kreatywnej',
            ],
        ],
        [
            'name' => 'Богема',
            'translation' => [
                'ru' => 'Богема',
                'en' => 'Bohemia',
                'pl' => 'Boheme',
            ],
        ],
        [
            'name' => 'Молодые мамаши',
            'translation' => [
                'ru' => 'Молодые мамаши',
                'en' => 'Young mothers',
                'pl' => 'Młode matki',
            ],
        ],
        [
            'name' => 'Работники физического труда (низкоквалифицированные)',
            'translation' => [
                'ru' => 'Работники физического труда (низкоквалифицированные)',
                'en' => 'Manual workers (low-skilled)',
                'pl' => 'Pracownicy fizyczni (o niskich kwalifikacjach)',
            ],
        ],
        [
            'name' => 'Работники физического труда (высококвалифицированные)',
            'translation' => [
                'ru' => 'Работники физического труда (высококвалифицированные)',
                'en' => 'Manual workers (highly skilled)',
                'pl' => 'Pracownicy fizyczni (wysoko wykwalifikowani)',
            ],
        ],
        [
            'name' => 'Сотрудники силовых структур',
            'translation' => [
                'ru' => 'Сотрудники силовых структур',
                'en' => 'Security officers',
                'pl' => 'Funkcjonariusze ochrony',
            ],
        ],
        [
            'name' => 'Управленцы',
            'translation' => [
                'ru' => 'Управленцы',
                'en' => 'Managers',
                'pl' => 'Menedżerowie',
            ],
        ],
        [
            'name' => 'Управленцы высшего эшелона',
            'translation' => [
                'ru' => 'Управленцы высшего эшелона',
                'en' => 'Top managers',
                'pl' => 'Najlepsi menedżerowie',
            ],
        ],
        [
            'name' => 'Бизнесмены и финансисты высшего эшелона',
            'translation' => [
                'ru' => 'Бизнесмены и финансисты высшего эшелона',
                'en' => 'Businessmen and financiers of the highest echelon',
                'pl' => 'Biznesmeni i najlepsi finansiści',
            ],
        ],
        [
            'name' => 'Обеспеченная молодежь',
            'translation' => [
                'ru' => 'Обеспеченная молодежь',
                'en' => 'Wealthy youth',
                'pl' => 'Bogata młodość',
            ],
        ],
        [
            'name' => 'Наемные специалисты',
            'translation' => [
                'ru' => 'Наемные специалисты',
                'en' => 'Hired professionals',
                'pl' => 'Zatrudnieni profesjonaliści',
            ],
        ],
        [
            'name' => 'Техническая интеллигенция',
            'translation' => [
                'ru' => 'Техническая интеллигенция',
                'en' => 'Technical intelligentsia',
                'pl' => 'Inteligencja techniczna',
            ],
        ],
        [
            'name' => 'Интеллигенты-специалисты',
            'translation' => [
                'ru' => 'Интеллигенты-специалисты',
                'en' => 'Intellectuals specialists',
                'pl' => 'Specjaliści od inteligencji',
            ],
        ],
        [
            'name' => 'Шоумены и творческие работники',
            'translation' => [
                'ru' => 'Шоумены и творческие работники',
                'en' => 'Showmen and artists',
                'pl' => 'Showmen i artyści',
            ],
        ],
        [
            'name' => 'Офисные работники',
            'translation' => [
                'ru' => 'Офисные работники',
                'en' => 'Office workers',
                'pl' => 'Pracownicy biurowi',
            ],
        ],
        [
            'name' => 'Пенсионеры',
            'translation' => [
                'ru' => 'Пенсионеры',
                'en' => 'Pensioners',
                'pl' => 'Emeryci i renciści',
            ],
        ],
        [
            'name' => 'Туристы',
            'translation' => [
                'ru' => 'Туристы',
                'en' => 'Tourists',
                'pl' => 'Turyści',
            ],
        ],
        [
            'name' => 'Инвалиды',
            'translation' => [
                'ru' => 'Инвалиды',
                'en' => 'Handicapped people',
                'pl' => 'Osoby niepełnosprawne',
            ],
        ],
        [
            'name' => 'Иммигранты',
            'translation' => [
                'ru' => 'Иммигранты',
                'en' => 'Immigrants',
                'pl' => 'Imigranci',
            ],
        ],
        [
            'name' => 'Водители профессионалы',
            'translation' => [
                'ru' => 'Водители профессионалы',
                'en' => 'Drivers are professionals',
                'pl' => 'Kierowcy to profesjonaliści',
            ],
        ],
        [
            'name' => 'Автолюбители',
            'translation' => [
                'ru' => 'Автолюбители',
                'en' => 'Car enthusiasts',
                'pl' => 'Miłośnicy samochodów',
            ],
        ],

    ];


    /**
     * @param SocialStatusClient $model
     * @return void
     */
    public function __construct(SocialStatusClient $model)
    {
        $this->model = $model;
    }
}
