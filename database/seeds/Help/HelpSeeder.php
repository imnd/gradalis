<?php


use App\Models\Language;
use App\Models\Help\Help;
use App\Models\Help\HelpCategory;
use App\Models\Help\HelpSection;
use App\Services\Helpers;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

/**
 * Class BusinessSeeder
 */
class HelpSeeder extends Seeder
{
    use TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        //TODO для теста, убрать
        $this->truncate('help');
        $this->truncate('help_category');
        $this->truncate('help_section');

        $langs = Language::all();
        $fakers = [
            'ru' => Faker::create('ru_RU'),
            'en' => Faker::create('en_EN'),
            'pl' => Faker::create('pl_PL')
        ];
        $this->saveSections();
        $this->saveCategories($fakers, $langs);

        for ($i = 0; $i <= 30; $i++) {

            $help = new Help();
            foreach ($langs as $lang) {
                if(empty($fakers[$lang->lang])){
                    continue;
                }
                $faker = $fakers[$lang->lang];
                $name = $faker->realText(20);
                $description = $faker->realText(500);
                $help->status = Help::STATUS_APPROVED;
                $help->category_id = rand(1, 10);
                $help->setTranslation('title', $lang->lang, $name);
                $help->setTranslation('description', $lang->lang, $description);

            }
            $help->save();
            //$help->addMediaFromUrl('https://avatars.mds.yandex.net/get-pdb/25978/51b72f68-915e-4759-b9c6-b9d4246eb992/s1200')
            //       ->toMediaCollection('news');
        }
        Schema::enableForeignKeyConstraints();
    }

    protected function saveCategories($fakers, $langs)
    {
        $categories = [
            ['name' => 'Партнерский маркетинг', 'section_id' => 1],
            ['name' => 'СРА и многие другие', 'section_id' => 1],
            ['name' => 'Партнерский маркетинг2', 'section_id' => 2],
            ['name' => 'СРА и многие другие2', 'section_id' => 2],
            ['name' => 'Что такое партнерская сеть', 'parent_id' => 1],
            ['name' => 'Инструменты', 'parent_id' => 1],
            ['name' => 'Важное', 'parent_id' => 1],
            ['name' => 'Что такое партнерская сеть', 'parent_id' => 3],
            ['name' => 'Важное', 'parent_id' => 3],
            ['name' => 'Инструменты', 'parent_id' => 3],
        ];
        foreach ($categories as $category) {
            $helpCategory = new HelpCategory();
            foreach ($langs as $lang) {
                $faker = $fakers[$lang->lang];
                $name = $faker->realText(20);
                $description = $faker->realText(500);
                $helpCategory->setTranslation('seo_title', $lang->lang, $name);
                $helpCategory->setTranslation('seo_description', $lang->lang, $description);
                $helpCategory->setTranslation('seo_keywords', $lang->lang, $faker->words(4, true));
            }
            $helpCategory->setTranslation('title', 'ru', $category['name']);
            if (isset($category['parent_id'])) {
                $helpCategory->parent_id = $category['parent_id'];
            }
            if (isset($category['section_id'])) {
                $helpCategory->section_id = $category['section_id'];
            }
            $helpCategory->save();
        }
    }
    
    protected function saveSections() {
        $sections = [
            ['name' => 'Начало работы'],
            ['name' => 'Теория'],
        ];
        foreach ($sections as $section) {
            $helpSection = new HelpSection();
            $helpSection->setTranslation('title', 'ru', $section['name']);
            $helpSection->save();
        }
    }


}
