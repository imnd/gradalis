<?php

use App\Models\Franchise\MainAdvertisingSourcesAttractClients;

class MainAdvertisingSourcesAttractClientsSeeder extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Наружная реклама',
            'translation' => [
                'ru' => 'Наружная реклама',
                'en' => 'Outdoor advertising',
                'pl' => 'Reklama zewnętrzna',
            ],
        ],
        [
            'name' => 'Медийная реклама',
            'translation' => [
                'ru' => 'Медийная реклама',
                'en' => 'Display advertising',
                'pl' => 'Reklamy displayowe',
            ],
        ],
        [
            'name' => 'Крауд-маркетинг',
            'translation' => [
                'ru' => 'Крауд-маркетинг',
                'en' => 'Crowd marketing',
                'pl' => 'Marketing tłumów',
            ],
        ],
        [
            'name' => 'Интернет-реклама',
            'translation' => [
                'ru' => 'Интернет-реклама',
                'en' => 'Internet advertising',
                'pl' => 'Reklama internetowa',
            ],
        ],
        [
            'name' => 'Рекомендации',
            'translation' => [
                'ru' => 'Рекомендации',
                'en' => 'Recommendations',
                'pl' => 'Zalecenia',
            ],
        ],
        [
            'name' => 'Рассылка/обзвон',
            'translation' => [
                'ru' => 'Рассылка/обзвон',
                'en' => 'Newsletter / Calling',
                'pl' => 'Newsletter / Dzwonienie',
            ],
        ],
    ];


    /**
     * @param MainAdvertisingSourcesAttractClients $model
     * @return void
     */
    public function __construct(MainAdvertisingSourcesAttractClients $model)
    {
        $this->model = $model;
    }
}
