<?php

use App\Models\Franchise\FamilyStatusClients;

class ClientFamilyStatuses extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Одинокие взрослые',
            'translation' => [
                'ru' => 'Одинокие взрослые',
                'en' => 'Single adults',
                'pl' => 'Pojedyncze osoby dorosłe',
            ],
        ],
        [
            'name' => 'Бездетные пары',
            'translation' => [
                'ru' => 'Бездетные пары',
                'en' => 'Childless couples',
                'pl' => 'Bezdzietne pary',
            ],
        ],
        [
            'name' => 'Семьи с детьми',
            'translation' => [
                'ru' => 'Семьи с детьми',
                'en' => 'Families with children',
                'pl' => 'Rodziny z dziećmi',
            ],
        ],

    ];

    /**
     * @param FamilyStatusClients $model
     * @return void
     */
    public function __construct(FamilyStatusClients $model)
    {
        $this->model = $model;
    }
}
