<?php

use App\Models\Franchise\Subcategories;

class SubCatFrachiseSeeder extends ListSeeder
{
    protected $rows = [
        [
            'name' => '',
            'translation' => [
                'ru' => '',
                'en' => '',
                'pl' => '',
            ],
        ],
    ];

    /**
     * @param Subcategories $model
     * @return void
     */
    public function __construct(Subcategories $model)
    {
        $this->model = $model;
    }
}
