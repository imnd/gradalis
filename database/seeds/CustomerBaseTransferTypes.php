<?php

use App\Models\Franchise\TransferCustomerBaseTypes;

class CustomerBaseTransferTypes extends ListSeeder
{
    protected $rows = [
        [
            'name' => 'Электронная база данных учета клиентов (CRM)',
            'translation' => [
                'ru' => 'Электронная база данных учета клиентов (CRM)',
                'en' => 'Electronic Customer Accounting Database (CRM)',
                'pl' => 'Elektroniczna baza danych księgowości klienta (CRM)',
            ],
        ],
        [
            'name' => 'Клиентский учет в excel-таблице',
            'translation' => [
                'ru' => 'Клиентский учет в excel-таблице',
                'en' => 'Customer accounting in excel-table',
                'pl' => 'Księgowość klienta w tabeli excel',
            ],
        ],
        [
            'name' => 'База данных потенциальных клиентов',
            'translation' => [
                'ru' => 'База данных потенциальных клиентов',
                'en' => 'Database of potential customers',
                'pl' => 'Baza potencjalnych klientów',
            ],
        ],
    ];

    /**
     * @param TransferCustomerBaseTypes $model
     * @return void
     */
    public function __construct(TransferCustomerBaseTypes $model)
    {
        $this->model = $model;
    }
}
