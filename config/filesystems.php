<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root'   => storage_path('app'),
        ],

        'public' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public'),
            'url'        => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],
        'data' => [
            'driver' => 'local',
            'root'   => storage_path('app/data'),
            //'url' => env('APP_URL').'/storage',
            //'visibility' => 'public',
        ],
        'public-web' => [
            'driver'     => 'local',
            'root'       => public_path(),
            'url'        => env('APP_URL') . '/',
            'visibility' => 'public',
        ],

        'business' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/business'),
            'url'        => env('APP_URL') . '/storage/business',
            'visibility' => 'public',
        ],
        'franchise' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/franchise'),
            'url'        => env('APP_URL') . '/storage/franchise',
            'visibility' => 'public',
        ],
        'franchise_objects' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/franchise_objects'),
            'url'        => env('APP_URL') . '/storage/franchise_objects',
            'visibility' => 'public',
        ],
        'franchise_reviews_photo' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/franchise_reviews_photo'),
            'url'        => env('APP_URL') . '/storage/franchise_reviews_photo',
            'visibility' => 'public',
        ],
        'franchise_reviews_materials' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/franchise_reviews_materials'),
            'url'        => env('APP_URL') . '/storage/franchise_reviews_materials',
            'visibility' => 'public',
        ],
        'franchise_news' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/franchise_news'),
            'url'        => env('APP_URL') . '/storage/franchise_news',
            'visibility' => 'public',
        ],
        'franchise_documents' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/franchise_documents'),
            'url'        => env('APP_URL') . '/storage/franchise_documents',
            'visibility' => 'public',
        ],
        'user' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/user'),
            'url'        => env('APP_URL') . '/storage/user',
            'visibility' => 'public',
        ],
        'users_service' => [
            'driver'     => 'local',
            'root'       => storage_path('app/public/users_service'),
            'url'        => env('APP_URL') . '/storage/users_service',
            'visibility' => 'public',
        ],
        's3' => [
            'driver' => 's3',
            'key'    => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url'    => env('AWS_URL'),
        ],
    ],
];
