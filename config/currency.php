<?php
return array(
    'cca_app_id' => 'e3defb24598e74586b04',
    'main'       => 'EUR',
    'currencies' =>
        array(
            'EUR' =>
                array(
                    'val'    => 'EUR',
                    'locale' => 'pl-PL',
                    'sign'   => '€',
                ),
            'PLN' =>
                array(
                    'val'    => 'PLN',
                    'locale' => 'pl-PL',
                    'sign'   => 'zł',
                ),
            'BTC' =>
                array(
                    'val'    => 'BTC',
                    'locale' => 'pl-PL',
                    'sign'   => '₿',
                ),
            'RUB' =>
                array(
                    'val'    => 'RUB',
                    'locale' => 'ru-RU',
                    'sign'   => '₽',
                ),
        ),
    'rates'      =>
        array(
            'EUR_PLN' => 4.294471,
            'PLN_EUR' => 0.232852,
            'EUR_BTC' => 0.00028,
            'BTC_EUR' => 3575.092932,
            'EUR_RUB' => 73.031711,
            'RUB_EUR' => 0.013694,
            'EUR_EUR' => 1,
        ),

    'priceRanges' => [
        'RUB' => [
            [
                'value' => [
                    0, 10000
                ],
                'text' => 'до ₽10 тыс.'
            ],
            [
                'value' => [
                    0, 500000
                ],
                'text' => 'до ₽500 тыс.'
            ],
            [
                'value' => [
                    0, 1000000
                ],
                'text' => 'до ₽1 млн.'
            ],
            [
                'value' => [
                    0, 10000000
                ],
                'text' => 'до ₽10 млн.'
            ],
        ],
        'EUR' => [
            [
                'value' => [
                    0, 1000
                ],
                'text' => 'до €1 тыс.'
            ],
            [
                'value' => [
                    0, 50000
                ],
                'text' => 'до €50 тыс.'
            ],
            [
                'value' => [
                    0, 100000
                ],
                'text' => 'до €100 тыс.'
            ],
            [
                'value' => [
                    0, 1000000
                ],
                'text' => 'до €1 млн.'
            ],
        ],
        'PLN' => [
            [
                'value' => [
                    0, 10000
                ],
                'text' => 'до zł 10 тыс.'
            ],
            [
                'value' => [
                    0, 500000
                ],
                'text' => 'до zł 500 тыс.'
            ],
            [
                'value' => [
                    0, 1000000
                ],
                'text' => 'до zł 1 млн.'
            ],
            [
                'value' => [
                    0, 10000000
                ],
                'text' => 'до zł 10 млн.'
            ],
        ],
        'BTC' => [
            [
                'value' => [
                    0, 10
                ],
                'text' => 'до ₿10'
            ],
            [
                'value' => [
                    0, 100
                ],
                'text' => 'до ₿100'
            ],
            [
                'value' => [
                    0, 1000
                ],
                'text' => 'до ₿1000'
            ],
            [
                'value' => [
                    0, 10000
                ],
                'text' => 'до ₿10 тыс.'
            ],
        ],
    ]

);
