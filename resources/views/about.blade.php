@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')
    {{--@php(dd(app()->getLocale()))--}}
    <div class="about">
        <section class="section pt-1 pb-0">
            <div class="container">
                <h1 class="section-title mb-0">@lang('about.title')</h1>
            </div>
        </section>
        <section class="section pt-1">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-8">
                        <div class="content">
                            @lang('about.our_about_text')
                        </div>
                    </div>

                    <div class="column is-4">
                        <div class="about__phrase">
                        <span class="has-text-weight-bold">
                            @lang('about.right_block')
                        </span>

                        </div>
                    </div>
                    <div class="column is-12">
                        <h2 class="mb-1 has-text-left">@lang('about.our_mission')</h2>
                        <div class="content">
                            @lang('about.our_mission_text')
                        </div>
                    </div>
                    {{--
                    <div class="column is-7-desktop is-12-tablet"><span class="about__count-clients">49 508</span></div>
                    <div class="column is-5-desktop is-12-tablet">
                    <span class="about__count-clients-desc">
                        @lang('about.q_clients_text')
                    </span>
                    </div>
                    --}}
                </div>
            </div>
        </section>
        <section class="section our-team">
            <div class="container">
                <h2 class="mb-2 has-text-left">@lang('about.our_team')</h2>
                <div class="columns is-multiline">
                    <div class="column is-3">
                        <div class="our-team__item">
                            <figure class="mb-0-5">
                                <img src="https://s.pfst.net/2015.10/8515416672106b1d8b1dd45a0495f01e4b8a4d03b652_b.jpg"
                                     alt="" class="has-border-radius">
                            </figure>
                            <h3>Станислав Суслов</h3>
                            <div class="team__item__position is-size-875 has-text-basic">
                                CEO, руководитель компании
                            </div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="our-team__item">
                            <figure class="mb-0-5">
                                <img src="http://www.stal-91.ru/images/11.jpg"
                                     alt="" class="has-border-radius">
                            </figure>
                            <h3>Андрей Зыков</h3>
                            <div class="team__item__position is-size-875 has-text-basic">
                                Старший менеджер
                            </div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="our-team__item">
                            <figure class="mb-0-5">
                                <img src="http://inside.axelspringer.com/media/store/styles/picture_image/s3/Malte%20Goesche%20_%20square.jpg"
                                     alt="" class="has-border-radius">
                            </figure>
                            <h3>Николай Шуляк</h3>
                            <div class="team__item__position is-size-875 has-text-basic">
                                Разработчик
                            </div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="our-team__item">
                            <figure class="mb-0-5">
                                <img src="https://i.pinimg.com/736x/43/5a/19/435a19a61a46a262e3092e6f3b6dde1d.jpg"
                                     alt="" class="has-border-radius">
                            </figure>
                            <h3>Ксения Никитина</h3>
                            <div class="team__item__position is-size-875 has-text-basic">
                                Переводчик
                            </div>
                        </div>
                    </div>
                    <div class="column is-12">
                        <div class="has-text-centered">
                            <a href="/vacancy"
                               class="button is-outlined is-info is-size-875 has-text-weight-bold h-3 px-1">
                                @lang('about.link_vacancy')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-4 is-hidden-touch">
                        <img src="/img/about-2.jpg" alt="" class="has-border-radius">
                    </div>
                    <div class="column is-8-desktop is-12-tablet">
                        <div class="content">
                            <h2 class="mb-2">@lang('about.our_tasks')</h2>
                            <img src="/img/about-2.jpg" alt="" class="is-hidden-desktop mb-1 mr-1 has-border-radius"
                                 align="left">
                            @lang('about.our_tasks_text')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
