@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')
    <section class="section py-0">
        <div class="container">
            <div class="register-container">
                <div class="tabs is-medium">
                    <ul>
                        <li class="is-active">
                            <a style="background-color: #3c4ccb; color: white;">
                                <span>@lang('reg.buyer.title')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('register.seller')}}" style="background-color: #f27313; color: white;">
                                <span>@lang('reg.seller.title')</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="columns is-multiline">
                    <div class="column">
                        <div class="card-register card-register_buyer card-auth">
                            <div class="card-auth__header">
                                <div class="card-auth__header__title has-text-left">
                                    <h1>@lang('reg.buyer.title')</h1>
                                </div>
                            </div>
                            <div class="card-body card-auth__body">
                                <form-register-buyer></form-register-buyer>
                            </div>
                        </div>
                    </div>
                    <div class="column is-two-fifths-desktop is-12-tablet py-0">
                        <div class="register-column-info-wrap">
                            <div class="register-column-info register-column-info_buyer">
                                <div class="register-column-info__title has-text-weight-bold">
                                    @lang('reg.buyer.right_block.title')
                                </div>
                                <div class="register-column-info__desc">
                                    @lang('reg.buyer.right_block.desc')
                                </div>
                                <figure class="register-column-info__img">
                                    <img src="{{ asset('/svg/people/people-2.svg') }}" alt="">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
