@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')


    <section class="section py-0">
        <div class="container">
            <div class="register-container">
                <div class="tabs is-medium">
                    <ul>
                        <li class="is-active">
                            <a style="background-color: #f27313; color: white;">
                                <span>@lang('reg.seller.title')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('register.buyer')}}" style="background-color: #3c4ccb; color: white;">
                                <span>@lang('reg.buyer.title')</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="columns is-multiline">
                    <div class="column">
                        <div class="card-register card-register_seller card-auth">
                            <div class="card-auth__header">
                                <div class="card-auth__header__title has-text-left">
                                    <h1>@lang('reg.seller.title')</h1>
                                </div>
                            </div>
                            <div class="card-body card-auth__body">
                                <form-register-seller></form-register-seller>
                            </div>
                        </div>
                    </div>
                    <div class="column is-two-fifths-desktop is-12-tablet py-0">
                        <div class="register-column-info-wrap">
                            <div class="register-column-info register-column-info_seller">
                                <div class="register-column-info__title has-text-weight-bold">
                                    @lang('reg.seller.right_block.title')
                                </div>
                                <div class="register-column-info__desc">
                                    @lang('reg.seller.right_block.desc')
                                </div>
                                <figure class="register-column-info__img">
                                    <img src="{{ asset('/svg/people/people-1.svg') }}" alt="">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
