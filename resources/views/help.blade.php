@extends('layouts.app')

@section('title', ($currentLevel ? $currentLevel->seo_title : app('translator')->getFromJson('help.title')))

@section('seo-tags')
    @if($currentLevel)
    <meta name="description" content="{{$currentLevel->seo_description}}">
    <meta name="keywords" content="{{$currentLevel->seo_keywords}}">
    @endif
@endsection

@section('head')

@endsection

@section('content')

    @include('includes.breadcrumb')
    <section class="section help py-0 pb-2">
        <div class="container">
            <div class="columns">
                <div class="column is-4">
                    <aside class="pt-1">
                        <nav class="aside-nav aside-nav_help  px-0">
                            @foreach($sections as $i=>$section)
                            @if($i >= 1)
                                <hr class="hr-basic my-1">
                            @endif
                                <h3 class="px-1-5 mb-0-5">{{ $section->title }}
                                </h3>
                            <ul class="px-1-5 menu-level-menu">
                                @foreach($section->categories as $category)
                                <li @click="toggleSubMenu" class="aside-nav__item {{ ($category->id !== $select) ?: 'is-active' }}
                                {{ (!$category->approvedHelps) ?: 'aside-nav__item-parent-sub-menu' }}
                                    {{ ($category->id === $parent || $category->id === $select) ? 'sub-menu-active' : '' }}

                                        ">
                                    <span>
                                        @if(!$category->approvedHelps->isEmpty())
                                            <span class="sub-menu-toggle"></span>
                                        @endif
                                        <a href="/help/{{$section->url . '/' . $category->url}}">
                                            <span class="aside-nav__item__title  is-active">{{ $category->title }}</span>
                                        </a>
                                        @foreach($category->approvedHelps as $approvedHelp)
                                        <ul class="sub-menu">
                                            {{--{{dd($select)}}--}}
                                            <li class="aside-nav__item {{ ($approvedHelp->id === $select) ? 'is-active' : '' }}">
                                                <a href="/help/{{$section->url . '/' . $category->url . '/' . $approvedHelp->url}}">
                                                    <span class="aside-nav__item__title">{{ $approvedHelp->title }}</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </span>
                                    @endforeach
                                </li>
                                @endforeach
                            </ul>
                            @endforeach
                        </nav>
                    </aside>

                </div>
                <div class="column is-8">
                    <section class="section pt-1 pb-0 px-0">
                        <h1 class="section-title mb-1-75">{{ ($currentLevel ? $currentLevel->seo_title : app('translator')->getFromJson('help.title'))}}</h1>
                    </section>
                    <section class="section pt-0 px-0 box">
                            <div class="content">
                                <div class="is-size-875">
                                    @if($help)
                                        <h3>{{ $help->title }}</h3>
                                        {!! $help->description !!}
                                    @elseif($helpCategory)
                                        <aside>
                                            <nav class="aside-nav px-0">
                                                <ul class="menu-level-menu">
                                                    <li class="aside-nav__item is-active sub-menu-active
                                                        {{ (!$helpCategory->approvedHelps) ?: 'aside-nav__item-parent-sub-menu' }}
                                                            ">
                                                        @foreach($helpCategory->approvedHelps as $approvedHelp)
                                                            <ul class="sub-menu">
                                                                {{--{{dd($select)}}--}}
                                                                <li class="aside-nav__item is-active">
                                                                    <a href="/help/{{$helpSection->url . '/' . $helpCategory->url . '/' . $approvedHelp->url}}">
                                                                        <span class="aside-nav__item__title">{{ $approvedHelp->title }}</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        @endforeach
                                                    </li>
                                                </ul>
                                            </nav>
                                        </aside>
                                    @elseif($helpSection)
                                        <aside>
                                            <nav class="aside-nav px-0">
                                                <ul class="menu-level-menu">
                                                    @foreach($helpSection->categories as $category)
                                                        <li class="aside-nav__item is-active sub-menu-active
                                                        {{ (!$category->approvedHelps) ?: 'aside-nav__item-parent-sub-menu' }}
                                                                ">

                                                            <a href="/help/{{$helpSection->url . '/' . $category->url}}">
                                                                <span class="aside-nav__item__title  is-active">{{ $category->title }}</span>
                                                            </a>
                                                            @foreach($category->approvedHelps as $approvedHelp)
                                                                <ul class="sub-menu">
                                                                    {{--{{dd($select)}}--}}
                                                                    <li class="aside-nav__item is-active">
                                                                        <a href="/help/{{$helpSection->url . '/' . $category->url . '/' . $approvedHelp->url}}">
                                                                            <span class="aside-nav__item__title">{{ $approvedHelp->title }}</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            @endforeach
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </nav>
                                        </aside>
                                    @else
                                        <aside>
                                            <nav class="aside-nav px-0">
                                                @foreach($sections as $i=>$section)
                                                    @if($i >= 1)
                                                        <hr class="hr-basic my-1">
                                                    @endif
                                                    <h3 class="mb-0-5">{{ $section->title }}
                                                    </h3>
                                                    <ul class="menu-level-menu">
                                                        @foreach($section->categories as $category)
                                                            <li class="aside-nav__item is-active sub-menu-active
                                                        {{ (!$category->approvedHelps) ?: 'aside-nav__item-parent-sub-menu' }}
                                                                    ">

                                                                <a href="/help/{{$section->url . '/' . $category->url}}">
                                                                    <span class="aside-nav__item__title  is-active">{{ $category->title }}</span>
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endforeach
                                            </nav>
                                        </aside>
                                    @endif
                                </div>
                            </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

@endsection
