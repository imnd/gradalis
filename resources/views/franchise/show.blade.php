@extends('layouts.app')

@section('title', $franchise->seo_title)

@section('seo-tags')
    <meta name="description" content="{{$franchise->seo_description}}">
    <meta name="keywords" content="{{$franchise->seo_keywords}}">
@endsection

@section('head')

    @include('parts.pixels.facebook', ['id' => $franchise->fb])
    @include('parts.pixels.google', ['id' => $franchise->google])
    @include('parts.pixels.mail', ['id' => $franchise->mail])
    @include('parts.pixels.vk', ['id' => $franchise->vk])

@endsection

@section('content')

    @include('includes.breadcrumb')

    <div class="object">

        <modal-add-request-doc
            :id="{{$franchise->id}}"
            :show="franchise.showModalAddRequestDoc"
            @close="franchise.showModalAddRequestDoc = false">
        </modal-add-request-doc>
        <!-- Section object header -->
        <section class="section pt-1 pb-0">
            <div class="container">
                <div class="object__header">
                    <h1 class="section-title">{{ $franchise->name }}</h1>
                    <div class="object__header__info">
                        <ul>
                            {{--TODO: Если не работает раскомментировать старую версию--}}
                            <li>@lang('franchises.single.id_object', ['id' => $franchise->id])</li>
                            <li>{{$franchise->created_at->formatLocalized('%d %B %Y')}}</li>
                            <li>@lang('franchises.single.quantity_views', ['count' => $franchise->show_count])</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section object content -->
        <section class="section is-paddingless">
            <div class="container">
                <div class="object__body">
                    <div class="object__body__top">
                        <div class="object__slider">
                            <!-- Swiper -->
                            <div class="swiper-container gallery-top swiper-object-main">
                                <div class="swiper-wrapper" id="swiper-object-main-media-gallery">
                                    @php
                                        $sortedFranchises = $franchise->getMedia('franchise')->sortBy(function ($media, $index) {
                                            return $media->getCustomProperty('cover') ? 0 : $index+1;
                                        })->values()->all();
                                    @endphp

                                    @foreach ($sortedFranchises as $image)
                                        <div
                                            class="swiper-slide"
                                            style="background-image:url({{$image->getUrl('watermark')}})"
                                            data-src="{{$image->getUrl('watermark')}}"></div>
                                    @endforeach
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                            <div class="swiper-container gallery-thumbs swiper-object-thumbs">
                                <div class="swiper-wrapper">
                                    @foreach ($sortedFranchises as $image)
                                        <div
                                            class="swiper-slide"
                                            style="background-image:url({{$image->getUrl('thumb')}})"></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="object__broker">
                            <div class="object__broker__top">
                                <div class="price-info">
                                    <span class="tag is-danger price-sale">-{{ $franchise->discount}}%</span>
                                    <div class="price mb-0">
                                        {{ format_price($franchise->discountPrice) }}
                                    </div>
                                    @if ($franchise->discount > 0)
                                        <span class="price-old">{{format_price($franchise->price)}}</span>
                                    @endif
                                </div>
                                <div class="has-text-basic is-size-875">
                                    @lang('franchises.single.Investment_size')
                                </div>
                                @if (!empty($franchise->broker))
                                    <hr class="hr-basic my-1"/>
                                    <div class="is-flex">
                                        <div class="object__broker__top__left">
                                            <figure class="object__broker__avatar">
                                                <img
                                                    src="{{ $franchise->brokerAvatar }}"
                                                    alt="{{ $franchise->brokerFullName }}"
                                                />
                                            </figure>
                                        </div>
                                        <div class="object__broker__top__right">
                                            <div
                                                class="is-size-6 has-text-info has-text-weight-bold has-text-decoration-underline">
                                                {{ $franchise->brokerFullName }}
                                            </div>
                                            <div class="object__broker__info is-size-875">
                                                Брокер сделки
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="object__broker__bottom is-flex">
                                @if (
                                       empty($franchise->broker)
                                    || empty($franchise->sell_with_broker)
                                )
                                    @auth
                                        <button
                                            @click="$store.state.dialogs.expanded = true; this.$store.state.chat.expanded = true; $store.state.dialogs.id = 5;"
                                            class="button is-success is-size-875 has-text-weight-bold modal-button"
                                        >
                                            <img src="{{ asset('/svg/icons/ic_ask_question.svg') }}"/>
                                            <span>@lang('franchises.single.ask_question')</span>
                                        </button>
                                    @else
                                        <a class="button is-success is-size-875 has-text-weight-bold modal-button"
                                           href="/register/buyer">
                                            <img src="{{ asset('/svg/icons/ic_ask_question.svg') }}"/>
                                            <span>@lang('franchises.single.ask_question')</span>
                                        </a>
                                    @endauth
                                @else
                                    <button
                                        @click="$store.state.dialogs.expanded = true"
                                        class="button is-success is-size-875 has-text-weight-bold modal-button">
                                        <img src="{{ asset('/svg/icons/ic_ask_question.svg') }}"/>
                                        <span>@lang('franchises.single.dialog_broker')</span>
                                    </button>
                                @endif
                            </div>
                        </div>
                        @auth
                            <a
                                href="#"
                                class="info-icon object-favorite"
                                v-tooltip="'@lang('franchises.single.add_to_favorites')'">
                                <img
                                    src="{{ asset('/svg/icons/ic_favorites_white.svg') }}"
                                    alt="@lang('franchises.single.add_to_favorites')"
                                />
                            </a>
                        @endauth
                    </div>
                    <div class="franchise-info mt-2 mb-2">
                        <div class="columns is-multiline">
                            <div class="column is-4">
                                <div class="franchise-info__company box">
                                    <div class="columns is-multiline">
                                        <div class="column is-4 is-flex has-align-items-center">
                                            <figure class="franchise-info__company__logo is-marginless">
                                                @if ($logoUrl = $franchise->getFirstMediaUrl('logo','thumb'))
                                                    <img src="{{$logoUrl}}" alt="">
                                                @endif

                                            </figure>
                                        </div>
                                        <div class="column is-8">
                                            <div class="franchise-info__company-name">
                                                <h3>{{ $franchise->name_business }}</h3>
                                                <div class="location is-size-875 is-flex has-align-items-center">
                                                    <div class="location__icon is-flex has-align-items-center">
                                                        <img src="{{ asset('/svg/icons/ic_location.svg') }}"
                                                             alt="location">
                                                    </div>
                                                    <div class="location__title">{{$franchise->country->translation}},
                                                        г. {{$franchise->city->translation}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column is-8">
                                <div class="box">
                                    <div class="columns is-multiline franchise-info__item-list">
                                        <div class="column is-3">
                                            <div class="franchise-info__item is-flex">
                                                <figure class="franchise-info__item__icon">
                                                    <img src="/img/franchises/ic_location.svg" alt="">
                                                </figure>
                                                <div class="">
                                                    <b>@lang('franchises.single.already_open')</b>
                                                    <div
                                                        class="has-text-weight-light">@lang('franchises.single.quantity_objects',['count' => count($franchise->objects)])</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column is-3">
                                            <div class="franchise-info__item is-flex">
                                                <figure class="franchise-info__item__icon">
                                                    <img src="/img/franchises/ic_money.svg" alt="">
                                                </figure>
                                                <div class="">
                                                    <b>@lang('franchises.single.profit_per_month')</b>
                                                    <div
                                                        class="has-text-weight-light"> {{format_price($franchise->profit)}}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column is-3">
                                            <div class="franchise-info__item is-flex">
                                                <figure class="franchise-info__item__icon">
                                                    <img src="/img/franchises/ic_sale.svg" alt="">
                                                </figure>
                                                <div class="">
                                                    <b>@lang('franchises.single.royalty')</b>
                                                    <div class="has-text-weight-light">
                                                        {{$franchise->roality}}
                                                        % @lang('franchises.single.in_month')</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column is-3">
                                            <div class="franchise-info__item is-flex">
                                                <figure class="franchise-info__item__icon">
                                                    <img src="/img/franchises/ic_graphic.svg" alt="">
                                                </figure>
                                                <div class="">
                                                    <b>@lang('franchises.single.payback')</b>
                                                    <div class="has-text-weight-light">
                                                        {{$franchise->payback}} @lang('franchises.single.short_month')</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tabs-wrap">
                        <div class="tabs is-size-875 is-marginless">
                            <ul>
                                <li class="tab is-active"
                                    onclick="openTab(event,'tab-main-info')">
                                    <a class="h-3 px-1-5">
                                        <span>@lang('franchises.single.gen_info')</span>
                                    </a>
                                </li>
                                @if(count($latestNews))
                                    <li class="tab"
                                        onclick="openTab(event,'tab-news')">
                                        <a class="h-3 px-1-5">
                                            <span>@lang('franchises.single.news')</span>
                                        </a>
                                    </li>
                                @endif
                                @if(count($objects))
                                    <li class="tab"
                                        onclick="openTab(event,'tab-open-projects')">
                                        <a class="h-3 px-1-5">
                                            <span>@lang('franchises.single.open_projects')</span>
                                        </a>
                                    </li>
                                @endif
                                @if(count($latestReviews))
                                    <li class="tab"
                                        onclick="openTab(event,'tab-reviews')">
                                        <a class="h-3 px-1-5">
                                            <span>@lang('franchises.single.reviews')</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>

                        <div class="box">
                            <div class="columns is-multiline">
                                <div class="column is-8">
                                    <div id="tab-main-info" class="content-tab">
                                        <div class="content">
                                            <h2>@lang('franchises.single.description')</h2>
                                            {!! $franchise->description !!}
                                            <h2>@lang('franchises.single.available_packages')</h2>
                                            <p>
                                                Франшиза предусматривает несколько пакетов на выбор. В зависимости от
                                                выбранного пакета будет манятся комплектация поддержки и прочее
                                            </p>
                                            <div class="columns is-multiline">
                                                @foreach($franchise->packages as $package)
                                                    <div class="column is-4">
                                                        <div class="card-franchise-package has-text-centered p-1-5">
                                                            <h5 class="card-franchise-package__title has-text-success has-text-weight-normal">
                                                                {{ $package->name }}
                                                            </h5>
                                                            <div class="card-franchise-package__desc">
                                                                <div
                                                                    class="is-size-7 card-franchise-package__desc__short has-text-weight-light">
                                                                    @foreach($package->include as $include)
                                                                        @php
                                                                            echo '<div class="mb-0-5">' . $include . '</div>';
                                                                        @endphp
                                                                    @endforeach
                                                                </div>
                                                                <div
                                                                    class="is-size-7 card-franchise-package__desc__full has-text-weight-light">
                                                                    {!! $package->benefits !!}
                                                                </div>
                                                            </div>
                                                            <div class="card-franchise-package__footer">
                                                                <div
                                                                    class="card-franchise-package__price is-size-875 has-text-weight-bold has-text-dark-blue">
                                                                    {{ MK::getPrice($package->price) }}
                                                                </div>
                                                                <div class="is-size-7 has-text-basic">
                                                                    Роялти {{$package->royalty_rate}}
                                                                    % {{$package->royalty_rate_period}}
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>

                                            <h2>@lang('franchises.single.video_presentation')</h2>
                                            <video class="w-full has-border-radius has-box-shadow mb-2" controls="">
                                                <source
                                                    src="{{ $franchise->video_url }}"
                                                    type="video/mp4">
                                                Your browser does not support HTML5 video.
                                            </video>

                                            <bulma-accordion :slide="{duration: '.3s', timerFunc: 'ease' }"
                                                             :dropdown="true" class="px-0" :icon="'custom'"
                                                             :initial-open-items="[1,2,3,4,5]">
                                                <bulma-accordion-item>

                                                    <h2 slot="title" class="mb-0 is-flex has-align-items-center">
                                                        <img src="{{ asset('/svg/icons/ic_object_finance.svg') }}"
                                                             alt="" class="icon mr-0-5">
                                                        <span>@lang('franchises.single.finance_info')</span>
                                                    </h2>

                                                    <div slot="content" class="pb-1">
                                                        <div
                                                            class="object__info__list is-size-875 has-text-dark-blue">
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.net_profit_per_month')</span>
                                                                <span>{{format_price($franchise->profit)}}</span>
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.payback')</span>
                                                                <span>{{$franchise->payback}} мес.</span>
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.annual_turnover')</span>
                                                                <span>{{format_price($franchise->profitability)}}</span>
                                                            </div>
                                                        </div>
                                                        <h3>@lang('franchises.single.rationale_for_financial_indicators')</h3>
                                                        <p>
                                                            {!! $franchise->justification_financial_indicators !!}
                                                        </p>
                                                    </div>
                                                </bulma-accordion-item>
                                                <hr class="hr-basic is-marginless">
                                                <bulma-accordion-item>
                                                    <h2 slot="title" class="mb-0 is-flex has-align-items-center">
                                                        <img
                                                            src="{{ asset('/svg/icons/ic_object_building.svg') }}"
                                                            alt=""
                                                            class="icon mr-0-5">
                                                        <span>@lang('franchises.single.real_estate')</span>
                                                    </h2>
                                                    <div slot="content" class="pb-1">
                                                        <div
                                                            class="object__info__list is-size-875 has-text-dark-blue">
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.form_of_the_cathedral')</span>
                                                                <span>{{($franchise->propertyType['slug']) ? $franchise->propertyType['slug'] : ''}}</span>
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.type_of_construction')</span>
                                                                <span>{{($franchise->propertyCategory['slug']) ? $franchise->propertyCategory['slug'] : ''}}</span>
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.square')</span>
                                                                <span>{{$franchise->number_square_meters}} м2</span>
                                                            </div>
                                                            @if($franchise->restrictions_operation)
                                                                <div class="object__info__list__item">
                                                                    <span>@lang('franchises.single.exploitation')</span>
                                                                    @if($franchise->list_restrictions_operation)
                                                                        <span>{{$franchise->listRestrictionsOperation()}}</span>
                                                                    @endif
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </bulma-accordion-item>
                                                <hr class="hr-basic is-marginless">
                                                <bulma-accordion-item>
                                                    <h2 slot="title" class="mb-0 is-flex has-align-items-center">
                                                        <img
                                                            src="{{ asset('/svg/icons/ic_target_audience.svg') }}"
                                                            alt=""
                                                            class="icon mr-0-5">
                                                        <span>@lang('franchises.single.target_audience')</span>
                                                    </h2>
                                                    <div slot="content" class="pb-1">
                                                        <div
                                                            class="object__info__list is-size-875 has-text-dark-blue">
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.type_of_audience_for_the_business')</span>
                                                                <span>B2C</span>
                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.target_audience_sex')</span>
                                                                @if($franchise->b2c['gender_target_audience'])
                                                                    <span>{{$franchise->b2c['gender_target_audience']}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.age_man')</span>
                                                                @if($franchise->b2c['age_men_from'] and $franchise->b2c['age_men_to'])
                                                                    <span>{{$franchise->b2c['age_men_from']}}
                                                                        - {{$franchise->b2c['age_men_to']}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.age_woman')</span>
                                                                @if($franchise->b2c['age_women_from'] and $franchise->b2c['age_women_to'])
                                                                    <span>{{$franchise->b2c['age_women_from']}}
                                                                        - {{$franchise->b2c['age_women_to']}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.sex_ratio')</span>
                                                                @if($franchise->b2c['sex_ratio_from'] and $franchise->b2c['sex_ratio_to'])
                                                                    <span> М: {{$franchise->b2c['sex_ratio_from']}}
                                                                        %  Ж: {{$franchise->b2c['sex_ratio_to']}}
                                                                        %</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.family_status')</span>
                                                                @if($franchise->b2c['family_status_clients'])
                                                                    <span>{{ $franchise->b2c->familyStatus() }}</span>
                                                                @endif
                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.alone_clients')</span>
                                                                @if($franchise->b2c['alone_clients'])
                                                                    <span>{{$franchise->b2c['alone_clients']}}</span>
                                                                @endif
                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.families_with_children_clients')</span>
                                                                @if($franchise->b2c['families_with_children_clients'])
                                                                    <span>{{$franchise->b2c['families_with_children_clients']}}</span>
                                                                @endif
                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.couples_clients')</span>
                                                                @if($franchise->b2c['couples_clients'])
                                                                    <span>{{$franchise->b2c['couples_clients']}}</span>
                                                                @endif
                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.social_status')</span>
                                                                @if($franchise->b2c['social_status_clients'])
                                                                    <span>{{$franchise->b2c->socialStatus()}}</span>
                                                                @endif
                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.child_clients')</span>
                                                                @if($franchise->b2c['child_clients'])
                                                                    <span>{{$franchise->b2c['child_clients']}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.students_clients')</span>
                                                                @if($franchise->b2c['students_clients'])
                                                                    <span>{{$franchise->b2c['students_clients']}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.pensioners_clients')</span>
                                                                @if($franchise->b2c['pensioners_clients'])
                                                                    <span>{{$franchise->b2c['pensioners_clients']}}</span>
                                                                @endif
                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.average_income_target_clients')</span>
                                                                @if($franchise->b2c['average_income_target_clients_from'] and $franchise->b2c['average_income_target_clients_from_to'])
                                                                    <span>от {{$franchise->b2c['average_income_target_clients_from']}}
                                                                        до {{$franchise->b2c['average_income_target_clients_from_to']}}</span>
                                                                @endif

                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.average_income_target_clients_to')</span>
                                                                @if($franchise->b2c['average_income_target_clients_to'])
                                                                    <span>{{$franchise->b2c['average_income_target_clients_to']}}</span>
                                                                @endif
                                                            </div>

                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2c.main_attract_client_adv_src')</span>
                                                                @if($franchise->b2c['main_attract_client_adv_src'])
                                                                    <span>{{$franchise->b2c->mainAttractClientAdvSrc()}}</span>
                                                                @endif
                                                            </div>

                                                        </div>
                                                    </div>
                                                </bulma-accordion-item>
                                                <hr class="hr-basic is-marginless">
                                                <bulma-accordion-item>
                                                    <h2 slot="title" class="mb-0 is-flex has-align-items-center">
                                                        <img
                                                            src="{{ asset('/svg/icons/ic_target_audience.svg') }}"
                                                            alt=""
                                                            class="icon mr-0-5">
                                                        <span>@lang('franchises.single.target_audience')</span>
                                                    </h2>
                                                    <div slot="content" class="pb-1">
                                                        <div
                                                            class="object__info__list is-size-875 has-text-dark-blue">
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.type_of_audience_for_the_business')</span>
                                                                <span>B2B</span>
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2b.average_check_clients')</span>
                                                                @if($franchise->b2b['average_check_clients'])
                                                                    <span>{{$franchise->b2b['average_check_clients']}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2b.have_existing_contracts')</span>
                                                                <span>{{($franchise->b2b['have_existing_contracts']) ? 'Yes' : 'No'}}</span>
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2b.main_category_business_partners')</span>
                                                                @if($franchise->b2b['main_category_business_partners'])
                                                                    <span>{{$franchise->b2b->main_category_business_partners()}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2b.count_perpetual_service_contracts')</span>
                                                                @if($franchise->b2b['count_perpetual_service_contracts'])
                                                                    <span>{{$franchise->b2b['count_perpetual_service_contracts']}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.b2b.main_attract_client_adv_src')</span>
                                                                @if($franchise->b2b['main_attract_client_adv_src'])
                                                                    <span>{{$franchise->b2b->mainAttractClientAdvSrc()}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </bulma-accordion-item>
                                                <hr class="hr-basic is-marginless">
                                                <bulma-accordion-item>
                                                    <h2 slot="title" class="mb-0 is-flex has-align-items-center">
                                                        <img src="{{ asset('/svg/icons/ic_law_form.svg') }}" alt=""
                                                             class="icon mr-0-5">
                                                        <span>@lang('franchises.single.legal_state')</span>
                                                    </h2>
                                                    <div slot="content" class="pb-1">
                                                        <div
                                                            class="object__info__list is-size-875 has-text-dark-blue">
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.legal_status')</span>
                                                                @if($franchise->legal_status)
                                                                    <span>{{$franchise->legal_status}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.taxation_system')</span>
                                                                @if($franchise->tax_system)
                                                                    <span>{{$franchise->tax_system}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.main_activities')</span>
                                                                @if($franchise->category->name)
                                                                    <span>{{$franchise->category->name}}</span>
                                                                @endif

                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.changes_in_the_profile_of_activities_in_the_legal_entity')</span>
                                                                <span>{{($franchise->changes_profile_legal_entity) ? 'Да' : 'Нет'}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </bulma-accordion-item>
                                                <hr class="hr-basic is-marginless">
                                                <bulma-accordion-item>
                                                    <h2 slot="title" class="mb-0 is-flex has-align-items-center">
                                                        <img
                                                            src="{{ asset('/svg/icons/ic_process.svg') }}"
                                                            alt=""
                                                            class="icon mr-0-5">
                                                        <span>@lang('franchises.single.business_processes')</span>
                                                    </h2>
                                                    <div slot="content" class="pb-1">
                                                        <div
                                                            class="object__info__list is-size-875 has-text-dark-blue">
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.schemes_of_work')</span>
                                                                <span>{{($franchise->transfer_work_schemes)? 'Входит' : 'Не входит'}}</span>
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.training')</span>
                                                                <span>{{($franchise->ready_training_materials)? 'Входит' : 'Не входит'}}</span>
                                                            </div>
                                                            <div class="object__info__list__item">
                                                                <span>@lang('franchises.single.customer_base')</span>
                                                                <span>{{($franchise->transfer_customer_base)? 'Входит' : 'Не входит'}}</span>
                                                            </div>
                                                            {{--                                                            <div class="object__info__list__item">--}}
                                                            {{--                                                                <span>@lang('franchises.single.certificates')</span>--}}
                                                            {{--                                                                <span>Не нужны</span>--}}
                                                            {{--                                                            </div>--}}
                                                        </div>
                                                    </div>
                                                </bulma-accordion-item>
                                                <hr class="hr-basic is-marginless">

                                            </bulma-accordion>
                                            <h2>@lang('franchises.single.training_and_support')</h2>
                                            <p>{!! $franchise->education !!} </p>
                                            <div class="buttons">
                                                @if (Auth::check())
                                                    <reserve-button
                                                        :id="{{$franchise->id}}"
                                                        :url="'/franchise-reserve'"
                                                        :type="'franchise'"/>
                                                @else
                                                    <a href="/register"
                                                       class="button is-success is-outlined has-text-weight-bold is-size-875 h-3">{{trans('strings.reserve')}}
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-news" class="content-tab" style="display: none;">
                                        <div class="content">
                                            <h2>@lang('franchises.single.franchise_news')</h2>
                                            <div>
                                                @foreach($latestNews as $news)
                                                    @php
                                                        $descFirstPart = mb_substr($news->description, 0, 270);
                                                        $descLength = mb_strlen($news->description);
                                                        $descLastPart =  $descLength > 270 ? mb_substr($news->description, 270, $descLength) : '';
                                                    @endphp
                                                    <article class="franchise-news">
                                                        <div class="columns is-multiline is-gapless is-marginless">
                                                            <div class="column is-6">
                                                                <figure class="franchise-news__thumb is-marginless">
                                                                    @if($newsImg = $news->getFirstMediaUrl('franchise_news'))
                                                                        <img
                                                                            src="{{$newsImg}}"
                                                                            alt="" class="has-border-radius">
                                                                    @endif
                                                                </figure>
                                                            </div>
                                                        </div>
                                                        <h3 class="franchise-news__title">{{$news->title}}</h3>
                                                        <time class="has-text-basic is-size-875 franchise-news__time">
                                                            {{$news->created_at->formatLocalized('%d %B %Y')}}
                                                        </time>
                                                        <div>
                                                            <!-- Excerpt News -->
                                                            <p class="franchise-news__excerpt is-size-875">
                                                                {{$descFirstPart}}
                                                            </p>
                                                            @if($descLastPart):
                                                            <bulma-accordion :slide="{
                                                                    duration: '.3s',
                                                                    timerFunc: 'ease'
                                                                }"
                                                                             :dropdown="true"
                                                                             class="accordion_flip accordion_with-excerpt px-0">
                                                                <bulma-accordion-item>
                                                                    <div slot="title" class="mb-0">
                                                                    <span
                                                                        class="has-text-info has-text-decoration-underline has-text-weight-normal is-size-875">
                                                                         <span
                                                                             class="show">@lang('franchises.single.news_more_show')</span>
                                                                        <span
                                                                            class="hide">@lang('franchises.single.news_more_close')</span>
                                                                    </span>
                                                                    </div>
                                                                    <div slot="content"
                                                                         class="pb-1 franchise-news__excerpt is-size-875">
                                                                        <!-- Continue News -->
                                                                        <p>{{$descLastPart}}</p>
                                                                    </div>
                                                                </bulma-accordion-item>
                                                            </bulma-accordion>
                                                            @endif
                                                        </div>
                                                    </article>
                                                @endforeach
                                            </div>
                                            <div class="buttons">
                                                <button @click="franchise.showModalBuy = true"
                                                        class="button is-success px-1 has-text-weight-bold is-size-875 h-3">
                                                    @lang('franchises.single.btn_buy')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-open-projects" class="content-tab" style="display: none;">
                                        <div class="content">
                                            <h2>@lang('franchises.single.our_facilities_on_map')</h2>
                                            <div class="mb-2">
                                                <div class="mb-1 has-text-basic">@lang('franchises.single.all_object')
                                                    {{count($objects)}}
                                                </div>
                                                @if($objectsMarkersData = $franchise->franchiseObjectsMarkersData())
                                                    <map-objects-franchises
                                                        :objects-markers="{{$objectsMarkersData['markersData']}}"
                                                        :center-coordinates="{{$objectsMarkersData['centerCoordinates']}}"></map-objects-franchises>
                                                @endif
                                            </div>
                                            <div>
                                                @foreach($objects as $object)
                                                    @php
                                                        $descFirstPart = mb_substr($object->description, 0, 270);
                                                        $descLength = mb_strlen($object->description);
                                                        $descLastPart =  $descLength > 270 ? mb_substr($object->description, 270, $descLength) : '';
                                                    @endphp
                                                    <article class="franchise-news" id="object_{{$object->id}}">
                                                        <div class="columns is-multiline">
                                                            <div class="column is-6">
                                                                <figure class="franchise-news__thumb is-marginless">
                                                                    @if($objectImg = $object->getFirstMediaUrl('franchise_objects'))
                                                                        <img
                                                                            src="{{$objectImg}}"
                                                                            alt="" class="has-border-radius">
                                                                    @endif
                                                                </figure>
                                                            </div>
                                                            <div class="column is-6">
                                                                <h3 class="franchise-news__title">{{$object->title}}</h3>
                                                                <div
                                                                    class="location is-size-875 is-flex has-align-items-center">
                                                                    <div class="icon is-flex has-justify-content-start">
                                                                        <img
                                                                            src="http://marketplacenew.local/svg/icons/ic_location.svg"
                                                                            alt=""></div>
                                                                    <div>{{$object->address}}</div>
                                                                </div>
                                                                <div class="is-size-875 has-text-dark-blue mt-1">
                                                                    <div>Телефон/Факс: {{$object->phone}}</div>
                                                                    <div>E-mail: {{$object->email}}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <!-- Excerpt News -->
                                                            <p class="franchise-news__excerpt is-size-875">
                                                                {{$descFirstPart}}
                                                            </p>
                                                            @if($descLastPart)
                                                                <bulma-accordion :slide="{
                                                                    duration: '.3s',
                                                                    timerFunc: 'ease'
                                                                }"
                                                                                 :dropdown="true"
                                                                                 class="accordion_flip accordion_with-excerpt px-0">
                                                                    <bulma-accordion-item>
                                                                        <div slot="title" class="mb-0">
                                                                    <span
                                                                        class="has-text-info has-text-decoration-underline has-text-weight-normal is-size-875">
                                                                        <span
                                                                            class="show">@lang('franchises.single.news_more_show')</span>
                                                                        <span
                                                                            class="hide">@lang('franchises.single.news_more_close')</span>
                                                                    </span>
                                                                        </div>
                                                                        <div slot="content"
                                                                             class="pb-1 franchise-news__excerpt is-size-875">
                                                                            <!-- Continue News -->
                                                                            <p>{{$descLastPart}}</p>
                                                                        </div>
                                                                    </bulma-accordion-item>
                                                                </bulma-accordion>
                                                            @endif
                                                        </div>
                                                    </article>
                                                @endforeach
                                            </div>
                                            <div class="buttons">
                                                <button @click="franchise.showModalBuy = true"
                                                        class="button is-success px-1 has-text-weight-bold is-size-875 h-3">
                                                    @lang('franchises.single.btn_buy')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-reviews" class="content-tab" style="display: none;">
                                        <div class="content">
                                            <div class="columns is-multiline">
                                                <div class="column">
                                                    <h2>@lang('franchises.single.franchise_reviews')</h2>
                                                </div>
                                                <div class="column is-narrow">
                                                    <div class="buttons">
                                                        <button @click="franchise.showModalAddReview = true"
                                                                class="button is-info px-1 has-text-weight-bold is-size-875 h-3">
                                                            @lang('franchises.single.btn_add_reviews')
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="is-size-875">
                                                <p>Целесообразно собрать отзывы и определить удовлетворенность
                                                    франчайзи, сильные и слабые стороны работы франчайзера, четкость
                                                    обратной связи с ним, наличие поддержки, соблюдение
                                                    обязательств, скорость решения задач, адекватность товара или
                                                    услуги текущим тенденциям, спросу, эффективность маркетинговых
                                                    инструментов, ценообразование и своевременность поставок</p>
                                            </div>
                                            <div class="mt-1">
                                                <!-- Swiper -->
                                                <div class="swiper-container swiper-franchise-reviews">
                                                    <div class="swiper-wrapper">
                                                        @foreach($latestReviews as $review)
                                                            <div class="swiper-slide">
                                                                <div class="franchise-review">
                                                                    <figure class="franchise-review__author-thumb">
                                                                        @if($reviewImg = $review->getFirstMediaUrl('franchise_reviews_photo'))
                                                                            <img
                                                                                src="{{$reviewImg}}"
                                                                                alt="">
                                                                        @endif
                                                                    </figure>
                                                                    <div class="franchise-review__info">
                                                                        <h4>{{$review->name}}</h4>
                                                                        <div class="is-size-7 has-text-basic">
                                                                            {{$review->position}}
                                                                        </div>
                                                                        <blockquote class="blockquote-v2 mt-1">
                                                                            {{$review->description}}
                                                                        </blockquote>
                                                                        <a @click="franchise.showModalShowReview = true"
                                                                           class="has-text-info has-text-decoration-underline is-size-875">
                                                                            @lang('franchises.single.btn_get_full_history')
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <!-- Add Arrows -->
                                                    <div class="swiper-button-next swiper-franchise-reviews-next"></div>
                                                    <div class="swiper-button-prev swiper-franchise-reviews-prev"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="column is-4">
                                    <div class="object__basic-short-info">
                                        <div class="list-properties">
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.category')</span>
                                                <span>{{$franchise->category->name}}</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.direction')</span>
                                                <span>{{$franchise->options['step1']['direction']['val']}}</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.year_foundation_company')</span>
                                                <span>{{$franchise->foundation_year}}</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.franchise_foundation_year')</span>
                                                <span>{{$franchise->start_year}}</span>
                                            </div>
                                            <hr>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.revolutions_per_month')</span>
                                                <span>{{format_price($franchise->profit)}}</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.total_costs')</span>
                                                <span>{{format_price($franchise->general_expenses)}}</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.monthly_expenses')</span>
                                                <span>{{format_price($franchise->month_expenses)}}</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.staff_costs')</span>
                                                <span>{{format_price($franchise->staff_costs)}}</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.real_estate_costs')</span>
                                                <span>{{format_price($franchise->property_costs)}}</span>
                                            </div>
                                            <hr>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.launch_time')</span>
                                                <span>{{ $franchise->launch_dates }} @lang('franchises.single.short_month')</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.exit_on_oper_profit')</span>
                                                <span>{{ $franchise->operating_profit }} @lang('franchises.single.short_month')</span>
                                            </div>
                                            <div class="list-properties__item">
                                                <span>@lang('franchises.single.yield_on_payback')</span>
                                                <span>{{ $franchise->return_payback }} @lang('franchises.single.short_month')</span>
                                            </div>

                                        </div>
                                        <div class="buttons">
                                            @if(Auth::check())
                                                @if ($franchise->certificates && Auth::user()->hasRole(\App\Models\Roles::ROLE_BUYER) && !Auth::user()->requestedDoc($franchise->id))
                                                    <a @click="franchise.showModalAddRequestDoc = true"
                                                       class="button is-success is-fullwidth has-text-weight-bold is-size-875 h-3">
                                                        @lang('franchises.single.get_docs')
                                                    </a>
                                                @endif
                                            @else
                                                <a href="/register"
                                                   class="button is-success is-fullwidth has-text-weight-bold is-size-875 h-3">
                                                    @lang('franchises.single.get_docs')
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="has-text-centered object__how-purchase">
                                        <a class="is-size-875 is-info" @click="showModal('modal-how-purchase')">
                                            <img src="{{ asset('/svg/icons/ic_lifesaver.svg') }}"
                                                 alt="@lang('franchises.single.how_does_purchase_take_place')">
                                            <span>@lang('franchises.single.how_does_purchase_take_place')</span>
                                        </a>
                                    </div>
                                    {{--<div class="mt-2">
                                        <bulma-accordion
                                            :slide="{ duration: '.3s', timerFunc: 'ease' }"
                                            :dropdown="true">
                                            <bulma-accordion-item class="has-background-light-grayish-blue has-border-radius mb-1">
                                                <h5 slot="title" class="mb-0 is-flex has-align-items-center">
                                                    <img src="{{ asset('/img/franchises/ic_age.png') }}"
                                                         alt=""
                                                         class="icon mr-0-5">

                                                    <span class="has-text-weight-bold is-size-875">
                                                        @lang('franchises.single.age_groups')
                                                    </span>
                                                </h5>
                                                <div slot="content" class="px-1-5 pb-1">
                                                    <div>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius
                                                        eligendi excepturi expedita explicabo iure libero quidem totam
                                                        voluptas.
                                                    </div>
                                                </div>
                                            </bulma-accordion-item>
                                            <bulma-accordion-item class="has-background-light-grayish-blue has-border-radius mb-1">
                                                <h5 slot="title" class="mb-0 is-flex has-align-items-center">
                                                    <img src="{{ asset('/img/franchises/ic_gender.png') }}"
                                                         alt=""
                                                         class="icon mr-0-5">
                                                    <span class="has-text-weight-bold is-size-875">Gender</span>
                                                </h5>
                                                <div slot="content" class="px-1-5 pb-1">
                                                    <div>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius eligendi excepturi expedita explicabo iure libero quidem totam voluptas.
                                                    </div>
                                                </div>
                                            </bulma-accordion-item>
                                            <bulma-accordion-item
                                                class="has-background-light-grayish-blue has-border-radius mb-1">
                                                <h5 slot="title" class="mb-0 is-flex has-align-items-center">
                                                    <img
                                                        src="{{ asset('/img/franchises/ic_ethnicity.png') }}"
                                                        alt=""
                                                        class="icon mr-0-5">
                                                    <span
                                                        class="has-text-weight-bold is-size-875">@lang('franchises.single.ethnic_groups')</span>
                                                </h5>
                                                <div slot="content" class="px-1-5 pb-1">
                                                    <div>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius eligendi excepturi expedita explicabo iure libero quidem totam voluptas.
                                                    </div>
                                                </div>
                                            </bulma-accordion-item>
                                        </bulma-accordion>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Section similar offers -->
        <section class="section">
            <div class="container">
                <h2 class="title section-title">@lang('franchises.single.similar_offers')</h2>
                <div class="columns is-multiline">
                    <div class="column is-4">
                        <div class="card card-object">
                            <div class="card-image">
                                <figure class="image is-16by9">
                                    <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                                         alt="Placeholder image">
                                </figure>
                                <span class="tag tag-payback">
                                    <span class="name">@lang('franchises.single.profit_per_month_2')</span>
                                    <span class="term">$ 25 000</span>
                                </span>
                                <!--<div class="info-icons-wrap info-icons-wrap_left">
                                    <span class="info-icon object-promo">
                                    <img src="/svg/icons/ic_recommended.svg" alt="Recommended">
                                    </span>
                                    <span class="info-icon object-sale"><img src="/svg/icons/ic_sale.svg" alt="Sale"></span>
                                </div>-->
                                <div class="info-icons-wrap info-icons-wrap_right">
                                    <a href="#" class="info-icon object-favorite has-tooltip"><img
                                            src="/svg/icons/ic_favorites_white.svg" alt="Fav"></a>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-object__header"><h3 class="title">ОАО ИнжВодСтрой</h3>
                                    <p class="location"><img src="/svg/icons/ic_location.svg" alt="Fav">
                                        <span>Польша, г. Билава</span>
                                    </p>
                                </div>
                                <div class="content card-object__content">
                                    <div class="list-properties">
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.category')</span> <span>IT-бизнес</span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.Investment_size')</span>
                                            <span><b>$48 000</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.payback')</span>
                                            <span><b>1 @lang('franchises.single.short_month')</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.royalty_free')</span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-object__footer">
                                    <a href="/franchises/single"
                                       class="button is-outlined is-fullwidth has-text-weight-bold is-info">
                                        @lang('franchises.single.more_about')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="card card-object">
                            <div class="card-image">
                                <figure class="image is-16by9">
                                    <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                                         alt="Placeholder image">
                                </figure>
                                <span class="tag tag-payback">
                                    <span class="name">@lang('franchises.single.profit_per_month_2')</span>
                                    <span class="term">$ 25 000</span>
                                </span>
                                <!--<div class="info-icons-wrap info-icons-wrap_left">
                                    <span class="info-icon object-promo">-->
                                <!--<img src="/svg/icons/ic_recommended.svg" alt="Recommended">-->
                                <!--</span>-->
                                <!--<span class="info-icon object-sale"><img src="/svg/icons/ic_sale.svg" alt="Sale"></span>
                            </div>-->
                                <div class="info-icons-wrap info-icons-wrap_right">
                                    <a href="#" class="info-icon object-favorite has-tooltip"><img
                                            src="/svg/icons/ic_favorites_white.svg" alt="Fav"></a>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-object__header"><h3 class="title">ОАО ИнжВодСтрой</h3>
                                    <p class="location"><img src="/svg/icons/ic_location.svg" alt="Fav">
                                        <span>Польша, г. Билава</span>
                                    </p>
                                </div>
                                <div class="content card-object__content">
                                    <div class="list-properties">
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.category')</span> <span>IT-бизнес</span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.Investment_size')</span>
                                            <span><b>$48 000</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.payback')</span>
                                            <span><b>1 @lang('franchises.single.short_month')</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.royalty_free')</span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-object__footer">
                                    <a href="/franchises/single"
                                       class="button is-outlined is-fullwidth has-text-weight-bold is-info">
                                        @lang('franchises.single.more_about')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="card card-object">
                            <div class="card-image">
                                <figure class="image is-16by9">
                                    <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                                         alt="Placeholder image">
                                </figure>
                                <span class="tag tag-payback">
                                    <span class="name">@lang('franchises.single.profit_per_month_2')</span>
                                    <span class="term">$ 25 000</span>
                                </span>
                                <!--<div class="info-icons-wrap info-icons-wrap_left">
                                    <span class="info-icon object-promo">
                                    <img src="/svg/icons/ic_recommended.svg" alt="Recommended">
                                    </span>
                                    <span class="info-icon object-sale"><img src="/svg/icons/ic_sale.svg" alt="Sale"></span>
                                </div>-->
                                <div class="info-icons-wrap info-icons-wrap_right">
                                    <a href="#" class="info-icon object-favorite has-tooltip"><img
                                            src="/svg/icons/ic_favorites_white.svg" alt="Fav"></a>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-object__header"><h3 class="title">ОАО ИнжВодСтрой</h3>
                                    <p class="location">
                                        <img src="/svg/icons/ic_location.svg" alt="Fav">
                                        <span>Польша, г. Билава</span>
                                    </p>
                                </div>
                                <div class="content card-object__content">
                                    <div class="list-properties">
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.category')</span>
                                            <span>IT-бизнес</span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.Investment_size')</span>
                                            <span><b>$48 000</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.payback')</span>
                                            <span><b>1 @lang('franchises.single.short_month')</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('franchises.single.royalty_free')</span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-object__footer">
                                    <a href="/franchises/single"
                                       class="button is-outlined is-fullwidth has-text-weight-bold is-info">
                                        @lang('franchises.single.more_about')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Modal -->
    <modal-buy-franchise
        :show="franchise.showModalBuy"
        @close="franchise.showModalBuy = false"/>

    <!-- Modal -->
    <modal-add-review-franchise
        :show="franchise.showModalAddReview"
        @close="franchise.showModalAddReview = false"/>

    <!-- Modal -->
    <modal-show-review-franchise
        :show="franchise.showModalShowReview"
        @close="franchise.showModalShowReview = false"/>


    <!-- Modal -->




    <!-- Modal -->
    <div class="modal" id="modal-how-purchase">
        <div class="modal-background" @click="hideModal('modal-how-purchase')"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">@lang('franchises.single.how_does_purchase_take_place')</p>
                <button class="delete close-modal" @click="hideModal('modal-how-purchase')" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div class="content">
                    <p>
                        @lang('franchises.single.modal_how_purchase.title')
                    </p>
                    <ol class="ol-big-counter">
                        <li>@lang('franchises.single.modal_how_purchase.step1')</li>
                        <li>@lang('franchises.single.modal_how_purchase.step2')</li>
                        <li>@lang('franchises.single.modal_how_purchase.step3')</li>
                        <li>@lang('franchises.single.modal_how_purchase.step4')</li>
                        <li>@lang('franchises.single.modal_how_purchase.step5')</li>
                        <li>@lang('franchises.single.modal_how_purchase.step6')</li>
                        <li>@lang('franchises.single.modal_how_purchase.step7')</li>
                    </ol>
                </div>
            </section>
            <footer class="modal-card-foot">
                <a href="#" class="button is-success is-fullwidth h-3">
                    @lang('franchises.single.modal_how_purchase.btn_submit_req')
                </a>
            </footer>
        </div>
    </div>



@endsection

