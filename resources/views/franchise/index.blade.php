@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <franchises-list price-ranges='{!! $priceRanges !!}'/>
@endsection
