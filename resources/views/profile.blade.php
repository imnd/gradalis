@extends('layouts.app')

@section('content')
    <section class="section py-0">
        <div class="container">
            @if ($message = Session::get('success_message'))
                <br>
                <div class="notification is-success">
                    <button class="delete"></button>
                    <div class="notification__icon">
                        <img src="{{ asset('/svg/icons/notification/ic_thankyou.svg') }}" class="svg" alt="">
                    </div>
                    <div class="notification__content-wrap">
                        <h5 class="notification__title has-text-weight-bold">
                            {{Session::get('success_message_title')}}
                        </h5>
                        <div class="notification__content content is-size-875">
                            {!! $message !!}
                        </div>
                    </div>
                </div>
            @endif
            <profile />
        </div>
    </section>
@endsection
