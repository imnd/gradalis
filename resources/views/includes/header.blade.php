<header class="header has-background-white">
    <div class="container">
        <nav class="navbar header__line-top is-flex">
            <div class="navbar-brand">
                <a class="navbar-item py-0" href="{{ url('/') }}">
                    <img
                        src="{{ asset('/svg/logo.svg') }}"
                        height="64"
                        alt="{{ config('app.name', 'Laravel') }}" />
                </a>
            {{--<div class="navbar-item navbar-contacts is-hidden-mobile">
            <a href="tel:8800202470" class="navbar-contacts__phone">8 800 2024 70</a>
            <div class="navbar-contacts__worktime">Работаем с 10:00 до 20:00</div>
            </div>--}}

            <!-- TODO: Возможно, нужно будет доработать -->
                @auth
                    @if(Auth::user()->user_id)
                        {{-- @if(false) --}}
                        <div class="navbar-item is-hidden-touch">
                            <div class="manager is-flex">
                                @if(Auth::user()->user->avatar)
                                    <figure class="manager__avatar">
                                        <img src="{{Auth::user()->user->avatar}}">
                                    </figure>
                                @endif
                                <div class="manager__info">
                                    <div class="manager__name has-text-weight-bold">
                                        {{Auth::user()->hasAnyRole([Roles::ROLE_ACCOUNT_MANAGER, Roles::ROLE_MEDIA_BUYER])}}
                                        @if(Auth::user()->hasAnyRole([Roles::ROLE_ACCOUNT_MANAGER, Roles::ROLE_MEDIA_BUYER]))
                                            @lang('header.your_manager')
                                        @elseif(Auth::user()->hasAnyRole(['Покупатель', 'Продавец']))
                                            @lang('header.your_broker')
                                        @endif
                                        {{Auth::user()->user->first_name}}
                                    </div>
                                    <div class="is-flex">
                                        @if(Auth::user()->user->phone)
                                            <a href="#" class="manager__phone is-size-875">
                                                <img src="/svg/icons/ic_call.svg" alt="" class="svg">
                                                <span>{{Auth::user()->user->phone}}</span>
                                            </a>
                                        @endif
                                        <a href="/profile/chat/dialog/add/{{Auth::user()->user->id}}"
                                           class="manager__chat is-size-875">
                                            <img src="/svg/icons/chat/ic_person.svg" alt="" class="svg">
                                            <span>@lang('header.write_to_chat')</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endauth
                {{-- @if(Auth::check() && (Auth::user()->hasRole('Продавец') || Auth::user()->hasRole('Покупатель')))
                    <div class="navbar-item is-hidden-touch">
                        <div class="manager is-flex">
                            <figure class="manager__avatar">
                                <img src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg" alt="">
                            </figure>
                            <div class="manager__info">
                                <div class="manager__name has-text-weight-bold">@lang('header.your_manager') Ирина</div>
                                <div class="is-flex">
                                    <a href="#" class="manager__phone is-size-875">
                                        <img src="/svg/icons/ic_call.svg" alt="" class="svg"><span>+38 (450) 566-56-43</span>
                                    </a>
                                    <a href="#" class="manager__chat is-size-875">
                                        <img src="/svg/icons/chat/ic_person.svg" alt="" class="svg"><span>@lang('header.write_to_chat')</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif --}}
            </div>


            <div class="navbar-end">
                <!-- Authentication Links -->
                @guest
                    <div class="navbar-item is-flex">
                        @if(Auth::check())
                            <a class="navbar-item rounded"
                               v-tooltip.bottom-center="'Чтобы добавить в избранное, нужно авторизоваться'">
                                <img src="{{ asset('/svg/icons/ic_favorites.svg') }}" alt="@lang('header.favorites')">
                            </a>
                        @endif
                        <a class="navbar-item basic login" href="{{ route('login') }}"><img
                                src="{{ asset('/svg/icons/ic_login.svg') }}" alt="@lang('header.sign_in')"><span
                                class="is-hidden-tablet-only">{{ __('Login') }}</span></a>
                        <div class="buttons has-addons buttons-business is-hidden-mobile">
                            <a href="{{route('register')}}" class="button is-warning">@lang('header.registration')</a>
                        </div>
                    </div>
                @else
                    <div class="navbar-item is-paddingless">
                        <div class="dropdown is-right is-hoverable">
                                <div class="dropdown-trigger">
                                    <a class="navbar-item rounded">
                                        <img src="{{ asset('/svg/icons/ic_notifications.svg') }}"
                                            alt="@lang('header.notifications')">
                                            @verbatim
                                                <div v-cloak>
                                                    <div  v-if="$store.state.notifications.length > 0"
                                                        class="tag is-danger is-rounded tag__count" >
                                                        {{$store.state.notifications.length}}
                                                    </div>
                                                </div>
                                            @endverbatim
                                        {{-- @if(Auth::user()->unreadNotifications)
                                            <span class="tag is-danger is-rounded tag__count">{{count(Auth::user()->unreadNotifications)}}</span>
                                        @endif --}}
                                    </a>
                                </div>
                                <header-notifications></header-notifications>
                        </div>
                        @if (Auth::check())
                            <a href="/profile/favorites" class="navbar-item rounded">
                                <img src="{{ asset('/svg/icons/ic_favorites.svg') }}" alt="@lang('header.favorites')">
                            </a>
                        @endif
                        @php
                            $url = '/profile';
                            if (auth()->user()->hasRole('Акаунт-менеджер')) {
                                $url = '/account';
                            }
                        @endphp
                        <div class="dropdown is-right is-hoverable">
                            <div class="dropdown-trigger">
                                <a href="{{$url}}" class="navbar-item basic user">
                                    <img src="{{ Auth::user()->avatarUrl ?: asset('/svg/icons/ic_login.svg') }}"
                                         aria-haspopup="true"
                                         aria-controls="dropdown-user-menu"><span
                                        class="is-hidden-mobile">{{ Auth::user()->full_name }}</span></a>
                            </div>
                            <div class="dropdown-menu" id="dropdown-user-menu" role="menu">
                                <div class="dropdown-content is-paddingless">

                                    <a class="dropdown-item" href="{{$url}}">@lang('header.personal_cabinet')</a>
                                    <hr class="dropdown-divider is-marginless">
                                    <a class="dropdown-item" href="/profile/chat">@lang('header.messages')</a>
                                    <hr class="dropdown-divider is-marginless">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        @lang('header.sign_out')
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">@csrf</form>
                                </div>
                            </div>
                        </div>
                        {{--<a href="/profile" class="navbar-item basic user"><img src="{{ Auth::user()->avatar }}"--}}
                        {{--alt="User"><span--}}
                        {{--class="is-hidden-mobile">{{ Auth::user()->full_name }}</span></a>--}}
                        @if(Auth::check() && (Auth::user()->hasRole('Медиа-баер') || Auth::user()->hasRole('Акаунт-менеджер')))
                            <a href="/broker/balance" class="navbar-item basic is-hidden-mobile">
                                <img src="{{ asset('/svg/icons/ic_balance.svg') }}" alt="@lang('header.balance')">
                                <span>{{ format_price(Auth::user()->balance) }}</span>
                            </a>

                            <a class="navbar-item basic is-hidden-tablet" v-tooltip.left-start="{
                                    content: '$100',
                                    trigger: 'click',
                            }">
                                <img src="{{ asset('/svg/icons/ic_balance.svg') }}" alt="@lang('header.balance')"></a>
                        @else
                            <a href="/profile/balance" class="navbar-item basic is-hidden-mobile">
                                <img src="{{ asset('/svg/icons/ic_balance.svg') }}" alt="@lang('header.balance')">
                                <span>{{ format_price(Auth::user()->balance) }}</span>
                            </a>

                            <a class="navbar-item basic is-hidden-tablet" v-tooltip.left-start="{
                                    content: '$100',
                                    trigger: 'click',
                            }">
                                <img src="{{ asset('/svg/icons/ic_balance.svg') }}" alt="@lang('header.balance')">
                            </a>
                        @endif

                        {{--<a class="navbar-item basic" href="{{ route('logout') }}" onclick="event.preventDefault();--}}
                        {{--document.getElementById('logout-form').submit();"><img--}}
                        {{--src="{{ asset('/svg/icons/ic_login.svg') }}" alt="Login"><span>{{ __('Logout') }}</span></a>--}}
                        {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                        {{--@csrf--}}
                        {{--</form>--}}
                    </div>
                @endguest

            </div>
        </nav>
    </div>
    <div class="navbar-divider is-block"></div>
    <div class="container">

        <!-- ToDo: Other menu in Broker Personal Area -->
    @if(Auth::check() && !Auth::user()->hasRole('Медиа-баер') && !Auth::user()->hasRole('Акаунт-менеджер'))
        <!-- Desktop Menu-->
            <nav class="navbar header__line-bottom is-hidden-touch" role="navigation" aria-label="main navigation">
                <div class="navbar-start main-menu">
                    <a href="{{ route('home') }}" class="navbar-item {{ request()->is('/') ? 'is-active' : '' }}">
                        <img src="{{ asset('/svg/icons/ic_home.svg') }}" alt="@lang('menu.home')"
                             class="icon"><span>@lang('menu.home')</span>
                    </a>
                <!--<a href="{{ route('business.index') }}"
                       class="navbar-item {{ request()->is('business*') ? 'is-active' : '' }}">
                        <img src="{{ asset('/svg/icons/ic_business.svg') }}" alt="@lang('menu.business')"
                             class="icon"><span>@lang('menu.business')</span>
                    </a> -->
                    <a class="navbar-item {{ request()->is('franchise*') ? 'is-active' : '' }}"
                       href="{{ route('franchise.index') }}">
                        <img src="{{ asset('/svg/icons/ic_franchise.svg') }}" alt="@lang('menu.franchise')"
                             class="icon"><span>@lang('menu.franchise')</span>
                    </a>
                    <a href="/user-services" class="navbar-item {{ request()->is('services*') ? 'is-active' : '' }}">
                        <img src="{{ asset('/svg/icons/ic_product.svg') }}" alt="@lang('menu.user_services')"
                             class="icon"><span>@lang('menu.user_services')</span>
                    </a>
                    <a href="/news" class="navbar-item {{ request()->is('news*') ? 'is-active' : '' }}">
                        <img src="{{ asset('/svg/icons/ic_news.svg') }}" alt="@lang('menu.news')"
                             class="icon"><span>@lang('menu.news')</span>
                    </a>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link {{ request()->is(['about','contacts','vacancy','reviews','help']) ? 'is-active' : '' }}">
                            <img src="{{ asset('/svg/icons/ic_about.svg') }}"
                                 alt="@lang('menu.about_items.about_company')"
                                 class="icon"><span>@lang('menu.about_items.about_company')</span>
                        </a>

                        <div class="navbar-dropdown">
                            <a href="/about" class="navbar-item">
                                @lang('menu.about_items.about_company')
                            </a>
                            <a href="/contacts" class="navbar-item">
                                @lang('menu.about_items.contacts')
                            </a>
                            <a href="/vacancy" class="navbar-item">
                                @lang('menu.about_items.vacancy')
                            </a>
                            {{--
                            <a href="/reviews" class="navbar-item">
                                @lang('menu.about_items.reviews')
                            </a>
                            --}}
                            <a href="/help" class="navbar-item">
                                @lang('menu.about_items.help')
                            </a>
                        </div>
                    </div>
                </div>

                <div class="navbar-end">
                    @include('includes.navbar-lang-desktop')
                </div>
            </nav>

            <!-- Tablet and mobile Menu-->
            <div class="header__line-bottom header__line-bottom_mobile is-flex is-hidden-desktop">

                @include('includes.navbar-main-mobile')

                @include('includes.navbar-lang-mobile')

            </div>
        @endif

    @include('includes.partner-nav')
    </div>

    @guest
        <div class="container">
            <nav class="navbar header__line-bottom is-hidden-touch" role="navigation" aria-label="main navigation">
                <div class="navbar-start main-menu">
                    <a href="{{ route('home') }}" class="navbar-item {{ request()->is('/') ? 'is-active' : '' }}">
                        <img src="{{ asset('/svg/icons/ic_home.svg') }}" alt="@lang('menu.home')"
                             class="icon"><span>@lang('menu.home')</span>
                    </a>
                    <a href="{{ route('business.index') }}"
                       class="navbar-item {{ request()->is('business*') ? 'is-active' : '' }}">
                        <img src="{{ asset('/svg/icons/ic_business.svg') }}" alt="@lang('menu.business')"
                             class="icon"><span>@lang('menu.business')</span>
                    </a>
                    <a class="navbar-item {{ request()->is('franchise*') ? 'is-active' : '' }}"
                       href="{{ route('franchise.index') }}">
                        <img src="{{ asset('/svg/icons/ic_franchise.svg') }}" alt="@lang('menu.franchise')"
                             class="icon"><span>@lang('menu.franchise')</span>
                    </a>
                    <a href="/user-services" class="navbar-item {{ request()->is('services*') ? 'is-active' : '' }}">
                        <img src="{{ asset('/svg/icons/ic_product.svg') }}" alt="@lang('menu.user_services')"
                             class="icon"><span>@lang('menu.user_services')</span>
                    </a>
                    <a href="/news" class="navbar-item {{ request()->is('news*') ? 'is-active' : '' }}">
                        <img src="{{ asset('/svg/icons/ic_news.svg') }}" alt="@lang('menu.news')"
                             class="icon"><span>@lang('menu.news')</span>
                    </a>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link {{ request()->is(['about','contacts','vacancy','reviews','help']) ? 'is-active' : '' }}">
                            <img src="{{ asset('/svg/icons/ic_about.svg') }}"
                                 alt="@lang('menu.about_items.about_company')"
                                 class="icon"><span>@lang('menu.about_items.about_company')</span>
                        </a>

                        <div class="navbar-dropdown">
                            <a href="/about" class="navbar-item">
                                @lang('menu.about_items.about_company')
                            </a>
                            <a href="/contacts" class="navbar-item">
                                @lang('menu.about_items.contacts')
                            </a>
                            <a href="/vacancy" class="navbar-item">
                                @lang('menu.about_items.vacancy')
                            </a>
                            {{--
                            <a href="/reviews" class="navbar-item">
                                @lang('menu.about_items.reviews')
                            </a>
                            --}}
                            <a href="/help" class="navbar-item">
                                @lang('menu.about_items.help')
                            </a>
                        </div>
                    </div>
                </div>

                <div class="navbar-end">
                    @include('includes.navbar-lang-desktop')
                </div>
            </nav>

            <!-- Tablet and mobile Menu --!>
            <div class="header__line-bottom header__line-bottom_mobile is-flex is-hidden-desktop">

                @include('includes.navbar-main-mobile')


                <div class="navbar-end">
                    @include('includes.navbar-lang-mobile')
                </div>

            </div>
        </div>
        <div class="buttons has-addons buttons-business buttons-business_mobile is-hidden-tablet">
            <a class="button is-link" data-desc="@lang('header.sign_up')">@lang('header.buy_business')</a>
            <a class="button is-warning" data-desc="@lang('header.sign_up')">@lang('header.sell_business')</a>
        </div>
    @endguest

</header>
