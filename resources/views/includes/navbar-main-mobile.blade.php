<div class="dropdown is-left navbar-item navbar-main-menu main-menu">

    <div class="dropdown-trigger">
        <div class="navbar-link navbar-item" aria-haspopup="true"
             aria-controls="dropdown-menu-main-mobile">
            {{--ToDo: вывести название и иконку текущей страницы--}}
            <img src="{{ asset('/svg/icons/ic_home.svg') }}" alt="@lang('menu.menu')"
                 class="icon"><span>@lang('menu.menu')</span>
        </div>
    </div>
    <div class="dropdown-menu" id="dropdown-menu-main-mobile" role="menu">
        <div class="dropdown-content">
            <a href="{{ route('home') }}" class="dropdown-item navbar-item">
                <img src="{{ asset('/svg/icons/ic_home.svg') }}" alt="@lang('menu.home')"
                     class="icon"><span>@lang('menu.home')</span>
            </a>
            <a href="{{ route('business.index') }}" class="dropdown-item navbar-item">
                <img src="{{ asset('/svg/icons/ic_business.svg') }}" alt="@lang('menu.business')" class="icon">
                <span>@lang('menu.business')</span>
            </a>
            <a href="{{ route('franchise.index') }}" class="dropdown-item navbar-item">
                <img src="{{ asset('/svg/icons/ic_franchise.svg') }}" alt="@lang('menu.franchise')" class="icon">
                <span>@lang('menu.franchise')</span>
            </a>
            <a href="/user-services" class="dropdown-item navbar-item">
                <img src="{{ asset('/svg/icons/ic_product.svg') }}" alt="@lang('menu.user_services')" class="icon">
                <span>@lang('menu.user_services')</span>
            </a>
            <a href="/news" class="dropdown-item navbar-item">
                <img src="{{ asset('/svg/icons/ic_news.svg') }}" alt="@lang('menu.news')" class="icon">
                <span>@lang('menu.news')</span>
            </a>

            <div class="dropdown is-left dropdown-item navbar-item">

                <div class="dropdown-trigger">
                    <div class="navbar-link">
                        <img src="{{ asset('/svg/icons/ic_about.svg') }}" alt="@lang('menu.about_items.about_company')"
                             class="icon"><span>@lang('menu.about_items.about_company')</span>
                    </div>
                </div>
                <div class="dropdown-menu">
                    <div class="dropdown-content">
                        <a href="/about" class="dropdown-item navbar-item">
                            <span>@lang('menu.about_items.about_company')</span>
                        </a>
                        <a href="/contacts" class="dropdown-item navbar-item">
                            <span>@lang('menu.about_items.contacts')</span>
                        </a>
                        <a href="/vacancy" class="dropdown-item navbar-item">
                            <span>@lang('menu.about_items.vacancy')</span>
                        </a>
                        <a href="/reviews" class="dropdown-item navbar-item">
                            <span>@lang('menu.about_items.reviews')</span>
                        </a>
                        <a href="/help" class="dropdown-item navbar-item">
                            <span>@lang('menu.about_items.help')</span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
