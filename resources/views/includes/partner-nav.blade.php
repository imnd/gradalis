@if(Auth::check() && (Auth::user()->hasRole('Медиа-баер') || Auth::user()->hasRole('Акаунт-менеджер')))
    <nav class="navbar header__line-bottom is-hidden-touch" role="navigation" aria-label="main navigation">
        <div class="navbar-start main-menu main-menu_long">
            @if(Auth::user()->hasRole('Акаунт-менеджер'))
                <a href="/account" class="navbar-item {{ request()->is('account*') ? 'is-active' : '' }}">
                    <img src="{{ asset('/svg/icons/ic_partners.svg') }}" alt="@lang('header.partners')"
                         class="icon"><span>@lang('header.partners')</span>
                </a>
            @endif

            <a href="/broker/summary2" class="navbar-item {{ request()->is('broker/summary2') ? 'is-active' : '' }}">
                <img src="{{ asset('/svg/icons/ic_complete_stat.svg') }}" alt="@lang('header.statistics')"
                     class="icon"><span>@lang('header.summary')</span>
            </a>

            <a href="/broker/realtime" class="navbar-item {{ request()->is('broker/realtime') ? 'is-active' : '' }}">
                <img src="{{ asset('/svg/icons/ic_real_stats.svg') }}" alt="@lang('header.statistics')"
                     class="icon"><span>@lang('header.statistics')</span>
            </a>
            <a href="/broker/web-offers" class="navbar-item {{ request()->is('broker/web-offers') ? 'is-active' : '' }}">
                <img src="{{ asset('/svg/icons/ic_offers.svg') }}" class="icon"
                     alt="@lang('header.offers')"><span>@lang('header.offers')</span>
            </a>
            <a href="/broker/leads" class="navbar-item {{ request()->is('broker/leads') ? 'is-active' : '' }}">
                <img src="{{ asset('/svg/icons/ic_leads.svg') }}" class="icon"
                     alt="@lang('header.leads')"><span>@lang('header.leads')</span>
            </a>
            <a href="/broker/chat" class="navbar-item {{ request()->is('broker/chat') ? 'is-active' : '' }}">
                <img src="{{ asset('/svg/icons/ic_messages.svg') }}" class="icon"
                     alt="@lang('header.messages')"><span>@lang('header.messages')</span>
            </a>
            <a href="/broker/settings" class="navbar-item {{ request()->is('broker/settings') ? 'is-active' : '' }}">
                <img src="{{ asset('/svg/icons/ic_profile_settings.svg') }}" class="icon"
                     alt="@lang('header.settings')"><span>@lang('header.settings')</span>
            </a>


        </div>

        <div class="navbar-end">
            @include('includes.navbar-lang-desktop')
        </div>
    </nav>

    <!-- Tablet and mobile Menu-->
    <div class="header__line-bottom header__line-bottom_mobile is-flex is-hidden-desktop">

        <div class="dropdown is-left navbar-item navbar-main-menu main-menu">

            <div class="dropdown-trigger">
                <div class="navbar-link navbar-item" aria-haspopup="true"
                     aria-controls="dropdown-menu-main-mobile">
                    {{--ToDo: вывести название и иконку текущей страницы--}}
                    <img src="{{ asset('/svg/icons/ic_home.svg') }}" alt="@lang('menu.menu')"
                         class="icon">
                    <span>@lang('menu.menu')</span>
                </div>
            </div>
            <div class="dropdown-menu" id="dropdown-menu-main-mobile" role="menu">
                <div class="dropdown-content">
                    @if(Auth::user()->hasRole('Акаунт-менеджер'))
                        <a href="/account" class="dropdown-item navbar-item ">
                            <img src="{{ asset('/svg/icons/ic_partners.svg') }}" alt="@lang('header.partners')"
                                 class="icon"><span>@lang('header.partners')</span>
                        </a>
                    @endif

                    <a href="/broker/summary2" class="dropdown-item navbar-item">
                        <img src="{{ asset('/svg/icons/ic_complete_stat.svg') }}" alt="@lang('header.statistics')"
                             class="icon"><span>@lang('header.summary')</span>
                    </a>

                    <a href="/broker/realtime" class="dropdown-item navbar-item">
                        <img src="{{ asset('/svg/icons/ic_real_stats.svg') }}" alt="@lang('header.statistics')"
                             class="icon"><span>@lang('header.statistics')</span>
                    </a>
                    <a href="/broker/web-offers" class="dropdown-item navbar-item">
                        <img src="{{ asset('/svg/icons/ic_offers.svg') }}" class="icon"
                             alt="@lang('header.offers')"><span>@lang('header.offers')</span>
                    </a>
                    <a href="/broker/leads" class="dropdown-item navbar-item">
                        <img src="{{ asset('/svg/icons/ic_leads.svg') }}" class="icon"
                             alt="@lang('header.leads')"><span>@lang('header.leads')</span>
                    </a>
                    <a href="/broker/chat" class="dropdown-item navbar-item">
                        <img src="{{ asset('/svg/icons/ic_messages.svg') }}" class="icon"
                             alt="@lang('header.messages')"><span>@lang('header.messages')</span>
                    </a>
                    <a href="/broker/settings" class="dropdown-item navbar-item">
                        <img src="{{ asset('/svg/icons/ic_profile_settings.svg') }}" class="icon"
                             alt="@lang('header.settings')"><span>@lang('header.settings')</span>
                    </a>

                </div>
            </div>
        </div>


        <div class="navbar-end">
            @include('includes.navbar-lang-mobile')
        </div>

    </div>
@endif

