<footer class="footer">
    <div class="container">
        <div class="columns is-multiline">
            <div class="column is-3-desktop is-12-tablet footer__column-first">
                <a class="footer__logo" href="{{ url('/') }}">
                    <img src="{{ asset('/svg/logo.svg') }}" width="155" height="48" alt="{{ config('app.name', 'Laravel') }}">
                </a>
                <div class="footer__contacts">
                    <a href="tel:8800202470" class="footer__contacts__phone">8 800 2024 70</a>
                    <div class="footer__contacts__worktime">@lang('footer.we_work_with') с 10:00 до 20:00</div>
                </div>
                <div class="footer__socials">
                    <ul>
                        <li>
                            <a href="#" target="_blank" class="fb">
                                <img src="{{ asset('/svg/icons/socials/ic_facebook.svg') }}" alt="@lang('footer.soc_fb')">
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="ok">
                                <img src="{{ asset('/svg/icons/socials/ic_ok.svg') }}" alt="@lang('footer.soc_ok')">
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="yt">
                                <img src="{{ asset('/svg/icons/socials/ic_youtube.svg') }}" alt="@lang('footer.soc_yt')">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="column is-9-desktop is-12-tablet">
                <div class="columns footer__nav">
                    {{--
                    <div class="column is-3">
                        <ul>
                            <li class="cat-title">
                                <a href="{{ route('business.index') }}">@lang('footer.business')</a>
                            </li>
                            <li>
                                <a href="/business/create">@lang('footer.sell_business')</a>
                            </li>
                            <li>
                                <a href="{{ route('business.index') }}">@lang('footer.buy_business')</a>
                            </li>
                            <li>
                                <a href="#">@lang('footer.consultation')</a>
                            </li>
                        </ul>
                    </div>
                    --}}
                    <div class="column is-4">
                        <ul>
                            <li class="cat-title">
                                <a href="{{ route('franchise.index') }}">@lang('footer.franchise')</a>
                            </li>
                            <li>
                                <a href="#">@lang('footer.popular_franchises')</a>
                            </li>
                            <li>
                                <a href="#">@lang('footer.no_down_payment')</a>
                            </li>
                            <li>
                                <a href="#">@lang('footer.for_small_city')</a>
                            </li>
                            <li>
                                <a href="#">@lang('footer.royalty_free')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="column is-4">
                        <ul>
                            <li class="cat-title">
                                <a href="/user-services">@lang('menu.user_services')</a>
                            </li>
                            <li>
                                <a href="/user-services?category=3">@lang('menu.services_for_seller')</a>
                            </li>
                            <li>
                                <a href="#">@lang('menu.services_for_buyers')</a>
                            </li>
                            <li>
                                <a href="/user-services?category=5">@lang('menu.services_for_programmer')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="column is-4">
                        <ul>
                            <li class="cat-title">
                                <a href="/about">@lang('footer.about')</a>
                            </li>

                            <li>
                                <a href="/contacts">@lang('menu.news')</a>
                            </li>

                            <li>
                                <a href="/contacts">@lang('menu.about_items.contacts')</a>
                            </li>

                            <li>
                                <a href="/vacancy">@lang('menu.about_items.vacancy')</a>
                            </li>

                            <li>
                                <a href="/help">@lang('menu.about_items.help')</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="columns">
                    <div class="column is-12">
                        <div class="footer__info">
                            @lang('footer.copyright',['site_name' => 'Gradalis Group'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
