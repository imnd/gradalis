<div class="buttons mb-0">
    <a
        @click="services.showModalOrder = true"
        class="button is-link is-size-875 h-3 has-text-weight-bold px-1-5 is-fullwidth">
        @lang('services.single.order')
    </a>
</div>
