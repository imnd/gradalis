@extends('layouts.app')

@section('title', $business->seo_title)

@section('seo-tags')
    <meta name="description" content="{{$business->seo_description}}">
    <meta name="keywords" content="{{$business->seo_keywords}}">
@endsection

@section('head')

    @include('parts.pixels.facebook', ['id' => $business->fb])
    @include('parts.pixels.google', ['id' => $business->google])
    @include('parts.pixels.mail', ['id' => $business->mail])
    @include('parts.pixels.vk', ['id' => $business->vk])

@endsection

@section('content')

    @include('includes.breadcrumb')

    <div class="object">
        <!-- Section object header -->
        <section class="section pt-1 pb-0">
            <div class="container">
                <div class="object__header">
                    <h1 class="section-title">{{$business->name}}</h1>
                    <div class="object__header__info">
                        <ul>
                            <li>ID Объекта: {{$business->id}}</li>
                            <li>{{ $business->created_at ? $business->created_at->formatLocalized('%d %B %Y') : ''}}</li>
                            <li>{{$business->show_count}} просмотров</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section object content -->
        <section class="section py-0">
            <div class="container">
                <div class="object__body object__body_promo box is-paddingless">
                    <div class="object__body__top">
                        <div class="object__slider">
                            <!-- Swiper -->
                            <div class="swiper-container gallery-top swiper-object-main">
                                <div class="swiper-wrapper" id="swiper-object-main-media-gallery">
                                    @foreach($business->getMedia('business') as $image)
                                        <div class="swiper-slide"
                                             style="background-image:url({{$image->getUrl('watermark')}})"
                                             data-src="{{$image->getUrl('watermark')}}"></div>
                                    @endforeach
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                            <div class="swiper-container gallery-thumbs swiper-object-thumbs">
                                <div class="swiper-wrapper">
                                    @foreach($business->getMedia('business') as $image)
                                        <div class="swiper-slide"
                                             style="background-image:url({{$image->getUrl('thumb')}})"></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="object__broker">
                            <div class="object__broker__top is-flex">
                                <div class="object__broker__top__left">
                                    <figure class="object__broker__avatar">
                                        <img
                                            src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg"
                                            alt="">
                                    </figure>
                                </div>
                                <div class="object__broker__top__right">
                                    {{--<div class="price-info">--}}
                                    {{--<div class="price">$18 000 560</div>--}}
                                    {{--</div>--}}

                                    <div class="price-info">
                                        @if($business->discount > 0)
                                            <span class="tag is-danger price-sale">-{{$business->discount}}%</span>
                                            <div
                                                class="price">{{format_price($business->price - $business->price*($business->discount/100))}}
                                            </div>
                                            <span
                                                class="price-old">{{format_price($business->price) }}
                                            </span>
                                        @else
                                            <div
                                                class="price">{{format_price($business->price)}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="object__broker__info is-size-875">
                                        @lang('business.show.broker_object') Дмитрий
                                    </div>
                                </div>
                            </div>
                            <div class="object__broker__bottom is-flex">
                                {{-- TODO временная заглушка --}}
                                <a @click="$store.state.dialogs.expanded = true"
                                   class="button is-success is-size-875 has-text-weight-bold modal-button">
                                    <img src="{{ asset('/svg/icons/ic_ask_question.svg') }}" alt="">
                                    <span>@lang('business.show.ask_object')</span>
                                </a>
                                {{-- <button class="button is-success is-size-875 has-text-weight-bold modal-button"
                                        @click="showModal('modal-question-object')">
                                    <img src="{{ asset('/svg/icons/ic_ask_question.svg') }}" alt="">
                                    <span>Задать вопрос по объекту</span>
                                </button> --}}
                            </div>
                        </div>

                        <span class="info-icon object-promo">
                            <img src="{{ asset('/svg/icons/ic_recommended.svg') }}" alt="Recommended">
                        </span>
                        @if (Auth::check())
                            @if ($business->favorites->isEmpty())
                                <a href="#" @click="toggleFavorite( {{ $business->id }}, 'business')"
                                   class="info-icon object-favorite" v-tooltip="'@lang('business.show.add_to_fav')'">
                                    <img src="{{ asset('/svg/icons/ic_favorites_white.svg') }}"
                                         alt="@lang('business.show.add_to_fav')">
                                </a>
                            @else
                                <a href="#" @click="toggleFavorite( {{$business->id}}, 'business')"
                                   class="info-icon object-favorite active"
                                   v-tooltip="'@lang('business.show.del_from_fav')'">
                                    <img src="{{ asset('/svg/icons/ic_favorites_white.svg') }}"
                                         alt="@lang('business.show.del_from_fav')">
                                </a>
                            @endif
                        @endif

                        {{--<span class="tag is-warning tag-object-sold">Продано</span>
                        <span class="info-icon object-sale">
                        <img src="{{ asset('/svg/icons/ic_sale.svg') }}" alt="Sale">
                        </span>--}}
                    </div>

                    <div class="slider-services">
                        <!-- Swiper -->
                        <div class="swiper-container swiper-object-services">
                            <div class="swiper-wrapper">
                                {{--ToDo: поправить логику заказа услуги в модалке--}}
                                <div class="swiper-slide">
                                    <!-- Example Layout card service -->
                                    <div class="card card-service-2">
                                        <div class="card-service-2__body">
                                            <div class="is-flex has-align-items-center">
                                                <figure class="card-service-2__icon is-flex has-align-items-center">
                                                    <img src="/svg/icons/services/ic_card.svg" alt="">
                                                </figure>
                                                <h4 class="card-service-2__title">Перевод документов</h4>
                                            </div>
                                            <div class="card-service-2__info">
                                                <div class="card-service-2__excerpt is-size-875 has-text-basic">
                                                    Наши специалисты переведут документы на английский и русский
                                                    документы на английский и русский.
                                                </div>
                                                <div class="card-service-2__add-info">
                                                    <div class="card-service-2__region-info is-flex">
                                                        <figure>
                                                            <img
                                                                src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg"
                                                                alt="">
                                                        </figure>
                                                        <div class="card-service-2__region-info__content">
                                                            <span
                                                                class="location is-size-875 is-flex align-items-center"><div>
                                                                    <div class="icon is-flex">
                                                                        <img
                                                                            src="/svg/icons/ic_location.svg"
                                                                            alt="Местоположение">
                                                                    </div>
                                                                    <div class="icon is-flex">
                                                                        <img src="/svg/icons/ic_flag_russian.svg"
                                                                             alt="">
                                                                    </div>
                                                                </div>
                                                                <span class="card-service-2__region-info__name">Россия, г. Санкт-Петербург</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="card-service-2__price is-size-875">
                                                        €150 за документ
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-service-2__footer">
                                            <a class="button is-link is-outlined is-size-875 has-text-weight-bold h-3"
                                               @click="services.showModalOrder = true">Заказать</a>
                                            <a href="/services/single"
                                               class="button is-clear is-clear_close is-size-875 h-3">
                                                <span class="has-text-decoration-underline">Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <!-- Example Layout card service -->
                                    <div class="card card-service-2">
                                        <div class="card-service-2__body">
                                            <div class="is-flex has-align-items-center">
                                                <figure class="card-service-2__icon is-flex has-align-items-center">
                                                    <img src="/svg/icons/services/ic_card.svg" alt="">
                                                </figure>
                                                <h4 class="card-service-2__title">Перевод документов</h4>
                                            </div>
                                            <div class="card-service-2__info">
                                                <div class="card-service-2__excerpt is-size-875 has-text-basic">
                                                    Наши специалисты переведут документы на английский и русский
                                                    документы на английский и русский.
                                                </div>
                                                <div class="card-service-2__add-info">
                                                    <div class="card-service-2__region-info is-flex">
                                                        <figure>
                                                            <img
                                                                src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg"
                                                                alt="">
                                                        </figure>
                                                        <div class="card-service-2__region-info__content">
                                                            <span
                                                                class="location is-size-875 is-flex align-items-center"><div>
                                                                    <div class="icon is-flex">
                                                                        <img
                                                                            src="/svg/icons/ic_location.svg"
                                                                            alt="Местоположение">
                                                                    </div>
                                                                    <div class="icon is-flex">
                                                                        <img src="/svg/icons/ic_flag_russian.svg"
                                                                             alt="">
                                                                    </div>
                                                                </div>
                                                                <span class="card-service-2__region-info__name">Россия, г. Санкт-Петербург</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="card-service-2__price is-size-875">
                                                        €150 за документ
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-service-2__footer">
                                            <a class="button is-link is-outlined is-size-875 has-text-weight-bold h-3"
                                               @click="services.showModalOrder = true">Заказать</a>
                                            <a href="/services/single"
                                               class="button is-clear is-clear_close is-size-875 h-3">
                                                <span class="has-text-decoration-underline">Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <!-- Example Layout card service -->
                                    <div class="card card-service-2">
                                        <div class="card-service-2__body">
                                            <div class="is-flex has-align-items-center">
                                                <figure class="card-service-2__icon is-flex has-align-items-center">
                                                    <img src="/svg/icons/services/ic_card.svg" alt="">
                                                </figure>
                                                <h4 class="card-service-2__title">Перевод документов</h4>
                                            </div>
                                            <div class="card-service-2__info">
                                                <div class="card-service-2__excerpt is-size-875 has-text-basic">
                                                    Наши специалисты переведут документы на английский и русский
                                                    документы на английский и русский.
                                                </div>
                                                <div class="card-service-2__add-info">
                                                    <div class="card-service-2__region-info is-flex">
                                                        <figure>
                                                            <img
                                                                src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg"
                                                                alt="">
                                                        </figure>
                                                        <div class="card-service-2__region-info__content">
                                                            <span
                                                                class="location is-size-875 is-flex align-items-center"><div>
                                                                    <div class="icon is-flex">
                                                                        <img
                                                                            src="/svg/icons/ic_location.svg"
                                                                            alt="Местоположение">
                                                                    </div>
                                                                    <div class="icon is-flex">
                                                                        <img src="/svg/icons/ic_flag_russian.svg"
                                                                             alt="">
                                                                    </div>
                                                                </div>
                                                                <span class="card-service-2__region-info__name">Россия, г. Санкт-Петербург</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="card-service-2__price is-size-875">
                                                        €150 за документ
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-service-2__footer">
                                            <a class="button is-link is-outlined is-size-875 has-text-weight-bold h-3"
                                               @click="services.showModalOrder = true">Заказать</a>
                                            <a href="/services/single"
                                               class="button is-clear is-clear_close is-size-875 h-3">
                                                <span class="has-text-decoration-underline">Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <!-- Example Layout card service -->
                                    <div class="card card-service-2">
                                        <div class="card-service-2__body">
                                            <div class="is-flex has-align-items-center">
                                                <figure class="card-service-2__icon is-flex has-align-items-center">
                                                    <img src="/svg/icons/services/ic_card.svg" alt="">
                                                </figure>
                                                <h4 class="card-service-2__title">Перевод документов</h4>
                                            </div>
                                            <div class="card-service-2__info">
                                                <div class="card-service-2__excerpt is-size-875 has-text-basic">
                                                    Наши специалисты переведут документы на английский и русский
                                                    документы на английский и русский.
                                                </div>
                                                <div class="card-service-2__add-info">
                                                    <div class="card-service-2__region-info is-flex">
                                                        <figure>
                                                            <img
                                                                src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg"
                                                                alt="">
                                                        </figure>
                                                        <div class="card-service-2__region-info__content">
                                                            <span
                                                                class="location is-size-875 is-flex align-items-center"><div>
                                                                    <div class="icon is-flex">
                                                                        <img
                                                                            src="/svg/icons/ic_location.svg"
                                                                            alt="Местоположение">
                                                                    </div>
                                                                    <div class="icon is-flex">
                                                                        <img src="/svg/icons/ic_flag_russian.svg"
                                                                             alt="">
                                                                    </div>
                                                                </div>
                                                                <span class="card-service-2__region-info__name">Россия, г. Санкт-Петербург</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="card-service-2__price is-size-875">
                                                        €150 за документ
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-service-2__footer">
                                            <a class="button is-link is-outlined is-size-875 has-text-weight-bold h-3"
                                               @click="services.showModalOrder = true">Заказать</a>
                                            <a href="/services/single"
                                               class="button is-clear is-clear_close is-size-875 h-3">
                                                <span class="has-text-decoration-underline">Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <!-- Example Layout card service -->
                                    <div class="card card-service-2">
                                        <div class="card-service-2__body">
                                            <div class="is-flex has-align-items-center">
                                                <figure class="card-service-2__icon is-flex has-align-items-center">
                                                    <img src="/svg/icons/services/ic_card.svg" alt="">
                                                </figure>
                                                <h4 class="card-service-2__title">Перевод документов</h4>
                                            </div>
                                            <div class="card-service-2__info">
                                                <div class="card-service-2__excerpt is-size-875 has-text-basic">
                                                    Наши специалисты переведут документы на английский и русский
                                                    документы на английский и русский.
                                                </div>
                                                <div class="card-service-2__add-info">
                                                    <div class="card-service-2__region-info is-flex">
                                                        <figure>
                                                            <img
                                                                src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg"
                                                                alt="">
                                                        </figure>
                                                        <div class="card-service-2__region-info__content">
                                                            <span
                                                                class="location is-size-875 is-flex align-items-center"><div>
                                                                    <div class="icon is-flex">
                                                                        <img
                                                                            src="/svg/icons/ic_location.svg"
                                                                            alt="Местоположение">
                                                                    </div>
                                                                    <div class="icon is-flex">
                                                                        <img src="/svg/icons/ic_flag_russian.svg"
                                                                             alt="">
                                                                    </div>
                                                                </div>
                                                                <span class="card-service-2__region-info__name">Россия, г. Санкт-Петербург</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="card-service-2__price is-size-875">
                                                        €150 за документ
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-service-2__footer">
                                            <a class="button is-link is-outlined is-size-875 has-text-weight-bold h-3"
                                               @click="services.showModalOrder = true">Заказать</a>
                                            <a href="/services/single"
                                               class="button is-clear is-clear_close is-size-875 h-3">
                                                <span class="has-text-decoration-underline">Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <!-- Example Layout card service -->
                                    <div class="card card-service-2">
                                        <div class="card-service-2__body">
                                            <div class="is-flex has-align-items-center">
                                                <figure class="card-service-2__icon is-flex has-align-items-center">
                                                    <img src="/svg/icons/services/ic_card.svg" alt="">
                                                </figure>
                                                <h4 class="card-service-2__title">Перевод документов</h4>
                                            </div>
                                            <div class="card-service-2__info">
                                                <div class="card-service-2__excerpt is-size-875 has-text-basic">
                                                    Наши специалисты переведут документы на английский и русский
                                                    документы на английский и русский.
                                                </div>
                                                <div class="card-service-2__add-info">
                                                    <div class="card-service-2__region-info is-flex">
                                                        <figure>
                                                            <img
                                                                src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg"
                                                                alt="">
                                                        </figure>
                                                        <div class="card-service-2__region-info__content">
                                                            <span
                                                                class="location is-size-875 is-flex align-items-center"><div>
                                                                    <div class="icon is-flex">
                                                                        <img
                                                                            src="/svg/icons/ic_location.svg"
                                                                            alt="Местоположение">
                                                                    </div>
                                                                    <div class="icon is-flex">
                                                                        <img src="/svg/icons/ic_flag_russian.svg"
                                                                             alt="">
                                                                    </div>
                                                                </div>
                                                                <span class="card-service-2__region-info__name">Россия, г. Санкт-Петербург</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="card-service-2__price is-size-875">
                                                        €150 за документ
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-service-2__footer">
                                            <a class="button is-link is-outlined is-size-875 has-text-weight-bold h-3"
                                               @click="services.showModalOrder = true">Заказать</a>
                                            <a href="/services/single"
                                               class="button is-clear is-clear_close is-size-875 h-3">
                                                <span class="has-text-decoration-underline">Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <!-- Example Layout card service -->
                                    <div class="card card-service-2">
                                        <div class="card-service-2__body">
                                            <div class="is-flex has-align-items-center">
                                                <figure class="card-service-2__icon is-flex has-align-items-center">
                                                    <img src="/svg/icons/services/ic_card.svg" alt="">
                                                </figure>
                                                <h4 class="card-service-2__title">Перевод документов</h4>
                                            </div>
                                            <div class="card-service-2__info">
                                                <div class="card-service-2__excerpt is-size-875 has-text-basic">
                                                    Наши специалисты переведут документы на английский и русский
                                                    документы на английский и русский.
                                                </div>
                                                <div class="card-service-2__add-info">
                                                    <div class="card-service-2__region-info is-flex">
                                                        <figure>
                                                            <img
                                                                src="https://image.freepik.com/free-photo/no-translate-detected_23-2147650966.jpg"
                                                                alt="">
                                                        </figure>
                                                        <div class="card-service-2__region-info__content">
                                                            <span
                                                                class="location is-size-875 is-flex align-items-center"><div>
                                                                    <div class="icon is-flex">
                                                                        <img
                                                                            src="/svg/icons/ic_location.svg"
                                                                            alt="Местоположение">
                                                                    </div>
                                                                    <div class="icon is-flex">
                                                                        <img src="/svg/icons/ic_flag_russian.svg"
                                                                             alt="">
                                                                    </div>
                                                                </div>
                                                                <span class="card-service-2__region-info__name">Россия, г. Санкт-Петербург</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="card-service-2__price is-size-875">
                                                        €150 за документ
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-service-2__footer">
                                            <a class="button is-link is-outlined is-size-875 has-text-weight-bold h-3"
                                               @click="services.showModalOrder = true">Заказать</a>
                                            <a href="/services/single"
                                               class="button is-clear is-clear_close is-size-875 h-3">
                                                <span class="has-text-decoration-underline">Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next swiper-object-services-next"></div>
                        <div class="swiper-button-prev swiper-object-services-prev"></div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>

                    <div class="p-1-5">
                        <div class="columns is-multiline is-flex">
                            <div class="column is-8-desktop is-12-tablet is-order-1-touch">
                                <div class="content">
                                    <h2>@lang('business.show.obj_desc')</h2>
                                    <div class="object__description">
                                        {!! $business->description !!}
                                    </div>
                                    <div class="object__info-wrap">
                                        <button
                                            class="button is-info is-outlined is-size-875 has-text-weight-bold object__info__toggle h-3"
                                            @click="object.showDetailedInformation = !object.showDetailedInformation"
                                            v-show="!object.showDetailedInformation"
                                        >
                                            @lang('business.show.btn_view_more')
                                        </button>

                                        <div class="object__info" v-if="object.showDetailedInformation">
                                            {{--@foreach($business->options as $key => $step)
                                            <div class="object__info__group">
                                            <h3 class="object__info__title">
                                            <img src="@lang('business.'.$key.'.icon')" alt="Info"
                                            class="icon">
                                            <span>@lang('business.'.$key.'.title')</span>
                                            </h3>
                                            {!! App\Services\Helpers::stepsGenerator($step)!!}
                                            </div>
                                            @endforeach--}}

                                            <bulma-accordion
                                                :slide="{duration: '.3s',timerFunc: 'ease'}"
                                                :dropdown="true"
                                                class="px-0">
                                                @if ($business->options)
                                                    @foreach ($business->options as $key => $step)
                                                        <bulma-accordion-item>
                                                            <h2 slot="title" class="mb-0 is-flex has-align-items-center">
                                                                <img
                                                                    src="@lang('business.'.$key.'.icon')"
                                                                    alt=""
                                                                    class="icon mr-0-5">
                                                                <span>@lang('business.'.$key.'.title')</span>
                                                            </h2>
                                                            <div slot="content" class="pb-1">
                                                                {!! steps_generator($step)!!}
                                                                {{--=======--}}
                                                                {{--<div class="object__info" v-show="object.showDetailedInformation">
                                                                <div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_info.svg') }}" alt="@lang('business.show.general_info')"
                                                                class="icon">
                                                                <span>@lang('business.show.general_info')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.reason_for_sale')</span>
                                                                <span>Переход в другую сферу интересов</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.name_company')</span>
                                                                <span>Invest.pl</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.activity_profile')</span>
                                                                <span>Венчурные инвестиции</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.address')</span>
                                                                <span>ul. Dluga 52, Warsaw 00-241, Poland </span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.zip_code')</span>
                                                                <span>022245</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.number_of_employees')</span>
                                                                <span>3</span>
                                                                <div class="object__info__list__item__subgroup">
                                                                <div class="object__info__list__item">
                                                                <span>Менеджер по продажам</span>
                                                                <span>€ 1 300</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>Директор по персоналу</span>
                                                                <span>€ 2 300</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>Руководитель направления</span>
                                                                <span>€ 3 300</span>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_target_audience.svg') }}"
                                                                alt="@lang('business.show.target_audience')"
                                                                class="icon">
                                                                <span>@lang('business.show.target_audience')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.target_audience')</span>
                                                                <span>Мужчины и женщины с высоким доходом</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.average_age_customers')</span>
                                                                <span>28-48</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.average_check')</span>
                                                                <span>€ 10 000</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.cost_of_attraction')</span>
                                                                <span>€ 1 000</span>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_object_finance.svg') }}"
                                                                alt="@lang('business.show.financial_indicators')"
                                                                class="icon">
                                                                <span>@lang('business.show.financial_indicators')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.revolutions_per_month')</span>
                                                                <span>€ 20 000</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>@lang('business.show.monthly_expenses')</span>
                                                                <span>€ 10 000</span>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_law_form.svg') }}" alt="@lang('business.show.legal_state')"
                                                                class="icon">
                                                                <span>@lang('business.show.legal_state')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_judge.svg') }}" alt=">@lang('business.show.legal_status_company')"
                                                                class="icon">
                                                                <span>@lang('business.show.legal_status_company')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_process.svg') }}"
                                                                alt="@lang('business.show.business_processes')"
                                                                class="icon">
                                                                <span>@lang('business.show.business_processes')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_globe.svg') }}" alt="@lang('business.show.intangible_assets')" class="icon">
                                                                <span>@lang('business.show.intangible_assets')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_money.svg') }}" alt="@lang('business.show.tangible_assets')" class="icon">
                                                                <span>@lang('business.show.tangible_assets')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="object__info__group">
                                                                <h3 class="object__info__title">
                                                                <img src="{{ asset('/svg/icons/ic_object_building.svg') }}"
                                                                alt="@lang('business.show.real_estate')"
                                                                class="icon">
                                                                <span>@lang('business.show.real_estate')</span>
                                                                </h3>
                                                                <div class="object__info__list">
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                </div>
                                                                <div class="object__info__list__item">
                                                                <span>Title</span>
                                                                <span>Value</span>
                                                                >>>>>>> translation-2--}}
                                                            </div>
                                                        </bulma-accordion-item>
                                                        <hr class="hr-basic is-marginless">
                                                    @endforeach
                                                @endif
                                            </bulma-accordion>
                                            <button
                                                class="button is-info is-outlined is-size-875 has-text-weight-bold object__info__toggle h-3"
                                                @click="object.showDetailedInformation = !object.showDetailedInformation"
                                            >
                                                @lang('business.show.btn_hide_more_info')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column is-4-desktop is-12-tablet">
                                <div class="object__basic-short-info">
                                    <div class="list-properties">
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.country')</span>
                                            <span>{{ $business->country->translation }}</span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.city')</span>
                                            <span>{{ $business->city->translation }}</span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.category')</span>
                                            <span>{{ $business->category->translation }}</span>
                                        </div>
                                        <hr>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.payback')</span>
                                            <span><b>{{ $business->payback }} @lang('business.show.months')</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.profit')</span>
                                            <span><b>{{ format_price($business->profitability) }}</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.net_profit')</span>
                                            <span><b>{{ format_price($business->revenue) }}</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.perc_profit')</span>
                                            <span><b>{{$business->profit}}%</b></span>
                                        </div>
                                        <hr>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.price_by_eur')</span>
                                            <span><b>{{ format_price($business->price - $business->price*($business->discount/100),true,'EUR') }}</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.price_by_gold')</span>
                                            <span><b>{{ format_price($business->price - $business->price*($business->discount/100),true,'PLN')  }}</b></span>
                                        </div>
                                        <div class="list-properties__item">
                                            <span>@lang('business.show.price_by_btc')</span>
                                            <span><b>{{format_price($business->price - $business->price*($business->discount/100),true,'BTC')}}</b></span>
                                        </div>
                                    </div>
                                    <div class="buttons">
                                        <a href="#"
                                           @click="showModal('modal-ask-documents')"
                                           class="button is-success has-text-weight-bold is-size-875 h-3">@lang('business.show.get_docs')</a>
                                        @if($business->status != 5)
                                            <reserve-button :id="{{$business->id}}"
                                                            :url="'/business/reserve'"/>
                                        @endif
                                    </div>
                                </div>
                                <div class="has-text-centered object__how-purchase">
                                    <a class="is-size-875 is-info" @click="showModal('modal-how-purchase')">
                                        <img src="{{ asset('/svg/icons/ic_lifesaver.svg') }}"
                                             alt="">
                                        <span> @lang('business.show.how_purchase')</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Section similar offers -->
        <section class="section">
            <div class="container">
                <h2 class="title section-title"> @lang('business.show.similar_offers')</h2>
                <div class="columns is-multiline">
                    <!-- ToDo: выводить 3 штуки -->
                    @include('parts.cards-object')
                </div>
            </div>
        </section>
    </div>

    <!-- Modal -->
    <div class="modal" id="modal-question-object">
        <div class="modal-background" @click="hideModal('modal-question-object')"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">Modal title</p>
                <button class="delete close-modal" @click="hideModal('modal-question-object')"
                        aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <!-- Content ... -->
            </section>
            <footer class="modal-card-foot">
                <button class="button is-success">Save changes</button>
                <button class="button">Cancel</button>
            </footer>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal" id="modal-ask-documents">
        <div class="modal-background" @click="hideModal('modal-ask-documents')"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title"> @lang('business.show.your_req_accept')</p>
                <button class="delete close-modal" @click="hideModal('modal-ask-documents')"
                        aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <p> @lang('business.show.your_req_sended')</p>
            </section>
            <footer class="modal-card-foot">
                <button @click="hideModal('modal-ask-documents')"
                        class="button is-success">@lang('business.show.close')</button>
            </footer>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal" id="modal-how-purchase">
        <div class="modal-background" @click="hideModal('modal-how-purchase')"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">@lang('business.show.how_purchase_2')</p>
                <button class="delete close-modal" @click="hideModal('modal-how-purchase')"
                        aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div class="content">
                    <p>
                        @lang('business.show.how_purchase_title')
                    </p>
                    <ol class="ol-big-counter">
                        <li>@lang('business.show.how_purchase_step1')</li>
                        <li>@lang('business.show.how_purchase_step2')</li>
                        <li>@lang('business.show.how_purchase_step3')</li>
                        <li>@lang('business.show.how_purchase_step4')</li>
                        <li>@lang('business.show.how_purchase_step5')</li>
                        <li>@lang('business.show.how_purchase_step6')</li>
                        <li>@lang('business.show.how_purchase_step7')</li>
                    </ol>
                </div>
            </section>
            <footer class="modal-card-foot">
                <a href="#" class="button is-success is-fullwidth h-3">Оставить заявку</a>
            </footer>
        </div>
    </div>

    {{--ToDo: Временно --}}
    <!--modal-->
    <order-service-modal
        :show="services.showModalOrder"
        @close="services.showModalOrder = false;"
        @success="services.showModalOrder = false; services.showModalOrderSuccess = true;"/>
    <order-service-modal-success
        :show="services.showModalOrderSuccess"
        @close="services.showModalOrderSuccess = false"/>
@endsection
