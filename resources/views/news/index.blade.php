@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <news-index news-category="{{$newsCategory}}"></news-index>
@endsection
