@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <div class="services-single">
        @include('users-services.parts.header')

        <section class="section">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-8">
                        <div class="box">
                            <div class="content">
                                <h1 class="mb-2">@lang('services.single.team_professionals')</h1>
                                @foreach ($workers as $worker)
                                    <article>
                                        <div class="columns is-multiline">
                                            <div class="column is-5">
                                                <figure class="is-marginless h-full">
                                                    <img src="{{ $worker->photoUrl }}" alt="" class="has-border-radius img-full-cover">
                                                </figure>
                                            </div>
                                            <div class="column is-7">
                                                <h3 class="mb-0">{{ $worker->name }}</h3>
                                                <div class="is-size-875 has-text-basic mb-0-5">
                                                    {{ $worker->position }}
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                @endforeach
                            </div>
                        </div>
                        @include('users-services.parts.bottom')
                    </div>
                    <div class="column is-4">
                        @include('users-services.parts.aside')
                    </div>
                </div>
            </div>
        </section>
        @include('users-services.parts.similar')
    </div>
@endsection
