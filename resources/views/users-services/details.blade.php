@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <div class="services-single">
        @include('users-services.parts.header')

        <section class="section">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-8">
                        <div class="box">
                            <div class="content">
                                {!! $service->details !!}
                            </div>
                        </div>
                        @include('users-services.parts.bottom')
                    </div>
                    <div class="column is-4">
                        @include('users-services.parts.aside')
                    </div>
                </div>
            </div>
        </section>
        @include('users-services.parts.similar')
    </div>
@endsection
