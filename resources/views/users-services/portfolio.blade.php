@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <div class="services-single">
        @include('users-services.parts.header')

        <section class="section">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-8">
                        <div class="box">
                            <div class="content">
                                <h1 class="mb-2">@lang('services.single.portfolio_our_works')</h1>

                                <div class="services-single__list-case">
                                    @foreach ($portfolio as $item)
                                        <article class="services-single__list-case__item">
                                            <div class="columns is-multiline">
                                                <div class="column is-5">
                                                    <figure class="is-marginless h-full">
                                                        <img src="{{ $item->photoUrl }}" class="has-border-radius img-full-cover">
                                                    </figure>
                                                </div>
                                                <div class="column is-12">
                                                    {{ $item->description }}
                                                </div>
                                            </div>
                                        </article>
                                        <hr class="is-info" />
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @include('users-services.parts.bottom')
                    </div>
                    <div class="column is-4">
                        @include('users-services.parts.aside')
                    </div>
                </div>
            </div>
        </section>
        @include('users-services.parts.similar')
    </div>
@endsection
