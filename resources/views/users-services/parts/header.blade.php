<section class="section section-services-single-header" style="background-image: url({{ $service->photoUrl }}">
    <div class="container">
        <div class="columns is-multiline">
            <div class="column is-8">
                <div class="section-services-single-header__title is-flex has-align-items-center">
                    <figure>
                        <img src="/svg/icons/ic_business_finder.svg" />
                    </figure>
                    <h1 class="has-text-white has-text-weight-bold mb-1-75">
                        {{ $service->name }}
                    </h1>
                </div>
                <div class="section-services-single-header__desc is-size-875 p-1-5">
                    {{ $service->description }}
                </div>
                <nav class="section-services-single-header__nav">
                    <ul class="buttons has-addons is-size-875 is-flex is-marginless">
                        @foreach ([
                            'show',
                            'processes',
                            'portfolio',
                            'workers',
                            'details',
                        ] as $action)
                            <?php $url = "/user-services/$action/{$service->id}"?>
                            <li>
                                <a href="{{ $url }}" class="{{ '/' . request()->path() === $url ? 'is-active' : '' }}">
                                    @lang("services.single.$action")
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
            <div class="column is-4">
                <div class="is-flex h-full has-align-items-end">
                    <div class="section-services-single-header_price-info-wrap p-1-5">
                        @include('users-services.parts.price-info')
                        @include('users-services.parts.btn-order')
                        <div class="is-size-875">
                            <i>@lang('services.single.leave_request')</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<order-service-modal
    :show="services.showModalOrder"
    @close="services.showModalOrder = false;"
    @success="services.showModalOrder = false; services.showModalOrderSuccess = true;">
</order-service-modal>

<order-service-modal-success
    :show="services.showModalOrderSuccess"
    @close="services.showModalOrderSuccess = false">
</order-service-modal-success>
