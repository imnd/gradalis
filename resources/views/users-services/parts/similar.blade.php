<section class="section has-background-white">
    <div class="container">
        <h2 class="mb-1">@lang('services.single.related_products')</h2>

        <div class="columns is-multiline">
            <div class="column is-4-desktop is-6-tablet">
                @include('users-services.parts.card')
            </div>
            <div class="column is-4-desktop is-6-tablet">
                @include('users-services.parts.card')
            </div>
            <div class="column is-4-desktop is-6-tablet">
                @include('users-services.parts.card')
            </div>
        </div>
    </div>
</section>
