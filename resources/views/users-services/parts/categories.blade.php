<div class="container">
    <div class="category-service-list">
        <div class="columns is-multiline is-gapless">
            @foreach ($categories as $category)
                <div class="column is-2-desktop is-4-tablet category-service-list__item-wrap">
                    <a
                        href="?category={{ $category->id }}"
                        class="category-service-list__item is-flex {{ isset($categoryId) && $categoryId === $category->id ? 'is-active' : '' }}">
                        <figure class="category-service-list__item__icon">
                            <img
                                src="{{ $category->iconUrl }}"
                                class="svg"
                                alt="{{ trans($category->name) }}"
                            />
                        </figure>
                        <div class="category-service-list__item__title is-size-875">
                            {{ trans($category->name) }}
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
