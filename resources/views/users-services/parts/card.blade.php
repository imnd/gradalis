<div class="card card-service-2">
    <div class="card-service-2__body">
        <div class="is-flex has-align-items-center">
            <figure class="card-service-2__icon is-flex has-align-items-center">
                <img src="{{ $service->iconUrl }}" alt="" />
            </figure>
            <h4 class="card-service-2__title">
                {{ $service->name }}
            </h4>
        </div>
        <div class="card-service-2__info">
            <div class="card-service-2__excerpt is-size-875 has-text-basic">
                {{ $service->description }}
            </div>
            <div class="card-service-2__add-info">
                <div class="card-service-2__region-info is-flex">
                    <figure>
                        <img
                            src="{{ $service->performerAvatarUrl }}"
                            alt="avatar"
                        />
                    </figure>
                    <div class="card-service-2__region-info__content">
                       <span class="location is-size-875 is-flex align-items-center">
                           <div>
                                <div class="icon is-flex">
                                    <img
                                        src="{{ asset('/svg/icons/ic_location.svg') }}"
                                        alt="@lang('strings.location')"
                                    />
                                </div>
                                <div class="icon is-flex">
                                    <img src="/svg/icons/ic_flag_{{  $service->countryIcon }}.svg" />
                                </div>
                            </div>
                            <div>
                               <div class="card-service-2__region-info__name">г. {{ $service->cityName }}</div>
                               <div class="card-service-2__region-info__name">{{ $service->countryName }}</div>
                            </div>
                      </span>
                    </div>
                </div>
                <div class="card-service-2__price is-size-875">
                    €{{ $service->price }}
                </div>
            </div>
        </div>
    </div>
    <div class="card-service-2__footer">
        @auth
            <a
                class="button is-link is-outlined is-size-875 has-text-weight-bold h-3"
                @click="userServices.showModalOrder = true"
            >
                @lang('strings.order')
            </a>
        @endauth
        <a
            class="button is-clear is-clear_close is-size-875 h-3"
            href="/user-services/show/{{ $service->id }}"
        >
            <span class="has-text-decoration-underline">
                @lang('strings.read_more')
            </span>
        </a>
    </div>
</div>
