<div class="columns is-multiline is-desktop">
    <div class="column is-narrow-desktop">
        <?php $url = "/user-services/view/{$service->id}"?>
        <a href="{{ $url }}" class="button is-info is-outlined is-size-875 h-3 px-1 is-fullwidth {{ request()->is($url) ? 'is-active' : '' }}">
            <span>@lang("services.single.show")</span>
        </a>
    </div>
    <div class="column">
        <div class="buttons has-addons is-size-875 is-flex is-marginless has-justify-content-between is-flex-1">
            @foreach ([
                'processes',
                'portfolio',
                'workers',
                'details',
            ] as $action)
                <?php $url = "/user-services/$action/{$service->id}"?>
                <a href="{{ $url }}" class="button is-info is-outlined is-size-875 h-3 is-flex-1 {{ '/' . request()->path() === $url ? 'is-active' : '' }}">@lang("services.single.$action")</a>
            @endforeach
        </div>
    </div>
</div>
