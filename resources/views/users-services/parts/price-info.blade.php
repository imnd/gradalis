<div class="is-size-875 mb-0-5">
    @lang('services.single.price')
</div>
<div class="price-info">
    <span class="tag is-danger price-sale">-{{ $service->discount }}%</span>
    <div class="price">
        €{{ $service->discountPrice }} за {{ $service->price_for }}
    </div>
    <span class="price-old">
        €{{ $service->price }}
    </span>
</div>
