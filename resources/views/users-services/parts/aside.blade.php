<aside class="h-full" data-sticky-container>
    <div>
        <div class="card card-object card-object_promo">
            <div class="card-image">
                <figure class="image is-16by9">
                    <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg" alt="Placeholder image">
                </figure>
                <span class="tag tag-payback">
                <span class="name">@lang('business.v_list.payback')</span>
                <span class="term">38 @lang('business.v_list.months')</span>
            </span>
                <div class="info-icons-wrap info-icons-wrap_left">
                    <span class="info-icon object-promo">
                        <img src="{{ asset('/svg/icons/ic_recommended.svg') }}"
                             alt="@lang('business.v_list.recommended')">
                    </span>
                </div>
                <div class="info-icons-wrap info-icons-wrap_right">
                    <a href="#" class="info-icon object-favorite"
                       v-tooltip="'@lang('business.v_list.add_to_favorites')'">
                        <img src="{{ asset('/svg/icons/ic_favorites_white.svg') }}"
                             alt="@lang('business.v_list.favorites')">
                    </a>
                </div>

            </div>
            <div class="card-content">
                <div class="card-object__header">
                    <h3 class="title">Мини-отель в собственность с видом на Невский проспект</h3>
                    <p class="location">
                        <img src="{{ asset('/svg/icons/ic_location.svg') }}" alt="@lang('business.v_list.location')">
                        <span>Россия, г. Санкт-Петербург</span>
                    </p>
                </div>

                <div class="content card-object__content">
                    <div class="list-properties">
                        <div class="list-properties__item">
                            <span>@lang('business.v_list.category')</span>
                            <span>Коммерческая</span>
                        </div>
                        <div class="list-properties__item">
                            <span>@lang('business.v_list.profit')</span>
                            <span><b>$48 000</b></span>
                        </div>
                        <div class="list-properties__item">
                            <span>@lang('business.v_list.net_profit')</span>
                            <span><b>$23 000</b></span>
                        </div>
                        <div class="list-properties__item">
                            <span>@lang('business.v_list.perc_profit')</span>
                            <span><b>78%</b></span>
                        </div>
                    </div>
                </div>
                <div class="card-object__footer">
                    <div class="price-info">
                        <div class="price">$18 000 560</div>
                    </div>
                    <a class="button is-success is-outlined is-fullwidth has-text-weight-bold">@lang('business.v_list.business_details_link')</a>
                </div>
            </div>
        </div>
    </div>
    <div class="pt-1-75 sticky">
        <div class="box">
            @include('users-services.parts.price-info')
            @include('users-services.parts.btn-order')
            <div class="is-size-875">
                <i>@lang('services.single.leave_request')</i>
            </div>
        </div>
    </div>
</aside>
