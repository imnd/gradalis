@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <div class="services-index">
        <section class="section section-filter-services is-flex">
            <div class="container is-flex-1">
                <div
                    class="section-filter-services__title has-text-white has-text-weight-bold has-text-centered mb-1-75">
                    @lang('services.index.title')
                </div>
                <services-filter />
                {{--<services-filter
                    :default_category="{{ $categoryId }}"
                    :default_country="{{ $country }}"
                    :default_region="{{ $region }}"
                    :default_city="{{ $city }}"
                    :default_term="{{ $term }}"
                />--}}
            </div>
        </section>

        <section class="section pt-1 pb-1 services-index__section-list-category">
            @include('users-services.parts.categories')
        </section>

        <div class="category-service-list-wrap">
            <section class="section pt-1 pb-1">
                <div class="container">
                    <h1 class="section-title is-flex section-title_category-service">
                        <figure class="section-title_category-service__icon">
                            <img
                                src="/svg/icons/services/category/ic_shopping-cart.svg"
                                class="svg"
                                alt="@lang('services.index.catalog_title')"
                            />
                        </figure>
                        <span>@lang('services.index.catalog_title')</span>
                    </h1>
                    <hr class="hr-basic is-marginless" />
                </div>
            </section>

            <section class="section pt-1 pb-1">
                <div class="container">
                    <div class="columns is-multiline">
                        @foreach ($services as $service)
                            <div class="column is-4-desktop is-6-tablet">
                                @include('users-services.parts.card')
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>

        <section class="section py-1">
            <div class="container">
                <?=$services->render()?>
            </div>
        </section>
    </div>

    <order-user-service-modal
        :show="userServices.showModalOrder"
        @close="userServices.showModalOrder = false;"
        @success="userServices.showModalOrder = false; userServices.showModalOrderSuccess = true;"
    />
    <order-service-modal-success
        :show="userServices.showModalOrderSuccess"
        @close="userServices.showModalOrderSuccess = false"
    />

@endsection
