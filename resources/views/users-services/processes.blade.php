@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <div class="services-single">
        @include('users-services.parts.header')

        <section class="section">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-8">
                        <div class="box">
                            <div class="content">
                                <h1 class="mb-2">
                                    Процесс
                                </h1>
                                @foreach ($processes as $i => $process)
                                <div class="mt-1 mb-2">
                                    <div class="timeline-process mb-1 mt-1">
                                        <div class="timeline-process__item">
                                            <div class="timeline-process__item__tag">
                                            <span
                                                class="tag is-danger has-text-uppercase has-text-weight-bold is-rounded">Шаг {{ $i + 1 }}</span>
                                            </div>
                                            <div class="timeline-process__item__marker has-background-white">
                                                <img src="{{ $process->iconUrl }}" alt="">
                                            </div>
                                            <div class="timeline-process__item__content">
                                                <h3 class="mb-0">{{ $process->name }}</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-1">
                                        {!! $process->description !!}
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @include('users-services.parts.bottom')
                    </div>
                    <div class="column is-4">
                        @include('users-services.parts.aside')
                    </div>
                </div>
            </div>
        </section>
        @include('users-services.parts.similar')
    </div>
@endsection
