@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <div class="services-single">
        @include('users-services.parts.header')
        <section class="section has-background-white">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-6">
                        <div class="content">
                            <h2 class="mb-1">
                                @lang('services.single.annotated')
                            </h2>
                            <div class="is-size-875">
                                {{ $service->annotation }}
                            </div>
                            <div class="mt-2-5">
                                @include('users-services.parts.price-info')
                                    @include('users-services.parts.btn-order')
                            </div>
                        </div>
                    </div>
                    <div class="column is-6">
                        <figure class="is-marginless h-full w-full">
                            <img
                                src="{{ $service->annotationImgUrl }}"
                                class="img-full-cover has-border-radius"
                            />
                        </figure>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-6">
                        <div class="content">
                            <h2 class="mb-1">
                                @lang('services.single.processes')
                            </h2>
                            <div class="timeline-process">
                                @foreach ($service->processes as $process)
                                <div class="timeline-process__item">
                                    <div class="timeline-process__item__tag">
                                        <span class="tag is-danger has-text-uppercase has-text-weight-bold is-rounded">{{ $process->name }}</span>
                                    </div>
                                    <div class="timeline-process__item__marker">
                                        <img src="{{ $process->iconUrl }}" alt="">
                                    </div>
                                    <div class="timeline-process__item__content">
                                        {!! $process->description !!}
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="has-text-centered mt-1">
                                <a href="/user-services/single/process" class="has-text-decoration-underline is-size-875">
                                    @lang('services.single.about_process')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="column is-6">
                        <div class="our-team">
                            <h2 class="mb-1">@lang('services.single.team')</h2>
                            <div class="columns is-multiline">
                                @foreach ($service->workers as $worker)
                                <div class="column is-6">
                                    <div class="our-team__item">
                                        <figure class="mb-0-5">
                                            <img
                                                src="{{ $worker->photoUrl }}"
                                                alt="">
                                        </figure>
                                        <h3>{{ $worker->name }}</h3>
                                        <div class="team__item__position is-size-875 has-text-basic">
                                            {{ $worker->position }}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="column is-6">
                                    <div class="is-flex has-align-items-center has-justify-content-center h-full">
                                        <a href="/services-new/single/team" class="button is-info is-size-875 has-text-weight-bold h-3 px-1">
                                            @lang('services.single.about_more_team')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section has-background-white">
            <div class="container">
                <h2 class="mb-1">@lang('services.single.promo_video')</h2>
                <embed
                    class="w-full has-border-radius has-box-shadow mb-2"
                    width="420"
                    height="315"
                    data="{{ $service->video_url }}">
                </embed>
                <div class="box">
                    <div class="columns is-multiline">
                        <div class="column is-8">
                            <div class="content">
                                <p>{{ $service->video_description }}</p>
                            </div>
                        </div>
                        <div class="column is-4">
                            <div>
                                @include('users-services.parts.price-info')
                                @include('users-services.parts.btn-order')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{--<section class="section">
            <div class="container">
                <h2 class="has-text-centered mb-1">@lang('services.single.our_cases')</h2>
                <div class="columns is-multiline">
                    <div class="column is-4">
                        <a href="#" class="services-single__case-card is-block">
                            <div class="card has-border-radius card-case">
                                <div class="card-image">
                                    <figure class="image is-16by9">
                                        <img src="http://f-lounge.ru/wp-content/uploads/2014/07/IMG_6127.jpg" class="has-border-radius_top">
                                    </figure>
                                </div>
                                <div class="card-content">
                                    <div class="content">
                                        OZON полностью перевел штаб-квартиру в «Москва-Сити»
                                    </div>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="column is-4">
                        <a href="#" class="services-single__case-card is-block">
                            <div class="card has-border-radius card-case">
                                <div class="card-image">
                                    <figure class="image is-16by9">
                                        <img src="http://f-lounge.ru/wp-content/uploads/2014/07/IMG_6127.jpg"
                                             class="has-border-radius_top">
                                    </figure>
                                </div>
                                <div class="card-content">
                                    <div class="content">
                                        OZON полностью перевел штаб-квартиру в «Москва-Сити»
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="column is-4">
                        <a href="#" class="services-single__case-card is-block">
                            <div class="card has-border-radius card-case">
                                <div class="card-image">
                                    <figure class="image is-16by9">
                                        <img src="http://f-lounge.ru/wp-content/uploads/2014/07/IMG_6127.jpg"
                                             class="has-border-radius_top">
                                    </figure>
                                </div>
                                <div class="card-content">
                                    <div class="content">
                                        OZON полностью перевел штаб-квартиру в «Москва-Сити»
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="column is-12">
                        <div class="buttons is-marginless has-justify-content-center">
                            <a href="/services-new/single/cases" class="button is-info is-outlined h-3 has-text-weight-bold is-size-875 px-1">@lang('services.single.btn_view_all_cases')</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>--}}

        @include('users-services.parts.similar')

    </div>

    <order-user-service-modal
        :show="userServices.showModalOrder"
        @close="userServices.showModalOrder = false;"
        @success="userServices.showModalOrder = false; userServices.showModalOrderSuccess = true;"
    />

@endsection

