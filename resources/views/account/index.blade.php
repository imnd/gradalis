@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <section class="section py-0">
        <div class="container">
            <account></account>
        </div>
    </section>

@endsection
