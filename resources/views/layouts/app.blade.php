<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js-inlinesvg">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="domain" content="{{ config('app.domain') }}">

    @yield('seo-tags')

    <title>@yield('title', config('app.name', 'Laravel'))</title>

    <!-- Scripts -->
    <script src="/js/global_variables.js"></script>
    <script src="/js/user.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">--}}

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @yield('head')
</head>
{{--<body oncopy="return false;">--}}
<body>
    <div id="app">
        @include('includes.header')

        <main class="main is-flex">
            @include('includes.messages')
            @yield('content')
        </main>

        @auth
            <chat-widget></chat-widget>
        @endauth

        @include('includes.footer')
    </div>
</body>
<script>
    window.Laravel.csrfToken = "{!! csrf_token() !!}";
    window.Laravel.apiToken = "{!! $user->api_token ?? null !!}";
</script>
</html>
