@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <section class="section pt-1 pb-0">
        <div class="container">
            <h1 class="section-title mb-0">@lang('vacancy.title')</h1>
        </div>
    </section>

    <section class="section pt-1 pb-1">
        <div class="container">
            @forelse  ($vacancies as $vacancy)
                <div class="vacancy-list">
                <article class="vacancy mb-1">
                        <h2 class="vacancy__title mb-1"><a href="/vacancy/{{$vacancy->url}}">{{$vacancy->name}}</a></h2>
                        @if($vacancy->city)
                            <div class="vacancy__location">
                                <img src="{{ asset('/svg/icons/ic_location.svg') }}">
                                <span>{{$vacancy->city->region->country->name}}, {{$vacancy->city->name}}</span>
                            </div>
                        @endif
                        <div class="vacancy__short-info mb-1">
                            <ul>
                                <li>{{$vacancy->work_time}}</li>
                                <li>{{$vacancy->language}}</li>
                            </ul>
                        </div>
                        <div class="mb-1">
                            <p>{{str_limit(strip_tags(html_entity_decode($vacancy->description)), 400, '...')  }} </p>
                        </div>
                        <div class="buttons mb-1">
                            <a href="/vacancy/{{$vacancy->url}}"
                                class="button is-info is-outlined h-3 px-1 has-text-weight-bold is-size-875">
                                @lang('vacancy.link_learn_more')
                            </a>
                        </div>
                    </article>
                </div>
            @empty
                @lang('vacancy.not_found')
            @endforelse

        </div>
    </section>

    @if($vacancies)
        <section class="section py-1">
            <div class="container">
                @include('parts.pagination', ['objects' => $vacancies])
            </div>
        </section>
    @endif

@endsection
