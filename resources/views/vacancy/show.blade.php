@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')

    <section class="section pt-1">
        <div class="container">
            <div class="vacancy-list">
                <article class="vacancy">
                    <div class="columns is-multiline">
                        <div class="column is-10 pb-0">
                            <h1 class="vacancy__title section-title mb-0">{{$vacancy->name}}</h1>
                        </div>
                        <div class="column is-2 pb-0 has-text-right-tablet">
                            <div class="vacancy__salary has-text-weight-bold">
                                {{$vacancy->salary}}
                            </div>
                        </div>
                    </div>
                    @if($vacancy->city)
                        <div class="vacancy__location">
                            <img src="{{ asset('/svg/icons/ic_location.svg') }}">
                            <span>{{$vacancy->city->region->country->name}}, {{$vacancy->city->name}}</span>
                        </div>
                     @endif
                    <div class="vacancy__short-info mb-1">
                        <ul>
                            <li>{{$vacancy->work_time}}</li>
                            <li>{{$vacancy->language}}</li>
                        </ul>
                    </div>
                    <div class="content mb-1">
                       {!! $vacancy->description !!}
                    </div>
                    <hr class="hr-basic">
                    <div class="buttons">
                        <button class="button is-info h-3 px-1 has-text-weight-bold is-size-875"
                                @click="vacancy.showResponseModal = true">
                            @lang('vacancy.single.btn_respond')
                        </button>
                        {{-- <button
                            class="button is-info is-outlined has-text-left-mobile h-3 px-1 has-text-weight-bold is-size-875">
                            Регистрация брокера продавца бизнеса
                        </button> --}}
                    </div>
                </article>
            </div>
        </div>
    </section>

    <vacancy-response v-model="vacancy.showResponseModal" vacancy-id="{{$vacancy->id}}"></vacancy-response>

@endsection
