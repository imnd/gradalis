<!-- Pagination -->
<nav class="pagination is-centered" role="navigation" aria-label="pagination">
        @if($objects->currentPage() > 1 && $objects->lastPage() > 1)
            <a href="{{ Request::url() . '?page=' . ($objects->currentPage() - 1) }}"
               class="pagination-previous pagination-nav button is-info mx-0"
            >
                <span class="pagination-nav__icon">{!! file_get_contents(asset('/svg/icons/ic_arrow_right.svg')) !!}</span>
                <span class="pagination-nav__title">@lang('pagination.prev_page')</span>
            </a>
        @else
            <a class="pagination-previous pagination-nav button is-info mx-0" disabled="true" >
                <span class="pagination-nav__icon">{!! file_get_contents(asset('/svg/icons/ic_arrow_right.svg')) !!}</span>
                <span class="pagination-nav__title">@lang('pagination.prev_page')</span>
            </a>
        @endif
    
        @if($objects->currentPage() !== $objects->lastPage())
            <a href="{{ Request::url() . '?page=' . ($objects->currentPage() + 1) }}"
               class="pagination-next pagination-nav button is-info mx-0">
                <span class="pagination-nav__title">@lang('pagination.next_page')</span>
                <span class="pagination-nav__icon">{!! file_get_contents(asset('/svg/icons/ic_arrow_right.svg')) !!}</span>
            </a>
        @else
            <a class="pagination-next pagination-nav button is-info mx-0" disabled="true">
                <span class="pagination-nav__title">@lang('pagination.next_page')</span>
                <span class="pagination-nav__icon">{!! file_get_contents(asset('/svg/icons/ic_arrow_right.svg')) !!}</span>
            </a>
        @endif
        <ul class="pagination-list">
            @for($i=1; $i <= $objects->lastPage(); $i++)
                @if($i === $objects->currentPage())
                    <li><a class="pagination-link is-current" aria-label="Goto page {{ $i }}">{{ $i }}</a></li>
                @else
                    <li><a href="{{ Request::url() . '?page=' . $i }}" class="pagination-link" aria-label="Goto page {{ $i }}">{{ $i }}</a></li>
                @endif
            @endfor
        </ul>
    </nav>
    