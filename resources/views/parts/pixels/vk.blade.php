@if($id)

{{--  VK-RTRG-354995-f6VZi  --}}
    <script type="text/javascript">
        !function(){var t=document.createElement("script");
        t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?160",
            t.onload=function(){VK.Retargeting.Init("{{$id}}"),
                VK.Retargeting.Hit()},document.head.appendChild(t)}();
    </script>
    <noscript>
        <img src="https://vk.com/rtrg?p={{$id}}" style="position:fixed; left:-999px;" alt=""/>
    </noscript>

@else

@endif
