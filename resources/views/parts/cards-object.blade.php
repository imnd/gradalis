<!-- Example Layout cards object -->
<div class="column is-4">
    <div class="card card-object">
        <div class="card-image">
            <figure class="image is-16by9">
                <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                     alt="Placeholder image">
            </figure>
            <span class="tag tag-payback">
                <span class="name">@lang('business.show.similar_offers_item.payback')</span>
                <span class="term">38 @lang('business.show.similar_offers_item.months')</span>
            </span>
            <div class="info-icons-wrap info-icons-wrap_left">
                <span class="info-icon object-sale">
                            <img src="{{ asset('/svg/icons/ic_sale.svg') }}" alt="Sale">
                        </span>
                <span class="info-icon object-promo">
                            <img src="{{ asset('/svg/icons/ic_recommended.svg') }}" alt="Recommended">
                        </span>
            </div>
            <div class="info-icons-wrap info-icons-wrap_right">
                <a href="#" class="info-icon object-favorite" v-tooltip="'@lang('business.show.similar_offers_item.add_to_fav')'">
                    <img src="{{ asset('/svg/icons/ic_favorites_white.svg') }}" alt="@lang('business.show.similar_offers_item.add_to_fav')">
                </a>
            </div>

        </div>
        <div class="card-content">
            <div class="card-object__header">
                <h3 class="title">Мини-отель в собственность с видом на Невский проспект</h3>
                <p class="location"><img src="{{ asset('/svg/icons/ic_location.svg') }}"
                                         alt="Fav"><span>Россия, г. Санкт-Петербург</span>
                </p>
            </div>

            <div class="content card-object__content">
                <div class="list-properties">
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.category')</span>
                        <span>Коммерческая</span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.revenue')</span>
                        <span><b>$48 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.profit')</span>
                        <span><b>$23 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.perc_profit')</span>
                        <span><b>78%</b></span>
                    </div>
                </div>
            </div>
            <div class="card-object__footer">
                <div class="price-info">
                    <div class="price">$18 000 560</div>
                </div>
                <a class="button is-link is-outlined is-fullwidth has-text-weight-bold">
                    @lang('business.show.similar_offers_item.btn_more')
                </a>
            </div>
        </div>
    </div>
</div>
<div class="column is-4">
    <div class="card card-object card-object_promo">
        <div class="card-image">
            <figure class="image is-16by9">
                <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                     alt="Placeholder image">
            </figure>
            <span class="tag tag-payback">
                            <span class="name">@lang('business.show.similar_offers_item.payback')</span>
                            <span class="term">38 @lang('business.show.similar_offers_item.months')</span>
                        </span>
            <div class="info-icons-wrap info-icons-wrap_left">
                <span class="info-icon object-sale">
                            <img src="{{ asset('/svg/icons/ic_sale.svg') }}" alt="Sale">
                        </span>
                <span class="info-icon object-promo">
                            <img src="{{ asset('/svg/icons/ic_recommended.svg') }}" alt="Recommended">
                        </span>
            </div>
            <div class="info-icons-wrap info-icons-wrap_right">
                <a href="#" class="info-icon object-favorite" v-tooltip="'@lang('business.show.similar_offers_item.add_to_fav')'">
                    <img src="{{ asset('/svg/icons/ic_favorites_white.svg') }}" alt="@lang('business.show.similar_offers_item.add_to_fav')">
                </a>
            </div>
        </div>
        <div class="card-content">
            <div class="card-object__header">
                <h3 class="title">Мини-отель в собственность с видом на Невский проспект</h3>
                <p class="location"><img src="{{ asset('/svg/icons/ic_location.svg') }}" alt="Fav"><span>Россия, г. Санкт-Петербург</span>
                </p>
            </div>

            <div class="content card-object__content">
                <div class="list-properties">
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.category')</span>
                        <span>Коммерческая</span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.revenue')</span>
                        <span><b>$48 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.profit')</span>
                        <span><b>$23 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.perc_profit')</span>
                        <span><b>78%</b></span>
                    </div>
                </div>
            </div>
            <div class="card-object__footer">
                <div class="price-info">
                    <div class="price">$18 000 560</div>
                </div>
                <a class="button is-success is-outlined is-fullwidth has-text-weight-bold">
                    @lang('business.show.similar_offers_item.btn_more')
                </a>
            </div>
        </div>
    </div>
</div>
<div class="column is-4">
    <div class="card card-object">
        <div class="card-image">
            <figure class="image is-16by9">
                <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                     alt="Placeholder image">
            </figure>
            <span class="tag tag-payback">
                            <span class="name">@lang('business.show.similar_offers_item.payback')</span>
                            <span class="term">38 @lang('business.show.similar_offers_item.months')</span>
                        </span>
            <div class="info-icons-wrap info-icons-wrap_left">
                <span class="info-icon object-sale">
                            <img src="{{ asset('/svg/icons/ic_sale.svg') }}" alt="Sale">
                        </span>
                <span class="info-icon object-promo">
                    <img src="{{ asset('/svg/icons/ic_recommended.svg') }}" alt="Recommended">
                </span>
            </div>
            <div class="info-icons-wrap info-icons-wrap_right">
                <a href="#" class="info-icon object-favorite" v-tooltip="'@lang('business.show.similar_offers_item.add_to_fav')'">
                    <img src="{{ asset('/svg/icons/ic_favorites_white.svg') }}" alt="@lang('business.show.similar_offers_item.add_to_fav')">
                </a>
            </div>
        </div>
        <div class="card-content">
            <div class="card-object__header">
                <h3 class="title">Мини-отель в собственность с видом на Невский проспект</h3>
                <p class="location"><img src="{{ asset('/svg/icons/ic_location.svg') }}"
                                         alt="Fav"><span>Россия, г. Санкт-Петербург</span>
                </p>
            </div>

            <div class="content card-object__content">
                <div class="list-properties">
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.category')</span>
                        <span>Коммерческая</span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.revenue')</span>
                        <span><b>$48 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.profit')</span>
                        <span><b>$23 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.perc_profit')</span>
                        <span><b>78%</b></span>
                    </div>
                </div>
            </div>
            <div class="card-object__footer">
                <div class="price-info">
                    <span class="tag is-danger price-sale">-15%</span>
                    <div class="price">$2 500 000</div>
                    <span class="price-old">$2 000 000</span>
                </div>
                <a class="button is-link is-outlined is-fullwidth has-text-weight-bold">
                    @lang('business.show.similar_offers_item.btn_more')
                </a>
            </div>
        </div>
    </div>
</div>
<div class="column is-4">
    <div class="card card-object">
        <div class="card-image">
            <figure class="image is-16by9">
                <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                     alt="Placeholder image">
            </figure>
            <span class="tag tag-payback">
                            <span class="name">@lang('business.show.similar_offers_item.payback')</span>
                            <span class="term">38 @lang('business.show.similar_offers_item.months')</span>
                        </span>

            <span class="tag is-warning tag-object-sold">Продано</span>

        </div>
        <div class="card-content">
            <div class="card-object__header">
                <h3 class="title">Мини-отель в собственность с видом на Невский проспект</h3>
                <p class="location"><img src="{{ asset('/svg/icons/ic_location.svg') }}"
                                         alt="Fav"><span>Россия, г. Санкт-Петербург</span>
                </p>
            </div>

            <div class="content card-object__content">
                <div class="list-properties">
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.category')</span>
                        <span>Коммерческая</span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.revenue')</span>
                        <span><b>$48 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.profit')</span>
                        <span><b>$23 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.perc_profit')</span>
                        <span><b>78%</b></span>
                    </div>
                </div>
            </div>
            <div class="card-object__footer">
                <div class="price-info">
                    <div class="price">$18 000 560</div>
                </div>
                <a class="button is-warning is-outlined is-fullwidth has-text-weight-bold">
                    @lang('business.show.similar_offers_item.btn_more')
                </a>
            </div>
        </div>
    </div>
</div>
<!-- -->
<div class="column is-4">
    <div class="card card-object">
        <div class="card-image">
            <figure class="image is-16by9">
                <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                     alt="Placeholder image">
            </figure>
            <span class="tag tag-payback">
                            <span class="name">@lang('business.show.similar_offers_item.payback')</span>
                            <span class="term">38 @lang('business.show.similar_offers_item.months')</span>
                        </span>
            <div class="info-icons-wrap info-icons-wrap_left">
                <span class="info-icon object-sale">
                            <img src="{{ asset('/svg/icons/ic_sale.svg') }}" alt="Sale">
                        </span>
                <span class="info-icon object-promo">
                            <img src="{{ asset('/svg/icons/ic_recommended.svg') }}" alt="Recommended">
                        </span>
            </div>
            <div class="info-icons-wrap info-icons-wrap_right">
                <a href="#" class="info-icon object-favorite" v-tooltip="'@lang('business.show.similar_offers_item.add_to_fav')'">
                    <img src="{{ asset('/svg/icons/ic_favorites_white.svg') }}" alt="@lang('business.show.similar_offers_item.add_to_fav')">
                </a>
            </div>
        </div>
        <div class="card-content">
            <div class="card-object__header">
                <h3 class="title">Мини-отель в собственность с видом на Невский проспект</h3>
                <p class="location"><img src="{{ asset('/svg/icons/ic_location.svg') }}"
                                         alt="Fav"><span>Россия, г. Санкт-Петербург</span>
                </p>
            </div>

            <div class="content card-object__content">
                <div class="list-properties">
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.category')</span>
                        <span>Коммерческая</span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.revenue')</span>
                        <span><b>$48 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.profit')</span>
                        <span><b>$23 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.perc_profit')</span>
                        <span><b>78%</b></span>
                    </div>
                </div>
            </div>
            <div class="card-object__footer">
                <div class="price-info">
                    <div class="price">$18 000 560</div>
                </div>
                <a class="button is-link is-outlined is-fullwidth has-text-weight-bold">
                    @lang('business.show.similar_offers_item.btn_more')
                </a>
            </div>
        </div>
    </div>
</div>
<div class="column is-4">
    <div class="card card-object">
        <div class="card-image">
            <figure class="image is-16by9">
                <img src="https://www.hoteloxford.com/data/jpg/foto-galleria-130.jpg"
                     alt="Placeholder image">
            </figure>
            <span class="tag tag-payback">
                            <span class="name">@lang('business.show.similar_offers_item.payback')</span>
                            <span class="term">38 @lang('business.show.similar_offers_item.months')</span>
                        </span>
            <div class="info-icons-wrap info-icons-wrap_left">
                <span class="info-icon object-sale">
                            <img src="{{ asset('/svg/icons/ic_sale.svg') }}" alt="Sale">
                        </span>
                <span class="info-icon object-promo">
                            <img src="{{ asset('/svg/icons/ic_recommended.svg') }}" alt="Recommended">
                        </span>
            </div>
            <div class="info-icons-wrap info-icons-wrap_right">
                <a href="#" class="info-icon object-favorite" v-tooltip="'@lang('business.show.similar_offers_item.add_to_fav')'">
                    <img src="{{ asset('/svg/icons/ic_favorites_white.svg') }}" alt="@lang('business.show.similar_offers_item.add_to_fav')">
                </a>
            </div>
        </div>
        <div class="card-content">
            <div class="card-object__header">
                <h3 class="title">Мини-отель в собственность с видом на Невский проспект</h3>
                <p class="location"><img src="{{ asset('/svg/icons/ic_location.svg') }}"
                                         alt="Fav"><span>Россия, г. Санкт-Петербург</span>
                </p>
            </div>

            <div class="content card-object__content">
                <div class="list-properties">
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.category')</span>
                        <span>Коммерческая</span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.revenue')</span>
                        <span><b>$48 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.profit')</span>
                        <span><b>$23 000</b></span>
                    </div>
                    <div class="list-properties__item">
                        <span>@lang('business.show.similar_offers_item.perc_profit')</span>
                        <span><b>78%</b></span>
                    </div>
                </div>
            </div>
            <div class="card-object__footer">
                <div class="price-info">
                    <div class="price">$18 000 560</div>
                </div>
                <a class="button is-link is-outlined is-fullwidth has-text-weight-bold">
                    @lang('business.show.similar_offers_item.btn_more')
                </a>
            </div>
        </div>
    </div>
</div>
