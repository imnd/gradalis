@extends('layouts.app')

@section('content')

    @include('includes.breadcrumb')


    <section class="section pt-1 pb-0">
        <div class="container">
            <h1 class="section-title mb-1">@lang('contacts.title')</h1>
        </div>
    </section>
    <section class="section pt-1">
        <div class="container contacts">
            <div class="columns is-multiline is-gapless">
                <div class="column is-6">
                    <div class="contacts__item mb-0-5">
                        <div class="columns is-multiline">
                            <div class="column is-6">
                                <div class="is-flex has-align-items-center">
                                     <span class="icon icon-1 mr-0-5">
                                        <img src="{{ asset('/svg/icons/ic_phone.svg') }}" class="svg">
                                     </span>
                                    <span
                                        class="is-size-875 has-text-weight-bold">@lang('contacts.phone_for_all_questions')</span>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div>
                                    <a href="#" class="has-text-weight-bold has-text-info">8 800 2024 70</a>
                                    <div class="contacts__item__desc is-size-875">@lang('contacts.we_work_with') 10:00
                                        до 20:00
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="contacts__item mb-0-5">
                        <div class="columns is-multiline">
                            <div class="column is-6">
                                <div class="is-flex has-align-items-center">
                                     <span class="icon icon-1 mr-0-5">
                                        <img src="{{ asset('/svg/icons/ic_location.svg') }}" class="svg">
                                     </span>
                                    <span
                                        class="is-size-875 has-text-weight-bold">@lang('contacts.headquarters_address')</span>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div>
                                    <span class="is-size-875">022245, ul. Vogla 28, Warsaw 02-963, Poland</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="contacts__item mb-0-5">
                        <div class="columns is-multiline">
                            <div class="column is-6">
                                <div class="is-flex has-align-items-center">
                                     <span class="icon icon-1 mr-0-5">
                                        <img src="{{ asset('/svg/icons/ic_phone.svg') }}" class="svg">
                                     </span>
                                    <span class="is-size-875 has-text-weight-bold">@lang('contacts.phone_local')</span>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div>
                                    <a href="#" class="has-text-weight-bold has-text-info">+7 (495) 567-53-76</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="contacts__item mb-0-5">
                        <div class="columns is-multiline">
                            <div class="column is-6">
                                <div class="is-flex has-align-items-center">
                                     <span class="icon icon-1 mr-0-5">
                                        <img src="{{ asset('/svg/icons/ic_mailbox.svg') }}" class="svg">
                                     </span>
                                    <span
                                        class="is-size-875 has-text-weight-bold">@lang('contacts.mailing_address')</span>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div>
                                    <span class="is-size-875">006436, Россия, Санкт-Петербург, ул. Ломоносова 123, пом. 65</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="contacts__item mb-0-5">
                        <div class="columns is-multiline">
                            <div class="column is-6">
                                <div class="is-flex has-align-items-center">
                                     <span class="icon icon-1 mr-0-5">
                                        <img src="{{ asset('/svg/icons/ic_mail.svg') }}" class="svg">
                                     </span>
                                    <span class="is-size-875 has-text-weight-bold">@lang('contacts.email')</span>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div>
                                    <a href="#" class="is-size-875 has-text-info has-text-decoration-underline">gradalis.group@gmail.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="contacts__item mb-0-5">
                        <div class="columns is-multiline">
                            <div class="column is-6">
                                <div class="is-flex has-align-items-center">
                                     <span class="icon icon-1 mr-0-5">
                                        <img src="{{ asset('/svg/icons/ic_quality.svg') }}" class="svg">
                                     </span>
                                    <span
                                        class="is-size-875 has-text-weight-bold">@lang('contacts.quality_and_service_department')</span>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div>
                                    <a href="#" class="has-text-weight-bold has-text-info">+7 (495) 567-53-76</a>
                                    <div class="contacts__item__desc is-size-875">@lang('contacts.head')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="contacts__item mb-0-5">
                        <div class="columns is-multiline">
                            <div class="column is-6">
                                <div class="is-flex has-align-items-center">
                                     <span class="icon icon-1 mr-0-5">
                                        <img src="{{ asset('/svg/icons/ic_security.svg') }}" class="svg">
                                     </span>
                                    <span
                                        class="is-size-875 has-text-weight-bold">@lang('contacts.security_department')</span>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div>
                                    <a href="#" class="is-size-875 has-text-info has-text-decoration-underline">gradalis.group@gmail.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <google-map></google-map>
    </section>

@endsection
