<?php

return [
    'your_manager' => 'Ваш менеджер:',
    'your_broker' => 'Ваш брокер:',
    'write_to_chat' => 'Написать в чат',
    'buy_business' => 'Купить бизнес',
    'sell_business' => 'Продать бизнес',
    'sign_up' => 'Регистрация',
    'sign_out' => 'Выйти',
    'sign_in' => 'Войти',
    'notifications' => 'Уведомления',
    'favorites' => 'Избранное',
    'personal_cabinet' => 'Личный кабинет',
    'messages' => 'Сообщения',
    'balance' => 'Баланс',
    'partners' => 'Партнеры',
    'statistics' => 'Статистика',
    'offers' => 'Офферы',
    'leads' => 'Лиды',
    'settings' => 'Настройки',
    'summary' => 'Сводные показатели',
    'registration' => 'Регистрация'
];
