<?php

return [
    'menu' => 'Меню',
    'home' => 'Главная',
    'business' => 'Бизнес',
    'franchise' => 'Франшизы',
    'user_services' => 'Каталог услуг',
    'services_for_seller' => 'Услуги для продавцов',
    'services_for_buyers' => 'Услуги для покупателей',
    'services_for_programmer' => 'Услуги для программиста',
    'investing' => 'Инвестирование',
    'news' => 'Новости',
    'about' => 'О компании',
    'about_items' => [
        'about_company' => 'О компании',
        'contacts' => 'Контакты',
        'vacancy' => 'Вакансии',
        'reviews' => 'Отзывы',
        'help' => 'Помощь',
    ],
];
