<?php

return [
    'mark_as_read' => 'Отметить как прочитанные',
    'marked_as_read' => 'Все уведомления отмечены как прочитанные',
    'new_message' => 'У вас новое сообщение в диалоге :link',
    'balance_changed' => 'Ваш текущий баланс: <b>€:balance</b>',
    'business_status_changed' => 'Статус вашего бизнеса ":name" изменен на: <b>:status</b>',
    'franchise_status_changed' => 'Статус вашей франшизы ":name" изменен на: <b>:status</b>',
    'new_object_view_request' => 'Новый <a href="/profile/">запрос на просмотр</a> для объекта: ":name"',
    'new_object_doc_request' => 'Новый <a href="/profile/">запрос документации</a> для объекта: ":name"',
];
