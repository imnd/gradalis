<?php

return [
    'we_work_with' => 'Работаем с',
    'soc-fb' => 'Facebook',
    'soc_ok' => 'Ok',
    'soc_yt' => 'Youtube',
    'business' => 'Бизнес',
    'sell_business' => 'Продать бизнес',
    'buy_business' => 'Купить бизнес',
    'consultation' => 'Индивидуальная консультация',
    'franchise' => 'Франшизы',
    'about' => 'О компании',
    'info' => 'Информация',
    'reviews' => 'Отзывы',
    'contacts' => 'Контакты',
    'copyright' => 'Предоставленные на сайте данные имеют информационный характер и не являются публичной офертой.<br>© 2019 «:site_name», официальный сайт',
    'popular_franchises' => 'Популярные франшизы',
    'no_down_payment' => 'Без первого взноса',
    'for_small_city' => 'Для маленьких городов',
    'royalty_free' => 'Без роялти',
];
