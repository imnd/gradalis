<?php


return array (
  'breadcrumb'  => array (
        'partners'        => 'Партнеры',
        'future_partners' => 'Будущие партнеры',
    ),
  'partners' => 'Партнеры',
  'cancel_changes' => 'Отменить изменения',
  'save_profile' => 'Сохранить изменения профиля',
  'email' => 
  array (
    'title' => 'Email',
    'placeholder' => 'Введите Email',
  ),
  'first_name' => 
  array (
    'title' => 'Имя пользователя',
    'placeholder' => 'Введите имя пользователя',
  ),
  'last_name' => 
  array (
    'title' => 'Фамилия пользователя',
    'placeholder' => 'Введите фамилию пользователя',
  ),
  'password' => 
  array (
    'title' => 'Новый пароль',
    'placeholder' => 'Введите новый пароль',
  ),
  'reg_ip' => 'IP при регистрации',
  'reg_time' => 'Время регистрации',
  'active_partners' => 'Активные партнеры',
  'future_partners' => 'Будущие партнеры',
  'help' => 'Помощь',
  'your_ref_link' => 'Ваша реферальная ссылка',
  'day' => 'День',
  'all' => 'Все',
  'views' => 'Показы',
  'hits' => 'Хиты',
  'write_message' => 'Написать сообщение',
  'message' => 'Сообщение',
  'clicks' => 'Клики',
  'payed_targets' => 'Целей оплачено',
  'open_commission' => 'Открытая комиссия',
  'approved_commission' => 'Одобренная комиссия',
  'open_leads' => 'Открытые лиды',
  'approved_leads' => 'Одобренные лиды',
  'in_realtime' => 'В реальном времени',
  'registrations' => 'Регистрации',
  'week' => 'Неделя',
  'account' => 'Аккаунт',
  'reg_date' => 'Дата регистрации',
  'moderate_messages' => 'Сообщ. на модер.',
  'country' => 'Страна',
  'leads' => 'Лиды',
  'sum_to_pay' => 'Сумма к выплате',
  'in_account' => 'В аккаунт',
  'settings' => 'Настройки',
  'await' => 'Ожидает одобрения',
  'block' => 'Заблокировать',
  'blocked' => 'Заблокирован',
  'activated' => 'Активирован',
  'block_label' => 'Блокировка',
  'unblock' => 'Разблокировать',
  'status' => 'Статус',
  'status_changed_message' => 'Статус партнера успешно изменен.',
  'view_count' => 'Количество показов',
  'hit_count' => 'Количество хитов',
  'click_count' => 'Количество кликов',
  'reg_count' => 'Количество регистраций',
  'delete' => 'Удалить',
  'main_info' => 'Основная информация',
  'sort' => 
  array (
    'title' => 'Сортировка',
    'placeholder' => 'Выберите',
    'options' => 
    array (
      0 => 
      array (
        'id' => '1',
        'name' => 'По дате конверсии',
      ),
    ),
  ),
  'time' => 
  array (
    'title' => 'Время',
    'placeholder' => 'Выберите',
    'options' => 
    array (
      0 => 
      array (
        'id' => '1',
        'name' => '1 минута',
      ),
      1 => 
      array (
        'id' => '2',
        'name' => '1 час',
      ),
    ),
  ),
  'offer' => 
  array (
    'title' => 'Оффер',
    'placeholder' => 'Выберите',
    'options' => 
    array (
      0 => 
      array (
        'id' => '1',
        'name' => 'Все офферы',
      ),
    ),
  ),
  'partner' => 
  array (
    'title' => 'Партнеры',
    'placeholder' => 'Выберите',
    'options' => 
    array (
      0 => 
      array (
        'id' => '1',
        'name' => 'Все партнеры',
      ),
    ),
  ),
  'compare' => 
  array (
    'title' => 'Сравнить',
    'placeholder' => 'Выберите',
    'options' => 
    array (
      0 => 
      array (
        'id' => '1',
        'name' => 'Без сравнения',
      ),
      1 => 
      array (
        'id' => '2',
        'name' => 'Показы',
      ),
      2 => 
      array (
        'id' => '3',
        'name' => 'Клики',
      ),
      3 => 
      array (
        'id' => '4',
        'name' => 'Регистрации',
      ),
    ),
  ),
  'data_in_chart' => 
  array (
    'title' => 'Данные в графике',
    'placeholder' => 'Выберите',
    'options' => 
    array (
      0 => 
      array (
        'id' => '1',
        'name' => 'Показы',
      ),
      1 => 
      array (
        'id' => '2',
        'name' => 'Клики',
      ),
      2 => 
      array (
        'id' => '3',
        'name' => 'Регистрации',
      ),
    ),
  ),
);