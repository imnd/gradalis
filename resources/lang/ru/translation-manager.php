<?php

return [
    'main'                    => 'Менеджер переводов',
    'navbar_sr'               => 'Скрыть/показать навигацию',
    'message_warn'            => 'Внимание, переводы не отображаются, пока они не будут экспортированы обратно в файл app/lang, использую команду <code>php artisan translation:export</code> или кнопку для публикации.',
    'message_import'          => 'Выполнен перевод, обработано <strong class="counter">N</strong>! Перезагрузите эту страницу для обновления групп!',
    'message_find'            => 'Поиск переводов закончен, найдено <strong class="counter">N</strong>!',
    'message_publish'         => 'Публикация переводов для группы \':group\' завершена!',
    'message_publish_all'     => 'Публикация переводов для всех групп завершена!',
    'app_new'                 => 'Добавить новые переводы',
    'rep_exi'                 => 'Заменить существующие переводы',
    'import_groups_btn'       => 'Импортировать группы',
    'import_groups_load'      => 'Загрузка..',
    'find_data_confirm'       => 'Вы уверены, что хотите сканировать папку приложения? Все найденные ключи перевода будут добавлены в базу данных.',
    'find_btn_dis'            => 'Поиск..',
    'find_btn'                => 'Поиск переводов в файлах',
    'publish_data_confirm'    => 'Вы уверены, что хотите опубликовать группу переводов \':group\'? Это перезапишет существующие языковые файлы.',
    'publish_btn_dis'         => 'Публикация..',
    'publish_btn'             => 'Опубликовать переводы',
    'publish_btn_back'        => 'Назад',
    'group_text'              => 'Выберите группу для отображения переводов группы. Если группы не отображаются, убедитесь, что вы выполнили миграции и импортировали переводы.',
    'group_label'             => 'Введите новое имя группы и начните редактировать переводы в этой группе',
    'group_btn_add'           => 'Добавить и редактировать ключи',
    'group_add_new_label'     => 'Добавление новых ключей в эту группу',
    'group_textarea_place'    => 'Добавьте 1 ключ в строку без префикса группы',
    'group_btn_submit'        => 'Добавить ключи',
    'main_table_total'        => 'Всего:',
    'main_table_changed'      => 'изменено:',
    'main_table_key'          => 'Ключ',
    'edit_data_title'         => 'Введите перевод',
    'delete_confirm'          => 'Вы уверены, что хотите удалить перевод для ',
    'locales_legend'          => 'Поддерживаемые языки',
    'locales_p'               => 'Текущие поддерживаемые языки:',
    'locale_remove_confirm'   => 'Вы уверены, что хотите удалить этот язык и все данные?',
    'form_add_locale_p'       => 'Введите новый ключ языка:',
    'form_add_locale_btn_dis' => 'Добавление..',
    'form_add_locale_btn'     => 'Добавить новый язык',
    'exp_all_leg'             => 'Экспорт всех переводов',
    'exp_all_confirm'         => 'Вы уверены, что хотите опубликовать все переводы группы? Это перезапишет существующие языковые файлы.',
    'exp_all_btn_dis'         => 'Публикация..',
    'exp_all_btn'             => 'Опубликовать все',
];
