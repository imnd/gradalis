<?php

return array (
    'show' =>
        array (
            'broker_object' => 'Брокер объекта:',
            'ask_object' => 'Задать вопрос по объекту',
            'add_to_fav' => 'Добавить в избранное',
            'del_from_fav' => 'Удалить из избранного',
            'obj_desc' => 'Описание объекта',
            'btn_view_more' => 'Посмотреть подробную информацию',
            'general_info' => 'Основная информация',
            'reason_for_sale' => 'Причина продажи',
            'name_company' => 'Название фирмы',
            'activity_profile' => 'Профиль деятельности',
            'address' => 'Адрес',
            'zip_code' => 'Индекс',
            'number_of_employees' => 'Количество сотрудников',
            'target_audience' => 'Целевая аудитория',
            'average_age_customers' => 'Средний возраст клиентов',
            'average_check' => 'Средний чек',
            'cost_of_attraction' => 'Стоимость привлечения',
            'financial_indicators' => 'Финансовые показатели',
            'revolutions_per_month' => 'Обороты в месяц',
            'monthly_expenses' => 'Расходы в месяц',
            'legal_state' => 'Юридическое состояние',
            'legal_status_company' => 'Правовое состояние компании',
            'business_processes' => 'Бизнес-процессы',
            'intangible_assets' => 'Нематериальные активы',
            'tangible_assets' => 'Материальные активы',
            'real_estate' => 'Недвижимость',
            'btn_hide_more_info' => 'Скрыть подробную информацию',
            'country' => 'Страна',
            'city' => 'Город',
            'category' => 'Категория',
            'payback' => 'Окупаемость',
            'months' => 'Месяцев',
            'profit' => 'Выручка',
            'net_profit' => 'Прибыль',
            'perc_profit' => 'Доходность',
            'price_by_eur' => 'Цена в евро',
            'price_by_gold' => 'Цена в золотых',
            'price_by_btc' => 'Цена в биткоинах',
            'btn_get_docs' => 'Получить документы',
            'how_purchase' => 'Как происходит покупка?',
            'similar_offers' => 'Похожие предложения',
            'similar_offers_item' =>
                array (
                    'payback' => 'окупаемость',
                    'months' => 'месяцев',
                    'add_to_fav' => 'Добавить в избранное',
                    'category' => 'Категория',
                    'revenue' => 'Выручка',
                    'profit' => 'Прибыль',
                    'perc_profit' => 'Доходность',
                    'btn_more' => 'Подробнее о бизнесе',
                ),
            'your_req_accept' => 'Ваш запрос принят',
            'your_req_sended' => 'Спасибо мы отправили запрос',
            'close' => 'Закрыть',
            'how_purchase_2' => 'Как происходит сделка?',
            'how_purchase_title' => 'Оставляйте заявку, это вас не обязывает ни к чему. Вместе с вами мы пройдем следующие шаги:',
            'how_purchase_step1' => 'Вам позвонит эксперт по бизнесу, чтобы уточнить критерии поиска и проконсультировать при необходимости',
            'how_purchase_step2' => 'Эксперт найдёт для вас самые подходящие варианты',
            'how_purchase_step3' => 'Вместе с вами эксперт едет на объект, чтобы посмотреть его «вживую»',
            'how_purchase_step4' => 'Если объект понравился, назначаем встречу с собственником',
            'how_purchase_step5' => 'Заключаем предварительный договор — до завершения сделки ваши деньги защищены и не переходят к продавцу бизнеса',
            'how_purchase_step6' => 'Вы проверяете бизнес — от недели',
            'how_purchase_step7' => 'Заключаем основной договор. К вам переходят все активы, а к продавцу — деньги',
            'translation_of_documents' => 'Перевод документов',
            'btn_more' => 'Подробнее',
        ),
    'upload_photo' =>
        array (
            'label' => 'Фотографии бизнеса',
            'title' => 'Загрузите фотографии',
            'subtitle' => '1000x1000 пикселей, до 8 Мб',
            'dictCancelUploadConfirmation' => 'Вы уверены, что хотите отменить загрузку?',
            'make_cover' => 'Сделать обложкой',
            'delete' => 'Удалить',
        ),
    'category' => 'Категория бизнеса',
    'saled' => 'Проданные',
    'index' =>
        array (
            'title' => 'Купить готовый бизнес в Польше',
            'category' => 'Категория',
        ),
    'step1' =>
        array (
            'icon' => '/svg/icons/ic_info.svg',
            'title' => 'Основная информация',
        ),
    'step2' =>
        array (
            'icon' => '/svg/icons/ic_object_finance.svg',
            'title' => 'Финансовые показатели',
        ),
    'step3' =>
        array (
            'icon' => '/svg/icons/ic_object_building.svg',
            'title' => 'Информация о недвижимости',
        ),
    'step4' =>
        array (
            'icon' => '/svg/icons/ic_process.svg',
            'title' => 'Бизнес процессы',
        ),
    'step5' =>
        array (
            'icon' => '/svg/icons/ic_money.svg',
            'title' => 'Материальные Активы',
        ),
    'step6' =>
        array (
            'icon' => '/svg/icons/ic_globe.svg',
            'title' => 'Нематериальные Активы',
        ),
    'step7' =>
        array (
            'icon' => '/svg/icons/ic_target_audience.svg',
            'title' => 'Целевая аудитория',
        ),
    'step8' =>
        array (
            'icon' => '/svg/icons/ic_law_form.svg',
            'title' => 'Юридическое состояние',
        ),
    'create' =>
        array (
            'addresses' =>
                array (
                    'title' => 'Адреса',
                ),
            'objects' =>
                array (
                    'title' => 'Объекты',
                ),
            'b2c' =>
                array (
                    'title' => 'B2C',
                ),
            'b2b' =>
                array (
                    'title' => 'B2B',
                ),
            'total' => 'Итого',
            'step2' => 'Шаг 2: Указать финансовые показатели',
            'step3' => 'Шаг 3: Информация о недвижимости',
            'step4' => 'Шаг 4: Бизнес процессы',
            'step5' => 'Шаг 5: Материальные Активы',
            'step6' => 'Шаг 6: Нематериальные Активы',
            'step7' => 'Шаг 7: Целевая аудитория',
            'step8' => 'Шаг 8: Юридическое состояние',
            'go_to_publish' => 'Перейти к публикации объявления',
            'i_want_to_sell_my_business_myself' => 'Я хочу продавать бизнес самостоятельно',
            'publish_info' => 'Способы размещения объявления до 50 дней',
            'publish_info_2' => 'Публикация вашего бизнеса на проекте абсолютно бесплатна. Мы отправим ваше объявление на модерацию и после проверки опубликуем его в каталоге бизнесов.',
            'traf_and_seo' => 'Трафик и SEO',
            'site_rev' => 'Доходы сайта',
            'site_exp' => 'Расходы сайта',
            'about_content_and_design' => 'О контенте и дизайне',
            'photo_and_img_on_site' => 'Фото и картинки на сайте',
            'text_on_site' => 'Тексты на сайте',
            'cancel_step' => 'Отменить',
            'back_step' => 'Вернуться назад',
            'business_processes' => 'Бизнес-процессы',
            'tangible_assets' => 'Материальные активы',
            'intangible_assets' => 'Нематериальные активы',
            'target_audience' => 'Целевая аудитория',
            'legal_status' => 'Юридическое состояние',
            'ad_placement' => 'Размещение объявления',
            'real_estate' => 'Недвижимость',
            'agreement_text_1' => 'Принимаю',
            'agreement_text_2' => 'условия данного раздела',
            'agreement_text_3' => 'и',
            'agreement_text_4' => 'договора о неразглашении (NDA)',
            'title' => 'Продать бизнес',
            'help' => '<span class="has-text-weight-bold">Не хотите заполнять?</span>&nbsp;<span>Наш менеджер поможет вам!</span>',
            'btn_more' => 'Подробнее',
            'main_info' => 'Основная информация',
            'finance_info' => 'Финансовые показатели',
            'labelReturnInvestment' => 'Окупаемость вложенных средств',
            'sold_success' => 'Успех в продаже бизнеса',
            'add_address' => 'Добавить еще один адрес',
            'remove_address' => 'Удалить этот адрес',
            'add_new_object' => 'Добавить объект недвижимости',
            'add_object' => 'Добавить еще один объект',
            'remove_object' => 'Удалить объект',
            'add_contract' => 'Добавить еще один объект',
            'remove_contract' => 'Удалить объект',
            'yn' =>
                array (
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Да',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Нет',
                                ),
                        ),
                ),
            'have' =>
                array (
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Есть',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Нет',
                                ),
                        ),
                ),
            'address' =>
                array (
                    'title' => 'Адрес бизнеса',
                    'placeholder' => 'Укажите адрес',
                ),
            'numberHouse' =>
                array (
                    'title' => 'Номер дома',
                    'placeholder' => '№',
                ),
            'housingHouse' =>
                array (
                    'title' => 'Корпус дома',
                    'placeholder' => 'Корпус',
                ),
            'numberOffice' =>
                array (
                    'title' => 'Номер офиса',
                    'placeholder' => '№',
                ),
            'index' =>
                array (
                    'title' => 'Индекс',
                    'placeholder' => '513',
                ),
            'nameLegalEntity' =>
                array (
                    'title' => 'Название юридического лица',
                    'placeholder' => 'Укажите название',
                ),
            'partBusiness' =>
                array (
                    'title' => 'Продается весь бизнес?',
                    'list' =>
                        array (
                            0 =>
                                array (
                                    'title' => 'Весь бизнес',
                                    'value' => 'all',
                                ),
                            1 =>
                                array (
                                    'title' => 'Доля компании',
                                    'value' => 'part',
                                ),
                            2 =>
                                array (
                                    'title' => 'Акции компании',
                                    'value' => 'stock',
                                ),
                        ),
                ),
            'numberShares' =>
                array (
                    'title' => 'Количество долей или акций',
                    'placeholder' => '75',
                ),
            'description' =>
                array (
                    'title' => 'Описание бизнеса',
                    'placeholder' => 'Опишите продаваемый бизнес',
                ),
            'yearFoundationBusiness' =>
                array (
                    'title' => 'Год основания бизнеса',
                    'placeholder' => '2007',
                ),
            'reasonSale' =>
                array (
                    'title' => 'Причина продажи',
                    'placeholder' => 'Опишите причину продажи',
                ),
            'nameVideoReview' =>
                array (
                    'title' => 'Название видеообзора',
                    'placeholder' => 'Укажите название видеообзора',
                ),
            'linkVideoReview' =>
                array (
                    'title' => 'Видеобзор бизнеса',
                    'placeholder' => 'http://',
                ),
            'theme' =>
                array (
                    'title' => 'Тематика безнеса',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Item1',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Item2',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Item3',
                                ),
                            3 =>
                                array (
                                    'id' => '4',
                                    'name' => 'Item4',
                                ),
                        ),
                ),
            'name' =>
                array (
                    'title' => 'Название бизнеса',
                    'placeholder' => 'Укажите название',
                ),
            'price' =>
                array (
                    'title' => 'Цена долей или акций',
                    'placeholder' => '100 000',
                ),
            'profitability' =>
                array (
                    'title' => 'Чистая средняя годовая прибыль',
                    'placeholder' => '1 000 000',
                ),
            'netAverageQuarterlyProfit' =>
                array (
                    'title' => 'Чистая средняя квартальная прибыль',
                    'placeholder' => '50 000',
                ),
            'revenue' =>
                array (
                    'title' => 'Средний годовой оборот',
                    'placeholder' => '10 000 000',
                ),
            'averageQuarterlyTurnover' =>
                array (
                    'title' => 'Средний квартальный оборот',
                    'placeholder' => '500 000',
                ),
            'costBusinessPerYear' =>
                array (
                    'title' => 'Затраты на коммерческую деятельность в год',
                    'placeholder' => '25 000',
                ),
            'costBusinessPerQuarter' =>
                array (
                    'title' => 'Затраты на коммерческую деятельность в квартал',
                    'placeholder' => '100 000',
                ),
            'payback' =>
                array (
                    'title' => 'Позитивный сценарий',
                    'placeholder' => '12',
                ),
            'negativeScenario' =>
                array (
                    'title' => 'Негативный сценарий',
                    'placeholder' => '36',
                ),
            'categoryProperty' =>
                array (
                    'title' => 'Тип собственности объекта',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Собственность',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Аренда',
                                ),
                        ),
                ),
            'typeProperty' =>
                array (
                    'title' => 'Тип недвижимости',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Недвижимость/землья общего назначения  ',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Недвижимость сельско-хозяйского назначения',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Недвижимость со зданиямии',
                                ),
                            3 =>
                                array (
                                    'id' => '4',
                                    'name' => 'Отдельная коммерческая собственная квартира/помещение',
                                ),
                            4 =>
                                array (
                                    'id' => '5',
                                    'name' => 'Отдельнoe собственнoe жилье/квартира',
                                ),
                            5 =>
                                array (
                                    'id' => '6',
                                    'name' => 'Отдельнoe собственнoe коммерческое помещение',
                                ),
                            6 =>
                                array (
                                    'id' => '7',
                                    'name' => 'Отдельнoe собственнoe жилое помещение',
                                ),
                        ),
                ),
            'numberSquareMeters' =>
                array (
                    'title' => 'Количество квадратных метров',
                    'placeholder' => '12',
                    'prevText' => 'м²',
                ),
            'priceSquareMeters' =>
                array (
                    'title' => 'Цена за квадратный метр',
                    'placeholder' => '1000',
                ),
            'priceIncludingVAT' =>
                array (
                    'title' => 'Цена с учетом НДС',
                ),
            'titleDocuments' =>
                array (
                    'title' => 'Правоустанавливающие документы',
                ),
            'restrictionsOperation' =>
                array (
                    'title' => 'Ограничения по эксплуатации',
                ),
            'listRestrictionsOperation' =>
                array (
                    'title' => 'Укажите ограничения',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Ипотека',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Залог на речиях находящих в помещении',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Ограниченное право собственности - сервитут прохода и транзита определённых лиц',
                                ),
                            3 =>
                                array (
                                    'id' => '4',
                                    'name' => 'Ограниченное право собственности',
                                ),
                            4 =>
                                array (
                                    'id' => '5',
                                    'name' => 'Ограниченное право собственности - сервитут передачи',
                                ),
                            5 =>
                                array (
                                    'id' => '6',
                                    'name' => 'Ограниченное право собственности - сервитут пожизненной квартиры',
                                ),
                            6 =>
                                array (
                                    'id' => '7',
                                    'name' => 'Долгосрочный прокат (аренда)',
                                ),
                            7 =>
                                array (
                                    'id' => '8',
                                    'name' => 'Аренда',
                                ),
                        ),
                ),
            'refRegisterEstate' =>
                array (
                    'title' => 'Справка из регистра недвижимости и зданий',
                ),
            'technicalPropertyPlan' =>
                array (
                    'title' => 'Технический план недвижимости',
                ),
            'coordinationRedevelopment' =>
                array (
                    'title' => 'Согласование перепланировок',
                ),
            'availabilityMortgage' =>
                array (
                    'title' => 'Наличие ипотеки по объекту',
                ),
            'totalAmountMortgage' =>
                array (
                    'title' => 'Общая сумма ипотеки',
                    'placeholder' => '1000',
                ),
            'repaidAmountMortgage' =>
                array (
                    'title' => 'Погашенная сумма',
                    'placeholder' => '1000',
                ),
            'balanceMortgage' =>
                array (
                    'title' => 'Остаток суммы ипотеки',
                    'placeholder' => '1000',
                ),
            'monthlyPaymentMortgage' =>
                array (
                    'title' => 'Ежемесячный платёж',
                    'placeholder' => '1000',
                ),
            'finalPaymentMortgageDate' =>
                array (
                    'title' => 'Окончательный срок выплаты',
                    'placeholder' => '25.02.2020',
                ),
            'availabilityLoanSecuredObject' =>
                array (
                    'title' => 'Наличие кредита под залог объекта',
                ),
            'totalAmountCredit' =>
                array (
                    'title' => 'Общая сумма кредита',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'repaidAmountCredit' =>
                array (
                    'title' => 'Погашенная сумма',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'balanceCredit' =>
                array (
                    'title' => 'Остаток суммы кредита',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'monthlyPaymentCredit' =>
                array (
                    'title' => 'Ежемесячный платёж',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'finalPaymentCreditDate' =>
                array (
                    'title' => 'Окончательный срок выплаты',
                    'placeholder' => '25.02.2020',
                ),
            'purposeCredit' =>
                array (
                    'selected' => '',
                    'title' => 'Назначение кредита',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'кредит на новые средства транспорта для бизнеса',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'кредит на покупку электронной техники',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'кредит на служебный транспорт и производственное оборудование',
                                ),
                            3 =>
                                array (
                                    'id' => '4',
                                    'name' => 'инвестиционный кредит',
                                ),
                            4 =>
                                array (
                                    'id' => '5',
                                    'name' => 'кредит на обслуживание заключенного контракта',
                                ),
                            5 =>
                                array (
                                    'id' => '6',
                                    'name' => 'консолидационный кредит',
                                ),
                            6 =>
                                array (
                                    'id' => '7',
                                    'name' => 'кредит на покупку объекта недвижимости',
                                ),
                            7 =>
                                array (
                                    'id' => '8',
                                    'name' => 'кредит для погашения задолженности перед контрагентами',
                                ),
                            8 =>
                                array (
                                    'id' => '9',
                                    'name' => 'кредит для погашения государственной задолженности по налогам / социальному страхованию',
                                ),
                            9 =>
                                array (
                                    'id' => '10',
                                    'name' => 'потребительский кредит',
                                ),
                            10 =>
                                array (
                                    'id' => '11',
                                    'name' => 'кредит на покупку личного автомобиля',
                                ),
                        ),
                ),
            'jointPropertyEstate' =>
                array (
                    'title' => 'Совместная собственность недвижимости',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Долевая собственность',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Совместное владение супружеской собственностью',
                                ),
                        ),
                ),
            'typeRelationshipCoowners' =>
                array (
                    'title' => 'Тип взаимоотношений собственников',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Долевая собственность',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Совместное владение супружеской собственностью',
                                ),
                        ),
                ),
            'monthlyUtilityCosts' =>
                array (
                    'title' => 'Ежемесячные расходы на комуннальные платежи',
                    'placeholder' => '1000',
                ),
            'houseBookNumber' =>
                array (
                    'title' => 'Номер домовой книги',
                    'placeholder' => '№',
                ),
            'presenceChanging' =>
                array (
                    'title' => 'Наличие чейнджа',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Item',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Item',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Item',
                                ),
                        ),
                ),
            'costChanging' =>
                array (
                    'title' => 'Стоимость чейнджа',
                    'placeholder' => '1000',
                ),
            'leaseTermTo' =>
                array (
                    'title' => 'Срок аренды объекта недвижимости до',
                    'placeholder' => '25.02.2020',
                ),
            'transferWorkSchemes' =>
                array (
                    'title' => 'Передаете ли схемы работы?',
                ),
            'monthTeach' =>
                array (
                    'title' => 'Сколько времени будете обучать?',
                    'placeholder' => '1',
                    'tooltip' => 'tooltip text',
                    'prevText' => 'Месяцев',
                ),
            'readyTrainingMaterials' =>
                array (
                    'title' => 'Готовы ли обучающие материалы?',
                ),
            'typeTrainingMaterials' =>
                array (
                    'title' => 'Тип обучающих материалов',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Электронный',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Бумажный',
                                ),
                        ),
                ),
            'transferCustomerBase' =>
                array (
                    'title' => 'Передаёте базы клиентов?',
                ),
            'typeTransferCustomerBase' =>
                array (
                    'title' => 'Тип передачи клиентской базы',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Excel',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Бумажная',
                                ),
                        ),
                ),
            'haveContractors' =>
                array (
                    'title' => 'Есть ли контрагенты?',
                ),
            'contractors' =>
                array (
                    'title' => 'Контрагенты',
                    'remove' => 'Удалить этого контрагента',
                    'add' => 'Добавить контрагента',
                    'direction' =>
                        array (
                            'title' => 'Направление контрагента',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => 'Из интернета',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => 'Из обявленний в прессе',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => 'По рекомендации',
                                        ),
                                ),
                        ),
                    'name' =>
                        array (
                            'title' => 'Название юридического лица',
                            'placeholder' => 'Укажите название',
                        ),
                    'phone' =>
                        array (
                            'title' => 'Телефон',
                            'placeholder' => '+48 456 566-09-23',
                        ),
                    'mail' =>
                        array (
                            'title' => 'Электронная почта',
                            'placeholder' => 'agent@mail.com',
                        ),
                ),
            'staff' =>
                array (
                    'remove' => 'Удалить этого сотрудника',
                    'add' => 'Добавить сотрудника',
                    'title' => 'Персонал',
                    'post' =>
                        array (
                            'title' => 'Должность',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => 'Управленческий персонал',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => 'Административный персонал',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => 'Менаджеры высшей степении',
                                        ),
                                    3 =>
                                        array (
                                            'id' => '4',
                                            'name' => 'Рабочие занятые в производственном процессе',
                                        ),
                                    4 =>
                                        array (
                                            'id' => '5',
                                            'name' => 'Водители / доставчики',
                                        ),
                                    5 =>
                                        array (
                                            'id' => '6',
                                            'name' => 'Персонал обслуживающий клиентов',
                                        ),
                                ),
                        ),
                    'termsContractDismissal' =>
                        array (
                            'title' => 'Условия договора об увольнении',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => '3 дня',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => '1 неделя ',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => '2 недели',
                                        ),
                                    3 =>
                                        array (
                                            'id' => '4',
                                            'name' => '1 месяц',
                                        ),
                                    4 =>
                                        array (
                                            'id' => '5',
                                            'name' => '3 месяца',
                                        ),
                                ),
                        ),
                    'monthlyWages' =>
                        array (
                            'title' => 'Месячная заработная плата',
                            'placeholder' => '1000',
                        ),
                    'taxAmountPerMonth' =>
                        array (
                            'title' => 'Сумма налогов, в месяц',
                            'placeholder' => '1000',
                        ),
                ),
            'leavingStaff' =>
                array (
                    'remove' => 'Удалить этого сотрудника',
                    'add' => 'Добавить сотрудника',
                    'title' => 'Персонал который уйдет',
                    'post' =>
                        array (
                            'title' => 'Профессиональная должность',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => 'Управленческий персонал',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => 'Административный персонал',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => 'Менеджеры высшего звена',
                                        ),
                                    3 =>
                                        array (
                                            'id' => '4',
                                            'name' => 'Рабочие занятые в производственном процессе',
                                        ),
                                    4 =>
                                        array (
                                            'id' => '5',
                                            'name' => 'Водители / курьеры',
                                        ),
                                    5 =>
                                        array (
                                            'id' => '6',
                                            'name' => 'Персонал обслуживающий клиентов',
                                        ),
                                ),
                        ),
                    'termsContractDismissal' =>
                        array (
                            'title' => 'Условия договора об увольнении',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => '3 дня',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => '1 неделя ',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => '2 недели',
                                        ),
                                    3 =>
                                        array (
                                            'id' => '4',
                                            'name' => '1 месяц',
                                        ),
                                    4 =>
                                        array (
                                            'id' => '5',
                                            'name' => '3 месяца',
                                        ),
                                ),
                        ),
                    'monthlyWages' =>
                        array (
                            'title' => 'Месячная заработная плата',
                            'placeholder' => '1000',
                        ),
                    'taxAmountPerMonth' =>
                        array (
                            'title' => 'Сумма налогов, в месяц',
                            'placeholder' => '1000',
                        ),
                ),
            'willStaffLeave' =>
                array (
                    'title' => 'Уйдет ли персонал за собственником?',
                ),
            'havePurchasedServices' =>
                array (
                    'title' => 'Есть ли выкупленные долгосрочные услуги?',
                ),
            'neededLicenses' =>
                array (
                    'title' => 'Нужны ли сертификаты, лицензии, разрешения по виду деятельности?',
                ),
            'certificates' =>
                array (
                    'title' => 'Услуги и сертификаты',
                    'remove' => 'Удалить этот сертификат',
                    'add' => 'Добавить сертификат',
                    'document' =>
                        array (
                            'title' => 'Укажите вид трудового договора',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => 'Договор на труд',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => 'Гражданско правовой договор',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => 'Договор подряда',
                                        ),
                                ),
                        ),
                    'type' =>
                        array (
                            'title' => 'Укажите тип сертификата или разрешительного документа',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => 'Item',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => 'Item',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => 'Item',
                                        ),
                                ),
                        ),
                    'licensePeriod' =>
                        array (
                            'title' => 'Срок действия лицензии или разрешительного документа',
                            'placeholder' => '25.02.2020',
                        ),
                ),
            'techDocEquipmentCD' =>
                array (
                    'title' => 'Техническая документация на оборудование (чеки или договор)?',
                    'placeholder' => 'Выберите',
                ),
            'techDocEquipment' =>
                array (
                    'title' => 'Техническая документация на оборудование?',
                    'placeholder' => 'Выберите',
                ),
            'conditionProperty' =>
                array (
                    'title' => 'Состояние имущества (по оценке собственника)',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Очень хорошее',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Хорошее',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Средние',
                                ),
                            3 =>
                                array (
                                    'id' => '4',
                                    'name' => 'Плохое - нужно менять или ремонтировать',
                                ),
                        ),
                ),
            'conditionEquipment' =>
                array (
                    'title' => 'Состояние оборудования, транспорта, техники (по оценке собственника)',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Очень хорошее',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Хорошее - минималный финансовый вклад',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Хорошее - минималный финансовый вклад',
                                ),
                            3 =>
                                array (
                                    'id' => '4',
                                    'name' => 'Хорошее - минималный финансовый вклад',
                                ),
                            4 =>
                                array (
                                    'id' => '5',
                                    'name' => 'Среднее - требует немедленного финансового вклада',
                                ),
                            5 =>
                                array (
                                    'id' => '6',
                                    'name' => 'Плохое - требует немедленного ремонта или немедленной замены',
                                ),
                        ),
                ),
            'pledgeEquipment' =>
                array (
                    'title' => 'Залог на оборудование, транспорт и дугие материльные активы?',
                    'placeholder' => 'Выберите',
                    'noResult' => 'Ничего не найдено',
                ),
            'listTransferredProperty' =>
                array (
                    'title' => 'Перечень имущества передаваемого продавцом',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Настольный компьютеры',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Мониторы',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Лаптопы',
                                ),
                            3 =>
                                array (
                                    'id' => '4',
                                    'name' => 'Принтеры',
                                ),
                            4 =>
                                array (
                                    'id' => '5',
                                    'name' => 'Автофургон, вес до 3,5 тоны',
                                ),
                            5 =>
                                array (
                                    'id' => '6',
                                    'name' => 'Автофургон, вес oт 3,5 до 15 тон',
                                ),
                            6 =>
                                array (
                                    'id' => '7',
                                    'name' => 'Грузовой автомобиль, вес более  15 тонн',
                                ),
                        ),
                ),
            'deductionsDepreciation' =>
                array (
                    'title' => 'Отчисления на амортизцию',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Осуществляются',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Не осуществляются',
                                ),
                        ),
                ),
            'amountDeductionsProfits' =>
                array (
                    'title' => 'Объем отчислений от прибыли',
                    'placeholder' => '75',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'saleSite' =>
                array (
                    'title' => 'Продается ли сайт?',
                    'placeholder' => 'Выберите',
                ),
            'yearCreationSite' =>
                array (
                    'title' => 'Год создания сайта',
                    'placeholder' => '2010',
                ),
            'cms' =>
                array (
                    'title' => 'Система управления сайтом',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'WordPress',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Joomla',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Drupal',
                                ),
                        ),
                ),
            'themeSite' =>
                array (
                    'title' => 'Тематика сайта',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Туризм',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Торговля',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Недвижимость',
                                ),
                        ),
                ),
            'salePhone' =>
                array (
                    'title' => 'Продается ли телефон?',
                    'placeholder' => 'Выберите',
                ),
            'phone' =>
                array (
                    'title' => 'Номер телефона',
                    'placeholder' => '880005553535',
                    'tooltip' => 'tooltip text',
                ),
            'intellectualProperty' =>
                array (
                    'title' => 'Интеллектуальная собственность?',
                    'placeholder' => 'Выберите',
                ),
            'intellectualPropertyList' =>
                array (
                    'title' => 'Укажите объекты интеллектуальной собственности',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Авторские права',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Know how',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Патенты',
                                ),
                            3 =>
                                array (
                                    'id' => '4',
                                    'name' => 'Торговая марка',
                                ),
                            4 =>
                                array (
                                    'id' => '5',
                                    'name' => 'Промышленный дизайн (полезная модель)',
                                ),
                        ),
                ),
            'visitorsPerDay' =>
                array (
                    'title' => 'Посетителей в день',
                    'placeholder' => '18 300',
                    'tooltip' => 'tooltip text',
                ),
            'viewsPerDay' =>
                array (
                    'title' => 'Просмотров в день',
                    'placeholder' => '12 300',
                    'tooltip' => 'tooltip text',
                ),
            'natureTraffic' =>
                array (
                    'title' => 'Характер трафика',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Естественный, за счет перехода посетителей со страниц поисковых систем',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Платный, за счет переходов по оплаченным ссылкам.',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Прямой, за счет переходов посетителей    непосредственно на сайт',
                                ),
                        ),
                ),
            'sourcesTraffic' =>
                array (
                    'title' => 'Источники трафика',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Google Adwords',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Facebook Ads',
                                ),
                        ),
                ),
            'sourceGoogle' =>
                array (
                    'title' => 'Источник: Google',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                ),
            'sourceYandex' =>
                array (
                    'title' => 'Источник: Яндекс',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                ),
            'additionalInformationTraffic' =>
                array (
                    'title' => 'Дополнительная информация по трафику',
                    'placeholder' => 'Приведите дополнительную информацию по вашему трафику',
                    'tooltip' => 'tooltip text',
                ),
            'haveIncomeSite' =>
                array (
                    'title' => 'Получаете ли доход с сайта?',
                    'placeholder' => 'Выберите',
                ),
            'averageMonthlyIncome' =>
                array (
                    'title' => 'Среднемесячный доход',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'sourcesIncome' =>
                array (
                    'title' => 'Источники дохода',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Item',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Item',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Item',
                                ),
                        ),
                ),
            'contextualAdvertising' =>
                array (
                    'title' => 'Контекстная реклама',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'contextualAdvertisingGAdsens' =>
                array (
                    'title' => 'Из них Google Adsens',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'bannerTizerNetworks' =>
                array (
                    'title' => 'Баннерные и тизирные сети',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'cpa' =>
                array (
                    'title' => 'CPA',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'banMonetizationSystems' =>
                array (
                    'title' => 'Бан в системах монетизации',
                ),
            'additionalInformationIncome' =>
                array (
                    'title' => 'Дополнительная информация по доходам',
                    'placeholder' => 'Предоставьте дополнительную информацию по доходам',
                    'tooltip' => 'tooltip text',
                ),
            'averageMonthlyExpensesSite' =>
                array (
                    'title' => 'Среднемесячный расход',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'categoriesExpensesSite' =>
                array (
                    'title' => 'Категории расходов',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Хостинг и администрирование',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Контент',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'SEO-продвижение',
                                ),
                        ),
                ),
            'contentExpensesSite' =>
                array (
                    'title' => 'Контент',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'hostingExpensesSite' =>
                array (
                    'title' => 'Хостинг и администрирование',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'otherExpensesSite' =>
                array (
                    'title' => 'Иные расходы',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'additionalInformationExpensesSite' =>
                array (
                    'title' => 'Дополнительная информация по расходам',
                    'placeholder' => 'Опишите дополнительную информацию по расходам',
                    'tooltip' => 'tooltip text',
                ),
            'siteDesign' =>
                array (
                    'title' => 'Дизайн сайта',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Уникальный',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Уникальный с шаблона',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Шаблон',
                                ),
                        ),
                ),
            'imgSiteOwn' =>
                array (
                    'title' => 'Собственные',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'imgSiteOtherSources' =>
                array (
                    'title' => 'Из других источников',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'textSiteCopyright' =>
                array (
                    'title' => 'Копирайт',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'textSiteManualRewrite' =>
                array (
                    'title' => 'Ручной рерайт',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'textSiteSavePaste' =>
                array (
                    'title' => 'Копи-паст',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'textSiteGenerated' =>
                array (
                    'title' => 'Генерированый',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'additionalInformationContentSite' =>
                array (
                    'title' => 'Дополнительная информация по контенту и дизайну',
                    'placeholder' => 'Опишите дополнительную информацию по контенту и дизайну',
                    'tooltip' => 'tooltip text',
                ),
            'saleGroupsSocNet' =>
                array (
                    'title' => 'Передаются ли группы в социальных сетях?',
                    'placeholder' => 'Выберите',
                ),
            'socNetList' =>
                array (
                    'title' => 'Укажите социальные сети',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Facebook',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Vkontakte',
                                ),
                        ),
                ),
            'socNetItems' =>
                array (
                    'title' => 'Группы в социальных сетях',
                    'link' =>
                        array (
                            'title' => 'Ссылка на группу',
                            'placeholder' => 'https://',
                            'tooltip' => 'tooltip text',
                        ),
                    'countUsers' =>
                        array (
                            'title' => 'Объём пользователей группы',
                            'placeholder' => '11 000',
                            'tooltip' => 'tooltip text',
                        ),
                    'haveIncome' =>
                        array (
                            'title' => 'Получаете ли доход с группы?',
                            'placeholder' => 'Выберите',
                        ),
                    'incomeYear' =>
                        array (
                            'title' => 'Доход с группы в год',
                            'placeholder' => '1000',
                            'tooltip' => 'tooltip text',
                            'prevText' => '€',
                        ),
                    'incomeQuarter' =>
                        array (
                            'title' => 'Доход с группы в квартал ',
                            'placeholder' => '1000',
                            'tooltip' => 'tooltip text',
                            'prevText' => '€',
                        ),
                    'turnoverYear' =>
                        array (
                            'title' => 'Оборот с группы в год',
                            'placeholder' => '1000',
                            'tooltip' => 'tooltip text',
                            'prevText' => '€',
                        ),
                    'turnoverQuarter' =>
                        array (
                            'title' => 'Оборот с группы в квартал ',
                            'placeholder' => '1000',
                            'tooltip' => 'tooltip text',
                            'prevText' => '€',
                        ),
                    'expensesYear' =>
                        array (
                            'title' => 'Расходы с группы в год',
                            'placeholder' => '1000',
                            'tooltip' => 'tooltip text',
                            'prevText' => '€',
                        ),
                    'expensesQuarter' =>
                        array (
                            'title' => 'Расходы с группы в квартал ',
                            'placeholder' => '1000',
                            'tooltip' => 'tooltip text',
                            'prevText' => '€',
                        ),
                    'themes' =>
                        array (
                            'title' => 'Тематика паблика',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => 'Развлечения',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => 'Fashion',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => 'Информационные',
                                        ),
                                ),
                        ),
                    'lang' =>
                        array (
                            'title' => 'Языковой сегмент группы',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => 'Item',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => 'Item',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => 'Item',
                                        ),
                                ),
                        ),
                    'methodTransferGroup' =>
                        array (
                            'title' => 'Способ передачи группы',
                            'placeholder' => 'Выберите',
                            'options' =>
                                array (
                                    0 =>
                                        array (
                                            'id' => '1',
                                            'name' => 'Item',
                                        ),
                                    1 =>
                                        array (
                                            'id' => '2',
                                            'name' => 'Item',
                                        ),
                                    2 =>
                                        array (
                                            'id' => '3',
                                            'name' => 'Item',
                                        ),
                                ),
                        ),
                ),
            'typeAudience' =>
                array (
                    'title' => 'Тип аудитории на которую расчитан бизнес',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'B2C',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'B2B',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'B2C - B2B',
                                ),
                        ),
                ),
            'genderTargetAudience' =>
                array (
                    'title' => 'Пол целевой аудитории',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Мужской',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Женский',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Не имеет значения',
                                ),
                        ),
                ),
            'ageMenFrom' =>
                array (
                    'title' => 'Возраст мужчин',
                    'placeholder' => '20',
                    'tooltip' => 'tooltip text',
                ),
            'ageMenTo' =>
                array (
                    'title' => 'Возраст мужчин',
                    'placeholder' => '48',
                    'tooltip' => 'tooltip text',
                ),
            'ageWomenFrom' =>
                array (
                    'title' => 'Возраст женщин',
                    'placeholder' => '20',
                    'tooltip' => 'tooltip text',
                ),
            'ageWomenTo' =>
                array (
                    'title' => 'Возраст женщин',
                    'placeholder' => '48',
                    'tooltip' => 'tooltip text',
                ),
            'sexRatio' =>
                array (
                    'title' => 'Соотношение полов',
                    'value' => '50',
                ),
            'familyStatusClients' =>
                array (
                    'title' => 'Семейный статус клиентов',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Одинокие взрослые',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Бездетные пары',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Семейные пары с детьми',
                                ),
                        ),
                ),
            'aloneClients' =>
                array (
                    'title' => 'Доля одиноких',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'couplesClients' =>
                array (
                    'title' => 'Доля бездетных пар',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'familiesWithChildrenClients' =>
                array (
                    'title' => 'Доля семейных пар с детьми',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'socialStatusClients' =>
                array (
                    'title' => 'Социальные статусы клиентов',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Средний класс',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Студенты',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Пенсионеры',
                                ),
                        ),
                ),
            'childClients' =>
                array (
                    'title' => 'Доля детей',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'studentsClients' =>
                array (
                    'title' => 'Доля студентов',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'pensionersClients' =>
                array (
                    'title' => 'Доля пенсионеров',
                    'placeholder' => '50',
                    'tooltip' => 'tooltip text',
                    'prevText' => '%',
                ),
            'averageIncomeTargetClientsFrom' =>
                array (
                    'title' => 'Средний уровень доходов целевых клиентов',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                ),
            'averageIncomeTargetClientsTo' =>
                array (
                    'title' => 'Средний уровень доходов целевых клиентов',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                ),
            'averageCheckClients' =>
                array (
                    'title' => 'Средний чек клиента',
                    'placeholder' => '1000',
                    'tooltip' => 'tooltip text',
                    'prevText' => '€',
                ),
            'mainAdvertisingSourcesAttractClients' =>
                array (
                    'title' => 'Основные рекламные источники привлечение клиентов',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Наружная реклама',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Медийная реклама',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Интернет-реклама',
                                ),
                        ),
                ),
            'mainCategoryBusinessPartners' =>
                array (
                    'title' => 'Основная категория партнёров по бизнесу',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Item',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Item',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Item',
                                ),
                        ),
                ),
            'haveExistingContracts' =>
                array (
                    'title' => 'Наличие действующих контрактов',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Item',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Item',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Item',
                                ),
                        ),
                ),
            'listContracts' =>
                array (
                    'title' =>
                        array (
                            'title' => 'Название контракта №',
                            'placeholder' => 'Укажите название контракта',
                            'tooltip' => 'tooltip text',
                        ),
                    'finalDate' =>
                        array (
                            'title' => 'Срок действия контракта №',
                            'tooltip' => 'tooltip text',
                            'placeholder' => '25.02.2020',
                        ),
                ),
            'countPerpetualServiceContracts' =>
                array (
                    'title' => 'Количество бессрочных договров на оказания улсуг',
                    'placeholder' => '4',
                    'tooltip' => 'tooltip text',
                ),
            'legalStatus' =>
                array (
                    'title' => 'Юридический статус',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Общество с ограниченной ответственностью',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Акционерное общество',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Коммандинтное товарищество (коммандитное общество)',
                                ),
                        ),
                ),
            'taxSystem' =>
                array (
                    'title' => 'Система налогообложения',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Подоходный налог для физических лиц (PIT общие правила)',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Подоходный налог для юридических лиц (CIT)',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Налог на добавленную стоимость (VAT)',
                                ),
                        ),
                ),
            'changesProfileLegalEntity' =>
                array (
                    'title' => 'Были изменения профиля деятельности в юридическом лице?',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Item',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Item',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Item',
                                ),
                        ),
                ),
            'havePenalties' =>
                array (
                    'title' => 'Непокрытые штрафные санкции, наложенные фискальными органами',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Item',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Item',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Item',
                                ),
                        ),
                ),
            'listPenalties' =>
                array (
                    'title' => 'Укажите штрафные санкции',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Административно-хозяйственные санкции',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Оперативно-хозяйственные санкции',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Неустойка по долговым обязательствам',
                                ),
                        ),
                ),
            'haveDisputableSituations' =>
                array (
                    'title' => 'Вовлечение в спорные коммерческие/административные ситуации?',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Item',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Item',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Item',
                                ),
                        ),
                ),
            'listDisputableSituations' =>
                array (
                    'title' => 'Укажите типы спорных ситуаций',
                    'placeholder' => 'Выберите',
                    'options' =>
                        array (
                            0 =>
                                array (
                                    'id' => '1',
                                    'name' => 'Судебный гражданский иск в качестве истца',
                                ),
                            1 =>
                                array (
                                    'id' => '2',
                                    'name' => 'Судебный хозяйственный иск в качестве истца',
                                ),
                            2 =>
                                array (
                                    'id' => '3',
                                    'name' => 'Арбитражный иск в качестве истца',
                                ),
                        ),
                ),
        ),
    'v_filter' =>
        array (
            'to' => 'до',
            'type_invest' => 'Тип инвестирования',
            'select_type' => 'Выберите тип',
            'enter_price' => 'Укажите цену',
            'price' => 'Цена',
            'enter_profit' => 'Желаемая прибыль',
            'net_profit_per_month' => 'Чистая прибыль в месяц',
            'number_of_months' => 'Количество месяцев',
            'payback_in_months' => 'Окупаемость в месяцах',
            'name_or_id' => 'Название или ID',
            'btn_reset_search' => 'Сбросить поиск',
            'btn_find' => 'Найти бизнес',
            'typeOptions' =>
                array (
                    'type_1' => 'Бизнес в продаже',
                    'type_2' => 'Бизнес в аренде',
                ),
            'range_slider' =>
                array (
                    'btn_apply' => 'Применить',
                    'btn_clear' => 'Очистить',
                    'from' => 'от',
                    'to' => 'до',
                ),
        ),
    'v_list' =>
        array (
            'payback' => 'окупаемость',
            'months' => 'месяцев',
            'recommended' => 'Рекомендуется',
            'sale' => 'Продано',
            'add_to_favorites' => 'Добавить в избранное',
            'favorites' => 'Избранное',
            'location' => 'Местоположение',
            'category' => 'Категория',
            'profit' => 'Выручка',
            'net_profit' => 'Прибыль',
            'perc_profit' => 'Доходность',
            'link_more_business' => 'Подробнее о бизнесе',
            'link_more_deal' => 'Подробнее о сделке',
            'btn_prev_page' => 'Предыдущая страница',
            'btn_next_page' => 'Следующая страница',
        ),
);
