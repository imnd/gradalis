<?php
return [
    'create' => [
        'choose' => 'Выберите',
        'help' => '<span class="has-text-weight-bold">Не хотите заполнять?</span>&nbsp;<span>Наш менеджер поможет вам!</span>',
        'btn_more' => 'Подробнее',
        'photos' => 'Фотографии',
        'sellSuccess' => 'Успех на продажу',
        'addresses' => [
            'title' => 'Адреса',
        ],
        'objects' => [
            'title' => 'Объекты',
        ],
        'b2c' => [
            'title' => 'B2C',
        ],
        'b2b' => [
            'title' => 'B2B',
        ],
        'title' => 'Продать',
        'main_info' => 'Основная информация',
        'finance_info' => 'Финансовые показатели',
        'property_requirements' => 'Требования к недвижимости',
        'business_processes' => 'Бизнес-процессы',
        'equipment' => 'Оборудование',
        'information' => 'Информация',
        'target_audience' => 'Целевая аудитория',
        'legal_state' => 'Юридическое состояние',
        'ad_placement' => 'Размещение объявления',
        'goToPostingAds' => 'Перейти к публикации объявления',
        'sold_success' => 'Успех на продажу',
        'add_package' => 'Добавить пакет',
        'remove_package' => 'Удалить пакет',
        'add_news' => 'Добавить новость',
        'remove_news' => 'Удалить новость',
        'add_review' => 'Добавить отзыв',
        'remove_review' => 'Удалить отзыв',
        'add_object' => 'Добавить объект',
        'remove_object' => 'Удалить объект',
        'yn' => [
            [
                'id' => 1,
                'name' => 'Да',
            ],
            [
                'id' => 2,
                'name' => 'Нет',
            ],
        ],
        'ny' => [
            'placeholder' => 'Выберите',
            'options' =>[
                [
                    'id' => 0,
                    'name' => 'Нет',
                ],
                [
                    'id' => 1,
                    'name' => 'Да',
                ]
            ]
        ],
        'have' => [
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Есть',
                ],
                [
                    'id' => 2,
                    'name' => 'Нет',
                ],
            ],
        ],
        'name' => [
            'title' => 'Название',
            'placeholder' => 'Укажите название',
        ],
        'category' => [
            'title' => 'Категория',
            'placeholder' => 'Выберите',
        ],
        'subcategory' => [
            'title' => 'Направление',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Торговля (дистрибуция)',
                ],
                [
                    'id' => 2,
                    'name' => 'Услуги',
                ],
                [
                    'id' => 3,
                    'name' => 'Производство',
                ],
                [
                    'id' => 4,
                    'name' => 'Общественное питание',
                ],
            ],
            'error' => [
                'on_empty_category' => 'Чтобы выбрать направление вам нужно сначало выбрать категорию'
            ]
        ],
        'foundationYear' => [
            'title' => 'Год основания',
            'placeholder' => '2007',
        ],
        'condition' => [
            'title' => 'Дополнительные платежи по договору франчайзинга',
            'list' => [
                [
                    'title' => 'Без дополнительной платы',
                    'value' => '0',
                ],
                [
                    'title' => 'Роялти',
                    'value' => 1,
                ],
                [
                    'title' => 'Паушальный взнос',
                    'value' => 2,
                ],
            ],
        ],
        'lump_sum' => [
            'title' => 'Сумма паушального взноса',
            'placeholder' => '10000',
        ],
        'video' => [
            'video_exists' => [
                'title' => 'Имеет ли франшиза видеообзор?',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 0,
                        'name' => 'Нет',
                    ],
                    [
                        'id' => 1,
                        'name' => 'Да',
                    ],
                ],
            ],
            'video_title' => [
                'title' => 'Название видеообзора',
                'placeholder' => 'Укажите название видеообзора',
            ],
            'video_url' => [
                'title' => 'Ссылка на видеобзор',
                'placeholder' => 'http://',
            ]
         ],
        'form_payment' => [
            'title' => 'Условия оплаты при покупке франшизы',
            'list' => [
                [
                    'title' => 'Вся сумма одним взносом',
                    'value' => '0',
                ],
                [
                    'title' => 'В рассрочку',
                    'value' => 1,
                ],
            ],
        ],
        'payment_term' => [
            'title' => 'Срок оплаты',
            'placeholder' => 'Срок оплаты в месяцах',
        ],
        'royalty_rate' => [
            'title' => 'Размер роялти',
            'placeholder' => '75',
        ],
        'royalty_rate_period' => [
            'title' => 'Периодичность выплаты роялти',
            'list' => [
                [
                    'title' => 'В месяц',
                    'value' => 1,
                ],
                [
                    'title' => 'В год',
                    'value' => 2,
                ],
            ],
        ],
        'totalCost' => [
            'title' => 'Общая стоимость',
            'placeholder' => '1 000',
            'prevText' => 'руб',
        ],
        'price_including_vat_bool' => [
            'title' => 'Цена с учетом НДС',
        ],
        'price_including_vat' => [
            'title' => 'Цена с учетом НДС',
        ],
        'discount' => [
            'title' => 'Скидка на покупку франшизы',
            'placeholder' => 'скидка на покупку франшизы',
        ],
        'have_packages' => [
            'title' => 'Имеет ли разные пакеты при продаже',
            'placeholder' => 'Выберите',
        ],
        'packages' => [
            'name' => [
                'title' => 'Название пакета',
                'placeholder' => 'Базовый',
            ],
            'cost' => [
                'title' => 'Стоимость',
                'placeholder' => '1 000',
                'prevText' => 'руб',
            ],
            'packageInclude' => [
                'title' => 'Что входит',
                'placeholder' => 'каждый пункт введите с новой строки',
                'options' => [
                ],
            ],
            'purchaseTerms' => [
                'title' => 'Условия покупки',
                'list' => [
                    [
                        'title' => 'Без ничего',
                        'value' => 0,
                    ],
                    [
                        'title' => 'Роялти',
                        'value' => 1,
                    ],
                    [
                        'title' => 'Паушальный взнос',
                        'value' => 2,
                    ],
                ],
            ],
            'packageBenefits' => [
                'title' => 'Преимущества пакета',
                'placeholder' => 'Описание пакета',
            ],
        ],
        'subcat_id' => [
            'title' => 'Подкатегория (направление)',
            'placeholder' => 'Укажите направление',
        ],
        'name_legal_entity' => [
            'title' => 'Наименование юридического лица (приватное поле, не для публикации)',
            'placeholder' => 'Введите название юридического лица',
        ],
        'name_business' => [
            'title' => 'Название бизнеса',
            'placeholder' => 'Введите название бизнеса',
        ],
        'name_franchise' => [
            'title' => 'Название франшизы',
            'placeholder' => 'Введите название франшизы',
        ],
        'logoCompany' => [
            'title' => 'Логотип компании',
        ],
        'descriptionFranchise' => [
            'title' => 'Описание франшизы',
            'placeholder' => 'Опишите Вашу франишизу',
        ],
        'netProfitMonth' => [
            'title' => 'Чистая прибыль в месяц',
            'placeholder' => '1 000',
            'prevText' => 'руб',
        ],
        'payback' => [
            'title' => 'Окупаемость',
            'placeholder' => '16',
            'prevText' => 'Месяцев',
        ],
        'turnover_month' => [
            'title' => 'Квартальный оборот (в месяц)',
            'placeholder' => '1 000',
            'prevText' => 'руб',
        ],
        'turnover_year' => [
            'title' => 'Годовой оборот',
            'placeholder' => '1000',
            'prevText' => 'руб',
        ],
        'general_expenses' => [
            'title' => 'Общие расходы по запуску',
            'placeholder' => '1000',
            'prevText' => 'руб',
        ],
        'month_expenses' => [
            'title' => 'Расходы в квартал',
            'placeholder' => '1000',
            'prevText' => 'руб',
        ],
        'staff_costs' => [
            'title' => 'Ежемесячные затраты на персонал',
            'placeholder' => '1000',
            'prevText' => 'руб',
        ],
        'property_costs' => [
            'title' => 'Расходы на эксплуатацию объектов недвижимости ',
            'placeholder' => '1000',
            'prevText' => 'руб',
        ],
        'cost_business_per_year' => [
            'title' => 'Затраты на коммерческую деятельность в год',
            'placeholder' => '1000',
            'prevText' => 'руб',
        ],
        'cost_business_per_quarter' => [
            'title' => 'Затраты на коммерческую деятельность в квартал',
            'placeholder' => '1000',
        ],
        'launch_dates' => [
            'title' => 'Сроки запуска',
            'placeholder' => '16',
            'prevText' => 'Месяцев',
        ],
        'return_payback' => [
            'title' => 'Выход на окупаемость',
            'placeholder' => '16',
            'prevText' => 'Месяцев',
        ],
        'exit_operts_profit' => [
            'title' => 'Выход на операционную прибыль',
            'placeholder' => '16',
            'prevText' => 'Месяцев',
        ],
        'justification_financial_indicators' => [
            'title' => 'Обоснование финансовых показателей',
            'placeholder' => 'Опишите обоснование финансовых показателей',
        ],
        'propertyCategories' => [
            'title' => 'Тип собственности объекта',
            'placeholder' => 'Выберите',
        ],
        'propertyType' => [
            'title' => 'Тип недвижимости',
            'placeholder' => 'Выберите',
        ],
        'number_square_meters' => [
            'title' => 'Количество кв. метров',
            'placeholder' => '12',
            'prevText' => 'м²',
        ],
        'price_square_meters' => [
            'title' => 'Цена за кв. метр',
            'title2' => 'Цена за кв. метр в месяц',
            'placeholder' => '1000',
        ],
        'title_documents' => [
            'title' => 'Правоустанавливающие документы',
        ],
        'restrictions_operation' => [
            'title' => 'Ограничения по эксплуатации',
        ],
        'list_restrictions_operation' => [
            'title' => 'Тип недопустимых ограничений по эксплуатации',
            'placeholder' => 'Выберите',
        ],
        'ref_register_estate' => [
            'title' => 'Справка из регистра недв. и зданий',
        ],
        'technical_property_plan' => [
            'title' => 'Технический план недвижимости',
        ],
        'coordination_redevelopment' => [
            'title' => 'Согласование перепланировок',
        ],
        'neededLicenses' => [
            'title' => 'Нужны ли сертификаты, лицензии, разрешения по виду деятельности?',
        ],
        'licensesAndCertificates' => [
            'title' => 'Лицензии и сертиификаты',
        ],
        'included_equipment' => [
            'title' => 'Включение стоимости оборудования в сумму контракта по покупке франшизы',
        ],
        'equipmentList' => [
            'title' => 'Перечень оборудования входящего в стоимость покупки',
            'placeholder' => 'Выберите'
        ],
        'condition_equipment' => [
            'title' => 'Состояние оборудования (по оценке собственника)',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Ждем справочник  :)',
                ],
                [
                    'id' => 2,
                    'name' => 'Я тоже жду... ',
                ],
                [
                    'id' => 3,
                    'name' => 'И я! :)',
                ],
            ],
        ],
        'discount_equipment' => [
            'title' => 'Скидка на покупку оборудования',
        ],
        'total_amount_equipment' => [
            'title' => 'Затраты на покупку оборудования',
            'placeholder' => '1 000',
            'prevText' => 'руб',
        ],
        'total_amount_equipment_with_discount' => [
            'title' => 'Затраты на покупку оборудования с учетом скидки',
            'placeholder' => '1 000',
            'prevText' => 'руб',
        ],
        'staff_training_equipment' => [
            'title' => 'Обучение персонала работе на оборудовании',
        ],
        'news' => [
            'title' => 'Новости',
        ],
        'newsName' => [
            'title' => 'Название',
            'placeholder' => 'Название новости',
        ],
        'newsDate' => [
            'title' => 'Дата публикации',
            'placeholder' => '01.01.2010',
        ],
        'newsPhotos' => [
            'title' => 'Фотографии новости',
        ],
        'newsText' => [
            'title' => 'Текст новости',
            'placeholder' => 'Добавьте текст новости',
        ],
        'reviews' => [
            'title' => 'Отзывы',
        ],
        'reviewsYourName' => [
            'title' => 'Имя',
            'placeholder' => 'Введите Ваше имя',
        ],
        'reviewsYourCompany' => [
            'title' => 'Компания',
            'placeholder' => 'Название компании',
        ],
        'email' => [
            'title' => 'Email',
            'placeholder' => 'Введите email',
        ],
        'position' => [
            'title' => 'Должность',
            'placeholder' => 'Введите название',
        ],
        'linkVideoReview' => [
            'title' => 'Ссылка на видео отзыв',
            'placeholder' => 'http://',
        ],
        'yourPhoto' => [
            'title' => 'Ваше фото',
        ],
        'textReview' => [
            'title' => 'Текст отзыва',
            'placeholder' => 'Добавьте текст отзыва',
        ],
        'photoAndDocs' => [
            'title' => 'Фотографии и документы',
        ],
        'objectsTitle' => [
            'title' => 'Название',
            'placeholder' => 'Введите название',
        ],
        'contactPhone' => [
            'title' => 'Телефон',
            'placeholder' => 'Контактный телефон',
        ],
        'addressGoogleMaps' => [
            'title' => 'Адреc на Google Maps',
            'placeholder' => 'Poland, Warsaw, Zamkowy Square 1/13, 00-262',
        ],
        'objectsDesc' => [
            'title' => 'Описание',
            'placeholder' => 'Описание объекта',
        ],
        'objectsPhotos' => [
            'title' => 'Фотографии объекта',
        ],
        'audienceTypes' => [
            'title' => 'Тип аудитории на которую расcчитан франшиза',
            'placeholder' => 'Выберите',
        ],
        'gender_target_audience' => [
            'title' => 'Пол целевой аудитории',
            'placeholder' => 'Выберите',
        ],
        'age_men' => [
            'title' => 'Возраст мужчин',
        ],
        'age_men_from' => [
            'title' => 'Возраст мужчин',
            'placeholder' => '20',
            'tooltip' => 'tooltip text',
        ],
        'age_men_to' => [
            'title' => 'Возраст мужчин',
            'placeholder' => '48',
            'tooltip' => 'tooltip text',
        ],
        'age_women' => [
            'title' => 'Возраст женщин',
        ],
        'age_women_from' => [
            'title' => 'Возраст женщин',
            'placeholder' => '20',
            'tooltip' => 'tooltip text',
        ],
        'age_women_to' => [
            'title' => 'Возраст женщин',
            'placeholder' => '48',
            'tooltip' => 'tooltip text',
        ],
        'sex_ratio' => [
            'title' => 'Соотношение полов',
            'value' => '50',
        ],
        'family_status_clients' => [
            'title' => 'Семейный статус клиентов',
            'placeholder' => 'Выберите',
        ],
        'alone_clients' => [
            'title' => 'Соотношение одиноких',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'couples_clients' => [
            'title' => 'Соотношение пар',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'families_with_children_clients' => [
            'title' => 'Соотношение семей с детьми',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'social_status_clients' => [
            'title' => 'Социальные статусы клиентов',
            'placeholder' => 'Выберите',
        ],
        'child_clients' => [
            'title' => 'Соотношение детей',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'students_clients' => [
            'title' => 'Соотношение студентов',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'pensioners_clients' => [
            'title' => 'Соотношение пенсонеров',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'average_income_target_clients_from' => [
            'title' => 'Средний уровень доходов целевых клиентов',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
        ],
        'average_income_target_clients_to' => [
            'title' => 'Средний уровень доходов целевых клиентов',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
        ],
        'average_check_clients' => [
            'title' => 'Средний чек клиента',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'main_attract_client_adv_src' => [
            'title' => 'Основные рекламные источники привлечение клиентов',
            'placeholder' => 'Выберите',
        ],
        'main_category_business_partners' => [
            'title' => 'Основная категория партнёров по франшизе',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Частное индивидуальное предприятие',
                ],
                [
                    'id' => 2,
                    'name' => 'Фермерское хозяйство',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) бухгалтерско-аудиторской сферы',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) медицинского сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) ветеринарного сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) транспортного сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) сектора услуг',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) IT-сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) инновационного сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) торгового сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) энергетического сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) финансового сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) производственного сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) логистического сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Маркетинговая или рекламная компания (фирма)',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) сельскохозяйственного сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) развлекательного сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Телекоммуникационная компания (фирма)',
                ],
                [
                    'id' => 3,
                    'name' => 'Компания (фирма) жилищно-коммунального сектора',
                ],
                [
                    'id' => 3,
                    'name' => 'Интернет-компания (фирма)',
                ],
                [
                    'id' => 3,
                    'name' => 'Е-коммерс компания (фирма)',
                ],
                [
                    'id' => 3,
                    'name' => 'Международная корпорация',
                ]

            ],
        ],
        'have_existing_contracts' => [
            'title' => 'Наличие действущих контрактов?',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 0,
                    'name' => 'Нет',
                ],
                [
                    'id' => 1,
                    'name' => 'Да',
                ]
            ],
        ],
        'contracts' => [
            'title' => [
                'title' => 'Название контракта №',
                'placeholder' => 'Укажите название контракта',
                'tooltip' => 'tooltip text',
            ],
            'finalDate' => [
                'title' => 'Срок действия контракта №',
                'tooltip' => 'tooltip text',
                'placeholder' => '25.02.2020',
            ],
            'remove_contract' => 'Удалить контракт',
            'add_contract' => 'Добавить контракт'
        ],
        'count_perpetual_service_contracts' => [
            'title' => 'Количество бессрочных договров на оказания улсуг',
            'placeholder' => 4,
            'tooltip' => 'tooltip text',
        ],
        'legal_status' => [
            'title' => 'Юридический статус франчайзи',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Ждем справочник  :)',
                ],
                [
                    'id' => 2,
                    'name' => 'Я тоже жду... ',
                ],
                [
                    'id' => 3,
                    'name' => 'И я! :)',
                ],
            ],
        ],
        'tax_system' => [
            'title' => 'Система налогообложения',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Ждем справочник  :)',
                ],
                [
                    'id' => 2,
                    'name' => 'Я тоже жду... ',
                ],
                [
                    'id' => 3,
                    'name' => 'И я! :)',
                ],
            ],
        ],
        'changes_profile_legal_entity' => [
            'title' => 'Были изменения профиля деятельности в юридическом лице?',
            'placeholder' => 'Выберите',
        ],
        'have_penalties' => [
            'title' => 'Непокрытые штрафные санкции, наложенные фискальными органами?',
            'placeholder' => 'Выберите',
        ],
        'penalties' => [
            'title' => 'Укажите штрафные санкции',
            'placeholder' => 'Выберите',
        ],
        'have_disputable_situations' => [
            'title' => 'Вовлечение в спорные коммерческие/административные ситуации?',
            'placeholder' => 'Выберите',
        ],
        'list_disputable_situations' => [
            'title' => 'Укажите типы спорных ситуаций',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Ждем справочник  :)',
                ],
                [
                    'id' => 2,
                    'name' => 'Я тоже жду... ',
                ],
                [
                    'id' => 3,
                    'name' => 'И я! :)',
                ],
            ],
        ],
        'address' => [
            'title' => 'Адрес бизнеса',
            'placeholder' => 'Укажите адрес',
        ],
        'numberHouse' => [
            'title' => 'Номер дома',
            'placeholder' => '№',
        ],
        'housingHouse' => [
            'title' => 'Корпус дома',
            'placeholder' => 'Корпус',
        ],
        'numberOffice' => [
            'title' => 'Номер офиса',
            'placeholder' => '№',
        ],
        'index' => [
            'title' => 'Индекс',
            'placeholder' => '513',
        ],
        'partBusiness' => [
            'title' => 'Продается весь бизнес?',
            'list' => [
                [
                    'title' => 'Весь бизнес',
                    'value' => 'all',
                ],
                [
                    'title' => 'Доля компании',
                    'value' => 'part',
                ],
                [
                    'title' => 'Акции компании',
                    'value' => 'stock',
                ],
            ],
        ],
        'numberShares' => [
            'title' => 'Количество долей или акций',
            'placeholder' => '75',
        ],
        'description' => [
            'title' => 'Описание бизнеса',
            'placeholder' => 'Опишите продаваемый бизнес',
        ],
        'yearFoundationBusiness' => [
            'title' => 'Год основания бизнеса',
            'placeholder' => '2007',
        ],
        'reasonSale' => [
            'title' => 'Причина продажи',
            'placeholder' => 'Опишите причину продажи',
        ],
        'theme' => [
            'title' => 'Тематика безнеса',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Item1',
                ],
                [
                    'id' => 2,
                    'name' => 'Item2',
                ],
                [
                    'id' => 3,
                    'name' => 'Item3',
                ],
                [
                    'id' => 4,
                    'name' => 'Item4',
                ],
            ],
        ],
        'price' => [
            'title' => 'Цена долей или акций',
            'placeholder' => '100000',
        ],
        'profitability' => [
            'title' => 'Чистая средняя годовая прибыль',
            'placeholder' => '1000000',
        ],
        'netAverageQuarterlyProfit' => [
            'title' => 'Чистая средняя квартальная прибыль',
            'placeholder' => '50000',
        ],
        'revenue' => [
            'title' => 'Средний годовой оборот',
            'placeholder' => '10000000',
        ],
        'averageQuarterlyTurnover' => [
            'title' => 'Средний квартальный оборот',
            'placeholder' => '500000',
        ],
        'negativeScenario' => [
            'title' => 'Негативный сценарий',
            'placeholder' => '36',
        ],
        'availabilityMortgage' => [
            'title' => 'Наличие ипотеки по объекту',
        ],
        'totalAmountMortgage' => [
            'title' => 'Общая сумма ипотеки',
            'placeholder' => '1000',
        ],
        'repaidAmountMortgage' => [
            'title' => 'Погашенная сумма',
            'placeholder' => '1000',
        ],
        'balanceMortgage' => [
            'title' => 'Остаток суммы ипотеки',
            'placeholder' => '1000',
        ],
        'monthlyPaymentMortgage' => [
            'title' => 'Ежемесячный платёж',
            'placeholder' => '1000',
        ],
        'finalPaymentMortgageDate' => [
            'title' => 'Окончательный срок выплаты',
            'placeholder' => '25.02.2020',
        ],
        'availabilityLoanSecuredObject' => [
            'title' => 'Наличие кредита под залог объекта',
        ],
        'jointPropertyEstate' => [
            'title' => 'Совместная собственность недвижимости',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Долевая собственность',
                ],
                [
                    'id' => 2,
                    'name' => 'Совместное владение супружеской собственностью',
                ],
            ],
        ],
        'typeRelationshipCoowners' => [
            'title' => 'Тип взаимоотношений сообственников',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Долевая собственность',
                ],
                [
                    'id' => 2,
                    'name' => 'Совместное владение супружеской собственностью',
                ],
            ],
        ],
        'monthlyUtilityCosts' => [
            'title' => 'Ежемесячные расходы на коммунальные платежи',
            'placeholder' => '1000',
        ],
        'houseBookNumber' => [
            'title' => 'Номер домовой книги',
            'placeholder' => '№',
        ],
        'presenceChanging' => [
            'title' => 'Наличие чейнджа',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Item',
                ],
                [
                    'id' => 2,
                    'name' => 'Item',
                ],
                [
                    'id' => 3,
                    'name' => 'Item',
                ],
            ],
        ],
        'costChanging' => [
            'title' => 'Стоимость чейнджа',
            'placeholder' => '1000',
        ],
        'leaseTermTo' => [
            'title' => 'Срок аренды до',
            'placeholder' => '25.02.2020',
        ],
        'transfer_work_schemes' => [
            'title' => 'Передаете ли схемы работы?',
        ],
        'month_teach' => [
            'title' => 'Сколько времени будете обучать?',
            'placeholder' => 1,
            'tooltip' => 'tooltip text',
            'prevText' => 'Месяцев',
        ],
        'ready_training_materials' => [
            'title' => 'Готовы ли обучающие материалы?',
        ],
        'typeTrainingMaterials' => [
            'title' => 'Тип обучающих материалов',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Электронные обучающие модули'
                ],
                [
                    'id' => 2,
                    'name' => 'Материал по тренингам'
                ],
                [
                    'id' => 3,
                    'name' => 'Руководство по эксплуатации'
                ],
                [
                    'id' => 4,
                    'name' => 'Видеоролики'
                ],
                [
                    'id' => 5,
                    'name' => 'Постоянный представитель'
                ]
            ]
        ],
        'transfer_customer_base' => [
            'title' => 'Передаёте базы клиентов?',
        ],
        'typeTransferCustomerBase' => [
            'title' => 'Тип передачи базы',
            'placeholder' => 'Выберите',
        ],
        'haveContractors' => [
            'title' => 'Есть ли контрагенты?',
        ],
        'contractors' => [
            'title' => 'Контрагенты',
            'remove' => 'Удалить этого контрагента',
            'add' => 'Добавить контрагента',
            'direction' => [
                'title' => 'Направление контрагента',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => 'Из интернета',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Из обявленний в прессе',
                    ],
                    [
                        'id' => 3,
                        'name' => 'По рекомендации',
                    ],
                ],
            ],
            'name' => [
                'title' => 'Название юридического лица',
                'placeholder' => 'Укажите название',
            ],
            'phone' => [
                'title' => 'Телефон',
                'placeholder' => '+48 456 566-09-23',
            ],
            'mail' => [
                'title' => 'Почта',
                'placeholder' => 'agent@mail.com',
            ],
        ],
        // перенести в отдельный файл
        'staff' => [
            'remove' => 'Удалить этого сотрудника',
            'add' => 'Добавить сотрудника',
            'title' => 'Персонал',
            'post' => [
                'title' => 'Должность',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => 'Управленческий персонал',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Административный персонал',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Менаджеры высшей степении',
                    ],
                    [
                        'id' => 4,
                        'name' => 'Рабочие занятые в производственном процессе',
                    ],
                    [
                        'id' => 5,
                        'name' => 'Водители / доставчики',
                    ],
                    [
                        'id' => 6,
                        'name' => 'Персонал обслуживающий клиентов',
                    ],
                ],
            ],
            'termsContractDismissal' => [
                'title' => 'Условия договора об увольнении',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => '3 дня',
                    ],
                    [
                        'id' => 2,
                        'name' => '1 неделя ',
                    ],
                    [
                        'id' => 3,
                        'name' => '2 недели',
                    ],
                    [
                        'id' => 4,
                        'name' => '1 месядц',
                    ],
                    [
                        'id' => 5,
                        'name' => '3 месядца',
                    ],
                ],
            ],
            'monthly_wages' => [
                'title' => 'Месячная заработная плата',
                'placeholder' => '1000',
            ],
            'tax_amount_per_month' => [
                'title' => 'Сумма налогов, в месяц',
                'placeholder' => '1000',
            ],
        ],
        'leavingStaff' => [
            'remove' => 'Удалить этого сотрудника',
            'add' => 'Добавить сотрудника',
            'title' => 'Персонал который уйдет',
            'post' => [
                'title' => 'Должность',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => 'Управленческий персонал',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Административный персонал',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Менаджеры высшей степении',
                    ],
                    [
                        'id' => 4,
                        'name' => 'Рабочие занятые в производственном процессе',
                    ],
                    [
                        'id' => 5,
                        'name' => 'Водители / доставчики',
                    ],
                    [
                        'id' => 6,
                        'name' => 'Персонал обслуживающий клиентов',
                    ]
                ],
            ],
            'termsContractDismissal' => [
                'title' => 'Условия договора об увольнении',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => '3 дня',
                    ],
                    [
                        'id' => 2,
                        'name' => '1 неделя ',
                    ],
                    [
                        'id' => 3,
                        'name' => '2 недели',
                    ],
                    [
                        'id' => 4,
                        'name' => '1 месяц',
                    ],
                    [
                        'id' => 5,
                        'name' => '3 месяца',
                    ],
                ],
            ],
            'monthly_wages' => [
                'title' => 'Месячная заработная плата',
                'placeholder' => '1000',
            ],
            'tax_amount_per_month' => [
                'title' => 'Сумма налогов, в месяц',
                'placeholder' => '1000',
            ],
        ],
        'willStaffLeave' => [
            'title' => 'Уйдет ли персонал за собственником?',
        ],
        'havePurchasedServices' => [
            'title' => 'Есть ли выкупленные долгосрочные услуги?',
        ],
        'neededLicenses' => [
            'title' => 'Нужны ли сертификаты, лицензии, разрешения по виду деятельности?',
        ],
        'certificates' => [
            'title' => 'Услуги и сертификаты',
            'remove' => 'Удалить этот сертификат',
            'add' => 'Добавить сертификат',
            'document' => [
                'title' => 'Наименование документа',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => 'Договор на труд',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Гражданско прововой договор',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Договор подряда',
                    ],
                ],
            ],
            'type' => [
                'title' => 'Тип документа',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => 'Item',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Item',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Item',
                    ],
                ],
            ],
            'licensePeriod' => [
                'title' => 'Срок действия',
                'placeholder' => '25.02.2025',
            ],
        ],
        'techDocEquipmentCD' => [
            'title' => 'Техническая документация на оборудование (чеки или договор)?',
            'placeholder' => 'Выберите',
        ],
        'techDocEquipment' => [
            'title' => 'Техническая документация на оборудование?',
            'placeholder' => 'Выберите',
        ],
        'conditionProperty' => [
            'title' => 'Состояние имущества (по оценке собственника)',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Очень хорошее',
                ],
                [
                    'id' => 2,
                    'name' => 'Хорошее',
                ],
                [
                    'id' => 3,
                    'name' => 'Средние',
                ],
                [
                    'id' => 4,
                    'name' => 'Плохое - нужно менять или ремонтировать',
                ],
            ],
        ],
        'pledgeEquipment' => [
            'title' => 'Залог на оборудование, транспорт и дугие материльные активы?',
            'placeholder' => 'Выберите',
            'noResult' => 'Ничего не найдено',
        ],
        'totalAmountCredit' => [
            'title' => 'Общая сумма кредита',
            'placeholder' => '1 000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'repaidAmountCredit' => [
            'title' => 'Погашенная сумма',
            'placeholder' => '1 000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'balanceCredit' => [
            'title' => 'Остаток суммы кредита',
            'placeholder' => '1 000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'monthlyPaymentCredit' => [
            'title' => 'Ежемесячный платёж',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'finalPaymentCreditDate' => [
            'title' => 'Окончательный срок выплаты',
            'placeholder' => '25.02.2020',
        ],
        'purposeCredit' => [
            'selected' => '',
            'title' => 'Назначение кредита',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'кредит на новые средства транспорта для бизнеса',
                ],
                [
                    'id' => 2,
                    'name' => 'на ИТ технику',
                ],
                [
                    'id' => 3,
                    'name' => 'кредит на машины и производственное оборудование',
                ],
                [
                    'id' => 4,
                    'name' => 'инвестиционный кредит',
                ],
                [
                    'id' => 5,
                    'name' => 'кредит на обслуживание нового контракта',
                ],
                [
                    'id' => 6,
                    'name' => 'консолидационный кредит',
                ],
                [
                    'id' => '7',
                    'name' => 'кредит на покупку недвижимости / квартиры',
                ],
                [
                    'id' => '8',
                    'name' => 'кредит для погашения задолженности перед контрагентами',
                ],
                [
                    'id' => '9',
                    'name' => 'кредит для погашения государственных задолженности - налоги и социальное страхование',
                ],
                [
                    'id' => '10',
                    'name' => 'потребительский кредит',
                ],
                [
                    'id' => '11',
                    'name' => 'кредит на покупку машины',
                ],
            ],
        ],
        'listTransferredProperty' => [
            'title' => 'Перечень имущества передаваемого продавцом',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Настольный компьютеры',
                ],
                [
                    'id' => 2,
                    'name' => 'Мониторы',
                ],
                [
                    'id' => 3,
                    'name' => 'Лаптопы',
                ],
                [
                    'id' => 4,
                    'name' => 'Принтеры',
                ],
                [
                    'id' => 5,
                    'name' => 'Автофургон, вес до 3,5 тоны',
                ],
                [
                    'id' => 6,
                    'name' => 'Автофургон, вес oт 3,5 до 15 тон',
                ],
                [
                    'id' => '7',
                    'name' => 'Грузовой автомобиль, вес более  15 тонн',
                ],
            ],
        ],
        'deductionsDepreciation' => [
            'title' => 'Отчисления на амортизцию',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Осуществляются',
                ],
                [
                    'id' => 2,
                    'name' => 'Не осуществляются',
                ],
            ],
        ],
        'amountDeductionsProfits' => [
            'title' => 'Объем отчислений от прибыли',
            'placeholder' => '75',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'saleSite' => [
            'title' => 'Продается ли сайт?',
            'placeholder' => 'Выберите',
        ],
        'yearCreationSite' => [
            'title' => 'Год создания сайта',
            'placeholder' => '2010',
        ],
        'cms' => [
            'title' => 'Система управления сайтом',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Item',
                ],
                [
                    'id' => 2,
                    'name' => 'Item',
                ],
                [
                    'id' => 3,
                    'name' => 'Item',
                ],
            ],
        ],
        'themeSite' => [
            'title' => 'Тематика сайта',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Туризм',
                ],
                [
                    'id' => 2,
                    'name' => 'Торговля',
                ],
                [
                    'id' => 3,
                    'name' => 'Недвижимости',
                ],
            ],
        ],
        'salePhone' => [
            'title' => 'Продается ли телефон?',
            'placeholder' => 'Выберите',
        ],
        'phone' => [
            'title' => 'Номер телефона',
            'placeholder' => '880005553535',
            'tooltip' => 'tooltip text',
        ],
        'intellectualProperty' => [
            'title' => 'Интеллектуальная собственность?',
            'placeholder' => 'Выберите',
        ],
        'intellectualPropertyList' => [
            'title' => 'Укажите объекты интеллектуальной собственности',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Авторские права',
                ],
                [
                    'id' => 2,
                    'name' => 'Know how',
                ],
                [
                    'id' => 3,
                    'name' => 'Патенты',
                ],
                [
                    'id' => 4,
                    'name' => 'Торговая марка',
                ],
                [
                    'id' => 5,
                    'name' => 'Промышленный дизайн (полезная модель)',
                ],
            ],
        ],
        'visitorsPerDay' => [
            'title' => 'Посетителей в день',
            'placeholder' => '18300',
            'tooltip' => 'tooltip text',
        ],
        'viewsPerDay' => [
            'title' => 'Просмотров в день',
            'placeholder' => '12300',
            'tooltip' => 'tooltip text',
        ],
        'natureTraffic' => [
            'title' => 'Характер трафика',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Item',
                ],
                [
                    'id' => 2,
                    'name' => 'Item',
                ],
                [
                    'id' => 3,
                    'name' => 'Item',
                ],
            ],
        ],
        'sourcesTraffic' => [
            'title' => 'Источники трафика',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Google Adwords',
                ],
                [
                    'id' => 2,
                    'name' => 'Facebook Ads',
                ],
            ],
        ],
        'sourceGoogle' => [
            'title' => 'Источник: Google',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
        ],
        'sourceYandex' => [
            'title' => 'Источник: Яндекс',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
        ],
        'additionalInformationTraffic' => [
            'title' => 'Дополнительная информация по трафику',
            'placeholder' => 'Опишите дополнительную информацию по вашему трафику',
            'tooltip' => 'tooltip text',
        ],
        'haveIncomeSite' => [
            'title' => 'Получаете ли доход из сайта?',
            'placeholder' => 'Выберите',
        ],
        'averageMonthlyIncome' => [
            'title' => 'Среднемесячный доход',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'sourcesIncome' => [
            'title' => 'Источники дохода',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Item',
                ],
                [
                    'id' => 2,
                    'name' => 'Item',
                ],
                [
                    'id' => 3,
                    'name' => 'Item',
                ],
            ],
        ],
        'contextualAdvertising' => [
            'title' => 'Контекстная реклама',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'contextualAdvertisingGAdsens' => [
            'title' => 'Из них Google Adsens',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'bannerTizerNetworks' => [
            'title' => 'Баннерные и тизирные сети',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'cpa' => [
            'title' => 'CPA',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'banMonetizationSystems' => [
            'title' => 'Бан в системах монетизации',
        ],
        'additionalInformationIncome' => [
            'title' => 'Дополнительная информация по доходам',
            'placeholder' => 'Опишите дополнительную информацию по доходам',
            'tooltip' => 'tooltip text',
        ],
        'averageMonthlyExpensesSite' => [
            'title' => 'Среднемесячный расход',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'categoriesExpensesSite' => [
            'title' => 'Категории расходов',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Item',
                ],
                [
                    'id' => 2,
                    'name' => 'Item',
                ],
                [
                    'id' => 3,
                    'name' => 'Item',
                ],
            ],
        ],
        'contentExpensesSite' => [
            'title' => 'Контент',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'hostingExpensesSite' => [
            'title' => 'Хостинг и администрирование',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'otherExpensesSite' => [
            'title' => 'Иные расходы',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'prevText' => 'руб',
        ],
        'additionalInformationExpensesSite' => [
            'title' => 'Дополнительная информация по расходам',
            'placeholder' => 'Опишите дополнительную информацию по расходам',
            'tooltip' => 'tooltip text',
        ],
        'siteDesign' => [
            'title' => 'Дизайн сайта',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Item',
                ],
                [
                    'id' => 2,
                    'name' => 'Item',
                ],
                [
                    'id' => 3,
                    'name' => 'Item',
                ],
            ],
        ],
        'imgSiteOwn' => [
            'title' => 'Собственные',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'imgSiteOtherSources' => [
            'title' => 'Из других источников',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'textSiteCopyright' => [
            'title' => 'Копирайт',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'textSiteManualRewrite' => [
            'title' => 'Ручной рерайт',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'textSiteSavePaste' => [
            'title' => 'Копи-паст',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'textSiteGenerated' => [
            'title' => 'Генерированый',
            'placeholder' => '50',
            'tooltip' => 'tooltip text',
            'prevText' => '%',
        ],
        'additionalInformationContentSite' => [
            'title' => 'Дополнительная информация по контенту и дизайну',
            'placeholder' => 'Опишите дополнительную информацию по контенту и дизайну',
            'tooltip' => 'tooltip text',
        ],
        'saleGroupsSocNet' => [
            'title' => 'Передаются ли группы в социальных сетях?',
            'placeholder' => 'Выберите',
        ],
        'socNetList' => [
            'title' => 'Укажите социальные сети',
            'placeholder' => 'Выберите',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Facebook',
                ],
                [
                    'id' => 2,
                    'name' => 'Vkontakte',
                ],
            ],
        ],
        'socNetItems' => [
            'title' => 'Группы в социальных сетях',
            'link' => [
                'title' => 'Ссылка на группу',
                'placeholder' => 'https://',
                'tooltip' => 'tooltip text',
            ],
            'countUsers' => [
                'title' => 'Объём пользователей группы',
                'placeholder' => '11000',
                'tooltip' => 'tooltip text',
            ],
            'haveIncome' => [
                'title' => 'Получаете ли доход с группы?',
                'placeholder' => 'Выберите',
            ],
            'incomeYear' => [
                'title' => 'Доход с группы в год',
                'placeholder' => '1000',
                'tooltip' => 'tooltip text',
                'prevText' => 'руб',
            ],
            'incomeQuarter' => [
                'title' => 'Доход с группы в квартал ',
                'placeholder' => '1000',
                'tooltip' => 'tooltip text',
                'prevText' => 'руб',
            ],
            'turnover_year' => [
                'title' => 'Оборот с группы в год',
                'placeholder' => '1000',
                'tooltip' => 'tooltip text',
                'prevText' => 'руб',
            ],
            'turnoverQuarter' => [
                'title' => 'Оборот с группы в квартал ',
                'placeholder' => '1000',
                'tooltip' => 'tooltip text',
                'prevText' => 'руб',
            ],
            'expensesYear' => [
                'title' => 'Расходы с группы в год',
                'placeholder' => '1000',
                'tooltip' => 'tooltip text',
                'prevText' => 'руб',
            ],
            'expensesQuarter' => [
                'title' => 'Расходы с группы в квартал ',
                'placeholder' => '1000',
                'tooltip' => 'tooltip text',
                'prevText' => 'руб',
            ],
            'themes' => [
                'title' => 'Тематика группы',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => 'Item',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Item',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Item',
                    ],
                ],
            ],
            'lang' => [
                'title' => 'Языковой сегмент группы',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => 'Item',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Item',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Item',
                    ],
                ],
            ],
            'methodTransferGroup' => [
                'title' => 'Способ передачи группы',
                'placeholder' => 'Выберите',
                'options' => [
                    [
                        'id' => 1,
                        'name' => 'Item',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Item',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Item',
                    ],
                ],
            ],
        ],
        'withPackage' => 'с пакетом',
        'package' => [
            'package' => 'Пакет',
            'title' => 'Способы размещения объявления до 50 дней',
            'text' => 'Публикация вашего бизнеса на проекте абсолютно бесплатна. Мы отправим ваше объявление на модерацию и после проверки опубликуем его в каталоге бизнесов.',
            'with_broker' => [
                'title' => 'Продать с брокером',
                'subtitle' => 'Коммисия от сделки',
                'text' => 'В данном случаи ваш объект будет размещен в нашей партнерской сети',
                'tooltips' => '25 567 пользователей выбрали этот тарифный план',
            ],
            'without_broker' => [
                'title' => 'Продавать самому',
                'text' => 'В данном случаи вы продаете объект самостоятельно',
                'tooltips' => '25 567 пользователей выбрали этот тарифный план',
            ],
        ],
        'documents' => [
            'success_added' => 'Документ успешно закачен'
        ],
        'complex' => 'Комплекс',
        'ratio' => 'Соотношение',
        'addObject' => 'Продать франшизу',
        'editObject' => 'Сохранить франшизу',
        'validation' => [
            'errors' => [
                'name' => ['Введите название франшизы и суть предложения о ее покупке'],
                'category_id' => ['Выберите и укажите вид экономической деятельности по франшизе'],
                'subcat_id' => [
                    'Выберите и укажите подкатегорию непосредственной деятельности по франшизе',
                    'Чтобы выбрать направление вам нужно сначало выбрать категорию'
                ],
                'foundation_year' =>['Укажите год запуска франшизы'],
                'royality' => ['Укажите долю от прибыли по франшизе, которую назначаете в качестве компенсации за право пользования франшизой.'],
                'lump_sum' => ['Укажите сумму паушального взноса'],
                'payment_term' => ['Укажите период в течении, которого необходимо внести сумму оплаты в случае покупки франшизы в рассрочку'],
                'price' => ['Укажите общую сумму'],
                'discount' => ['Укажите скидку предоствляяемую фрнчайзи на покупку франшизы'],
                'have_packages' => ['Укажите наличие или отсутствие пакетов с различными условиями ведения бизнеса по франшизе'],
                'packages' => [
                    'name' => ['Введиите название пакета'],
                    'price' => ['Укажите цену пакета'],
                    'royalty_rate' => ['Укажите размер роялти'],
                    'lump_sum' => ['Укажите сумму паушального взноса'],
                    'include' => ['Укажите что входит пакет, каждая с новой строки'],
                    'benefits' => ['Опишите примущества пакета']
                ],
                'media' => ['Добавьте фотографии франшизы. Минимум 5 штук'],
                'video_exists' => ['Выберите Да если франшиза имеет видеобзор'],
                'video_title' => ['Введите название проморолика по франшизе'],
                'video_url' => ['Вставьте рабочую ссылку на проморолик по франшизе.'],
                'country_id' => ['Укажите страну запуска фрншизы'],
                'region_id' => ['Укажите регион запуска франшизы'],
                'city_id' => ['Укажите город запуска франшизы'],
                'description' => ['Введите подробное описание франшизы и коммерчеких действий по франшизе'],
                'name_legal_entity' => ['Введите полное и правильное наименавание юридического лица - владельца франшизы, облдающего правами заключения договора о продаже франшизы.'],
                'name_business' => ['Укажите название франшизы или компании франчайзера'],
                'logo' => ['Добавьте лого франшизы'],

                'transfer_work_schemes' => ['Подтвердите наличие или отсутствие рабочей модели ведения бизнеса по франшизе готовой к передче франчайзи.'],
                'month_teach' => ['Укажите срок обучения фрвнчайзи ведению бизнеса по франшизе.'],
                'ready_training_materials' => ['Подтврдите или опровергните готовность материалрв для обучения персонала франчайзи методам ведения бизнеса по франшизе.'],
                'type_training_materials' => ['Выберите и укажите в каком виде подготовлен для франчайзи обучающий материал по франшизе.'],
                'transfer_customer_base' => ['Подтвердите или опровергните наличие готовой к передаче франчайзи клиентской базы по франшизе.'],
                'type_transfer_customer_base' => ['Подтвердите или опровергните наличие готовой к передаче франчайзи клиентской базы по франшизе.'],
                'staff' => [
                    'post_id' => ['Укажите должность'],
                    'monthly_wages' => ['Укажите месячную заработную плату'],
                    'tax_amount_per_month' => ['Укажите сумму налогов, в месяц'],
                ],
                'have_certificates' => ['Укажите нужны ли сертификаты, лицензии, разрешения по виду деятельности'],
                'certificates' => [
                    'certificate_type_id' => ['Укажите тип документа'],
                    'certificate_document_id' => ['Укажите наименование документа'],
                    'license_period' => ['Укажите срок действия документа'],
                ],
                'category_property_id' => ['Выберите требуемый тип собственности недвижимости по франшизе'],
                'property_type_id' => ['Выберите требуемый для ведения бизнеса тип недвижимости по франшизе '],
                'number_square_meters' => ['Укажите требуемое количество квадратных метров общей площади объекта недвижимости по франшизе.'],
                'price_square_meters' => ['Укажите приемлемую стоимость квадратного метра объекта недвижимости по франшизе'],
                'title_documents' => ['Подтвердите или опровергните неободимость наличия у франчайзи документов подтверждающих права собственности на объект недвижимости экслуатируемый по франшизе'],
                'restrictions_operation' => ['Укажите о наличии или отутствии огрничений по эксплуотации объекта недвижимости недопустимых для ведения бизнеса по франшизе'],
                'list_restrictions_operation' => ['Укажите недопустимые для ведения бизнеса по франшизе огрничения по эксплуотации объекта недвижимости.'],
                'coordination_redevelopment' => ['Укажите утвердительно или отрицательно о необходимоости соглсования перепланировок на выбрнном объекте недвижимости для ведения бизнеса по франшизе'],

                'included_equipment' => ['Укажите утвердительно или отрицательно о включении стоимости оборудования по франшизе в догоор о ее покупке'],
                'list_equipment' => ['Укажите тип оборудования передаваемый фрнчайзи по договору о покупке франшизы'],
                'condition_equipment_id' => ['Укажите предельно приемлемый уровень изношенности и рабтоспособности оборудования по франшизе.'],
                'discount_equipment' => ['Укажите размер скидки предоставляемой франчайзи на покупку оборудования по франшизе'],
                'total_amount_equipment' => ['Укажите сумму достаточную для покупки необходимого оборудовния по франшизе при номинальном вложении средств'],
                'total_amount_equipment_with_discount' => ['Укажите сумму стоимости оборудовния по франшизе ч учетом предоставленной скидки для фрнчайзи'],
                'staff_training_equipment' => ['Укажите о проведении обучения сотрудников франчайзи работе на оборудовнии  по франшизе'],

                'profitability' => ['Укажите ожидемую часть балансовой прибыли, которая останется в распоряжении франчайзи за вычетом всех расходов по франшизе в квартал при номинальном вложении средств.'],
                'profit' => ['Укажите ожидаемый период времени необходимый для покрытия всех затрат по запуску франшизы при номинальном вложении средств.'],
                'turnover_month' => ['Укажите ожидаемый квартльный оборот по франшизе при номинальном вложении средств.'],
                'turnover_year' => ['Укажите ожидаемый годовой оборот по франшизе при номинальном вложении средств'],
                'general_expenses' => ['Укажите ожидаемый уровень общих расходов по запуску франшизы при номинальном вложении средств '],
                'month_expenses' => ['Укажите ожидаемый уровень месячных расходов по франшизе при номинальном вложении средств '],
                'staff_costs' => ['Определите и укажите ожидаемый ежемесячный уровень затрат в среднем на персонал по франшизе при номинальном вложении средств'],
                'property_costs' => ['Укажите ожидаемые совокупные затраты на эксплуатацию объектов недвижимости по франшизе при номинальном вложении средств'],
                'cost_business_per_year' => ['Укажите ожидаемые годовые издержки обращения по франшизе при номинальном вложении средств'],
                'cost_business_per_quarter' => ['Укажите ожидаемые квртальные издержки обрщения по франшизе при номинальном вложении средств'],
                'launch_dates' => ['Укажите срок необходимый для запуска франшизы при номинльном вложении средств и при соблюдении указанных в данном предложении условий.'],
                'justification_financial_indicators' => ['Введите краткое обоснование приведенных данных финансовй деятельности по франшизе'],

                'legal_status' => ['Выберите и укажите статусы юридических лиц, обладание которыми позволяет осуществлять коммерческую деятельность по франшизе.'],
                'tax_system' => ['Выберите и укажите систему налогообложения применяемую по франшизе'],

                'type_audience' => ['Укажите и выберите тип основной клиентуры, на которую нацелена коменрческя деятельность по франшизе'],
                'b2c' => [
                    'gender_target_audience' => ['Укажите половую приндлежность основной массы клюиентов, на которых рассчитана франшиза'],
                    'age_men' => ['Укажите средний возраст мужской части целевой аудитории клиентов по франшизе'],
                    'age_women' => ['Укажите средний возраст женской части целевой аудитории клиентов по франшизе'],
                    'sex_ratio' => ['Укажите процентное соотношение полов '],
                    'family_status_clients' => ['Укажите семейный статус клиентов'],
                    'family_status_clients_stat' => ['Укажите долю в процентном выражении клиентов по франшизе с семейным статусом соответсующим текущему полю'],
                    'social_status_clients' => ['Укажите социальные статусы клиентов'],
                    'social_status_clients_stat' => ['Укажите долю в процентном выражении клиентов по франшизе с социальным статусом соответсующим текущему полю'],
                    'average_income_target_clients' => ['Укажите прогнозируемый средний уровень дохода основной массы клиентов со статусом физических лиц по фрншизе'],
                    'average_check_clients' => ['Укажите прогнозируемую сумму среднего чека со статусом физического лица по франшизе'],
                    'main_attract_client_adv_src' => ['Укажите перечень типов действенных рекламных каналов привлечения целевой клиентуры по франшизе на рынке b2c'],
                ],
                'b2b' => [
                    'main_category_business_partners' => ['Укажите перечень клиентов со сттусом юридического лица по франшизе, которые являются целевой аудиторией'],
                    'have_existing_contracts' => ['Подтвердите или опровергните наличие дейтвующих контрактов с контрагентами по франшизе, передваемых франчайзи'],
                    'contracts' => [
                        'name' => ['Введите название дейтвующего контракта с контрагентом по франшизе, передаваеого франчйзи в рамках договора о франчайзинге'],
                        'date' => ['Укажите дату прекрщения действия договора с контрагентом по франшизе, передаваеого франчйзи в рамках договора о франчайзинге']
                    ],
                    'count_perpetual_service_contracts' => ['Приведите точное количество контрактов на оказание услуг клиентам со статусом юридических лиц по франшизе без указания сроков действия, передаваемых франчайзи'],
                    'average_check_clients' => ['Укажите прогнозируемую сумму среднего чека со статусом юридического лица по франшизе'],
                    'main_attract_client_adv_src' => ['Укажите перечень типов действенных рекламных каналов привлечения целевой клиентуры со статусом юридичекого лица по франшизе на рынке b2b'],
                ]
            ],
            'otherErrors' => [
                'text_min' => 'Длина текста должна больше > ',
                'text_max' => 'Длина текста не может превышать < ',
                'number_min' => 'Значение должно быть больше > ',
                'number_max' => 'Значение должно быть меньше < ',
                'max_elements' => 'Выбрано максимальное количество опций. Удалите одну опцию чтобы выбрать другую',
                'from_to_from_empty' => 'Заполните поле "от" правильным значением',
                'from_to_to_empty' => 'Заполните поле "до" правильным значением',
                'from_to_from_grater_to' => 'Поле "от" больше чем поле "до". Введите правильный интервал',
            ]
        ]
    ],
];




