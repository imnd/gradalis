<?php

return array (
  'title' => 'Контакты',
  'phone_for_all_questions' => 'По всем вопросам',
  'we_work_with' => 'Работаем с',
  'phone_local' => 'Локальный телефон',
  'email' => 'Контактный email',
  'security_department' => 'Отдел безопасности',
  'headquarters_address' => 'Адрес главного офиса',
  'mailing_address' => 'Почтовый адрес',
  'quality_and_service_department' => 'Отдел качества и сервиса',
  'head' => 'Руководитель',
);
