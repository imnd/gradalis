<?php

return [
    'title' => 'Вакансии',
    'link_learn_more' => 'Подробнее',
    'not_found' => 'Вакансий нет',
    'response_send' => 'Отклик на вакансию отправлен',
    'single' => [
        'btn_respond' => 'Откликнуться',
        'btn_reg_business_seller_broker' => 'Регистрация брокера продавца бизнеса',
    ],
    'form_resp' => [
        'title' => 'Оставить отклик на вакансию',
        'name' => [
            'title' => 'Ваше имя',
            'placeholder' => 'Введите ваше имя',
        ],
        'phone' => [
            'title' => 'Контактный телефон',
            'placeholder' => '+38 (456) 566-09-23',
        ],
        'email' => [
            'title' => 'Контактный email',
            'placeholder' => 'Введите ваш email',
        ],
        'resume_link' => [
            'title' => 'Ссылка на резюме',
            'placeholder' => 'Вставте ссылку',
        ],
        'comment' => [
            'title' => 'Комментарий',
            'placeholder' => 'Сопроводительный текст',
        ],
    ]
];