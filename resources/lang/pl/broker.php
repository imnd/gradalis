<?php

return array (
  'help' => 'Pomoc',
  'all_offers' => 'Wszystkie oferty',
  'web-offers' => 'Oferty internetowe',
  'active_offers' => 'Aktywne oferty',
  'new_offers' => 'Nowe oferty',
  'favorites_offers' => 'Wyborowe oferty',
);
