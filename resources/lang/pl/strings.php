<?php

return array (
  'region' => 'Region',
  'city' => 'Miasto',
  'country' => 'Kraj',
  'step' => 'Krok',
  'cancel' => 'Skasować',
  'cancel_all' => 'Skasować wszystkie',
  'save' => 'Zachować',
  'reserve' => 'Zarezerwować',
  'select' => 'Wytypować',
  'select_all' => 'Wybrać wszystkie',
  'send' => 'Wystosować',
  'select_placeholder' => 'Wytypujcie',
  'select_category' => 'Wybierz kategorię',
  'termsThisSection' => 'warunki określone w sekcji',
  'upTo5Pieces' => 'do 5 sztuk',
  'comeBack' => 'Wrócić się',
);
