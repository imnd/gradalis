<?php

return array (
  'all' => 'Wszystkie',
  'sum_to_pay' => 'Kwota do wypłaty',
  'write_message' => 'Napisać wiadomość',
  'time' => 
  array (
    'title' => 'Czas',
    'options' => 
    array (
      0 => 
      array (
        'id' => '1',
        'name' => '1 minuta',
      ),
      1 => 
      array (
        'id' => '2',
        'name' => '1 godzinę',
      ),
    ),
  ),
  'week' => 'Tydzień',
);
