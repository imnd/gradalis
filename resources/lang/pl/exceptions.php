<?php

return array (
  'backend' => 
  array (
    'access' => 
    array (
      'roles' => 
      array (
        'already_exists' => 'Taka nazwa roli już istnieje. Prosi, wybrać inną nazwę.',
        'cant_delete_admin' => 'Nie możecie wydalić rolę administratora.',
        'create_error' => 'Niemożliwie stworzyć rolę. Prosi, spróbować później.',
        'delete_error' => 'Niemożliwie wydalić rolę. Prosi, spróbować później.',
        'has_users' => 'Nie możecie wydalić tę rolę, ona jest związana z istniejącymi użytkownikami.',
        'needs_permission' => 'Вы должны выбрать по крайней мере одно разрешение для этой роли.',
        'not_found' => 'Rola nie istnieje',
        'update_error' => 'Niemożliwie odnowić rolę. Prosi, spróbować później.',
      ),
      'users' => 
      array (
        'already_confirmed' => 'Ten użytkownik już jest potwierdzony.',
        'cant_confirm' => 'Powstał problem z potwierdzeniem ewidencyjnego zapisu użytkownika.',
        'cant_deactivate_self' => 'Dla siebie nie możecie to zrobić',
        'cant_delete_admin' => 'Nie możecie wydalić głównego administratora.',
        'cant_delete_own_session' => 'Nie możecie wydalić swoją własną sesję',
        'cant_delete_self' => 'Nie możecie  siebie wydalić',
        'cant_restore' => 'Ten użytkownik jest nie usuwany, toż nie może być odnowiony.',
        'cant_unconfirm_admin' => 'Nie możecie potwierdzić super administratora.',
        'cant_unconfirm_self' => 'Nie możecie siebie potwierdzić',
        'create_error' => 'Niemożliwie stworzyć użytkownika. Prosi, spróbować później.',
        'delete_error' => 'Niemożliwie wydalić użytkownika. Prosi, spróbować później.',
        'delete_first' => 'Ten użytkownik powinien być miękko usuwany, wcześniej on może być ostatecznie usuwany.',
        'email_error' => 'Ten E-mail adres należy innemu użytkownikowi.',
        'mark_error' => 'Niemożliwie odnowić użytkownika. Prosi, spróbować później.',
        'not_confirmed' => 'Ten użytkownik jest nie potwierdzony.',
        'not_found' => 'Takiego użytkownika nie istnieje.',
        'restore_error' => 'Niemożliwie odnowić użytkownika. Prosi, spróbować później.',
        'role_needed' => 'Musicie wybrać co najmniej jedną rolę.',
        'role_needed_create' => 'Musicie wybrać przynajmniej jedną rolę.',
        'session_wrong_driver' => 'Dla użycia tej funkcji, te sesje muszą wykorzystać bazę danych.',
        'social_delete_error' => 'Nie udało się wydalić socjalny ewidencyjny zapis u użytkownika.',
        'update_error' => 'Niemożliwie odnowić użytkownika. Prosi, spróbować później.',
        'update_password_error' => 'Niemożliwie zmienić parol użytkownika. Prosi, spróbować później.',
      ),
    ),
  ),
  'frontend' => 
  array (
    'auth' => 
    array (
      'confirmation' => 
      array (
        'already_confirmed' => 'Ewidencyjny zapis już jest potwierdzony.',
        'confirm' => 'Potwierdźcie ewidencyjny zapis!',
        'created_confirm' => 'Wasz ewidencyjny zapis pomyślnie jest stworzony. Odprawiliśmy zawiadomienie na E-mail dla potwierdzenia waszego ewidencyjnego zapisu.',
        'created_pending' => 'Ewidencyjny zapis jest zapisany i znajduje się na rozpatrzeniu. Po zatwierdzeniu waszego ewidencyjnego zapisu, będzie odprawiono zawiadomienie na wasz E-mail.',
        'mismatch' => 'Nieprawidłowy kod potwierdzenia.',
        'not_found' => 'Takiego kodu nie istnieje.',
        'pending' => 'Obecnie ewidencyjny zapis znajduje się na rozpatrzeniu.',
        'resend' => 'Wasz ewidencyjny zapis jest nie potwierdzony. Prosi naciśnijcie na powoływanie dla potwierdzenia w liście wysłanym na E-mail czy <a href=":url">nacisnąć tutaj</a>, żeby ponownie odprawić potwierdzenie na Wasz E-mail.',
        'resent' => 'Nowe parametry potwierdzenia są wysłane na wasz	E-mail adres.',
        'success' => 'Wasz ewidencyjny zapis pomyślnie jest potwierdzony',
      ),
      'deactivated' => 'Wasz ewidencyjny zapis był usuwany',
      'email_taken' => 'Ten E-mail adres już jest zajęty.',
      'password' => 
      array (
        'change_mismatch' => 'Niewierny stary parol.',
        'reset_problem' => 'Nie udało się zmienić parol. Powtórzcie zapytanie na przemianę parolu.',
      ),
      'registration_disabled' => 'Rejestracja obecnie jest zamknięta, przychodzicie później',
    ),
  ),
);
