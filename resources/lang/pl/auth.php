<?php

return array (
  'reg_executor' => 
  array (
    'title' => 'Rejestracja wykonawcy',
    'form' => 
    array (
      'name' => 
      array (
        'label' => 'Wasze imię',
      ),
      'btn_submit' => 'Potwierdzić rejestrację',
      'surname' => 
      array (
        'label' => 'Wasze nazwisko',
      ),
      'social' => 
      array (
        'placeholder' => 'https://',
      ),
      'companyActivity' => 
      array (
        'label' => 'Działalność',
      ),
      'companyName' => 
      array (
        'label' => 'Nazwa firmy',
        'placeholder' => 'Nazwa',
      ),
      'phone' => 
      array (
        'placeholder' => '+48 (456) 566-09-23',
        'label' => 'Telefon kontaktowy',
      ),
      'email' => 
      array (
        'placeholder' => 'Wpisz email',
        'label' => 'Kontaktowy adres email',
      ),
      'agree_2' => 'warunkami korzystania z usługi',
      'agree_1' => 'Z',
      'agree_3' => 'zapoznać',
    ),
    'right_block' => 
    array (
      'title' => 'narzędzia do wykonawcy',
    ),
  ),
  'reg_media_buyer' => 
  array (
    'form' => 
    array (
      'btn_submit' => 'Potwierdzić rejestrację',
      'wayAttractCustomersAdd' => 
      array (
        'label' => 'Swoją wersję',
        'placeholder' => 'Napisz swoją wersję',
      ),
      'wayAttractCustomers' => 
      array (
        'label' => 'Sposób na przyciągnięcie klientów',
      ),
      'telegram' => 
      array (
        'label' => 'Telegram',
        'placeholder' => 'Login Telegram',
      ),
      'skype' => 
      array (
        'placeholder' => 'Login Skype',
        'label' => 'Skype',
      ),
      'phone' => 
      array (
        'label' => 'Telefon kontaktowy',
        'placeholder' => '+48 (456) 566-09-23',
      ),
      'email' => 
      array (
        'placeholder' => 'Wpisz email',
        'label' => 'Kontaktowy adres email',
      ),
      'surname' => 
      array (
        'label' => 'Wasze nazwisko',
      ),
      'name' => 
      array (
        'label' => 'Wasze imię',
      ),
      'agree_3' => 'zapoznać',
      'agree_1' => 'Z',
      'agree_2' => 'warunkami korzystania z usługi',
    ),
    'right_block' => 
    array (
      'title' => 'Wygodne narzędzia do mediów bayera',
    ),
    'title' => 'Rejestracja wykonawcy',
  ),
  'email' => 'Adres e-mail',
  'confirm_password' => 'Potwierdź hasło',
  'link_forgot' => 'Nie pamiętasz hasła?',
  'password' => 'Hasło',
  'reg_broker' => 
  array (
    'form' => 
    array (
      'btn_submit' => 'Potwierdzić rejestrację',
      'email' => 
      array (
        'label' => 'Kontaktowy adres email',
        'placeholder' => 'Wprowadzasz email',
      ),
      'companyName' => 
      array (
        'label' => 'Nazwa firmy',
        'placeholder' => 'Nazwa',
      ),
      'languages' => 
      array (
        'label' => 'Jakimi językami władają?',
      ),
      'name' => 
      array (
        'label' => 'Wasze imię',
      ),
      'legal_status' => 
      array (
        'label' => 'Firma lub osoba fizyczna',
      ),
      'categoryBusiness' => 
      array (
        'label' => 'Branza biznesu w której pracujesz',
      ),
      'phone' => 
      array (
        'placeholder' => '+48 (456) 566-09-23',
      ),
      'surname' => 
      array (
        'label' => 'https://',
      ),
      'agree_2' => 'warunkami korzystania z usługi',
      'agree_3' => 'zapoznać',
      'agree_1' => 'Z',
    ),
    'title' => 'Rejestracja brokera',
  ),
  'passwords' => 
  array (
    'reset' => 'Resetowanie hasła',
    'btn_reset' => 'Wysłać link resetowania hasła',
    'back_login' => 'Przypomniał sobie hasło?',
  ),
  'login' => 'Wejście',
  'remember_me' => 'Zapamiętaj mnie',
);
