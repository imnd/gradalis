<?php

return array (
  'title' => 'Skontaktuj się z nami',
  'phone_for_all_questions' => 'W przypadku wszystkich pytań ',
  'we_work_with' => 'Pracujemy w dni robocze od',
  'phone_local' => 'Telefon lokalny',
  'email' => 'e-mail kontaktowy',
  'mailing_address' => 'Adres do korespondencji',
  'headquarters_address' => 'Adres głównej siedziby firmy',
  'quality_and_service_department' => 'Kierownik:',
  'security_department' => 'Dział bezpieczeństwa',
  'head' => 'Kierownik',
);
