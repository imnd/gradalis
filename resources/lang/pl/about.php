<?php

return array (
  'title' => 'O firmie',
  'our_mission' => 'Nasza misja',
  'our_team' => 'Nasz zespół',
  'our_tasks' => 'Wartości i zadania',
  'link_vacancy' => 'Zobacz wszystkie oferty pracy firmy',
  'q_clients_text' => 'клиентов поверили в наши возможности и силы',
  'right_block' => 'Мы помогаем предпринимателям делать правильные вложения средств. Именно это делает нас непобедимыми на рынке.',
  'our_about_text' => '<p>GradalisGroup - międzynarodowa kompania z platformą dla sprzedaży/kupna biznesu i franszyzy, a także jest agregatorem usług dla przedsiębiorców i osób, zaangażowanych w sferę komercyjną.</p>
                            <p>
                                Proponujemy efektywny sposób wyjścia na europejski rynek właścicielom komercji, pomożemy skooperować się z przedsiębiorcami i finansistami z innych krajów. Zapewniamy wykwalifikowaną pomoc początkującym przedsiębiorcom i doświadczonym inwestorom biznesowym w poszukiwaniu udanych projektów inwestycyjnych i debugowaniu procesów biznesowych.
                            </p>
                            <p>
                            Podstawą naszej kompanii jest:
                             <ul>
                                 <li>Ciągłe poszukiwanie optymalnych biznes-decyzji.</li>
                                 <li>Praca nad wynikiem spójnego zespołu.</li>
                                 <li>Energiczne i owocne osiąganie celów.</li>
                             </ul>
                            </p>',
  'our_mission_text' => '<p>Nasza misja - pozwolić ludziom zorientowanym na biznes zajmować się komercją, realizować im zuchwałe pomysły biznesowy i tworzenie własnego ekośrodowiska. </p>
                            <p>
                                Kompania GradalisGroup ma na celu przeformatować komercyjne stosunki pod warunki cyfrowej gospodarki, nadać im współczesny impuls, wspierając się, przy czym na wymagające standardy europejskich praktyk biznesowych.
                            </p>
                            <p>
                               Nasza bezpośrednia działalność skierowana jest do współpracujących osób, które sprzedają biznes ludziom, którzy chcą go kupić. W tym celu pomagamy tworzyć najlepsze oferty, zapewniać dostęp do obiektów i franczyz. Użytkownicy z pomocą przemyślanej i rozwiniętej funkcjonalności otrzymują:
                            </p>
                            <p>
                            <ul>
                                <li>pełny pakiet usług online / offline zakupu lub sprzedaży gotowej firmy;</li>
                                <li>możliwość szerokiego wyboru inwestycji biznesowych i kapitałowych;</li>
                                <li>wszystkie konieczne dane do we właściwym czasie przeprowadzenia ugody dowolnej trudności;</li>
                                <li>gotowe i uregulowane kanały komunikacji z wszystkimi zaangażowanymi stronami w ugodzie;</li>
                                <li>sposoby przyciągnięcia w porę profesjonalistów w dziedzinie pomocy prawnej i finansowej;</li>
                                <li>opracowana marketingowa strona internetowa-platforma z pakietami gotowych rozwiązań do promocji ofert handlowych i obiektów biznesowych.</li>
                             </ul>
                            </p>
                            <p>
                               Klienci strony internetowej - szerokie audytorium, wśród którego inwestorzy zagraniczne i miejscowe, biznesmeni różnego kalibru, finansjery i top menedżerowie, którzy podejmują kluczowe decyzje, a także raczkujący handlowcy. Zwrócenie do naszych usług gwarantowano podwyższa szanse dołączenia kapitału do gotowego biznesu i inwestycyjnych projektów.
                            </p>',
  'our_tasks_text' => '<p>Nasze zadanie - organizacja "czystej" ugody. Udział profesjonalistów eliminuje ryzyko związane z wyborem kierunku działalności, zawyżonymi kosztami. Wykwalifikowana pomoc prawna może być udzielona w trakcie całej procedury od wyboru obiektu biznesowego, przez okres ugody i wprowadzenie pełnych praw właściciela. Dobrze skoordynowana praca pracowników GradalisGroup zwiększa możliwość opłacalnych i kompetentnych inwestycji funduszy.
                            </p>
                            <p>
                                W trakcie pracy rozwiązujemy następujące zadania:
                            </p>
                            <p>
                            <ul>
                                <li>obsługa transakcji sprzedaży lub zakupu firmy, franczyzy na rynkach międzynarodowych lub lokalnych;</li>
                                <li>pomoc przedsiębiorcom we wdrażaniu kompetentnego audytu lub oceny działalności;</li>
                                <li>pomoc w sprawach prawnych i finansowych;</li>
                                <li>zapewnienie wygody i funkcjonalności korzystania z usługi klientom.</li>
                            </ul>
                            </p>
                            <p>
                                Dzięki naszemu profesjonalnemu wsparciu bezpieczna i dochodowa praca z gotowym biznesem stanie się legalna, a także z powodzeniem otworzy działalność franczyzową.
                            </p>',
);
