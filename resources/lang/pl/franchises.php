<?php

return array (
  'create' => 
  array (
    'direction' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
          'name' => 'Franczyza dystrybucyjna (handlowa)',
        ),
        1 => 
        array (
          'id' => '2',
          'name' => 'Franczyza usługowa',
        ),
        2 => 
        array (
          'id' => '3',
          'name' => 'Franczyza produkcyjna',
        ),
        3 => 
        array (
          'id' => '4',
          'name' => 'Franczyza gastronomiczna',
        ),
      ),
      'title' => 'Rodzaje franczyzy',
    ),
    'leavingStaff' => 
    array (
      'termsContractDismissal' => 
      array (
        'options' => 
        array (
          0 => 
          array (
            'id' => '1',
            'name' => '3 dni',
          ),
          1 => 
          array (
            'id' => '2',
            'name' => '1 tydzień',
          ),
          2 => 
          array (
            'id' => '3',
            'name' => '2 tygodnie',
          ),
          3 => 
          array (
            'id' => '4',
            'name' => '1 miesiąc',
          ),
          4 => 
          array (
            'name' => '3 miesiące',
            'id' => '5',
          ),
        ),
      ),
    ),
    'average_check_clients' => 
    array (
      'placeholder' => '1000',
      'prevText' => 'zł',
    ),
    'main_info' => 'Podstawowe informacje',
    'discount' => 
    array (
      'placeholder' => '14',
    ),
    'numberShares' => 
    array (
      'title' => 'Ilość udziałów czy akcji',
      'placeholder' => '75',
    ),
    'number_square_meters' => 
    array (
      'placeholder' => '12',
      'prevText' => 'м²',
    ),
    'business_processes' => 'Procesy biznesowe',
    'categoriesExpensesSite' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
      ),
    ),
    'yearFoundationFranchises' => 
    array (
      'title' => 'Rok założenia franszyzy',
      'placeholder' => '2007',
    ),
    'yn' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
      ),
    ),
    'description' => 
    array (
      'placeholder' => 'Opisz sprzedawany biznesu',
      'title' => 'Opis biznesu',
    ),
    'descriptionFranchise' => 
    array (
      'placeholder' => 'Opisz swoje franczyzy',
      'title' => 'Opis franczyzy',
    ),
    'deductionsDepreciation' => 
    array (
      'title' => 'Odpisy amortyzacyjne',
    ),
    'cpa' => 
    array (
      'placeholder' => '1000',
      'title' => 'CPA',
    ),
    'yearFoundationBusiness' => 
    array (
      'title' => 'Rok założenia biznesowe',
      'placeholder' => '2007',
    ),
    'yearCreationSite' => 
    array (
      'placeholder' => '2010',
      'title' => 'Rok utworzenia strony internetowej',
    ),
    'condition' => 
    array (
      'list' => 
      array (
        2 => 
        array (
          'title' => 'Ryczałt',
        ),
      ),
    ),
    'packages' => 
    array (
      'purchaseTerms' => 
      array (
        'list' => 
        array (
          2 => 
          array (
            'title' => 'Ryczałt',
          ),
        ),
      ),
    ),
    'have_packages' => 
    array (
      'title' => 'Czy franczyza ma różne pakiety przy sprzedaży',
    ),
    'have_penalties' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
      ),
    ),
  ),
  'add_object' => 
  array (
    'address' => 
    array (
      'placeholder' => 'Poland, Warsaw, Zamkowy Square 1/13, 00-262',
    ),
    'phone' => 
    array (
      'placeholder' => '+48 456 566-09-23',
      'label' => 'Telefon',
    ),
    'name' => 
    array (
      'placeholder' => 'Nazwa obiektu',
      'label' => 'Nazwa',
    ),
    'photo_object' => 'Zdjęcia obiektu',
    'title' => 'Dodać nowy obiekt',
  ),
  'upload_materials' => 
  array (
    'subtitle' => '1000x1000 pikseli, JPG, PNG, do 8 mb',
  ),
  'upload_photo' => 
  array (
    'subtitle' => '1000x1000 pikseli, JPG, PNG, do 8 mb',
  ),
  'single' => 
  array (
    'real_estate' => 'Nieruchomość',
    'net_profit_per_month' => 'Wskaźniki finansowe',
    'news' => 'Nowości franczyzy',
  ),
  'add_review' => 
  array (
    'video' => 
    array (
      'placeholder' => 'http://',
    ),
    'btn_submit' => 'Zostawić opinię',
  ),
  'add_news' => 
  array (
    'btn_submit' => 'Dodać wiadomość',
    'title' => 'Dodać wiadomość',
    'author' => 
    array (
      'placeholder' => 'Autor publikacji',
      'label' => 'Autor',
    ),
    'date' => 
    array (
      'label' => 'Data publikacji',
    ),
  ),
  'v_filter' => 
  array (
    'free_contribution' => 'Bez ryczałtu',
  ),
);
