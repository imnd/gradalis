<?php

return array (
  'add_service' => 
  array (
    'category' => 
    array (
      'label' => 'Kategorie',
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
        1 => 
        array (
          'id' => '2',
        ),
        2 => 
        array (
          'id' => '3',
        ),
        3 => 
        array (
          'id' => '4',
        ),
      ),
    ),
    'search_service' => 
    array (
      'label' => 'Znaleźć usługe',
    ),
  ),
  'balance' => 
  array (
    'r_via_cc' => 'Płatność kartą kredytową Visa i MasterCard',
    'r_via_pp' => 'Przez PayPal',
    'r_via_ps' => 'Za pośrednictwem systemów Płatności',
    'r_via_cash_desc' => 'Przelew bankowy i',
    'history' => 'Historia transakcji',
    'table_head' => 
    array (
      'amount' => 'Kwota płatności',
    ),
  ),
  'breadcrumb' => 
  array (
    'profile' => 'Kształtownik',
    'legal' => 'Warunki umowy',
    'purchased_service_detail' => 'Szczegółowa usługa',
    'your_services' => 'Twoja usługa',
  ),
  'chat_dialog' => 
  array (
    'btn_confirm' => 'Potwierdzić',
    'search_by_history' => 'Wyszukiwanie wiadomosci w historii',
    'start_typing_msg' => 'Zacznij pisać wiadomość',
    'btn_delete' => 'Usunąć',
    'btn_approve' => 'Zaakceptować',
    'no_result' => 'Wiadomości nie ma',
  ),
  'chat_dialogs' => 
  array (
    'settings' => 
    array (
      'seller' => 'Klientela',
      'buyer' => 'Sprzedawcy',
    ),
  ),
  'chat' => 
  array (
    'widget' => 
    array (
      'messages' => 'Wiadomości',
    ),
    'title' => 'Wiadomości',
    'msg_statuses' => 
    array (
      'delivered' => 'Dostarczone',
    ),
    'dialog_broker' => 'Napisać do brokera',
    'write_to_broker' => 'Napisz do brokera',
    'dialog_support' => 'Napisać do pomocy technicznej',
    'in_support' => 'pomocy technicznej',
    'or_write' => 'или напишите do',
  ),
  'objects' => 
  array (
    'requests_table' => 
    array (
      'head' => 
      array (
        'country' => 'Kraj',
      ),
    ),
    'sold' => 'Sprzedane obiekty biznesu',
    'services' => 'Usługi',
  ),
  'franchises' => 
  array (
    'sold' => 'Sprzedane franszyzy',
  ),
  'legal' => 
  array (
    'title' => 'Warunki i umowy',
  ),
  'nav' => 
  array (
    'legal' => 'Warunki i umowy',
    'user_service' => 'Konstruktor usług',
  ),
  'user_services' => 
  array (
    'btn_add_service' => 'Dodać usługę',
    'btn_add_new_service' => 'Dodać nową usługę',
    'form_label' => 
    array (
      'name' => 'Nazwa',
    ),
    'link_edit' => 'Redagować',
    'status' => 'Status',
    'title' => 'Swoje usługi',
    'product_name' => 'Название продукта портала',
  ),
  'purchased_service' => 
  array (
    'title' => 'Proces zamówionej usługi',
  ),
  'settings' => 
  array (
    'email_notifications' => 
    array (
      'paid_services' => 'Płatne usługi',
      'promo' => 'Promocje i oferty',
    ),
  ),
  'purchased_services' => 
  array (
    'table_head' => 
    array (
      'product_name' => 'Nazwa produktu',
      'result' => 'Wynik',
      'status' => 'Status',
    ),
  ),
  'trips_detail' => 
  array (
    'company_payment_receipt' => 'Kwit opłaty za uslugi',
    'company_reg' => 'Rejestracja przedsiębiorstwa',
    'btn_delete_doc' => 'Usunąć',
    'upload_doc' => 
    array (
      'delete' => 'Usunąć',
    ),
  ),
  'favorites' => 
  array (
    'btn_delete' => 'Usunąć z pobranych',
    'category' => 'Kategoria',
  ),
  'user_service' => 
  array (
    'portfolio' => 'Portfolio',
    'promo_video_desc' => 'Opis',
    'name' => 'Nazwa',
  ),
);
