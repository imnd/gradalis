<?php

return array(
    'create' => array(
        'sourcesTraffic' => array(
            'title' => 'Źródła trafficu',
            'placeholder' => 'Wytypujcie',
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Google Adwords',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Facebook Ads',
                ),
            ),
        ),
        'number_square_meters' => array(
            'prevText' => 'м²',
            'placeholder' => '12',
            'title' => 'Ilość metrów kwadratowych',
        ),
        'otherExpensesSite' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'tooltip' => 'tooltip text',
            'title' => 'Inne koszty',
        ),
        'socNetItems' => array(
            'turnover_year' => array(
                'prevText' => 'zł',
                'placeholder' => '1000',
            ),
            'expensesQuarter' => array(
                'placeholder' => '1000',
                'prevText' => 'zł',
            ),
            'expensesYear' => array(
                'placeholder' => '1000',
                'prevText' => 'zł',
            ),
            'incomeQuarter' => array(
                'placeholder' => '1000',
                'prevText' => 'zł',
            ),
            'turnoverQuarter' => array(
                'prevText' => 'zł',
                'placeholder' => '1000',
            ),
            'incomeYear' => array(
                'placeholder' => '1000',
                'prevText' => 'zł',
            ),
            'lang' => array(
                'placeholder' => 'Wytypujcie',
            ),
            'link' => array(
                'placeholder' => 'https://',
            ),
            'methodTransferGroup' => array(
                'placeholder' => 'Wytypujcie',
            ),
            'themes' => array(
                'placeholder' => 'Wytypujcie',
            ),
            'haveIncome' => array(
                'placeholder' => 'Wybierz',
            ),
        ),
        'cpa' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'CPA',
        ),
        'contextualAdvertisingGAdsens' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
        ),
        'contentExpensesSite' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'Kontent',
        ),
        'bannerTizerNetworks' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'Ban systemów monetyzacji',
        ),
        'balanceCredit' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'Resztę kwoty kredytu',
        ),
        'averageMonthlyIncome' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'Przeciętny miesięczny dochód',
        ),
        'averageMonthlyExpensesSite' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'Przeciętne miesięczne wydatki',
        ),
        'average_check_clients' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'Średni rachunek zakupu',
        ),
        'leavingStaff' => array(
            'termsContractDismissal' => array(
                'options' => array(
                    1 => array(
                        'name' => '1 tydzień',
                        'id' => '2',
                    ),
                    2 => array(
                        'id' => '3',
                        'name' => '2 tygodnie',
                    ),
                    3 => array(
                        'id' => '4',
                        'name' => '1 miesiąc',
                    ),
                    4 => array(
                        'id' => '5',
                        'name' => '3 miesiące',
                    ),
                    0 => array(
                        'id' => '1',
                        'name' => '3 dni',
                    ),
                ),
                'title' => 'Warunki umowy o zwolnieniu',
                'placeholder' => 'Wytypujcie',
            ),
            'monthly_wages' => array(
                'placeholder' => '1000',
                'title' => 'Miesięczna płaca zarobkowa',
            ),
            'tax_amount_per_month' => array(
                'placeholder' => '1000',
                'title' => 'Kwota podatku do miesiąca',
            ),
            'add' => 'Dodać pracownika',
            'remove' => 'Usunąć danego pracownika',
            'post' => array(
                'title' => 'Profesjonalne stanowisko',
                'placeholder' => 'Wytypujcie',
            ),
        ),
        'about_content_and_design' => 'O contentcie i designie',
        'cost_business_per_year' => array(
            'placeholder' => '25 000',
            'title' => 'Koszty na komercyjną działalność do roku',
        ),
        'cost_business_per_quarter' => array(
            'placeholder' => '100 000',
            'title' => 'Koszty na komercyjną działalność do  kwartału',
        ),
        'contractors' => array(
            'mail' => array(
                'placeholder' => 'agent@mail.com',
                'title' => 'e-mail',
            ),
            'name' => array(
                'title' => 'Nazwa osoby prawnej',
                'placeholder' => 'Wprowadź nazwę',
            ),
            'add' => 'Dodać kontrahenta',
            'direction' => array(
                'title' => 'Skierowanie kontrahenta',
                'placeholder' => 'Wytypujcie',
            ),
            'title' => 'Kontrahenci',
            'remove' => 'Usunąć danego kontrahenta',
            'phone' => array(
                'placeholder' => '+48 456 566-09-23',
                'title' => 'Telefon',
            ),
        ),
        'contextualAdvertising' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'Reklama kontekstowa',
        ),
        'conditionProperty' => array(
            'options' => array(
                3 => array(
                    'id' => '4',
                ),
                2 => array(
                    'id' => '3',
                ),
                1 => array(
                    'id' => '2',
                ),
                0 => array(
                    'id' => '1',
                ),
            ),
            'title' => 'Stan majątku (według oceny właściciela)',
            'placeholder' => 'Wytypujcie',
        ),
        'condition_equipment' => array(
            'options' => array(
                5 => array(
                    'id' => '6',
                ),
                4 => array(
                    'id' => '5',
                ),
                0 => array(
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                ),
                3 => array(
                    'id' => '4',
                ),
            ),
            'placeholder' => 'Wytypujcie',
            'title' => 'Stan sprzętu, transportu, techniki (według oceny właściciela)',
        ),
        'average_income_target_clients_to' => array(
            'placeholder' => '1000',
            'title' => 'Średni dochód klientów docelowych',
        ),
        'average_income_target_clients_from' => array(
            'placeholder' => '1000',
            'title' => 'Średni dochód klientów docelowych',
        ),
        'amountDeductionsProfits' => array(
            'prevText' => '%',
            'placeholder' => '75',
            'title' => 'Ilość odliczeń od dochodu',
        ),
        'alone_clients' => array(
            'prevText' => '%',
            'placeholder' => '50',
            'title' => 'Odsetek samotnych',
        ),
        'age_women_to' => array(
            'placeholder' => '48',
            'title' => 'Wiek kobiet',
        ),
        'age_women_from' => array(
            'placeholder' => '20',
            'title' => 'Wiek kobiet',
        ),
        'age_men_from' => array(
            'placeholder' => '20',
            'title' => 'Wiek mężczyzn',
        ),
        'age_men_to' => array(
            'placeholder' => '48',
            'title' => 'Wiek mężczyzn',
        ),
        'monthlyUtilityCosts' => array(
            'placeholder' => '1000',
            'title' => 'Miesięczne koszty na komunalne spłaty',
        ),
        'monthlyPaymentMortgage' => array(
            'placeholder' => '1000',
            'title' => 'Miesięcznych płatności',
        ),
        'monthlyPaymentCredit' => array(
            'placeholder' => '1000',
            'prevText' => 'zł',
            'title' => 'Miesięcznych płatności',
        ),
        'numberShares' => array(
            'placeholder' => '75',
            'title' => 'Ilość udziałów czy akcji',
        ),
        'negativeScenario' => array(
            'placeholder' => '36',
            'title' => 'Scenariusz negatywny',
        ),
        'netAverageQuarterlyProfit' => array(
            'placeholder' => '50 000',
            'title' => 'Czysty średni kwartalny dochód',
        ),
        'numberHouse' => array(
            'placeholder' => '№',
            'title' => 'Numer domu',
        ),
        'numberOffice' => array(
            'placeholder' => '№',
            'title' => 'Numer biura',
        ),
        'price' => array(
            'placeholder' => '100 000',
            'title' => 'Cena akcji lub udziałów',
        ),
        'price_square_meters' => array(
            'placeholder' => '1000',
            'title' => 'Cena za metr kwadratowy',
        ),
        'profitability' => array(
            'placeholder' => '1 000 000',
            'title' => 'Czysty średni roczny dochód',
        ),
        'repaidAmountCredit' => array(
            'placeholder' => '1000',
            'prevText' => 'zł',
            'title' => 'Kwota wypłacona',
        ),
        'repaidAmountMortgage' => array(
            'placeholder' => '1000',
            'title' => 'Kwota wypłacona',
        ),
        'revenue' => array(
            'placeholder' => '10 000 000',
            'title' => 'Średni roczny obrót',
        ),
        'totalAmountCredit' => array(
            'prevText' => 'zł',
            'placeholder' => '1000',
            'title' => 'Lączna kwota kredytu',
        ),
        'visitorsPerDay' => array(
            'placeholder' => '18 300',
            'title' => 'Liczba odwiedzających dziennie',
        ),
        'hostingExpensesSite' => array(
            'placeholder' => '1000',
            'prevText' => 'zł',
            'title' => 'Hosting i administracja',
        ),
        'costChanging' => array(
            'placeholder' => '1000',
        ),
        'averageQuarterlyTurnover' => array(
            'placeholder' => '500 000',
            'title' => 'Średni kwartalny obrót',
        ),
        'certificates' => array(
            'document' => array(
                'options' => array(
                    0 => array(
                        'name' => 'Umowa o pracę',
                        'id' => '1',
                    ),
                    1 => array(
                        'id' => '2',
                        'name' => 'Cywilno prawna umowa o pracę (Umowa zlecenie)',
                    ),
                    2 => array(
                        'id' => '3',
                        'name' => 'Umowa o dzieło',
                    ),
                ),
                'placeholder' => 'Wytypujcie',
                'title' => 'Określ rodzaj umowy o pracę',
            ),
            'type' => array(
                'placeholder' => 'Wytypujcie',
                'options' => array(
                    0 => array(
                        'id' => '1',
                    ),
                    1 => array(
                        'id' => '2',
                    ),
                    2 => array(
                        'id' => '3',
                    ),
                ),
                'title' => 'Wybierz rodzaje certyfikatu lub pozwoleniem',
            ),
            'title' => 'Usługi i certyfikaty',
            'remove' => 'Usunąć danego certyfikatu',
            'licensePeriod' => array(
                'title' => 'Okres ważności licencji  lub pozwoleniem',
                'placeholder' => '25.02.2020',
            ),
            'add' => 'Dodać certyfikat',
        ),
        'propertyCategories' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Wlasność',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Wynajem',
                ),
            ),
            'placeholder' => 'Wytypujcie',
            'title' => 'Typ własności obiektu nieruchomośći',
        ),
        'add_address' => 'Doliczyć jeszcze jeden adres',
        'index' => array(
            'title' => 'Indeks',
        ),
        'cms' => array(
            'options' => array(
                0 => array(
                    'name' => 'WordPress',
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Joomla',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'Drupal',
                ),
            ),
            'placeholder' => 'Wytypujcie',
            'title' => 'System zarządzania treścią (CMS)',
        ),
        'gender_target_audience' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Mężczyźni',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Kobiety',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'Nie ma znaczenia',
                ),
            ),
            'placeholder' => 'Wytypujcie',
            'title' => 'Gender grupy docelowej',
        ),
        'social_status_clients' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Klasa średnia',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Studenci',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'Emeryci i renciści',
                ),
            ),
            'placeholder' => 'Wytypujcie',
            'title' => 'Status socjalny klientów',
        ),
        'legal_status' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Spolka z ograniszona odpowiedzialnoscia (sp. z o.o.)',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Spółka kapitałowa',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'Spółka komandytowa',
                ),
            ),
            'placeholder' => 'Wytypujcie',
            'title' => 'Status prawny',
        ),
        'tax_system' => array(
            'options' => array(
                0 => array(
                    'name' => 'Podatek dochodowy od osób fizycznych (zasady ogólne)',
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Podatek od dochodów spółek (PIT)',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'Podatek od towarów i usług (VAT)',
                ),
            ),
            'title' => 'System opodatkowania',
            'placeholder' => 'Wytypujcie',
        ),
        'add_contract' => 'Doliczyć jeszcze jeden obiekt',
        'add_new_object' => 'Doliczyć obiekt nieruchomośći',
        'add_object' => 'Doliczyć jeszcze jeden obiekt nieruchomośći',
        'step3' => 'Krok 3: Informacja o nieruchomośći',
        'main_info' => 'Podstawowa Informacja',
        'month_teach' => array(
            'placeholder' => '1',
            'prevText' => 'Miesięcy',
            'title' => 'Ile czasu będzie się nauczać?',
        ),
        'ad_placement' => 'Dodać ogłoszenie',
        'have' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Jeść',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Nie',
                ),
            ),
            'placeholder' => 'Wytypujcie',
        ),
        'categoriesExpensesSite' => array(
            'placeholder' => 'Wytypujcie',
            'options' => array(
                1 => array(
                    'name' => 'Kontent',
                    'id' => '2',
                ),
                0 => array(
                    'id' => '1',
                    'name' => 'Hosting i administracja',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'SEO promocja',
                ),
            ),
            'title' => 'Kategorie wydatków',
        ),
        'changes_profile_legal_entity' => array(
            'placeholder' => 'Wytypujcie',
            'title' => 'Były przemiany profilu działalności w osobie prawnej?',
        ),
        'legal_status' => 'Stan prawny',
        'step8' => 'Krok 8: Stan prawny',
        'sold_success' => 'Sukces w sprzedaży biznesu',
        'help' => '<span class="has-text-weight-bold">Nie chcesz wypełniać?</span> <span>Nasz menedżer ci pomoże!</span>',
        'title' => 'Sprzedać biznes',
        'step2' => 'Krok 2: Wskazać wskaźniki finansowe',
        'finance_info' => 'Wskaźniki finansowe',
        'labelReturnInvestment' => 'Opłacalność włożonych kosztów (stopa zwrotu inwestycji)',
        'leaseTermTo' => array(
            'placeholder' => '25.02.2020',
            'title' => 'Na jaki okres wynajmuje się nieruchomość',
        ),
        'b2c' => array(
            'title' => 'B2C',
        ),
        'b2b' => array(
            'title' => 'B2B',
        ),
        'real_estate' => 'Nieruchomość',
        'btn_more' => 'Zobacz więcej',
        'price_including_vat_bool' => array(
            'title' => 'Cena z uwzględnieniem VAT',
        ),
        'title_documents' => array(
            'title' => 'Dokumenty tytułowe',
        ),
        'restrictions_operation' => array(
            'title' => 'Ograniczenia w eksploatacji',
        ),
        'technical_property_plan' => array(
            'title' => 'Techniczny plan nieruchomości',
        ),
        'textSiteCopyright' => array(
            'placeholder' => '50',
            'prevText' => '%',
        ),
        'ref_register_estate' => array(
            'title' => 'Zaświadczenie z registra nieruchomości i budowl',
        ),
        'list_restrictions_operation' => array(
            'title' => 'Wskażcie ograniczenia',
            'placeholder' => 'Wytypujcie',
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Hipoteka',
                ),
                7 => array(
                    'id' => '9',
                    'name' => 'Zobowiązania wynajmu',
                ),
                6 => array(
                    'id' => '7',
                ),
                3 => array(
                    'id' => '4',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                ),
                4 => array(
                    'id' => '5',
                ),
                5 => array(
                    'id' => '6',
                ),
            ),
        ),
        'availabilityMortgage' => array(
            'title' => 'Obecność hipoteki po obiekcie',
        ),
        'availabilityLoanSecuredObject' => array(
            'title' => 'Obecność kredytu za kaucją obiektu',
        ),
        'jointPropertyEstate' => array(
            'title' => 'Wspólna własność nieruchomości',
            'placeholder' => 'Wytypujcie',
        ),
        'houseBookNumber' => array(
            'title' => 'Numer księgi domowej',
            'placeholder' => '№',
        ),
        'imgSiteOtherSources' => array(
            'placeholder' => '50',
            'prevText' => '%',
        ),
        'step4' => 'Krok 4: Procesy biznesowe',
        'type_property' => array(
            'title' => 'Typ nieruchomości',
            'placeholder' => 'Wytypujcie',
        ),
        'yn' => array(
            'placeholder' => 'Wybierz',
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Tak',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Nie',
                ),
            ),
        ),
        'partBusiness' => array(
            'title' => 'Sprzedaje się cały biznes (zakres sprzedaży)?',
            'list' => array(
                2 => array(
                    'title' => 'Akcji spółki',
                    'value' => 'stock',
                ),
                0 => array(
                    'value' => 'all',
                    'title' => 'W pełną własność (całość firmy)',
                ),
                1 => array(
                    'value' => 'part',
                    'title' => 'Udział w biznesie',
                ),
            ),
        ),
        'pensioners_clients' => array(
            'placeholder' => '50',
            'prevText' => '%',
            'title' => 'Odsetek emerytów i rencistów',
        ),
        'payback' => array(
            'placeholder' => '12',
            'title' => 'Scenariusz pozytywny',
        ),
        'yearFoundationBusiness' => array(
            'title' => 'Rok założenia biznesu',
            'placeholder' => '2007',
        ),
        'address' => array(
            'title' => 'Lokalizacja biznesu',
            'placeholder' => 'Dodaj adres',
        ),
        'nameLegalEntity' => array(
            'title' => 'Nazwa osoby prawnej',
            'placeholder' => 'Wprowadź nazwę',
        ),
        'name' => array(
            'title' => 'Nazwa biznesu',
            'placeholder' => 'Wprowadź nazwę',
        ),
        'reasonSale' => array(
            'title' => 'Przyczyna sprzedaży biznesu',
            'placeholder' => 'Objaśnij przyczyny sprzedażu biznesu',
        ),
        'description' => array(
            'title' => 'Opis biznesu',
            'placeholder' => 'Opisz sprzedawany biznes',
        ),
        'families_with_children_clients' => array(
            'prevText' => '%',
            'placeholder' => '50',
            'title' => 'Odsetek rodzinnych par z dziećmi',
        ),
        'linkVideoReview' => array(
            'title' => 'Film promocyjny o biznesu',
            'placeholder' => 'http://',
        ),
        'contracts' => array(
            'finalDate' => array(
                'placeholder' => '25.02,2020',
                'title' => 'Okres obowiązywania umowy №',
            ),
            'title' => array(
                'title' => 'Nazwa kontraktu №',
                'placeholder' => 'Wprowadź nazwę kontraktu',
            ),
        ),
        'linkVideoReview' => array(
            'title' => 'Nazwa filmu promocyjnego',
            'placeholder' => 'Wprowadź nazwę  filmu promocyjnego',
        ),
        'housingHouse' => array(
            'placeholder' => 'blok',
            'title' => 'Numer bloku',
        ),
        'objects' => array(
            'title' => 'Obiekty',
        ),
        'ready_training_materials' => array(
            'title' => 'Czy materiały szkoleniowe są gotowe?',
        ),
        'typeTrainingMaterials' => array(
            'title' => 'Rodzaj materiałów szkoleniowych',
            'placeholder' => 'Wytypujcie',
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Elektroniczne',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Papierowy',
                ),
            ),
        ),
        'viewsPerDay' => array(
            'placeholder' => '12 300',
            'title' => 'Liczba wyświetleń dziennie',
        ),
        'transfer_customer_base' => array(
            'title' => 'Przekazujecie bazy klientów?',
        ),
        'haveContractors' => array(
            'title' => 'Czy są kontrahenci?',
        ),
        'business_processes' => 'Procesy biznesowe',
        'staff' => array(
            'termsContractDismissal' => array(
                'title' => 'Warunki umowy o zwolnieniu',
                'placeholder' => 'Wytypujcie',
                'options' => array(
                    4 => array(
                        'id' => '5',
                        'name' => '3 miesiąca',
                    ),
                    3 => array(
                        'id' => '4',
                        'name' => '1 miesiąc',
                    ),
                    0 => array(
                        'id' => '1',
                        'name' => '3 dni',
                    ),
                    1 => array(
                        'id' => '2',
                        'name' => '1 tydzień',
                    ),
                    2 => array(
                        'id' => '3',
                        'name' => '2 tygodnie',
                    ),
                ),
            ),
            'title' => 'Personel',
            'add' => 'Dodać pracownika',
            'monthly_wages' => array(
                'placeholder' => '1000',
                'title' => 'Miesięczna płaca zarobkowa',
            ),
            'post' => array(
                'placeholder' => 'Wytypujcie',
                'title' => 'Posada w pracy',
            ),
            'tax_amount_per_month' => array(
                'title' => 'Kwota podatku w miesiącu',
                'placeholder' => '1000',
            ),
            'remove' => 'Usunąć danego pracownika',
        ),
        'have_disputable_situations' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                ),
            ),
            'title' => 'Dołączenie do sytuacji spornych komercyjnych /administracyjnych',
            'placeholder' => 'Wytypujcie',
        ),
        'transfer_work_schemes' => array(
            'title' => 'Przekazujecie bazy schematy pracy?',
        ),
        'students_clients' => array(
            'placeholder' => '50',
            'prevText' => '%',
            'title' => 'Odsetek studentów',
        ),
        'willStaffLeave' => array(
            'title' => 'Czy pójdzie personel za właścicielem?',
        ),
        'typeTransferCustomerBase' => array(
            'options' => array(
                0 => array(
                    'name' => 'Excel',
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Papierowa',
                ),
            ),
            'placeholder' => 'Wytypujcie',
            'title' => 'Rodzaj bazy danych klientow',
        ),
        'havePurchasedServices' => array(
            'title' => 'Czy są zakupione usługi długoterminowe?',
        ),
        'neededLicenses' => array(
            'title' => 'Czy potrzebuje certyfikatów, licencji, zezwoleń według rodzaju działalności?',
        ),
        'listTransferredProperty' => array(
            'placeholder' => 'Wytypujcie',
            'title' => 'Wykaz majątku wysyłanego przez sprzedawcę',
        ),
        'main_attract_client_adv_src' => array(
            'placeholder' => 'Wytypujcie',
            'title' => 'Główne reklamowe źródła dołączenie klientów',
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Reklama zewnętrzna',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Medialna reklama',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'Reklama internetowa',
                ),
            ),
        ),
        'main_category_business_partners' => array(
            'title' => 'Główna kategoria partnerów biznesowych',
            'options' => array(
                0 => array(
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                ),
            ),
            'placeholder' => 'Wytypujcie',
        ),
        'natureTraffic' => array(
            'placeholder' => 'Wytypujcie',
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Ruch organiczny',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                ),
            ),
        ),
        'presenceChanging' => array(
            'placeholder' => 'Wytypujcie',
        ),
        'purposeCredit' => array(
            'placeholder' => 'Wytypujcie',
            'options' => array(
                4 => array(
                    'id' => '5',
                    'name' => 'kredyt na obsługę zawartej umowy',
                ),
                0 => array(
                    'id' => '1',
                    'name' => 'kredyt na nowe środki transportu dla biznesu',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'kredyt na zakup sprzętu elektronicznego',
                ),
                10 => array(
                    'id' => '11',
                    'name' => 'kredyt na zakup samochodu osobowego',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'kredyt na służbowy transport i produkcyjny sprzęt',
                ),
                3 => array(
                    'id' => '4',
                    'name' => 'kredyt inwestycyjny',
                ),
                5 => array(
                    'id' => '6',
                    'name' => 'kredyt konsolidacyjny',
                ),
                6 => array(
                    'id' => '7',
                    'name' => 'kredyt na zakup nieruchomości',
                ),
                7 => array(
                    'id' => '8',
                    'name' => 'kredyt dla zgaszenia zadłużenia przed kontrahentami',
                ),
                8 => array(
                    'id' => '9',
                    'name' => 'kredyt dla zgaszenia państwowego zadłużenia w sprawie podatków ubezpieczenia społecznego',
                ),
                9 => array(
                    'id' => '10',
                    'name' => 'kredyt konsumencki',
                ),
            ),
            'title' => 'Przeznaczenie kredytu',
        ),
        'siteDesign' => array(
            'placeholder' => 'Wytypujcie',
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Unikatowy',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'Szablon',
                ),
            ),
            'title' => 'Design strony internetowej',
        ),
        'socNetList' => array(
            'placeholder' => 'Wytypujcie',
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Facebook',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Instagram',
                ),
            ),
        ),
        'sourceGoogle' => array(
            'placeholder' => '1000',
        ),
        'sourceYandex' => array(
            'placeholder' => '1000',
        ),
        'sourcesIncome' => array(
            'placeholder' => 'Wytypujcie',
            'title' => 'Źródła dochodu',
        ),
        'themeSite' => array(
            'placeholder' => 'Wytypujcie',
        ),
        'typeRelationshipCoowners' => array(
            'placeholder' => 'Wytypujcie',
        ),
        'intangible_assets' => 'Wartości niematerialne',
        'step6' => 'Krok 5: Wartości niematerialne',
        'step5' => 'Krok 5: Aktywy materialne',
        'step7' => 'Krok 7: Audytorium docelowe',
        'tangible_assets' => 'Aktywy materialne',
        'target_audience' => 'Audytorium docelowe',
        'pledgeEquipment' => array(
            'noResult' => 'Nic nie znaleziono',
            'title' => 'Zastaw sprzętu, transportu i innych środków trwałych?',
            'placeholder' => 'Wybierz',
        ),
        'photo_and_img_on_site' => 'Obrazy i zdjęcia na stronie',
        'phone' => array(
            'placeholder' => '880005553535',
            'title' => 'Numer telefonu',
        ),
        'remove_contract' => 'Usunąć obiekt',
        'remove_object' => 'Usunąć obiekt',
        'remove_address' => 'Usunąć  danego adresu',
        'balanceMortgage' => array(
            'placeholder' => '1000',
            'title' => 'Resztę kwoty hipoteki',
        ),
        'addresses' => array(
            'title' => 'Adresy',
        ),
        'child_clients' => array(
            'placeholder' => '50',
            'prevText' => '%',
            'title' => 'Odsetek dzieci',
        ),
        'couples_clients' => array(
            'title' => 'Odsetek bezdzietnych par',
            'placeholder' => '50',
            'prevText' => '%',
        ),
        'deductionsDepreciation' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Wykonują',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Nie wykonują',
                ),
            ),
            'title' => 'Odpisy amortyzacyjne',
            'placeholder' => 'Wytypujcie',
        ),
        'count_perpetual_service_contracts' => array(
            'placeholder' => '4',
            'title' => 'Ilość bezterminowych umów na świadczenie usług',
        ),
        'family_status_clients' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                    'name' => 'Samotne dorośli',
                ),
                2 => array(
                    'name' => 'Rodzinne pary z dziećmi',
                    'id' => '3',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'Bezdzietne pary',
                ),
            ),
            'placeholder' => 'Wytypujcie',
            'title' => 'Status rodzinny klientów',
        ),
        'sex_ratio' => array(
            'value' => '50',
            'title' => 'Współzależność płci',
        ),
        'techDocEquipmentCD' => array(
            'title' => 'Dokumentacja techniczna sprzętu (czeki lub umowa)?',
            'placeholder' => 'Wybierz',
        ),
        'techDocEquipment' => array(
            'title' => 'Dokumentacja techniczna sprzętu?',
            'placeholder' => 'Wybierz',
        ),
        'saleSite' => array(
            'title' => 'Czy sprzedaje się strona internetowa?',
            'placeholder' => 'Wybierz',
        ),
        'salePhone' => array(
            'title' => 'Czy sprzedaje się telefon?',
            'placeholder' => 'Wybierz',
        ),
        'intellectualProperty' => array(
            'title' => 'Własność intelektualna?',
            'placeholder' => 'Wybierz',
        ),
        'imgSiteOwn' => array(
            'prevText' => '%',
            'placeholder' => '50',
        ),
        'intellectualPropertyList' => array(
            'title' => 'Wskażcie obiekty intelektualnej własności',
            'placeholder' => 'Wytypujcie',
        ),
        'yearCreationSite' => array(
            'title' => 'Rok utworzenia strony internetowej',
            'placeholder' => '2010',
        ),
        'additionalInformationTraffic' => array(
            'title' => 'Dodatkowa informacja po trafficu',
            'placeholder' => 'Opisujecie dodatkowe informacje po trafficu',
        ),
        'haveIncomeSite' => array(
            'title' => 'Czy otrzymujecie dochód ze strony internetowej?',
        ),
        'have_penalties' => array(
            'options' => array(
                0 => array(
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                ),
            ),
            'title' => 'Niekryte karne sankcje, obciążone fiskalnymi organami',
            'placeholder' => 'Wytypujcie',
        ),
        'additionalInformationExpensesSite' => array(
            'title' => 'Dodatkowa informacja o kosztach',
            'placeholder' => 'Opiszcie dodatkową informację po kosztach',
        ),
        'additionalInformationContentSite' => array(
            'title' => 'Dodatkowa informacja o contentcie i designie',
            'placeholder' => 'Opiszcie dodatkową informację o contentcie i designie',
        ),
        'list_disputable_situations' => array(
            'title' => 'Wskażcie typy spornych sytuacji',
            'placeholder' => 'Wytypujcie',
        ),
        'penalties' => array(
            'title' => 'Wskażcie karne sankcje',
            'options' => array(
                0 => array(
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                    'name' => 'Kara umowna za długów',
                ),
            ),
            'placeholder' => 'Wytypujcie',
        ),
        'have_existing_contracts' => array(
            'title' => 'Obecność czynnych kontraktów',
            'options' => array(
                0 => array(
                    'id' => '1',
                ),
                1 => array(
                    'id' => '2',
                ),
                2 => array(
                    'id' => '3',
                ),
            ),
            'placeholder' => 'Wytypujcie',
        ),
        'audienceTypes' => array(
            'title' => 'Typ audytorium, na której jest obliczony biznes',
            'options' => array(
                2 => array(
                    'name' => 'B2C - B2B',
                    'id' => '3',
                ),
                0 => array(
                    'id' => '1',
                    'name' => 'B2C',
                ),
                1 => array(
                    'id' => '2',
                    'name' => 'B2B',
                ),
            ),
            'placeholder' => 'Wytypujcie',
        ),
        'totalAmountMortgage' => array(
            'placeholder' => '1000',
            'title' => 'Lączna kwota hipoteki',
        ),
        'back_step' => 'Wrócić się',
        'banMonetizationSystems' => array(
            'title' => 'Ban w systemach zarabiania',
        ),
        'cancel_step' => 'Cofnąć',
        'finalPaymentMortgageDate' => array(
            'placeholder' => '25.02.2020',
            'title' => 'Ostateczny termin płatności',
        ),
        'finalPaymentCreditDate' => array(
            'placeholder' => '25.02.2020',
            'title' => 'Ostateczny termin płatności',
        ),
        'traf_and_seo' => 'Trafik internetowy i SEO',
        'saleGroupsSocNet' => array(
            'title' => 'Przekazują czy portali społecznościowych na świecie?',
            'placeholder' => 'Wybierz',
        ),
        'site_exp' => 'Koszty strony internetowej',
        'coordination_redevelopment' => array(
            'title' => 'Uzgodnienie o przebudowaniu pomieszczenia',
        ),
        'additionalInformationIncome' => array(
            'title' => 'Dodatkowe informacje o dochodach',
            'placeholder' => 'Przedstajecie dodatkowe informacje o dochodach',
        ),
        'go_to_publish' => 'Przystąpić do publikacji oferty',
        'agreement_text_1' => 'Przyjmuję',
        'agreement_text_2' => 'warunki tego rozdziału',
        'agreement_text_3' => 'i',
        'agreement_text_4' => 'umowy o niezgłaszaniu (NDA)',
    ),
    'show' => array(
        'how_purchase' => 'Jak odbywa się kupno?',
        'city' => 'Miasto',
        'country' => 'Kraj',
        'zip_code' => 'Indeks',
        'legal_state' => 'Stan prawny',
        'general_info' => 'Podstawowa Informacja',
        'financial_indicators' => 'Wskaźniki finansowe',
        'how_purchase_2' => 'Jak odbywa się umowa?',
        'real_estate' => 'Nieruchomość',
        'btn_more' => 'Zobacz więcej',
        'months' => 'Miesięcy',
        'similar_offers_item' => array(
            'months' => 'miesięcy',
            'payback' => 'opłacalność (stopa zwrotu inwestycji)',
            'revenue' => 'Utarg',
            'btn_more' => 'Więcej o biznesie',
        ),
        'business_processes' => 'Procesy biznesowe',
        'intangible_assets' => 'Wartości niematerialne',
        'tangible_assets' => 'Aktywy materialne',
        'target_audience' => 'Audytorium docelowe',
        'payback' => 'Opłacalność (stopa zwrotu inwestycji)',
        'name_company' => 'Nazowa firmy',
        'reason_for_sale' => 'Powód sprzedaży',
        'profit' => 'Utarg',
        'net_profit' => 'Zysk',
        'number_of_employees' => 'Liczba pracowników firmy',
        'btn_order' => 'Zakazać',
        'btn_view_more' => 'Zobacz szczegółowe informacje',
        'del_from_fav' => 'usunąć z ulubionych',
        'how_purchase_title' => 'Zostawajcie zapotrzebowanie, to was nie zobowiązuje nie do czego. Razem z wami przejdziemy następne kroki:',
    ),
    'category' => 'Wszystkie branże',
    'step8' => array(
        'title' => 'Stan prawny',
        'icon' => '/svg/icons/ic_law_form.svg',
    ),
    'step1' => array(
        'title' => 'Podstawowa Informacja',
        'icon' => '/svg/icons/ic_info.svg',
    ),
    'step2' => array(
        'title' => 'Wskaźniki finansowe',
        'icon' => '/svg/icons/ic_object_finance.svg',
    ),
    'step3' => array(
        'icon' => '/svg/icons/ic_object_building.svg',
        'title' => 'Informacja o nieruchomośći',
    ),
    'step4' => array(
        'icon' => '/svg/icons/ic_process.svg',
        'title' => 'Procesy biznesowe',
    ),
    'step5' => array(
        'icon' => '/svg/icons/ic_money.svg',
        'title' => 'Aktywy materialne',
    ),
    'step6' => array(
        'icon' => '/svg/icons/ic_globe.svg',
        'title' => 'Wartości niematerialne',
    ),
    'step7' => array(
        'icon' => '/svg/icons/ic_target_audience.svg',
        'title' => 'Audytorium docelowe',
    ),
    'v_list' => array(
        'months' => 'miesięcy',
        'btn_next_page' => 'Następna strona',
        'btn_prev_page' => 'Poprzednia strona',
        'recommended' => 'Zaleca się',
        'sale' => 'Sprzedane',
        'location' => 'Umiejscowienie',
        'net_profit' => 'Zysk',
        'perc_profit' => 'Dochodowość',
        'payback' => 'opłacalność',
        'profit' => 'Utarg',
        'business_details_link' => 'Więcej o biznesie',
        'deal_details_link' => 'Więcej informacji na temat transakcji',
    ),
    'upload_photo' => array(
        'label' => 'Zdjęcia biznesu',
        'subtitle' => '1000x1000 pikseli, JPG, PNG, do 8 mb',
        'delete' => 'Usunąć',
        'dictCancelUploadConfirmation' => 'Jesteście pewne, że chcecie skasować ładowanie?',
        'make_cover' => 'Zrobić okładką',
    ),
    'v_filter' => array(
        'enter_price' => 'Wprowadź cenę',
        'btn_find' => 'Wyszukać firmę',
    ),
);
