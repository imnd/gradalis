<?php

return array (
  'create' => 
  array (
    'number_square_meters' => 
    array (
      'prevText' => 'м²',
      'placeholder' => '12',
    ),
    'leavingStaff' => 
    array (
      'termsContractDismissal' => 
      array (
        'options' => 
        array (
          4 => 
          array (
            'name' => '3 months',
            'id' => '5',
          ),
          3 => 
          array (
            'name' => '1 month',
            'id' => '4',
          ),
          0 => 
          array (
            'id' => '1',
            'name' => '3 days',
          ),
          1 => 
          array (
            'id' => '2',
            'name' => '1 week',
          ),
          2 => 
          array (
            'id' => '3',
            'name' => '2 weeks',
          ),
        ),
      ),
      'monthly_wages' =>
      array (
        'placeholder' => '1000',
      ),
      'tax_amount_per_month' =>
      array (
        'placeholder' => '1000',
      ),
      'post' => 
      array (
        'title' => 'Career post',
      ),
    ),
    'cost_business_per_year' => 
    array (
      'placeholder' => '25 000',
    ),
    'cost_business_per_quarter' => 
    array (
      'placeholder' => '100 000',
    ),
    'contractors' => 
    array (
      'mail' => 
      array (
        'placeholder' => 'agent@mail.com',
        'title' => 'e-mail',
      ),
      'phone' => 
      array (
        'title' => 'Telefon',
        'placeholder' => '+48 456 566-09-23',
      ),
    ),
    'contextualAdvertisingGAdsens' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'contextualAdvertising' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'contentExpensesSite' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'conditionProperty' => 
    array (
      'options' => 
      array (
        3 => 
        array (
          'id' => '4',
        ),
        2 => 
        array (
          'id' => '3',
        ),
        1 => 
        array (
          'id' => '2',
        ),
        0 => 
        array (
          'id' => '1',
        ),
      ),
    ),
    'condition_equipment' => 
    array (
      'options' => 
      array (
        5 => 
        array (
          'id' => '6',
        ),
        4 => 
        array (
          'id' => '5',
        ),
        0 => 
        array (
          'id' => '1',
        ),
        1 => 
        array (
          'id' => '2',
        ),
        2 => 
        array (
          'id' => '3',
        ),
        3 => 
        array (
          'id' => '4',
        ),
      ),
    ),
    'bannerTizerNetworks' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'averageMonthlyIncome' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'averageMonthlyExpensesSite' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'average_income_target_clients_to' =>
    array (
      'placeholder' => '1000',
    ),
    'average_income_target_clients_from' =>
    array (
      'placeholder' => '1000',
    ),
    'average_check_clients' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'amountDeductionsProfits' => 
    array (
      'prevText' => '%',
      'placeholder' => '75',
    ),
    'alone_clients' => 
    array (
      'prevText' => '%',
      'placeholder' => '50',
    ),
    'age_women_to' =>
    array (
      'placeholder' => '48',
    ),
    'age_women_from' => 
    array (
      'placeholder' => '20',
    ),
    'age_men_from' =>
    array (
      'placeholder' => '20',
    ),
    'age_men_to' =>
    array (
      'placeholder' => '48',
    ),
    'monthlyUtilityCosts' => 
    array (
      'placeholder' => '1000',
    ),
    'monthlyPaymentCredit' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'monthlyPaymentMortgage' => 
    array (
      'placeholder' => '1000',
    ),
    'numberShares' => 
    array (
      'placeholder' => '75',
    ),
    'negativeScenario' => 
    array (
      'placeholder' => '36',
    ),
    'netAverageQuarterlyProfit' => 
    array (
      'placeholder' => '50 000',
    ),
    'numberHouse' => 
    array (
      'placeholder' => '№',
    ),
    'numberOffice' => 
    array (
      'placeholder' => '№',
    ),
    'otherExpensesSite' => 
    array (
      'placeholder' => '1000',
      'prevText' => '€',
    ),
    'price' => 
    array (
      'placeholder' => '100 000',
    ),
    'price_square_meters' => 
    array (
      'placeholder' => '1000',
    ),
    'profitability' => 
    array (
      'placeholder' => '1 000 000',
    ),
    'repaidAmountCredit' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'repaidAmountMortgage' => 
    array (
      'placeholder' => '1000',
    ),
    'revenue' => 
    array (
      'placeholder' => '10 000 000',
    ),
    'socNetItems' => 
    array (
      'expensesQuarter' => 
      array (
        'prevText' => '€',
        'placeholder' => '1000',
      ),
      'expensesYear' => 
      array (
        'prevText' => '€',
        'placeholder' => '1000',
      ),
      'incomeQuarter' => 
      array (
        'placeholder' => '1000',
        'prevText' => '€',
      ),
      'incomeYear' => 
      array (
        'prevText' => '€',
        'placeholder' => '1000',
      ),
      'turnover_year' => 
      array (
        'prevText' => '€',
        'placeholder' => '1000',
      ),
      'turnoverQuarter' => 
      array (
        'prevText' => '€',
        'placeholder' => '1000',
      ),
    ),
    'totalAmountCredit' => 
    array (
      'placeholder' => '1000',
      'prevText' => '€',
    ),
    'visitorsPerDay' => 
    array (
      'placeholder' => '18 300',
    ),
    'hostingExpensesSite' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'cpa' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
      'title' => 'CPA',
    ),
    'costChanging' => 
    array (
      'placeholder' => '1000',
    ),
    'balanceCredit' => 
    array (
      'prevText' => '€',
      'placeholder' => '1000',
    ),
    'averageQuarterlyTurnover' => 
    array (
      'placeholder' => '500 000',
    ),
    'propertyCategories' =>
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
          'name' => 'Freehold estate',
        ),
        1 => 
        array (
          'id' => '2',
          'name' => 'Leasehold estate',
        ),
      ),
    ),
    'index' => 
    array (
      'title' => 'Index',
    ),
    'cms' => 
    array (
      'title' => 'Content Management System',
      'options' => 
      array (
        0 => 
        array (
          'name' => 'WordPress',
          'id' => '1',
        ),
        1 => 
        array (
          'id' => '2',
          'name' => 'Joomla',
        ),
        2 => 
        array (
          'id' => '3',
          'name' => 'Drupal',
        ),
      ),
    ),
    'gender_target_audience' =>
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
          'name' => 'Men',
        ),
        1 => 
        array (
          'id' => '2',
          'name' => 'Women',
        ),
        2 => 
        array (
          'id' => '3',
          'name' => 'It\'s dosen\'t matter',
        ),
      ),
    ),
    'social_status_clients' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
        1 => 
        array (
          'id' => '2',
        ),
        2 => 
        array (
          'id' => '3',
        ),
      ),
    ),
    'legal_status' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'name' => 'Limited liability company',
        ),
        1 => 
        array (
          'id' => '2',
          'name' => 'Joint stock company',
        ),
        2 => 
        array (
          'id' => '3',
          'name' => 'Limited & Unlimited Partnerships',
        ),
      ),
    ),
    'tax_system' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
          'name' => 'Personal Income Tax  (PIT)',
        ),
        1 => 
        array (
          'id' => '2',
          'name' => 'Corporate Income Tax (CIT)',
        ),
        2 => 
        array (
          'id' => '3',
          'name' => 'Value-added tax (VAT)',
        ),
      ),
    ),
    'month_teach' => 
    array (
      'placeholder' => '1',
      'prevText' => 'Months',
    ),
    'leaseTermTo' => 
    array (
      'placeholder' => '25.02.2020',
    ),
    'b2c' => 
    array (
      'title' => 'B2C',
    ),
    'b2b' => 
    array (
      'title' => 'B2B',
    ),
    'textSiteCopyright' => 
    array (
      'placeholder' => '50',
      'prevText' => '%',
    ),
    'houseBookNumber' => 
    array (
      'placeholder' => '№',
    ),
    'imgSiteOtherSources' => 
    array (
      'placeholder' => '50',
      'prevText' => '%',
    ),
    'pensioners_clients' =>
    array (
      'placeholder' => '50',
      'prevText' => '%',
    ),
    'payback' => 
    array (
      'placeholder' => '12',
    ),
    'yearFoundationBusiness' => 
    array (
      'placeholder' => '2007',
    ),
    'viewsPerDay' => 
    array (
      'placeholder' => '12 300',
    ),
    'typeTransferCustomerBase' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
          'name' => 'Excel',
        ),
        1 => 
        array (
          'id' => '2',
        ),
      ),
    ),
    'sourceGoogle' => 
    array (
      'placeholder' => '1000',
    ),
    'sourceYandex' => 
    array (
      'placeholder' => '1000',
    ),
    'staff' => 
    array (
      'monthly_wages' =>
      array (
        'placeholder' => '1000',
      ),
      'termsContractDismissal' => 
      array (
        'options' => 
        array (
          0 => 
          array (
            'id' => '1',
            'name' => '3 days',
          ),
          1 => 
          array (
            'id' => '2',
            'name' => '1 week',
          ),
          2 => 
          array (
            'id' => '3',
            'name' => '2 weeks',
          ),
          3 => 
          array (
            'id' => '4',
            'name' => '1 month',
          ),
          4 => 
          array (
            'id' => '5',
            'name' => '3 months',
          ),
        ),
      ),
    ),
    'balanceMortgage' => 
    array (
      'placeholder' => '1000',
    ),
    'couples_clients' => 
    array (
      'placeholder' => '50',
      'prevText' => '%',
    ),
    'deductionsDepreciation' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
      ),
    ),
    'count_perpetual_service_contracts' =>
    array (
      'placeholder' => '1',
    ),
    'family_status_clients' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
        1 => 
        array (
          'id' => '2',
        ),
        2 => 
        array (
          'id' => '3',
        ),
      ),
    ),
    'families_with_children_clients' => 
    array (
      'placeholder' => '50',
      'prevText' => '%',
    ),
    'typeTrainingMaterials' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
        1 => 
        array (
          'id' => '2',
        ),
      ),
    ),
    'purposeCredit' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
        1 => 
        array (
          'id' => '2',
        ),
        10 => 
        array (
          'id' => '11',
        ),
        2 => 
        array (
          'id' => '3',
        ),
      ),
    ),
    'yearCreationSite' => 
    array (
      'placeholder' => '2010',
    ),
    'totalAmountMortgage' => 
    array (
      'placeholder' => '1000',
    ),
    'natureTraffic' => 
    array (
      'options' => 
      array (
        0 => 
        array (
          'id' => '1',
        ),
        1 => 
        array (
          'id' => '2',
        ),
        2 => 
        array (
          'id' => '3',
        ),
      ),
    ),
  ),
  'category' => 'Test2',
  'show' => 
  array (
    'city' => 'City',
    'country' => 'Country',
    'zip_code' => 'Index',
    'months' => 'Months',
    'similar_offers_item' => 
    array (
      'months' => 'months',
    ),
  ),
  'v_list' => 
  array (
    'months' => 'months',
    'btn_next_page' => 'Next page',
    'btn_prev_page' => 'Previous page',
  ),
);
