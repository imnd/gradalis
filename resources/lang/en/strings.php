<?php

return array (
  'region' => 'Region',
  'city' => 'City',
  'country' => 'Country',
  'save' => 'Save',
  'reserve' => 'Reserving',
  'step' => 'Step',
  'send' => 'Sending',
);
