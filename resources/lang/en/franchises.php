<?php

return array (
  'create' => 
  array (
    'direction' => 
    array (
      'options' => 
      array (
        3 => 
        array (
          'name' => 'Food service',
          'id' => '4',
        ),
        2 => 
        array (
          'id' => '3',
          'name' => 'Industry',
        ),
        0 => 
        array (
          'id' => '1',
          'name' => 'Commerce',
        ),
        1 => 
        array (
          'id' => '2',
          'name' => 'Service',
        ),
      ),
    ),
    'leavingStaff' => 
    array (
      'termsContractDismissal' => 
      array (
        'options' => 
        array (
          0 => 
          array (
            'id' => '1',
            'name' => '3 days',
          ),
          1 => 
          array (
            'id' => '2',
            'name' => '1 week',
          ),
          2 => 
          array (
            'id' => '3',
            'name' => '2 weeks',
          ),
          3 => 
          array (
            'id' => '4',
            'name' => '1 month',
          ),
          4 => 
          array (
            'id' => '5',
            'name' => '3 months',
          ),
        ),
      ),
    ),
    'number_square_meters' => 
    array (
      'prevText' => 'м²',
    ),
    'cpa' => 
    array (
      'placeholder' => '1000',
      'title' => 'CPA',
    ),
  ),
);
