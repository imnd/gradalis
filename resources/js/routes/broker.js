const RealTime = () => import('../components/broker/views/RealTime');
const Balance = () => import('../components/broker/views/Balance');
const Leads = () => import('../components/broker/views/Leads');
const Settings = () => import('../components/broker/views/Settings');
const SummaryIndicators = () => import('../components/broker/views/SummaryIndicators');
const WebOffers = () => import('../components/broker/views/WebOffers');
const WebOffer = () => import('../components/broker/views/WebOffer');
const BrokerChat = () => import('../components/broker/views/Chat');
const SummaryIndicators2 = () => import('../components/broker/views/SummaryIndicators2');
export default [
    /* Broker */
    {
        path: '/broker/', redirect: '/broker/summary',
        meta: {breadcrumb: {label: 'Кабинет', langKey: 'cabinet'}},
    },
    {
        path: '/broker/realtime', component: RealTime,
        meta: {breadcrumb: {label: 'В реальном времени', parent: 'cabinet', langKey: 'real_time'}},
    },
    {
        path: '/broker/balance', component: Balance,
        meta: {breadcrumb: {label: 'Баланс', parent: 'cabinet', langKey: 'balance'}},
    },
    {
        path: '/broker/leads', component: Leads,
        meta: {breadcrumb: {label: 'Лиды', parent: 'cabinet', langKey: 'leads'}},
    },
    {
        path: '/broker/chat', component: BrokerChat,
        meta: {breadcrumb: {label: 'Сообщения', parent: 'cabinet', langKey: 'messages'}},
        children: [
            {
                path: 'new-dialog/:user_id',
                component: BrokerChat,
                name: 'broker/new-dialog'
            }
        ]
    },
    {
        path: '/broker/settings', component: Settings,
        meta: {breadcrumb: {label: 'Настройки', parent: 'cabinet', langKey: 'settings'}},
    },
    {
        path: '/broker/summary', component: SummaryIndicators,
        meta: {breadcrumb: {label: 'Сводные показатли', parent: 'cabinet', langKey: 'summary'}},
    },
    {
        path: '/broker/summary2', component: SummaryIndicators2,
        meta: {breadcrumb: {label: 'Сводные показатли', parent: 'cabinet', langKey: 'summary'}},
    },
    {
        path: '/broker/web-offers', component: WebOffers,
        meta: {breadcrumb: {label: 'Офферы', parent: 'cabinet', langKey: 'offers'}}
    },
    {
        path: '/broker/web-offer/:id', component: WebOffer,
        meta: {breadcrumb: {label: 'Оффер', parent: 'cabinet', langKey: 'offer'}}
    }
]
