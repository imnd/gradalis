const AccountPartners = () => import('../components/account/views/Partners');
const AccountPartnersFuture = () => import('../components/account/views/FuturePartners');
const AccountPartnerSettings = () => import('../components/account/views/SettingsPartner');

export default [
    /* Account */
    {path: '/account/', redirect: '/account/partners'},
    {
        path: '/account/partners',
        component: AccountPartners,
        meta: {breadcrumb: {label: 'Партнеры', langKey: 'partners'}}
    },
    {
        path: '/account/partners/future', component: AccountPartnersFuture,
        meta: {breadcrumb: {label: 'Будущие партнеры', parent: 'partners', langKey: 'future_partners'}}
    },
    {
        path: '/account/partner/:userId/settings',
        component: AccountPartnerSettings,
        props: true,
        name: 'partner-settings'
    },
]
