import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios/index';
import swal from 'vue-sweetalert2/src/index';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        lang: document.documentElement.lang,
        user: window.user,
        currency: window.currency,
        rates: window.rates,
        dialogs: {
            expanded: false,
            id: null
        },
        chat: {
            expanded: false,
            to: null,
            theme: '',
            message: '',
        },
        partner: window.partner,
        languages: ['ru', 'pl', 'en'],
        notifications: window.user && window.user.notifications.length > 0 ? window.user.notifications : []
    },
    mutations: {
        add_notification(state, notification) {
            state.notifications.unshift(notification)
        },
        setNotifications(state, notifications){
            state.notifications = notifications
        },
        removeNotification(state, notification) {
            state.notifications = state.notifications.filter((item) => item.id !== notification.id)
        }
    },
    actions: {
        getNotifications({commit}){
            axios.get('/notification/getUnread').then(res =>{
                commit('setNotifications', res.data)
            }).catch(e => {
                swal({type: 'error', title: e.response.status, text: e.response.data.message});
            })
        },

        removeNotification({ commit }, notification) {
            commit('removeNotification', notification)

            axios.delete('/notification/delete/'+notification.id).then(res =>{
            }).catch(e => {
                swal({type: 'error', title: e.response.status, text: e.response.data.message});
            })
        },
    }
})
