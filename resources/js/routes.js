import Vue from 'vue'
import Router from 'vue-router'
import BrokerRoutes from './routes/broker';
import AccountRoutes from './routes/account';

Vue.use(Router);
window.trans = (string) => _.get(window.i18n, string);

const ServiceBuyCompanyLegalEntities = () => import('./components/profile/services/buy-company/LegalEntities');
const ServiceBuyCompanyDocuments = () => import('./components/profile/services/buy-company/Documents');
const ServiceBuyCompanyResults = () => import('./components/profile/services/buy-company/Results');
const ServiceBuyCompanyForm = () => import('./components/profile/services/buy-company/Form');
const ServiceRecruitingForm = () => import('./components/profile/services/recruiting/Form');
const ServiceRecruitingProcess = () => import('./components/profile/services/recruiting/Process');

const NewFranchise = () => import('./components/profile/franchise/New');

const Register = () => import('./components/register/Register');

// Folders

const baseRoutes = [
    /* Seller */
    {
        path: '/profile/',
        redirect: '/profile/purchased-services',
        name: 'profile',
        meta: {breadcrumb: {label: 'Профиль', langKey: 'profile'}}
    },
    {
        path: '/profile/chat', component: require('./components/profile/views/chat').default,
        meta: {breadcrumb: {label: 'Сообщения', parent: 'profile', langKey: 'messages'}},
        children: [
            {
                path: 'new-dialog/:user_id',
                component: require('./components/profile/views/chat').default,
                name: 'new-dialog'
            }
        ]
    },
    {
        path: '/profile/settings',
        component: require('./components/profile/views/settings').default,
        meta: {breadcrumb: {parent: 'profile', label: 'Настройки', langKey: 'settings'}}
    },
    // {
    //     path: '/profile/products',
    //     component: require('./components/profile/views/products').default,
    //     meta: {breadcrumb: {parent: 'profile', label: 'Продукты'}}
    // },
    {
        path: '/profile/purchased-services',
        component: require('./components/profile/views/purchased-services').default,
        name: 'purchased-services',
        meta: {breadcrumb: {parent: 'profile', label: 'Купленные продукты', langKey: 'purchased_services'}}
    },
    {
        path: '/profile/reserved-business',
        component: require('./components/profile/business/Reserved').default,
        name: 'reserved-business',
        meta: {breadcrumb: {parent: 'profile', label: 'Купленные продукты'}}
    },
    {
        name: 'purchased-service-detail',
        path: '/profile/purchased-services/:id',
        component: require('./components/profile/views/purchased-service').default,
        meta: {
            breadcrumb: {
                parent: 'purchased-services',
                label: 'Детальная услуга',
                langKey: 'purchased_service_detail'
            }
        },
    },
    {
        path: '/profile/favorites',
        component: require('./components/profile/views/favorites').default,
        meta: {breadcrumb: {parent: 'profile', label: 'Избранное', langKey: 'favorites'}}
    },
    {
        path: '/profile/legal',
        component: require('./components/profile/views/legal').default,
        meta: {breadcrumb: {parent: 'profile', label: 'Условия договора', langKey: 'legal'}}
    },
    {
        path: '/profile/balance',
        component: require('./components/profile/views/balance').default,
        meta: {breadcrumb: {parent: 'profile', label: 'Баланс', langKey: 'balance'}}
    },
    {
        path: '/profile/business',
        component: require('./components/profile/business/List').default,
        meta: {breadcrumb: {
            parent: 'profile',
            label: trans('profile.businesses.title'),
        }}
    },
    {
        path: '/profile/reserved',
        component: require('./components/profile/franchise/Reserved').default,
        name: 'reserved-franchise',
        meta: {breadcrumb: {parent: 'profile', label: 'Купленные продукты'}}
    },
    {
        path: '/profile/franchise',
        component: require('./components/profile/franchise/List').default,
        meta: {breadcrumb: {
                parent: 'profile',
                label: trans('profile.franchises.title')
            }}
    },
    {
        path: '/profile/object-requests/view',
        component: require('./components/profile/views/object-requests').default,
        meta: {breadcrumb: {parent: 'profile', label: 'Запросы', langKey: 'object_requests'}}
    },

    {
        path: '/profile/object-requests/doc',
        component: require('./components/profile/views/doc-requests').default,
        meta: {breadcrumb: {parent: 'profile', label: 'Запросы', langKey: 'object_requests'}}
    },

        /* Buyer */
        {
            path: '/profile/trips',
            component: require('./components/profile/trips/List').default,
            meta: {breadcrumb: {parent: 'profile', label: 'Ваши поездки', langKey:'trips'}}
        },
    {
        path: '/profile/trips/:id', component: require('./components/profile/trips/Detail').default,
        meta: {breadcrumb: {parent: 'profile', label: 'Ваши поездки',langKey:'trips'}}},
    // ToDo: Cтатичная верстка
    {
        path: '/profile/franchise-new',
        component: NewFranchise,
        meta: {breadcrumb: {parent: 'profile', label: 'Запросы', langKey: 'object_requests'}}
    },

    /* Buyer */
    {
        path: '/profile/trips',
        component: require('./components/profile/trips/List').default,
        meta: {breadcrumb: {parent: 'profile', label: 'Ваши поездки', langKey: 'trips'}}
    },
    {path: '/profile/trips/:id', component: require('./components/profile/trips/Detail').default},

    /* Services */
    // ToDo: поправить ссылки
    {
        path: '/profile/services/buy-company/legal-entities', component: ServiceBuyCompanyLegalEntities,
        meta: {breadcrumb: {parent: 'profile', label: 'Покупка юридического лица', langKey: 'buy_company'}}
    },
    {
        path: '/profile/services/buy-company/documents', component: ServiceBuyCompanyDocuments,
        meta: {breadcrumb: {parent: 'profile', label: 'Покупка юридического лица', langKey: 'buy_company'}}
    },
    {
        path: '/profile/services/buy-company/results', component: ServiceBuyCompanyResults,
        meta: {breadcrumb: {parent: 'profile', label: 'Покупка юридического лица', langKey: 'buy_company'}}
    },
    {
        path: '/profile/services/buy-company/create', component: ServiceBuyCompanyForm,
        meta: {breadcrumb: {parent: 'profile', label: 'Покупка юридического лица', langKey: 'buy_company'}}
    },
    {
        path: '/profile/services/recruiting/create', component: ServiceRecruitingForm,
        meta: {
            breadcrumb: {
                parent: 'profile',
                label: 'Подбор персонала',
                langKey: 'recruiting'
            }
        }
    },
    {
        path: '/profile/services/recruiting/process', component: ServiceRecruitingProcess,
        meta: {
            breadcrumb: {
                parent: 'profile',
                label: 'Подбор персонала',
                langKey: 'recruiting'
            }
        }
    },

    /* user services */
    {
        path: '/profile/user-services/list',
        component: require('./components/profile/user-service/List').default,
        meta: {
            breadcrumb: {
                parent: 'profile',
                label: 'Мои услуги',
                langKey: 'your_services'
            }
        }
    },
    {
        path: '/profile/user-services/create',
        component: require('./components/profile/user-service/Form').default,
        meta: {
            breadcrumb: {
                parent: 'profile',
                label: 'Новая услуга',
                langKey: 'your_services'
            }
        },
    },
    {
        path: '/profile/user-services/update/:id',
        component: require('./components/profile/user-service/Form').default,
        meta: {
            breadcrumb: {
                parent: 'profile',
                label: 'Ваша услуга',
                langKey: 'your_services'
            }
        },
    },

    /* 404 */
    // TODO сделать 404 в SPA роутах
    // {
    //      path: "*",
    //      beforeEnter: () => { window.location.pathname = '/404'
    //      }
    // }
];

const routes = baseRoutes.concat(
    BrokerRoutes,
    AccountRoutes
);
export default new Router({
    mode: 'history',
    scrollBehavior: () => ({y: 0}),
    routes: routes
})
