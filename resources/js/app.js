require('@/bootstrap');

import Vue from 'vue';
import VTooltip from 'v-tooltip'
import Vuebar from 'vuebar'
import Vuelidate from 'vuelidate'
import PortalVue from 'portal-vue'
import VueSweetalert2 from 'vue-sweetalert2';
import router from '@/routes'
import store from '@/store/index'
import * as VueGoogleMaps from 'vue2-google-maps'
import inlineSVG from 'inline-svg';
import Swiper from 'swiper';
import VueMomentTz from 'vue-moment-tz';
import VueMq from 'vue-mq'
import VSwitch from 'v-switch-case';

import Sticky from 'sticky-js';
import {BulmaAccordion, BulmaAccordionItem} from "vue-bulma-accordion";

import 'lightgallery.js';

let sticky = new Sticky('.sticky');

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDs7VrVm9-Uc98tTj0eYIRgNkisaLQsWlg',
        libraries: 'places', // This is required if you use the Autocomplete plugin
    },
});

Vue.use(VueMq, {
    breakpoints: {
        mobile: 768,
        tablet: 1024,
        desktop: 1215,
    }
});

window.Vue = Vue;

Vue.prototype.lroute = require('./laravel-routes');

window.trans = (string) => _.get(window.i18n, string);

window.lang = document.documentElement.lang;

Vue.prototype.trans = (string, args) => {
    let value = _.get(window.i18n, string);
    _.eachRight(args, (paramVal, paramKey) => {
        value = _.replace(value, `:${paramKey}`, paramVal);
    });
    return value ? value : '!TranslateNotFound!';
};

Vue.use(VueSweetalert2);
Vue.use(VTooltip);
Vue.use(PortalVue);
Vue.use(Vuebar);
Vue.use(VSwitch);
Vue.use(VueMomentTz);
Vue.use(Vuelidate);

/*// TODO !! Это дело плохо работает с классом form.js !! переписать или там, или тут
function errorResponseHandler(error) {
    // check for errorHandle config
    if (error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false) {
        return Promise.reject(error);
    }

    if (error.response) {
    // Если это не ошибка валидации ларавела
    // TODO добавить редирект на login, если авторизация истекла
    if(error.response.status !== 422)
        Vue.swal({type: 'error', title: error.response.status, text: error.response.data.message});
    }
}

apply interceptor on response
window.axios.interceptors.response.use(
    response => response,
    errorResponseHandler
);*/

axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': Laravel.csrfToken,
    'Authorization': 'Bearer ' + Laravel.apiToken,
};
const SvgIcon = () => import('./components/SvgIcon');
Vue.component('svg-icon', SvgIcon);

Vue.component('business-list', require('./components/business/List.vue').default);
Vue.component('news-index', require('./components/news/Index.vue').default);
Vue.component('news-filter', require('./components/news/NewsFilter.vue').default);
Vue.component('reserve-button', require('./components/ReserveButton.vue').default);
Vue.component('main-list', require('./components/business/MainList.vue').default);
Vue.component('main-list-franchise', require('./components/franchises/MainList.vue').default);
Vue.component('chat', require('./components/chat/chat.vue').default);
Vue.component('dialogs-widget', require('./components/chat/dialogs-widget.vue').default);
Vue.component('chat-widget', require('./components/chat/dialogs-widget.vue').default);

Vue.component('example-form-short', require('./components/ExampleFormShort.vue').default);
Vue.component('example-form-filter', require('./components/business/FormFilter.vue').default);
Vue.component('example-form-change-password', require('./components/ExampleFormChangePassword.vue').default);
Vue.component('example-form-profile-spa', require('./components/ExampleFormProfileSPA.vue').default);
Vue.component('example-ui-form', require('./components/ExampleUiForm.vue').default);
Vue.component('example-file-upload-vue', require('./components/ExampleFileUploadVue.vue').default);

Vue.component('form-register-seller', require('./components/register/FormRegisterSeller').default);
Vue.component('form-register-buyer', require('./components/register/FormRegisterBuyer').default);
const FormRegisterExecutor = () => import('./components/register/FormRegisterExecutor');
Vue.component('form-register-executor', FormRegisterExecutor);
const FormRegisterMediaBuyer = () => import('./components/register/FormRegisterMediaBuyer');
Vue.component('form-register-media-buyer', FormRegisterMediaBuyer);
const FormRegisterBroker = () => import('./components/register/FormRegisterBroker');
Vue.component('form-register-broker', FormRegisterBroker);

Vue.component('register', require('./components/register/Register').default);

Vue.component('errorPage', require('./components/ErrorPage').default);

Vue.component('form-add-business', require('./components/business/Form').default);
Vue.component('upload-photo-business', require('./components/UploadPhotoBusiness.vue').default);

Vue.component('profile', require('./components/profile/profile').default);
Vue.component('header-notifications', require('./components/notifications/header-notifications').default);

Vue.component('home', require('./components/Home').default);


/**
 * Глобальные миксины
 */
Vue.mixin({
    destroyed() {
        // //console.log("I've been destroyed");
    },
    methods: {
        $t(translatableObject) {
            return translatableObject ? translatableObject[this.$store.state.lang] : '';
        },
        $writeTo(to, message = '', theme = '', type = '') {
            this.$store.state.chat.to = to;
            this.$store.state.chat.message = message;
            this.$store.state.chat.theme = theme;
            this.$store.state.chat.type = type;
            this.$store.state.dialogs.expanded = true;
        },
        $userCan(permission) {
            if (!this.$store.state.user)
                return false;
            return _.find(this.$store.state.user.permissions, item => {
                return item.name == permission
            })
        },
        $userIs(role) {
            if (!this.$store.state.user) {
                return false;
            }
            return _.find(this.$store.state.user.roles, item => {
                return item.name === role
            })
        },
        $formatPrice(value, locale = this.$store.state.currency.locale, currency = this.$store.state.currency.val) {
            let opt = {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0,
            };
            if (this.$store.state.currency.val !== 'EUR') {
                value *= this.$store.state.rates['EUR_' + this.$store.state.currency.val];
            }
            if (this.$store.state.currency.val === 'BTC') {
                opt = {
                    minimumFractionDigits: 0,
                    maximumFractionDigits: 6,
                };
            }
            return value.toLocaleString(locale, {
                style: 'currency',
                currency: currency,
                ...opt
            })
        },
        $getDateTime(dbDateTime, with_time = false) {
            let monthNames = [
                'Январь',
                'Февраль',
                'Март',
                'Апрель',
                'Май',
                'Июнь',
                'Июль',
                'Август',
                'Сентябрь',
                'Ноябрь',
                'Декабрь',
            ];
            let date = new Date(Date.parse(dbDateTime));
            return date.getDate() + ' ' + monthNames[(''+date.getMonth()).slice(-2)] + ' ' + date.getFullYear() + ( with_time ? ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds(): '' );
            //return date.getDate() + '-' + ('0' + date.getMonth()).slice(-2) + '-' + date.getFullYear()
        },
    },
});

Vue.component('broker-offers-modal-create-link', require('./components/broker/offers/ModalCreateLink').default);

Vue.component('broker-real-time-indicators', require('./components/broker/RealTimeIndicators').default);
Vue.component('broker', require('./components/broker/broker').default);

const VacancyResponce = () => import('./components/vacancy/Response');

Vue.component('vacancy-response', VacancyResponce);

// review
Vue.component('add-review', require('./components/reviews/Add').default);
Vue.component('show-review', require('./components/reviews/Show').default);

// services
const ServicesFilter = () => import('./components/profile/user-service/Filter');
Vue.component('services-filter', ServicesFilter);

// services modal
const OrderServiceModal = () => import('./components/services/modals/Order');
Vue.component('order-service-modal', OrderServiceModal);

const ServiceModalOrderSuccess = () => import('./components/services/modals/OrderSuccess');
Vue.component('order-service-modal-success', ServiceModalOrderSuccess);

const OrderUserServiceModal = () => import('./components/profile/user-service/modals/Order');
Vue.component('order-user-service-modal', OrderUserServiceModal);

// account
Vue.component('account', require('./components/account/Account').default);

// map
const Map = () => import('./components/Map');
Vue.component('google-map', Map);

// franchise
Vue.component('cards-franchises-new', require('./components/profile/franchise/New').default);
const ModalBuyFranchise = () => import('./components/franchises/ModalBuyFranchise');
Vue.component('modal-buy-franchise', ModalBuyFranchise);
const ModalAddReviewFranchise = () => import('./components/profile/franchise/modals/AddReview');
Vue.component('modal-add-review-franchise', ModalAddReviewFranchise);
const ModalShowReviewFranchise = () => import('./components/franchises/ModalShowReview');
Vue.component('modal-show-review-franchise', ModalShowReviewFranchise);
const ModalAddRequestDoc = () => import('./components/franchises/ModalAddRequestDoc');
Vue.component('modal-add-request-doc', ModalAddRequestDoc);

const MapObjectsFranchises = () => import('./components/franchises/MapObjects');
Vue.component('map-objects-franchises', MapObjectsFranchises);
Vue.component('franchises-list', require('./components/franchises/List.vue').default);
Vue.component('franchises-form', require('./components/franchises/Form').default);

Vue.component('modal-upload-document', require('./components/profile/franchise/modals/UploadDocument').default);

// динамический перевод хлебных крошек
router.options.routes.map(route => {
    if (
           route.meta !== undefined
        && route.meta.breadcrumb !== undefined
        && route.meta.breadcrumb.langKey !== undefined
    ) {
        route.meta.breadcrumb.label = trans('profile.routes.' + route.meta.breadcrumb.langKey) || route.meta.breadcrumb.label;
    }
    return route;
});

const app = new Vue({
    el: '#app',
    components: {
        BulmaAccordion,
        BulmaAccordionItem
    },
    data: {
        object: {
            // Переменные, используемые на странице объекта
            showDetailedInformation: false
        },
        vacancy: {
            // Переменные, используемые на страницах вакансий
            showResponseModal: false
        },
        reviews: {
            // Переменные, используемые на страницах отзывов
            showAddReviewModal: false,
            showSingleReviewModal: false,
            showSingleReviewModalID: null
        },
        franchise: {
            // Переменные, используемые на странице франшизы
            showModalBuy: false,
            showModalAddReview: false,
            showModalShowReview: false,
            showModalAddRequestDoc: false,
        },
        // Переменные, используемые на страницах услуг
        services: {
            showModalOrder: false,
            showModalOrderSuccess: false
        },
        // Переменные, используемые на страницах услуг
        userServices: {
            showModalOrder: false,
            showModalOrderSuccess: false
        },
    },
    router,
    store,
    mounted() {
        // Подписка на уведомления
        if (this.$store.state.user) {
            // Имя канала для уведомлений можно поменять, надо смотреть как https://github.com/laravel/ideas/issues/202
            if (window.Echo === undefined) {
                return;
            }
            window.Echo
                .private('App.Models.Auth.User.' + this.$store.state.user.id)
                .notification((notification) => {
                    this.$store.commit('add_notification', notification);
                });
        }
    },
    methods: {
        showModal(id) {
            document.getElementById(id).classList.add('is-active');
        },
        hideModal(id) {
            document.getElementById(id).classList.remove('is-active');
        },
        showSingleReviewModal(id) {
            this.reviews.showSingleReviewModal = true;
            this.reviews.showSingleReviewModalID = id;
        },
        toggleSubMenu(event) {
            let el = null;
            el = event.target.closest('.aside-nav__item');
            el.classList.toggle("sub-menu-active");
        },
        /**
         * @param {integer} id
         * @param {string} type - business или franchise
         */
        toggleFavorite(id, type) {
            axios.post('/profile/favorites/' + type + '/' + id)
                .then(res => {
                    this.$swal({type: 'success', text: res.data.message});
                    this.$emit('fetch-data');
                }).catch(e => {
                this.$swal({type: 'error', title: e.response.status, text: e.response.data.message});
            })
        }
    }
});

store.state.status = {
    franchise: {
        // Новая
        NEW: 0,
        // В ожидании
        AWAIT: 1,
        // Прошел модерацию
        MODERATED: 2,
        // Отклонен
        DECLINED: 3,
        // Продан
        SOLD: 4,
        // Зарезервирован
        RESERVED: 4,
    },
    service: {
        // Новая
        NEW: 0,
        // В ожидании
        AWAIT: 1,
        // Активный
        MODERATED: 2,
        // Отклонен
        DECLINED: 3,
    }
};

// Форма франшиз. Условия покупки=Без ничего
store.state.CONDITION_WITHOUT_ANYTHING = 0;
// Форма франшиз. Условия покупки=Роялити
store.state.CONDITION_ROYALTY = 1;
// Форма франшиз. Условия покупки=Паушальный взнос
store.state.CONDITION_LUMP_SUM = 2;

// Роли юзеров
store.state.ROLE_EXECUTIVE = 'Исполнитель';
store.state.ROLE_BUYER = 'Покупатель';
store.state.ROLE_SELLER = 'Продавец';

// Форма франшиз. Видео в модерации
store.state.VIDEO_ON_MODERATION= 0;
// Форма франшиз. Видео одобрен
store.state.VIDEO_APPROVED = 1;
// Форма франшиз. Видео откленен
store.state.VIDEO_DECLINED = 2;

inlineSVG.init({
    svgSelector: 'img.svg', // the class attached to all images that should be inlined
    initClass: 'js-inlinesvg', // class added to <html>
}, function () {
    //console.log('All SVGs inlined');
});


/**
 * Слайдер новостей на главной
 */
window.Swiper = Swiper;
var newsHomeSwiper = new Swiper('.swiper-news-home', {
    slidesPerView: 1,
    spaceBetween: 30,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
    },
    breakpointsInverse: true,
    breakpoints: {
        // when window width is >= 768px
        768: {
            slidesPerView: 3
        }
    }
});

/**
 * Слайдер информационный на главной (слева)
 */
var swiperHomeInfoLeft = new Swiper('.swiper-home-info-left', {
    spaceBetween: 30,
    effect: 'fade',
    loop: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
    },
});

/**
 * Слайдер информационный на главной (Правый)
 */
var swiperHomeInfoRight = new Swiper('.swiper-home-info-right', {
    spaceBetween: 30,
    effect: 'fade',
    loop: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
    },
});

/**
 * Слайдер на странице объекта (основной)
 */
var swiperObjectThumbs = new Swiper('.swiper-object-thumbs', {
    spaceBetween: 16,
    slidesPerView: 3,
    // loop: true,
    // freeMode: false,
    // loopedSlides: 5, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpointsInverse: true,
    breakpoints: {
        // when window width is >= 1088px
        1088: {
            slidesPerView: 5
        }
    }
});
var swiperObjectMain = new Swiper('.swiper-object-main', {
    spaceBetween: 0,
    // loop:true,
    // loopedSlides: 5, //looped slides should be the same
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: swiperObjectThumbs,
    },
});

/**
 * Слайдер услуг на странице объекта
 */
var swiperObjectServices = new Swiper('.swiper-object-services', {
    slidesPerView: 1,
    spaceBetween: 8,
    loop: false,
    navigation: {
        nextEl: '.swiper-object-services-next',
        prevEl: '.swiper-object-services-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
    },
    breakpointsInverse: true,
    breakpoints: {
        // when window width is >= 768px
        768: {
            slidesPerView: 2
        },
        // when window width is >= 1088px
        1088: {
            slidesPerView: 3
        }
    }
});

/**
 * Слайдер отзывов на странице франшизы
 */
var swiperFranchiseReviews = new Swiper('.swiper-franchise-reviews', {
    slidesPerView: 1,
    centeredSlides: true,
    spaceBetween: 0,
    loop: false,
    freeMode: false,
    width: 230,
    setWrapperSize: true,
    navigation: {
        nextEl: '.swiper-franchise-reviews-next',
        prevEl: '.swiper-franchise-reviews-prev',
    },
    breakpointsInverse: true,
    breakpoints: {
        // when window width is >= 768px
        768: {
            slidesPerView: 3,
            width: 700,
        }
    }
});

/**
 *  Functions
 */
function getAll(selector) {
    return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
}

/**
 *  Dropdowns
 */
var $dropdowns = getAll('.dropdown:not(.is-hoverable)');

if ($dropdowns.length > 0) {
    $dropdowns.forEach(function ($el) {
        $el.addEventListener('click', function (event) {
            let target = event.target;
            if ((!target.closest('.dropdown-trigger')) && (target.closest('.dropdown.is-active')) && !target.classList.contains('is-info') && !target.classList.contains('is-clear')) {
                return;
            } else {
                event.stopPropagation();
                $el.classList.toggle('is-active');
            }
        });
    });

    document.addEventListener('click', function (event) {
        let target = event.target;
        if (!target.closest('.dropdown.is-active')) {
            closeDropdowns();
        }
    });
}

function closeDropdowns() {
    $dropdowns.forEach(function ($el) {
        $el.classList.remove('is-active');
    });
}

/**
 *  Tabs
 */
window.openTab = function openTab(evt, tabName) {
    let i, x, tablinks;
    let target = evt.target;
    let tabsWrap = target.closest(".tabs-wrap");
    x = tabsWrap.querySelectorAll(".content-tab");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = tabsWrap.querySelectorAll(".tab");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" is-active", "");
    }
    tabsWrap.querySelector('#' + tabName).style.display = "block";
    evt.currentTarget.className += " is-active";
};

/**
 *  lightGallery
 */
lightGallery(document.getElementById('swiper-object-main-media-gallery'));


// ToDo: Временно
/**
 *  Category Product Select (Temp)
 */

window.changeCatProduct = function changeCatProduct(evt) {
    let i, x, target;

    if (evt.target.tagName === 'a') {
        target = evt.target;
    } else {
        target = evt.target.closest('a');
    }

    let listWrap = target.closest(".category-service-list");
    x = listWrap.querySelectorAll(".category-service-list__item");

    for (i = 0; i < x.length; i++) {
        x[i].classList.remove('is-active');
    }

    target.classList.add('is-active');
};

