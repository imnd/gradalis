<?php

Breadcrumbs::for('home', function ($trail) {
    $trail->push(trans('menu.home'), route('home'));
});

//region BUSINESS
Breadcrumbs::for('business.index', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.business'), route('business.index'));
});

Breadcrumbs::for('business.show', function ($trail, $model) {
    $trail->parent('business.index');
    $trail->push($model->name, route('business.show', $model->id));
});

Breadcrumbs::for('business.edit', function ($trail, $model) {
    $trail->parent('business.index');
    $trail->push($model->name, route('business.edit', $model->id));
});

Breadcrumbs::for('business.create', function ($trail) {
    $trail->parent('business.index');
    $trail->push(trans('business.create.title'), route('business.create'));
});
//endregion

//region FRANCHISE
Breadcrumbs::for('franchise.index', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.franchise'), route('franchise.index'));
});

Breadcrumbs::for('franchise.create', function ($trail) {
    $trail->parent('franchise.index');
    $trail->push(trans('franchises.create.title'), route('franchise.create'));
});

Breadcrumbs::for('franchise.show', function ($trail, $model) {
    $trail->parent('franchise.index');
    $trail->push($model->name, route('franchise.show', $model->id));
});

Breadcrumbs::for('franchise.edit', function ($trail, $model) {
    $trail->parent('franchise.index');
    $trail->push($model->name, route('franchise.edit', $model->id));
});
//endregion
Breadcrumbs::for('user-services.index', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.user_services'), route('user-services.index'));
});

Breadcrumbs::for('user-services.single', function ($trail) {
    $trail->parent('user-services.index');
    $trail->push(trans('menu.user_services'), route('user-services.single'));
});

Breadcrumbs::for('user-services.single.details', function ($trail) {
    $trail->parent('user-services.index');
    $trail->push(trans('menu.user_services'), route('user-services.single.details'));
});
Breadcrumbs::for('user-services.single.team', function ($trail) {
    $trail->parent('user-services.index');
    $trail->push(trans('menu.user_services'), route('user-services.single.team'));
});

Breadcrumbs::for('user-services.single.cases', function ($trail) {
    $trail->parent('user-services.index');
    $trail->push(trans('menu.user_services'), route('user-services.single.cases'));
});

Breadcrumbs::for('user-services.single.processes', function ($trail) {
    $trail->parent('user-services.index');
    $trail->push(trans('menu.user_services'), route('user-services.single.processes'));
});

Breadcrumbs::for('news.index', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.news'), route('news.index'));
});
Breadcrumbs::for('news.show', function ($trail, $newsCategory, $model) {
    $trail->parent('news.index');
    if($newsCategory->parent){
        $trail->push($newsCategory->parent->title, route('news.index', ['newsCategory' => $newsCategory->parent->url]));
    }
    $trail->push($newsCategory->title, route('news.index', ['newsCategory' => $newsCategory->url]));
    $trail->push($model->title, route('news.show', ['news' => $model, 'newsCategory' => $newsCategory]));
});

Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.about'), route('about'));
});

Breadcrumbs::for('contacts', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.about_items.contacts'), route('contacts'));
});

Breadcrumbs::for('vacancy.index', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.about_items.vacancy'), route('vacancy.index'));
});

Breadcrumbs::for('vacancy.show', function ($trail, $model) {
    $trail->parent('vacancy.index');
    $trail->push($model->name, route('vacancy.show', ['vacancy' => $model]));
});

Breadcrumbs::for('reviews', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('menu.about_items.reviews'), route('reviews'));
});

Breadcrumbs::for('help.index', function ($trail, $helpSection = null, $helpCategory = null, $help = null) {
    $trail->parent('home');
    $trail->push(trans('menu.about_items.help'), route('help.index'));
    if($helpSection && $helpCategory){
        $trail->push($helpCategory->title, route('help.index',  ['helpSection' => $helpSection->url, 'helpCategory' => $helpCategory->url ]));
    }

    if($helpSection && $helpCategory && $help){
        $trail->push($help->title, route('help.index',  ['helpSection' => $helpSection->url, 'helpCategory' => $helpCategory->url , 'help' => $help->url ]));
    }

});

Breadcrumbs::for('help.show', function ($trail, $model) {
    $trail->parent('help.index');
    $trail->push($model->title, route('help.show',
        $model->id));
});

Breadcrumbs::for('register.seller', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('header.sell_business'), route('register.seller'));
});

Breadcrumbs::for('register.buyer', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('header.buy_business'), route('register.buyer'));
});

Breadcrumbs::for('register.executor', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('auth.reg_executor.title'), route('register.executor'));
});

Breadcrumbs::for('register.broker', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('auth.reg_broker.title'), route('register.broker'));
});
Breadcrumbs::for('register.media-buyer', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('auth.reg_media_buyer.title'), route('register.media-buyer'));
});

Breadcrumbs::for('login', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('auth.login'), route('login'));
});
