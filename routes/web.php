<?php

use Illuminate\Support\Facades\Route,
    App\Models\Roles,
    App\Models\Business\Business,
    App\Models\ObjectRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/bb', function() {
    ObjectRequest::create([
        'object_id' => 7,
        'object_type' => 'App\Models\Business\Business',
        'type' => 'view',
        'user_id' => 1
    ]);
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('confirm-email/{key}', 'ProfileController@confirmEmail');

Auth::routes(['verify' => true]);
Route::impersonate();
Route::domain('ref.' . config('app.domain'))->group(function () {
    Route::get('invitation/{token}', 'InvitationController@invitation');
});

Route::get('lang/{lang}', [
    'as' => 'lang.switch',
    'uses' => 'LanguageController@switchLang'
]);
Route::get('currency/{lang}', [
    'as' => 'currency.switch',
    'uses' => 'CurrencyController@switchCurrency'
]);

// JS Localization
Route::get('/js/lang.js', 'CoreController@lang')->name('assets.lang');

/**
 * User data for vuex
 */
Route::get('/js/user.js', 'CoreController@user')->name('assets.user');
Route::get('/js/laravel-routes.js', 'CoreController@laravelRoutes')->name('assets.laravel-routes');

// JS Global variables
Route::get('/js/global_variables.js', 'CoreController@globalVariables')->name('assets.global_variables');

Route::get('/business/single', function () {
    return view('business-single');
})->middleware('auth');

Route::get('/register', function () {
    return view('auth.register');
})->name('register')->middleware('guest');;

Route::get('/ui', function () {
    return view('ui');
})->middleware('auth');

Route::group([
    'prefix' => '/spa/',
], function () {
    Route::get('favorites', function () {
        return view('spa.favorites');
    });
    Route::get('legal', function () {
        return view('spa.legal');
    });
    Route::get('services', function () {
        return view('spa.services');
    });
    Route::get('settings', function () {
        return view('spa.settings');
    });
    Route::get('balance', function () {
        return view('spa.balance');
    });
    Route::get('purchased-services', function () {
        return view('spa.purchased-services');
    });
    Route::get('objects', function () {
        return view('spa.objects');
    });
    Route::get('view-request', function () {
        return view('spa.view-request');
    });
});

Route::get('/add-business', function () {
    return view('add-business');
});
Route::get('/add-business/method', function () {
    return view('add-business-method');
});

/**
 * Static
 */
Route::get('/about', function () {
    return view('about');
})->name('about');

Route::group([
    'as' => 'vacancy.',
    'prefix' => 'vacancy'
], function () {
    Route::get('/', 'VacancyController@list')->name('index');
    Route::get('/{vacancyUrl}', 'VacancyController@show')->name('show');
    Route::post('/{vacancy}/response', 'VacancyController@createResponse')->name('response');
});

Route::get('/b/{id}', function ($id) {
    return Business::find($id)->getFirstMediaUrl('business', 'thumb');
});

Route::get('/contacts', function () {
    return view('contacts');
})->name('contacts');

Route::get('/reviews', 'ReviewsController@index')->name('reviews');

Route::namespace('Help')
    ->as('help.')
    ->prefix('/help')
    ->group(function () {
        Route::get('/{helpSection?}/{helpCategory?}/{help?}', 'HelpController@index')
            ->name('index')
            ->where('helpSection', '[a-z0-9-]+')
            ->where('helpCategory', '[a-z0-9-]+')
            ->where('help', '[a-z0-9-]+');
    });

/**
 * Account
 */
Route::middleware(['role:' . Roles::ROLE_ACCOUNT_MANAGER])
    ->namespace('Account')
    ->prefix('account')
    ->group(function () {
        Route::get('/{vue_capture?}', 'AccountController@index')
            ->where('vue_capture', '.*')
            ->middleware('auth');
    });

/**
 * API Routes
 */
Route::namespace('Api')
    ->group(function () {
        Route::middleware('guest')
            ->group(function () {
                Route::group([
                    'prefix' => 'register',
                ], function () {
                    Route::post('buyer', 'RegisterController@registerBuyer');
                    Route::post('seller', 'RegisterController@registerSeller');
                    Route::post('executor', 'RegisterController@registerExecutor');
                });
                Route::post('/code-verify', 'RegisterController@codeVerify');
                Route::get('get-company-activities', 'RegisterController@companyActivities');
            });

        Route::group([
            'prefix' => 'chat',
            'middleware' => 'auth'
        ], function () {
            Route::get('', 'ChatController@index');
            Route::post('dialog/add', 'ChatController@newDialog');
            Route::get('dialogs', 'ChatController@getDialogs');
            Route::get('dialog/{dialog}', 'ChatController@getDialog');
            Route::get('user-dialog', 'ChatController@getUserDialog');
            Route::get('dialog/{dialog}/messages', 'ChatController@getMessages');
            Route::post('message/add', 'ChatController@newMessage');
            Route::delete('message/{message}', 'ChatController@deleteMessage');
            Route::patch('message/{message}/accept', 'ChatController@acceptMessage');
        });

        Route::get('/account-partner/{partner}', 'AccountController@getPartner');
        Route::get('/account-get-addressees', 'AccountController@getAddressees');

        Route::get('/broker-get-summary', 'BrokerController@getSummary');
        Route::get('/broker-get-offers', 'BrokerController@getOffers');
        Route::get('/broker-get-offers-summary', 'BrokerController@getOffersSummary');

        Route::group([
            'prefix' => 'location',
            'namespace' => 'Lists',
        ], function () {
            Route::get('get-countries', 'LocationController@getCountries');
            Route::get('get-cities/{region}', 'LocationController@getCities');
            Route::get('get-regions/{country}', 'LocationController@getRegions');
        });
        Route::get('/offer-get/{id}', 'OfferController@get');

        Route::get('/news-get', 'NewsController@get');
        Route::get('/news-get-categories', 'NewsController@getCategories');
        Route::get('/news-get-parent-categories', 'NewsController@getParentCategories');
        Route::get('/hp-news', 'NewsController@hpNews');

        Route::group([
            'prefix' => 'business'
        ], function () {
            Route::middleware('auth:api')
                ->group(function () {
                    Route::patch('sell-with-broker/{id}', 'BusinessController@sellWithBroker')->middleware('auth:api');
                    Route::post('change-price/{id}', 'BusinessController@changePrice')->middleware('auth:api');
                    Route::post('reserve', 'BusinessController@reserve');
                    Route::post('image-remove/{business}', 'BusinessController@imageRemove');
                });
            Route::get('get', 'BusinessController@get');
            Route::get('get-by-id/{business}', 'BusinessController@getById');
            Route::get('get-ranges', 'BusinessController@getRanges');
            Route::get('get-categories', 'BusinessController@getCategories');
            Route::get('get-references', 'BusinessController@getReferences');
        });
        Route::group([
            'prefix' => 'franchise'
        ], function () {
            Route::middleware('auth:api')
                ->group(function () {
                    Route::patch('sell-with-broker/{id}', 'FranchiseController@sellWithBroker')->middleware('auth:api');
                    Route::post('change-price/{id}', 'FranchiseController@changePrice')->middleware('auth:api');
                    Route::post('reserve', 'FranchiseController@reserve');
                    Route::post('image-remove/{franchise}', 'FranchiseController@imageRemove');
                    Route::post('object-image-remove/{franchise_object}', 'FranchiseController@objectImageRemove');
                });
            Route::get('get-list', 'FranchiseController@getList');
            Route::get('get/{franchise}', 'FranchiseController@getById');
            Route::get('get-categories', 'FranchiseController@getCategories');
            Route::get('get-references', 'FranchiseController@getReferences');
        });

        Route::middleware('auth:api')
            ->group(function () {
                //Route::middleware(['role:' . Roles::ROLE_ACCOUNT_MANAGER])->group(function(){
                Route::post('/account-get-partners', 'AccountController@getPartners');
                Route::post('/account-partner/{partner}/update', 'AccountController@updatePartner');
                Route::post('/account-get-summary', 'AccountController@getSummary');
                Route::post('/account-partner-status-change', 'AccountController@partnerStatusChange');
                Route::post('/account-chart-data', 'AccountController@getChartData');
                //});

                Route::post('/start-verify', 'RegisterController@startVerify');
                //  Route::post('/code-verify', 'RegisterController@codeVerify');

                Route::post('/review-add', 'ReviewsController@addReview');
                Route::post('/review-show', 'ReviewsController@showReview');

                Route::post('/broker-chart-data', 'BrokerController@getChartData');
                Route::post('/broker-lead-chart-data', 'BrokerController@getLeadChartData');
                Route::post('/broker-get-leads-details', 'BrokerController@getLeadsDetails');

                Route::post('/business-image-upload', 'BusinessController@imageUpload');

                //Route::middleware(['role:' . Roles::ROLE_MEDIA_BUYER])->group(function() {
                Route::post('/offer-bookmark', 'OfferController@bookmark');
                Route::post('/offer-all', 'OfferController@index');
                Route::post('/invitation-create', 'OfferController@invitationCreate');
                //});
            });
});

Route::group([
    'as' => 'franchise.',
    'prefix' => 'franchise',
    'namespace' => 'Franchise',
], function () {
    Route::middleware('auth')->group(function () {
        Route::get('create', function() {
            return view('franchise.create');
        })->name('create');
        Route::post('create', 'FranchiseController@store');
        Route::get('edit/{franchise}', 'FranchiseController@edit')->name('edit');
        Route::post('update/{id}', 'FranchiseController@update');
        Route::delete('delete/{id}', 'FranchiseController@delete');
        Route::post('save/docs', 'FranchiseDocAttachmentController@store');
    });
    Route::get('', 'FranchiseController@index')->name('index');
    Route::get('{id}', 'FranchiseController@show');
    Route::get('get-price-ranges/{currency}', 'FranchiseController@priceRangesByCurrency');
});

Route::group([
    'as' => 'business.',
    'prefix' => 'business',
    'namespace' => 'Business',
], function () {
    Route::middleware('auth')->group(function () {
        Route::get('create', function() {
            return view('business.create');
        });
        Route::post('create', 'BusinessController@store');
        Route::get('edit/{id}', 'BusinessController@edit');
        Route::post('update/{id}', 'BusinessController@update');
        Route::delete('delete/{id}', 'BusinessController@delete');
    });
    Route::get('', function() {
        return view('business.index');
    })->name('index');
    Route::get('{id}', 'BusinessController@show');
});

Route::middleware('auth')
    ->group(function () {
        Route::get('/broker/{vue_capture?}', function() {
            return view('broker.index');
        })->where('vue_capture', '.*')->name('broker.index');
    });

Route::group([
    'as' => 'news.',
    'prefix' => 'news',
    'namespace' => 'News',
], function () {
    Route::get('{newsCategory?}', 'NewsController@index')
        ->name('index')
        ->where('newsCategory', '[a-z0-9-]+');
    Route::get('/{newsCategory}/{news}', 'NewsController@show')
        ->name('show')
        ->where('newsCategory', '[a-z0-9-]+');
});

/**
 * user-services
 */
Route::group([
    'as' => 'user-services.',
    'prefix' => 'user-services',
    'namespace' => 'Services',
], function () {
    Route::get('', 'UsersServicesController@index')->name('index');
    foreach ([
        'show',
        'processes',
        'portfolio',
        'workers',
        'details',
    ] as $action) {
        Route::get("$action/{id}", "UsersServicesController@$action")->name($action);
    }
});

/**
 * Личный кабинет
 */
Route::group([
    'prefix' => 'profile',
    'middleware' => 'auth'
], function () {

    Route::get('', function() {
        /* View для SPA личного кабинета */
        return view('profile');
    });

    Route::post('upload-avatar', 'ProfileController@uploadAvatar');
    Route::post('update', 'ProfileController@update');
    Route::get('cities', 'ProfileController@getCities');
    Route::post('password', 'ProfileController@updatePassword');

    Route::get('favorite', 'ProfileController@getFavorites');
    Route::post('favorites/business/{object}', 'ProfileController@toggleFavoriteBusiness');
    Route::post('favorites/franchise/{object}', 'ProfileController@toggleFavoriteFranchise');

    Route::group([
        'namespace' => 'Api',
        'prefix' => 'franchise',
    ], function () {
        Route::post('add-object/{id}/{objectId?}', 'FranchiseController@addObject');
        Route::post('add-news/{id}/{newsId?}', 'FranchiseController@addNews');
        Route::post('add-review/{id}/{reviewId?}', 'FranchiseController@addReview');
    });

    Route::get('purchased_services', 'ProfileController@getPurchasedServices');
    Route::get('reserved-businesses', 'ProfileController@getReservedBusinesses');
    Route::get('reserved-franchises', 'ProfileController@getReservedFranchises');

    Route::get('get-businesses', 'ProfileController@getBusinesses');
    Route::get('get-franchises', 'ProfileController@getFranchises');
    Route::get('get-franchise-reviews/{id}', 'ProfileController@getFranchiseeReviews');
    Route::get('get-franchise-objects/{id}', 'ProfileController@getFranchiseeObjects');
    Route::get('get-franchise-news/{id}', 'ProfileController@getFranchiseeNews');
    Route::delete('delete-reviews-item/{id}', 'ProfileController@deleteReviewsItem');
    Route::delete('delete-objects-item/{id}', 'ProfileController@deleteObjectsItem');
    Route::delete('delete-news-item/{id}', 'ProfileController@deleteNewsItem');
    Route::get('reviews-item-details/{id}', 'ProfileController@reviewDetails');
    Route::get('objects-item-details/{id}', 'ProfileController@objectDetails');
    Route::get('news-item-details/{id}', 'ProfileController@newsDetails');

    Route::get('balance/transactions', 'ProfileController@getPaymentTransactions');

    Route::get('requests/{type}', 'ProfileController@getObjectRequests');
    Route::get('franchise-requests/{type}/{id}', 'ProfileController@getFranchiseRequests');
    Route::get('requests-buyer/{type}', 'ProfileController@getBuyerObjectRequests');
    Route::patch('request/{viewRequest}/reject', 'ProfileController@rejectObjectViewRequest');
    Route::patch('request/{viewRequest}/status/accepted', 'ProfileController@acceptObjectViewRequest');
    Route::get('request/requests-show-docs/{viewRequest}', 'ProfileController@requestShowDocs');
    Route::post('create-request-doc', 'ProfileController@createRequestDoc');

    Route::prefix('api')
        ->group(function () {
            Route::get('purchased_service/{service}',
                'ProfileController@getPurchasedService')->middleware('BelongsToAuthUser:service');
            Route::get('objects', 'ProfileController@getObjects');
            Route::patch('object/{type}/{id}/status/{status}', 'ProfileController@setObjectStatus');

            Route::namespace('Api')
                ->group(function () {
                    Route::get('object/{type}/{id}', 'ObjectController@getStats');
                    Route::get('objects/search/{search?}', 'ObjectController@search');
                    Route::get('travel/{travel}', 'TravelController@get');
                    Route::get('trips', 'TravelController@list');

                    Route::prefix('trip')
                        ->group(function () {
                            Route::post('', 'TravelController@create');
                            Route::post('comment', 'TravelController@createComment');
                            Route::post('flight', 'TravelController@createFlight');
                            Route::post('hotel', 'TravelController@createHotel');
                            Route::post('view', 'TravelController@createObjectView');
                            Route::post('consult', 'TravelController@createObjectConsultation');
                        });

                    Route::get('seller-consultations', 'TravelController@getConsultations');
                });

            Route::get('terms', 'ProfileController@getTermsOfUse');
            Route::get('seller-consultations', 'Api\TravelController@getConsultations');
            Route::post('remove-franchise-document/{franchise}', 'ProfileController@removeFranchiseDocument');
        });
});

Route::group([
    'namespace' => 'Api\Services',
    'prefix' => 'api',
], function () {
    Route::group([
        'prefix' => 'user-service',
    ], function () {
        Route::get('get-categories', 'UsersServicesController@getCategories');
        Route::get('get-status-labels', 'UsersServicesController@getStatusLabels');
        Route::group([
            'middleware' => 'auth',
        ], function () {
            Route::get('list', 'UsersServicesController@getList');
            Route::get('{id}', 'UsersServicesController@getItem');
            Route::post('store', 'UsersServicesController@store');
            Route::post('update/{service}', 'UsersServicesController@update');
            Route::delete('delete/{service}', 'UsersServicesController@delete');
            Route::post('save-file/{fieldName}/{service}', 'UsersServicesController@saveFile');
            Route::post('save-relation-file/{fieldName}/{relationName}/{service}/{id}', 'UsersServicesController@saveRelationFile');
            Route::post('save-relation-files/{fieldName}/{relationName}/{service}/{id}', 'UsersServicesController@saveRelationFiles');
        });
    });
    Route::get('service/get-tariffs', 'ServiceController@getTariffs');
});

/* Фикс для возможных конфликтов роутов ларавела и личного кабинета */
Route::get('/profile/{vue_capture?}', function() {
    return view('profile');
})
    ->where('vue_capture', '[\/\w\.-]*')
    ->middleware('auth');

Route::get('/notification/markAsRead', 'NotificationController@markAsRead')->middleware('auth');
Route::get('/notification/getUnread', 'NotificationController@getUnread')->middleware('auth');
Route::delete('/notification/delete/{id}', 'Api\NotificationController@delete')->middleware('auth');

Route::namespace('Api\Lists')
    ->prefix('franchises')
    ->group(function () {
        Route::resources([
            'certificate-types' => 'CertificateTypesController',
            'certificate-documents' => 'CertificateDocumentsController',
            'categories' => 'CategoriesController',
            'subcategories' => 'SubcategoriesController',
            'post-types' => 'PostTypesController',
            'property-categories' => 'PropertyCategoriesController',
            'property-types' => 'PropertyTypesController',
            'transfer-customer-base-types' => 'TransferCustomerBaseTypesController',
            'training-material-types' => 'TrainingMaterialTypesController',
            'audience-types' => 'AudienceTypesController',
            'penalties' => 'PenaltiesController',
            'equipments' => 'EquipmentsController',
            'operation-restrictions' => 'OperationRestrictionsController',
            'gender-target-audiences' => 'GenderTargetAudienceController',
            'family-status-client' => 'FamilyStatusClientController',
            'social-status-client' => 'SocialStatusClientController',
            'main-attract-clients-adv-srcs' => 'MainAdvertisingSourcesAttractClientsController'
        ]);
    });
