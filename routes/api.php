<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')
    ->get('/user', function (Request $request) {
        return $request->user();
    });

/*Route::group([
    'namespace' => 'Api\Services',
], function () {
    Route::group([
        'prefix' => 'user-service',
    ], function () {
        Route::get('get-categories', 'UsersServicesController@getCategories');
        Route::group([
            'middleware' => 'auth',
        ], function () {
            Route::get('list', 'UsersServicesController@getList');
            Route::get('{service}', 'UsersServicesController@get');
            Route::post('add', 'UsersServicesController@store');
            Route::post('update/{service}', 'UsersServicesController@update');
            Route::post('save-icon/{service}', 'UsersServicesController@saveIcon');
        });
    });
    Route::get('service/get-tariffs', 'ServiceController@getTariffs');
});*/
